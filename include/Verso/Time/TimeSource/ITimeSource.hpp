#pragma once

#include <Verso/System/UString.hpp>
#include <Verso/Time/Timestamp.hpp>

namespace Verso {


class ITimeSource
{
public:
	virtual ~ITimeSource() = default;

	virtual UString getName() const = 0;
	virtual int64_t getCurrentMicrosecs() = 0;
};


} // End namespace Verso
