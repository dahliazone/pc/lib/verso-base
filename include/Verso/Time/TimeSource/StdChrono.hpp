#pragma once

#include <Verso/Time/TimeSource/ITimeSource.hpp>

namespace Verso {


class StdChrono : public ITimeSource
{
public:
	StdChrono() = default;
	virtual ~StdChrono() override = default;

public: // interface ITimeSource
	VERSO_BASE_API virtual UString getName() const override;
	VERSO_BASE_API virtual int64_t getCurrentMicrosecs() override;
};


} // End namespace Verso
