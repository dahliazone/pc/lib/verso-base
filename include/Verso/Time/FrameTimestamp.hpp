#pragma once

#include <Verso/Time/FrameTimer.hpp>
#include <Verso/Time/RecordFrameTimer.hpp>

namespace Verso {


class FrameTimestamp
{
private:
	size_t updateFrame;
	size_t renderFrame;
	Timestamp dt;
	Timestamp elapsed;
	Timestamp targetDt;
	double lastAverageDt;

public:
	FrameTimestamp() :
		updateFrame(0),
		renderFrame(0),
		dt(),
		elapsed(),
		targetDt(),
		lastAverageDt(0.0)
	{
	}


	FrameTimestamp(const FrameTimer& frameTimer) :
		updateFrame(frameTimer.getUpdateFrame()),
		renderFrame(frameTimer.getRenderFrame()),
		dt(frameTimer.getDt()),
		elapsed(frameTimer.getElapsed()),
		targetDt(frameTimer.getTargetDt()),
		lastAverageDt(frameTimer.getLastAverageDt())
	{
	}


	FrameTimestamp(const RecordFrameTimer& recordFrameTimer) :
		updateFrame(recordFrameTimer.getUpdateFrame()),
		renderFrame(recordFrameTimer.getRenderFrame()),
		dt(recordFrameTimer.getDt()),
		elapsed(recordFrameTimer.getElapsed()),
		targetDt(recordFrameTimer.getTargetDt()),
		lastAverageDt(recordFrameTimer.getDt().asSeconds())
	{
	}


	FrameTimestamp(size_t updateFrame, size_t renderFrame, Timestamp dt, Timestamp elapsed, Timestamp targetDt, double lastAverageDt) :
		updateFrame(updateFrame),
		renderFrame(renderFrame),
		dt(dt),
		elapsed(elapsed),
		targetDt(targetDt),
		lastAverageDt(lastAverageDt)
	{
	}


public: // getters & setters

	size_t getUpdateFrame() const
	{
		return updateFrame;
	}


	size_t getRenderFrame() const
	{
		return renderFrame;
	}


	Timestamp getDt() const
	{
		return dt;
	}


	Timestamp getElapsed() const
	{
		return elapsed;
	}


	Timestamp getTargetDt() const
	{
		return targetDt;
	}


	double getFps() const
	{
		return 1.0 / dt.asSeconds();
	}


	double getLastAverageDt() const
	{
		return lastAverageDt;
	}


	double getLastAverageFps() const
	{
		return 1.0 / lastAverageDt;
	}


public: // toString
	UString toString() const
	{
		UString str("dt=");
		str.append2(dt.asSeconds());
		str += ", elapsed=";
		str.append2(elapsed.asSeconds());
		return str;
	}


	UString toStringDebug() const
	{
		UString str("FrameTimestamp(");
		str += toString();
		str += ", targetDt=";
		str.append2(targetDt.asMilliseconds());
		str += " ms, updateFrame=";
		str.append2(updateFrame);
		str += ", renderFrame=";
		str.append2(renderFrame);
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const FrameTimestamp& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso
