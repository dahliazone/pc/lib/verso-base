#pragma once

#include <Verso/Time/Timestamp.hpp>
#include <Verso/Time/TimeSource/StdChrono.hpp>

namespace Verso {


class Clock
{
private:
	StdChrono defaultSource;
	ITimeSource* source;
	Timestamp started;

private:
	VERSO_BASE_API Clock();

public:
	VERSO_BASE_API static Clock& instance()
	{
		static Clock clock;
		return clock;
	}

	VERSO_BASE_API ~Clock() = default;

public:
	VERSO_BASE_API ITimeSource* getSource();
	VERSO_BASE_API void changeSource(ITimeSource* source);
	VERSO_BASE_API Timestamp getElapsed() const;

public: // toString
	VERSO_BASE_API UString toString() const;
	VERSO_BASE_API UString toStringDebug() const;

	friend std::ostream& operator <<(std::ostream& ost, const Clock& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso
