#pragma once

#include <Verso/Time/ITimer.hpp>

namespace Verso {


class Timer : public ITimer
{
private:
	Timestamp started;

public:
	VERSO_BASE_API Timer();

	virtual ~Timer() override = default;

public: // interface ITimer
	VERSO_BASE_API virtual Timestamp getElapsed() const override;

	VERSO_BASE_API virtual Timestamp restart() override;

public: // toString (interface ITimer)
	VERSO_BASE_API virtual UString toString() const override;

	VERSO_BASE_API virtual UString toStringDebug() const override;
};


} // End namespace Verso
