#pragma once

#include <Verso/Time/SteppedTimer.hpp>

namespace Verso {


class FrameTimer
{
private:
	double targetFps;
	SteppedTimer frameTimer;

	Timestamp dt;
	Timestamp elapsed;
	size_t updateFrame;
	size_t renderFrame;

	Timestamp measureFrequency;
	Timestamp measureSumTime;
	size_t measureSumUpdateFrames;
	size_t measureSumRenderFrames;
	double lastAverageDt;
	size_t lastUpdateFrames;
	size_t lastRenderFrames;

public:
	VERSO_BASE_API FrameTimer(
			double targetFps = 0.0, Timestamp measureFrequency = Timestamp::seconds(0.5));

public:
	VERSO_BASE_API void reset(
			double targetFps = 0.0, Timestamp measureFrequency = Timestamp::seconds(0.5));

	VERSO_BASE_API void update();
	VERSO_BASE_API void addRenderFrame();

public: // getters & setters
	VERSO_BASE_API double getTargetFps() const;
	VERSO_BASE_API Timestamp getTargetDt() const;
	VERSO_BASE_API double getFps() const;
	VERSO_BASE_API Timestamp getDt() const;
	VERSO_BASE_API Timestamp getElapsed() const;
	VERSO_BASE_API size_t getUpdateFrame() const;
	VERSO_BASE_API size_t getRenderFrame() const;
	VERSO_BASE_API Timestamp getMeasureFrequency() const;
	VERSO_BASE_API double getLastAverageDt() const;
	VERSO_BASE_API double getLastAverageFps() const;
	VERSO_BASE_API size_t getLastUpdateFrames() const;
	VERSO_BASE_API size_t getLastRenderFrames() const;
	VERSO_BASE_API size_t getLastSkippedFrames() const;

public: // toString
	VERSO_BASE_API UString toString() const;
	VERSO_BASE_API UString toStringDebug() const;

	friend std::ostream& operator <<(std::ostream& ost, const FrameTimer& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso
