#pragma once

#include <Verso/Time/ITimer.hpp>

namespace Verso {


class ManualTimer : public ITimer
{
private:
	Timestamp elapsed;

public: // ManualTimer specific
	VERSO_BASE_API ManualTimer();

	VERSO_BASE_API virtual ~ManualTimer() override;

public:
	VERSO_BASE_API void advance(const Timestamp& amount);

public: // interface ITimer
	VERSO_BASE_API virtual Timestamp getElapsed() const override;

	VERSO_BASE_API virtual Timestamp restart() override;

public: // toString (interface ITimer)
	VERSO_BASE_API virtual UString toString() const override;

	VERSO_BASE_API virtual UString toStringDebug() const override;
};


} // End namespace Verso
