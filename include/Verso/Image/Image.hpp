#pragma once

#include <Verso/Math/AspectRatio.hpp>
#include <Verso/Math/RgbaColorf.hpp>
#include <Verso/Math/Rect.hpp>
#include <Verso/Image/ImageSaveFormat.hpp>
#include <Verso/Math/ColorGenerator.hpp>

namespace Verso {


class Image
{
private:
	bool created;
	Vector2i resolution;
	std::uint8_t* pixelData; // format RGB or RGBA
	int components; // RGB = 3, RGBA = 4
	UString sourceFileName;
	AspectRatio targetAspectRatio;

public:
	VERSO_BASE_API Image();

	VERSO_BASE_API Image(const Image& original);

	VERSO_BASE_API Image(Image&& original) noexcept;

	VERSO_BASE_API Image& operator =(const Image& original);

	VERSO_BASE_API Image& operator =(Image&& original) noexcept;

	VERSO_BASE_API ~Image();

public:
	VERSO_BASE_API void create(int width, int height, bool alpha, const AspectRatio& targetAspectRatio, const RgbaColorf& color = ColorGenerator::getColor(ColorRgb::Black));

	VERSO_BASE_API void create(const Vector2i& resolution, bool alpha, const AspectRatio& targetAspectRatio, const RgbaColorf& color = ColorGenerator::getColor(ColorRgb::Black));

	VERSO_BASE_API void createFromMemory(int width, int height, bool alpha, const std::uint8_t* pixelData, const AspectRatio& targetAspectRatio);

	VERSO_BASE_API void createFromMemory(const Vector2i& resolution, bool alpha, const std::uint8_t* pixelData, const AspectRatio& targetAspectRatio);

	VERSO_BASE_API bool createFromFile(const UString& fileName, bool alpha, const AspectRatio& targetAspectRatio);

	VERSO_BASE_API bool isCreated() const;

	VERSO_BASE_API void destroy() VERSO_NOEXCEPT;

	VERSO_BASE_API void applyAlphaMaskFromColor(const RgbaColorf& color, std::uint8_t alpha = 0);

	VERSO_BASE_API void clear(const RgbaColorf& color);

	VERSO_BASE_API void clear(const RgbaColorf& color, const Recti& area);

	VERSO_BASE_API void copy(const Image& image, int destX, int destY, const Recti& sourceRect = Recti(0, 0, 0, 0), bool applyAlpha = false);

	VERSO_BASE_API void copy(const Image& image, const Vector2i& destPosition, const Recti& sourceRect = Recti(0, 0, 0, 0), bool applyAlpha = false);

	VERSO_BASE_API void flipHorizontally();

	VERSO_BASE_API void flipVertically();

	VERSO_BASE_API std::uint8_t* getPixelData();

	VERSO_BASE_API RgbaColorf getPixel(int x, int y) const;

	VERSO_BASE_API RgbaColorf getPixel(const Vector2i& position) const;

	VERSO_BASE_API void setPixel(int x, int y, const RgbaColorf& color);

	VERSO_BASE_API void setPixel(const Vector2i& position, const RgbaColorf& color);

	VERSO_BASE_API bool saveToFile(const UString& fileName, const ImageSaveFormat& imageSaveFormat, bool discardAlpha = false) const;

	VERSO_BASE_API Vector2i getResolutioni() const;

	VERSO_BASE_API Vector2f getResolutionf() const;

	VERSO_BASE_API UString getSourceFileName() const;

	VERSO_BASE_API size_t getBytesPerPixel() const;

	VERSO_BASE_API size_t getBitsPerPixel() const;

	VERSO_BASE_API AspectRatio getAspectRatioTarget() const;

	VERSO_BASE_API AspectRatio getAspectRatioActual() const;

private:
	void allocateBuffer(const Vector2i& resolution, bool alpha, const AspectRatio& targetAspectRatio);

	void freeBuffer();

	void copyFromBuffer(const Vector2i& resolution, const std::uint8_t* pixelData);

public: // toString
	VERSO_BASE_API UString toString() const;

	VERSO_BASE_API UString toStringDebug() const;

	friend std::ostream& operator <<(std::ostream& ost, const Image& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso
