#pragma once

#include <Verso/System/UString.hpp>
#include <cstdint>
#include <ostream>

namespace Verso {


enum class ImageSaveFormat : std::int8_t {
	Unset = 0,
	Png,
	Bmp,
	Tga,
	Size
};


// returns ImageSaveFormat::Unset for any erroneous string
inline ImageSaveFormat stringToImageSaveFormat(const UString& str)
{
	if (str.equals("Png")) {
		return ImageSaveFormat::Png;
	}
	else if (str.equals("Bmp")) {
		return ImageSaveFormat::Bmp;
	}
	else if (str.equals("Tga")) {
		return ImageSaveFormat::Tga;
	}
	else {
		return ImageSaveFormat::Unset;
	}
}


inline UString imageSaveFormatToString(const ImageSaveFormat& imageSaveFormat)
{
	if (imageSaveFormat == ImageSaveFormat::Unset) {
		return "Unset";
	}
	else if (imageSaveFormat == ImageSaveFormat::Png) {
		return "Png";
	}
	else if (imageSaveFormat == ImageSaveFormat::Bmp) {
		return "Bmp";
	}
	else if (imageSaveFormat == ImageSaveFormat::Tga) {
		return "Tga";
	}
	else if (imageSaveFormat == ImageSaveFormat::Size) {
		return "Size";
	}
	else {
		return "Unknown value";
	}
}


inline UString imageSaveFormatToFileExtension(const ImageSaveFormat& imageSaveFormat)
{
	if (imageSaveFormat == ImageSaveFormat::Png) {
		return "png";
	}
	else if (imageSaveFormat == ImageSaveFormat::Bmp) {
		return "bmp";
	}
	else if (imageSaveFormat == ImageSaveFormat::Tga) {
		return "tga";
	}
	else {
		return "invalid";
	}
}


inline std::ostream& operator <<(std::ostream& ost, const ImageSaveFormat& imageSaveFormat)
{
	return ost << imageSaveFormatToString(imageSaveFormat);
}


} // End namespace Verso
