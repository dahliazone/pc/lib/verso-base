#pragma once

#if defined(WIN32) || defined _WIN32 || defined __CYGWIN__
#	if defined(VERSO_BASE_BUILD_DYNAMIC)
#		ifdef __GNUC__
#			define VERSO_BASE_API __attribute__ ((dllexport))
#		else
#			define VERSO_BASE_API __declspec(dllexport) // Note: actually gcc seems to also supports this syntax.
#			define VERSO_BASE_EXPIMP
#		endif
#	else
#		ifdef __GNUC__
#			define VERSO_BASE_API __attribute__ ((dllimport))
#		else
#			define VERSO_BASE_API __declspec(dllimport) // Note: actually gcc seems to also supports this syntax.
#			define VERSO_BASE_EXPIMP extern
#		endif
#	endif
#	define VERSO_BASE_PRIVATE
#else
#	if __GNUC__ >= 4
#		define VERSO_BASE_API __attribute__ ((visibility ("default")))
#		define VERSO_BASE_PRIVATE  __attribute__ ((visibility ("hidden")))
#	else
#		define VERSO_BASE_API
#		define VERSO_BASE_PRIVATE
#	endif
#endif
