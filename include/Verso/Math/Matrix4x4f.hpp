#pragma once

#include <Verso/System/assert.hpp>
#include <Verso/Math/Vector3.hpp>
#include <Verso/Math/Vector4.hpp>
#include <Verso/Math/Math.hpp>
#include <Verso/Math/AspectRatio.hpp>
#include <ostream>
#include <limits>

namespace Verso {


class Matrix4x4f
{
public:
	float data[4][4]; // row major order

public:
	Matrix4x4f()
	{
		memcpy(this->data, Identity().data, 16 * sizeof(float));
	}


	/* // \TODO: VC++ 2012 doesn't support initializer_list<T>
	Matrix4x4f(initializer_list<float> c)
	{
		VERSO_ASSERT("verso-base", c.size() == 16);
		copy(c.begin(), c.end(), &data[0][0]);
	}*/


	// @data[16] row major order
	Matrix4x4f(const float* data)
	{
		memcpy(this->data, data, 16 * sizeof(float));
	}


	// row major order
	// 	 m00  m01  m02  0
	//   m10  m11  m12  0
	//   m20  m21  m22  0
	//    0    0    0   1
	Matrix4x4f(float m00, float m01, float m02,
			   float m10, float m11, float m12,
			   float m20, float m21, float m22)
	{
		data[0][0] = m00;    data[0][1] = m01;    data[0][2] = m02;    data[0][3] = 0.0f;
		data[1][0] = m10;    data[1][1] = m11;    data[1][2] = m12;    data[1][3] = 0.0f;
		data[2][0] = m20;    data[2][1] = m21;    data[2][2] = m22;    data[2][3] = 0.0f;
		data[3][0] = 0.0f;   data[3][1] = 0.0f;   data[3][2] = 0.0f;   data[3][3] = 1;
	}


	// row major order
	// 	 m00  m01  m02  m03
	//   m10  m11  m12  m13
	//   m20  m21  m22  m23
	//   m30  m31  m32  m33
	Matrix4x4f(float m00, float m01, float m02, float m03,
			   float m10, float m11, float m12, float m13,
			   float m20, float m21, float m22, float m23,
			   float m30, float m31, float m32, float m33)
	{
		data[0][0] = m00; data[0][1] = m01; data[0][2] = m02; data[0][3] = m03;
		data[1][0] = m10; data[1][1] = m11; data[1][2] = m12; data[1][3] = m13;
		data[2][0] = m20; data[2][1] = m21; data[2][2] = m22; data[2][3] = m23;
		data[3][0] = m30; data[3][1] = m31; data[3][2] = m32; data[3][3] = m33;
	}


	Matrix4x4f(const Matrix4x4f& original)
	{
		memcpy(this->data, &original.data, 16 * sizeof(float));
	}


	float getElement(int row, int column) const
	{
		VERSO_ASSERT_MSG("verso-base", row >= 0 && row < 4, "Row must be between [0,3].");
		VERSO_ASSERT_MSG("verso-base", column >= 0 && column < 4, "Column must be between [0,3].");

		return data[row][column];
	}


	void setElement(int row, int column, float value)
	{
		VERSO_ASSERT_MSG("verso-base", row >= 0 && row < 4, "Row must be between [0,3].");
		VERSO_ASSERT_MSG("verso-base", column >= 0 && column < 4, "Column must be between [0,3].");

		data[row][column] = value;
	}


	Vector4f getRow(int row) const
	{
		VERSO_ASSERT_MSG("verso-base", row >= 0 && row < 4, "Row must be between [0,3].");

		return Vector4f(
					data[row][0],
				data[row][1],
				data[row][2],
				data[row][3]);
	}


	void setRow(int row, const Vector4f& vec4)
	{
		VERSO_ASSERT_MSG("verso-base", row >= 0 && row < 4, "Row must be between [0,3].");
		data[row][0] = vec4.x;
		data[row][1] = vec4.y;
		data[row][2] = vec4.z;
		data[row][3] = vec4.w;
	}


	Vector4f getColumn(int column) const
	{
		VERSO_ASSERT_MSG("verso-base", column >= 0 && column < 4, "Column must be between [0,3].");

		return Vector4f(
				data[0][column],
				data[1][column],
				data[2][column],
				data[3][column]);
	}


	void setColumn(int column, const Vector4f& vec4)
	{
		VERSO_ASSERT_MSG("verso-base", column >= 0 && column < 4, "Column must be between [0,3].");
		data[0][column] = vec4.x;
		data[1][column] = vec4.y;
		data[2][column] = vec4.z;
		data[3][column] = vec4.w;
	}


	// @data[16] row major order
	void set(const float* data)
	{
		memcpy(this->data, data, 16 * sizeof(float));
	}


	// row major order
	// 	 m00  m01  m02  0
	//   m10  m11  m12  0
	//   m20  m21  m22  0
	//    0    0    0   1
	void set(float m00, float m01, float m02,
			 float m10, float m11, float m12,
			 float m20, float m21, float m22)
	{
		data[0][0] = m00;    data[0][1] = m01;    data[0][2] = m02;    data[0][3] = 0.0f;
		data[1][0] = m10;    data[1][1] = m11;    data[1][2] = m12;    data[1][3] = 0.0f;
		data[2][0] = m20;    data[2][1] = m21;    data[2][2] = m22;    data[2][3] = 0.0f;
		data[3][0] = 0.0f;   data[3][1] = 0.0f;   data[3][2] = 0.0f;   data[3][3] = 1;
	}


	// row major order
	// 	 m00  m01  m02  m03
	//   m10  m11  m12  m13
	//   m20  m21  m22  m23
	//   m30  m31  m32  m33
	void set(float m00, float m01, float m02, float m03,
			 float m10, float m11, float m12, float m13,
			 float m20, float m21, float m22, float m23,
			 float m30, float m31, float m32, float m33)
	{
		data[0][0] = m00; data[0][1] = m01; data[0][2] = m02; data[0][3] = m03;
		data[1][0] = m10; data[1][1] = m11; data[1][2] = m12; data[1][3] = m13;
		data[2][0] = m20; data[2][1] = m21; data[2][2] = m22; data[2][3] = m23;
		data[3][0] = m30; data[3][1] = m31; data[3][2] = m32; data[3][3] = m33;
	}


	void set(const Matrix4x4f& original)
	{
		memcpy(this->data, original.data, 16 * sizeof(float));
	}


	Vector3f multiplyByVector(const Vector3f& v) const
	{
		return Vector3f(
					v.x * data[0][0] + v.y * data[0][1] + v.z * data[0][2],
				v.x * data[1][0] + v.y * data[1][1] + v.z * data[1][2],
				v.x * data[2][0] + v.y * data[2][1] + v.z * data[2][2]);
	}


	Vector4f multiplyByVector(const Vector4f& v) const
	{
		return Vector4f(
					v.x * data[0][0] + v.y * data[0][1] + v.z * data[0][2] + v.w * data[0][3],
				v.x * data[1][0] + v.y * data[1][1] + v.z * data[1][2] + v.w * data[1][3],
				v.x * data[2][0] + v.y * data[2][1] + v.z * data[2][2] + v.w * data[2][3],
				v.x * data[3][0] + v.y * data[3][1] + v.z * data[3][2] + v.w * data[3][3]);
	}


	Matrix4x4f multiplyByMatrix(const Matrix4x4f& matrix) const
	{
		Matrix4x4f out; // Defaults to identity

		for (size_t row = 0; row < 4; ++row) {
			for (size_t column = 0; column < 4; ++column) {
				out.data[row][column] =
						(data[row][0] * matrix.data[0][column]) +
						(data[row][1] * matrix.data[1][column]) +
						(data[row][2] * matrix.data[2][column]) +
						(data[row][3] * matrix.data[3][column]);
			}
		}

		return out;
	}


public: // operators
	Matrix4x4f& operator =(const Matrix4x4f& original)
	{
		if (this != &original) {
			memcpy(&this->data, &original.data, 16 * sizeof(float));
		}
		return *this;
	}


	Vector3f operator *(const Vector3f& right) const
	{
		return multiplyByVector(right);
	}


	Vector4f operator *(const Vector4f& right) const
	{
		return multiplyByVector(right);
	}


	Matrix4x4f operator *(const Matrix4x4f& right) const
	{
		return multiplyByMatrix(right);
	}


public: // static
	static const Matrix4x4f& Zero()
	{
		static const Matrix4x4f zero({ 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f });
		return zero;
	}


	static const Matrix4x4f& Identity()
	{
		static const Matrix4x4f identity({ 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f });
		return identity;
	}


	static Matrix4x4f transpose(const Matrix4x4f& right)
	{
		Matrix4x4f out;
		for (size_t row = 0; row < 4; ++row) {
			for (size_t column = 0; column < 4; ++column) {
				out.data[row][column] = right.data[column][row];
			}
		}
		return out;
	}


	static Matrix4x4f inverse(const Matrix4x4f& right)
	{
		VERSO_FAIL("verso-base"); // \TODO: implement. check: http://negativeprobability.blogspot.fi/2011/11/affine-transformations-and-their.html
		(void)right; return Matrix4x4f();
	}


	// Creates a matrix for projecting two-dimensional coordinates onto the screen.
	static Matrix4x4f orthographicProjection(float left, float right, float bottom, float top)
	{
		Matrix4x4f out;

		out.data[0][0] = 2.0f / (right - left);
		out.data[1][1] = 2.0f / (top - bottom);
		out.data[2][2] = -1.0f;
		out.data[0][3] = -(right + left) / (right - left);
		out.data[1][3] = -(top + bottom) / (top - bottom);

		return out;
	}


	// Creates a matrix for an orthographic parallel viewing volume (left-handed).
	static Matrix4x4f orthographicProjection(float left = -1.0f, float right = 1.0f,
											 float top = -1.0f, float bottom = 1.0f,
											 float nearPlane = 0.1f, float farPlane = 100.0f)
	{
		VERSO_ASSERT("verso-base", farPlane > nearPlane);

		Matrix4x4f out;

		out.data[0][0] = 2.0f / (right - left);
		out.data[1][1] = 2.0f / (top - bottom);
		out.data[2][2] = 2.0f / (farPlane - nearPlane);
		out.data[0][3] = -(right + left) / (right - left);
		out.data[1][3] = -(top + bottom) / (top - bottom);
		out.data[2][3] = -(farPlane + nearPlane) / (farPlane - nearPlane);

		return out;
	}


	// Creates a matrix for a symmetric perspective-view frustum (left-handed).
	static Matrix4x4f perspectiveProjection(const AspectRatio& aspectRatio, Degree fovY, float nearPlane, float farPlane)
	{
		VERSO_ASSERT("verso-base", std::abs(aspectRatio.value - std::numeric_limits<float>::epsilon()) > 0.0f);

		Matrix4x4f out(Matrix4x4f::Zero());
		const float tanHalfFovy = std::tan(Math::degreesToRadians(fovY) / 2.0f);

		out.data[0][0] = 1.0f / (aspectRatio.value * tanHalfFovy);
		out.data[1][1] = 1.0f / (tanHalfFovy);
		out.data[2][2] = (farPlane + nearPlane) / (farPlane - nearPlane);
		out.data[3][2] = 1.0f;
		out.data[2][3] = -(2.0f * farPlane * nearPlane) / (farPlane - nearPlane);

		return out;
	}


	// Builds a perspective projection matrix based on a field of view (left-handed).
	static Matrix4x4f perspectiveProjectionFov(const Vector2f& size, float fov, float nearPlane, float farPlane)
	{
		VERSO_ASSERT("verso-base", size.x > 0.0f);
		VERSO_ASSERT("verso-base", size.y > 0.0f);
		VERSO_ASSERT("verso-base", fov > 0.0f);

		const float rad = fov;
		const float h = std::cos(0.5f * rad) / std::sin(0.5f * rad);
		const float w = h * size.y / size.x;
		Matrix4x4f out(Matrix4x4f::Zero());

		out.data[0][0] = w;
		out.data[1][1] = h;
		out.data[2][2] = (farPlane + nearPlane) / (farPlane - nearPlane);

		out.data[3][2] = 1.0f;
		out.data[2][3] = -(2.0f * farPlane * nearPlane) / (farPlane - nearPlane);

		return out;
	}


	static Matrix4x4f translate(const Matrix4x4f& matrix, const Vector3f& position)
	{
		return translate(matrix, position.x, position.y, position.z);
	}


	static Matrix4x4f translate(const Matrix4x4f& matrix, float x, float y, float z)
	{
		Matrix4x4f out(matrix);
		out.setColumn(3, matrix.getColumn(0) * x + matrix.getColumn(1) * y + matrix.getColumn(2) * z + matrix.getColumn(3));

		return out;
	}


	static Matrix4x4f rotate(const Matrix4x4f& matrix, Degree angle, const Vector3f axis)
	{
		const Radian a = Math::degreesToRadians(angle);
		const float cosA = std::cos(a);
		const float sinA = std::sin(a);

		Vector3f axisNorm = axis.getNormalized();
		Vector3f temp(axisNorm * (1.0f - cosA));

		Matrix4x4f rotMatrix(Matrix4x4f::Zero());
		rotMatrix.data[0][0] = cosA + temp.x * axisNorm.x;
		rotMatrix.data[0][1] = 0 + temp.y * axisNorm.x - sinA * axisNorm.z;
		rotMatrix.data[0][2] = 0 + temp.z * axisNorm.x + sinA * axisNorm.y;

		rotMatrix.data[1][0] = 0 + temp.x * axisNorm.y + sinA * axisNorm.z;
		rotMatrix.data[1][1] = cosA + temp.y * axisNorm.y;
		rotMatrix.data[1][2] = 0 + temp.z * axisNorm.y - sinA * axisNorm.x;

		rotMatrix.data[2][0] = 0 + temp.x * axisNorm.z - sinA * axisNorm.y;
		rotMatrix.data[2][1] = 0 + temp.y * axisNorm.z + sinA * axisNorm.x;
		rotMatrix.data[2][2] = cosA + temp.z * axisNorm.z;

		Matrix4x4f out(Matrix4x4f::Zero());
		out.setColumn(0, matrix.getColumn(0) * rotMatrix.data[0][0] + matrix.getColumn(1) * rotMatrix.data[1][0] + matrix.getColumn(2) * rotMatrix.data[2][0]);
		out.setColumn(1, matrix.getColumn(0) * rotMatrix.data[0][1] + matrix.getColumn(1) * rotMatrix.data[1][1] + matrix.getColumn(2) * rotMatrix.data[2][1]);
		out.setColumn(2, matrix.getColumn(0) * rotMatrix.data[0][2] + matrix.getColumn(1) * rotMatrix.data[1][2] + matrix.getColumn(2) * rotMatrix.data[2][2]);
		out.setColumn(3, matrix.getColumn(3));

		return out;
	}


	static Matrix4x4f scale(const Matrix4x4f& matrix, float uniformScale)
	{
		return Matrix4x4f::scale(matrix, uniformScale, uniformScale, uniformScale);
	}


	static Matrix4x4f scale(const Matrix4x4f& matrix, const Vector3f& scale)
	{
		return Matrix4x4f::scale(matrix, scale.x, scale.y, scale.z);
	}


	static Matrix4x4f scale(const Matrix4x4f& matrix, float scaleX, float scaleY, float scaleZ)
	{
		Matrix4x4f out;
		out.setColumn(0, matrix.getColumn(0) * scaleX);
		out.setColumn(1, matrix.getColumn(1) * scaleY);
		out.setColumn(2, matrix.getColumn(2) * scaleZ);
		out.setColumn(3, matrix.getColumn(3));

		return out;
	}


	static Matrix4x4f lookRotation(const Vector3f& forward, const Vector3f& upward)
	{
		VERSO_FAIL("verso-base"); // \TODO: implement
		(void)forward; (void)upward;
		return Matrix4x4f();
	}

	static Matrix4x4f lookAt(const Vector3f& eye, const Vector3f& target, const Vector3f& upwards = Vector3f::up())
	{
		Matrix4x4f out; // Defaults to identity

//		const Vector3f forward((target - eye).getNormalized());
//		const Vector3f side(forward.crossProduct(upwards).getNormalized());
//		const Vector3f up(side.crossProduct(forward));

//		out.data[0][0] =  side.x;
//		out.data[0][1] =  side.y;
//		out.data[0][2] =  side.z;
//		out.data[1][0] =  up.x;
//		out.data[1][1] =  up.y;
//		out.data[1][2] =  up.z;
//		out.data[2][0] = -forward.x;
//		out.data[2][1] = -forward.y;
//		out.data[2][2] = -forward.z;
//		out.data[0][3] = -side.dotProduct(eye);
//		out.data[1][3] = -up.dotProduct(eye);
//		out.data[2][3] =  forward.dotProduct(eye);

				// left-handed
				Vector3f forward((target - eye).getNormalized());
				Vector3f left(Vector3f(upwards.crossProduct(forward)).getNormalized());
				Vector3f up(forward.crossProduct(left));

				out.data[0][0] = left.x;
				out.data[0][1] = left.y;
				out.data[0][2] = left.z;
				out.data[1][0] = up.x;
				out.data[1][1] = up.y;
				out.data[1][2] = up.z;
				out.data[2][0] = forward.x;
				out.data[2][1] = forward.y;
				out.data[2][2] = forward.z;
				out.data[0][3] = -(left.dotProduct(eye));
				out.data[1][3] = -(up.dotProduct(eye));
				out.data[2][3] = -(forward.dotProduct(eye));

		return out;
	}


public: // toString
	UString toString(const UString& newLinePadding) const
	{
		UString str;
		UString newLinePadding2 = newLinePadding + "  ";

		for (size_t row = 0; row < 4; ++row) {
			str += "\n" + newLinePadding2 + "[ ";
			for (size_t column = 0; column < 4; ++column) {
				str.append2(data[row][column]);
				if (column != 3) {
					str += ",  ";
				}
			}
			str += " ]";
		}
		return str;
	}


	UString toStringDebug(const UString& newLinePadding) const
	{
		UString str("Matrix4x4f(");
		str += toString(newLinePadding);
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Matrix4x4f& right)
	{
		return ost << right.toString("");
	}
};


inline bool operator ==(const Matrix4x4f& left, const Matrix4x4f& right)
{
	for (size_t row = 0; row<4; ++row) {
		for (size_t column=0; column<4; ++column) {
			if (left.data[row][column] != right.data[row][column]) {
				return false;
			}
		}
	}
	return true;
}


inline bool operator !=(const Matrix4x4f& left, const Matrix4x4f& right)
{
	return !(left == right);
}


} // End namespace Verso
