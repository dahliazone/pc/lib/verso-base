#pragma once

#include <Verso/Math/Vector2.hpp>

namespace Verso {


template<typename T>
class Vector2KeyframeElement
{
public:
	float seconds;
	Vector2<T> value;
	Vector2<T> control1;
	Vector2<T> control2;

public:
	Vector2KeyframeElement<T>(float seconds, const Vector2<T>& value,
							  const Vector2<T>& control1 = Vector2<T>(),
							  const Vector2<T>& control2 = Vector2<T>()) :
		seconds(seconds),
		value(value),
		control1(control1),
		control2(control2)
	{
	}


public: // toString
	UString toString() const
	{
		UString str("{ seconds=");
		str.append2(seconds);
		str += ", value=";
		str += value.toString();
		str += ", control1=";
		str += control1.toString();
		str += ", control2=";
		str += control2.toString();
		str += " }";
		return str;
	}


	UString toStringDebug() const
	{
		UString str("Vector2KeyframeElement<T>(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Vector2KeyframeElement<T>& right)
	{
		return ost << right.toString();
	}
};

typedef Vector2KeyframeElement<int> Vector2iKeyframeElement;
typedef Vector2KeyframeElement<unsigned int> Vector2uKeyframeElement;
typedef Vector2KeyframeElement<float> Vector2fKeyframeElement;


template<typename T>
inline bool operator ==(const Vector2KeyframeElement<T>& left, const Vector2KeyframeElement<T>& right)
{
	return (left.seconds == right.seconds) &&
			(left.value == right.value) &&
			(left.control1 == right.control1) &&
			(left.control2 == right.control2);
}


template<typename T>
inline bool operator !=(const Vector2KeyframeElement<T>& left, const Vector2KeyframeElement<T>& right)
{
	return !(left == right);
}


} // End namespace Verso
