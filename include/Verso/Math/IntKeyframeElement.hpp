#pragma once

#include <Verso/System/UString.hpp>

namespace Verso {


class IntKeyframeElement
{
public:
	float seconds;
	int value;
	int control1;
	int control2;

public:
	IntKeyframeElement(float seconds, int value,
					   int control1 = 0.0f,
					   int control2 = 0.0f) :
		seconds(seconds),
		value(value),
		control1(control1),
		control2(control2)
	{
	}


public: // toString
	UString toString() const
	{
		UString str("{ seconds=");
		str.append2(seconds);
		str += ", value=";
		str.append2(value);
		str += ", control1=";
		str.append2(control1);
		str += ", control2=";
		str.append2(control2);
		str += " }";
		return str;
	}


	UString toStringDebug() const
	{
		UString str("IntKeyframeElement(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const IntKeyframeElement& right)
	{
		return ost << right.toString();
	}
};


inline bool operator ==(const IntKeyframeElement& left, const IntKeyframeElement& right)
{
	return (left.seconds == right.seconds) &&
			(left.value == right.value) &&
			(left.control1 == right.control1) &&
			(left.control2 == right.control2);
}


inline bool operator !=(const IntKeyframeElement& left, const IntKeyframeElement& right)
{
	return !(left == right);
}


} // End namespace Verso
