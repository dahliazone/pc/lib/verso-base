#pragma once

#include <Verso/Math/Easing.hpp>
#include <Verso/Math/InterpolationType.hpp>
#include <Verso/Math/Interpolation.hpp>
#include <Verso/Math/Vector2KeyframeElement.hpp>

namespace Verso {


template<typename T>
class Vector2Keyframes
{
public:
	EasingType easingType;
	InterpolationType interpolationType;
	bool looping;
	std::vector< Vector2KeyframeElement<T> > data;

public:
	Vector2Keyframes<T>() :
		easingType(EasingType::Linear),
		interpolationType(InterpolationType::Undefined),
		looping(false),
		data()
	{
	}


	explicit Vector2Keyframes<T>(
			const EasingType& easingType,
			const InterpolationType& interpolationType,
			bool looping,
			const std::vector< Vector2KeyframeElement<T> >& data) :
		easingType(easingType),
		interpolationType(interpolationType),
		looping(looping),
		data(data)
	{
	}


	explicit Vector2Keyframes<T>(const Vector2<T>& singleValue) :
		easingType(EasingType::Linear),
		interpolationType(InterpolationType::Constant),
		looping(false),
		data()
	{
		data.push_back(Vector2KeyframeElement<T>(0.0f, singleValue));
	}


	Vector2Keyframes<T>(const Vector2Keyframes<T>& original) :
		easingType(original.easingType),
		interpolationType(original.interpolationType),
		looping(original.looping),
		data(original.data)
	{
	}


	Vector2Keyframes<T>(Vector2Keyframes<T>&& original) noexcept :
		easingType(std::move(original.easingType)),
		interpolationType(std::move(original.interpolationType)),
		looping(std::move(original.looping)),
		data(std::move(original.data))
	{
	}


	Vector2Keyframes<T>& operator =(const Vector2Keyframes<T>& original)
	{
		if (this != &original) {
			easingType = original.easingType;
			interpolationType = original.interpolationType;
			looping = original.looping;
			data = original.data;
		}
		return *this;
	}


	Vector2Keyframes<T>& operator =(Vector2Keyframes<T>&& original) noexcept
	{
		if (this != &original) {
			easingType = std::move(original.easingType);
			interpolationType = std::move(original.interpolationType);
			looping = std::move(original.looping);
			data = std::move(original.data);
		}
		return *this;
	}


	~Vector2Keyframes<T>()
	{
	}


public:
	void add(const Vector2KeyframeElement<T>& element)
	{
		if (data.size() > 0 && element.seconds <= data[data.size() - 1].seconds) {
			UString error("FloatKeyframe seconds at index ");
			error.append2(data.size() - 1);
			error += " was not bigger than previous value!";
			VERSO_ILLEGALPARAMETERS("verso-base", error.c_str(), "");
		}
		data.push_back(element);
	}


	void clear()
	{
		data.clear();
	}


	Vector2KeyframeElement<T>& operator[](size_t index)
	{
		return data[index];
	}


	const Vector2KeyframeElement<T>& operator[](size_t index) const
	{
		return data[index];
	}


	Vector2f getValueInterpolated(double seconds) const
	{
		const float lengthSeconds = data[data.size() - 1].seconds;
		if (looping == true && seconds > lengthSeconds) {
			seconds = seconds - floor(seconds / lengthSeconds) * lengthSeconds;
		}
		float delta = static_cast<float>(seconds) / lengthSeconds;
		if (delta > 1.0f) {
			delta = 1.0f;
		}
		const float deltaEased = Easing::byType(easingType, delta);
		const float secondsEased = deltaEased * lengthSeconds;

		switch (interpolationType)
		{
		case InterpolationType::Constant:
			return getValueInterpolatedLinear(secondsEased); // handles single value case
		case InterpolationType::Linear:
			return getValueInterpolatedLinear(secondsEased);
		case InterpolationType::QuadraticBezier:
			return getValueInterpolatedQuadraticBezier(secondsEased);
		default:
			std::cout << "Unsupported InterpolationType=" << interpolationTypeToString(interpolationType) << std::endl;
			VERSO_FAIL("verso-base");
		}
	}

private:
	Vector2f getValueInterpolatedLinear(double seconds) const
	{
		size_t length = data.size();

		if (length == 0) {
			return Vector2f();
		}
		else if (length == 1) {
			return data[0].value;
		}

		for (size_t i = 0; i < length - 1; ++i) {
			if (seconds >= data[i].seconds && seconds <= data[i + 1].seconds) {
				Vector2f prevValue(data[i].value);
				Vector2f nextValue(data[i + 1].value);

				float innerElapsedSeconds = static_cast<float>(seconds) - data[i].seconds;
				float innerLengthSeconds = data[i+1].seconds - data[i].seconds;
				float innerSeconds = innerElapsedSeconds / innerLengthSeconds;

				return Interpolation::lerp<Vector2f>(prevValue, nextValue, innerSeconds);
			}
		}

		return data[length - 1].value;
	}


	Vector2f getValueInterpolatedQuadraticBezier(double seconds) const
	{
		size_t length = data.size();

		if (length == 0) {
			return Vector2f();
		}
		else if (length == 1) {
			return data[0].value;
		}

		for (size_t i = 0; i < length - 1; ++i) {
			if (seconds >= data[i].seconds && seconds <= data[i + 1].seconds) {
				Vector2f prevValue(data[i].value);
				Vector2f nextValue(data[i + 1].value);
				Vector2f control1Value(data[i].control1);

				float innerElapsedSeconds = static_cast<float>(seconds) - data[i].seconds;
				float innerLengthSeconds = data[i+1].seconds - data[i].seconds;
				float innerSeconds = innerElapsedSeconds / innerLengthSeconds;

				return Interpolation::quadraticBezier<Vector2f>(prevValue, control1Value, nextValue, innerSeconds);
			}
		}

		return data[length - 1].value;
	}

public:
	EasingType getEasingType() const
	{
		return easingType;
	}


	void setEasingType(const EasingType& easingType)
	{
		this->easingType = easingType;
	}


	InterpolationType getInterpolationType() const
	{
		return interpolationType;
	}


	void setInterpolationType(const InterpolationType& interpolationType)
	{
		this->interpolationType = interpolationType;
	}


	bool isLooping() const
	{
		return looping;
	}


	void setLooping(bool looping)
	{
		this->looping = looping;
	}


	size_t size() const
	{
		return data.size();
	}


public: // toString
	UString toString(const UString& newLinePadding) const
	{
		if (data.size() == 1) {
			return data[0].value.toString();
		}

		UString str;
		str += "{\n" + newLinePadding + "  easingType=\"";
		str += easingTypeToString(easingType);
		str += "\", interpolationType=";
		str += interpolationTypeToString(interpolationType);
		str += "\", looping=";
		if (looping == true) {
			str += "true";
		}
		else {
			str += "false";
		}
		str += ",\n" + newLinePadding + "  data=[\n";
		for (const auto& value : data) {
			str += newLinePadding + "    ";
			str += value.toString();
			str += ",\n";
		}
		str += newLinePadding + "  ].length=";
		str.append2(data.size());
		str += "\n" + newLinePadding + "}";
		return str;
	}


	UString toStringDebug(const UString& newLinePadding) const
	{
		UString str("Vector2Keyframes<T>(");
		str += toString(newLinePadding);
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Vector2Keyframes<T>& right)
	{
		return ost << right.toString();
	}
};


typedef Vector2Keyframes<int> Vector2iKeyframes;
typedef Vector2Keyframes<unsigned int> Vector2uKeyframes;
typedef Vector2Keyframes<float> Vector2fKeyframes;


} // End namespace Verso
