#pragma once

#include <Verso/Math/Range.hpp>
#include <cmath>
#include <ctime>

namespace Verso {


class Random
{
public: // static

	static void initSeed(unsigned int seed)
	{
		srand(seed);
	}

	static void initSeedWithTime()
	{
		srand(static_cast<unsigned int>(time(nullptr)));
	}


	static int intRange(const Rangei& range)
	{
		return intRange(range.minValue, range.maxValue);
	}

	static int intRange(int low, int high)
	{
		if  (low == high)
			return low;

		if (low > high) {
			int swap = low;
			low = high;
			high = swap;
		}

		return (rand() % (high - low + 1)) + low;
	}


	static unsigned int uintRange(const Rangeu& range)
	{
		return uintRange(range.minValue, range.maxValue);
	}

	static unsigned int uintRange(unsigned int low, unsigned int high)
	{
		if  (low == high)
			return low;

		if (low > high) {
			unsigned int swap = low;
			low = high;
			high = swap;
		}

		return (static_cast<unsigned int>(rand()) % (high - low + 1)) + low;
	}


	static float floatRange(const Rangef& range)
	{
		return floatRange(range.minValue, range.maxValue);
	}

	static float floatRange(float low, float high)
	{
		if  (low == high)
			return low;

		if (low > high) {
			float swap = low;
			low = high;
			high = swap;
		}

		return (rand() /
				(static_cast<float>(RAND_MAX) + 1.0f))
				* (high - low) + low;
	}


	static double doubleRange(const Ranged& range)
	{
		return doubleRange(range.minValue, range.maxValue);
	}

	static double doubleRange(double low, double high)
	{
		if (low == high)
			return  low;

		if (low > high) {
			double swap = low;
			low = high;
			high = swap;
		}

		return (rand() /
				(static_cast<double>(RAND_MAX) + 1.0f))
				* (high - low) + low ;
	}
};


} // End namespace Verso
