#pragma once

#include <Verso/Math/Vector2.hpp>
#include <Verso/System/Exception/ObjectNotFoundException.hpp>

#include <vector>

namespace Verso {


// Declare operator so it can be used in the inline code
class AspectRatio;
bool operator ==(const AspectRatio& left, const AspectRatio& right);


class AspectRatio
{
public:
	float value;
	UString textual;

public:
	AspectRatio() :
		value(0),
		textual("")
	{
	}


	AspectRatio(float value, const UString& textual) :
		value(value),
		textual(textual)
	{
	}


	AspectRatio(const Vector2i& resolution, const UString& textual) :
		value(0),
		textual()
	{
		setByResolution(resolution);
		if (textual.size() != 0) {
			this->textual = textual;
		}
	}


	explicit AspectRatio(const UString& textual) :
		value(0),
		textual()
	{
		size_t pos = textual.findFirstOf(':');
		if (pos != std::string::npos) {
			if (pos > 0) {
				float ratio1 = textual.substring(0, static_cast<int>(pos)).toValue<float>();
				if (ratio1 <= 0.0f) {
					VERSO_ILLEGALFORMAT("verso-base", "Cannot parse textual aspect ratio, ratio1 should be larger than zero", textual.c_str());
				}

				if (pos + 1 < textual.size()) {
					float ratio2 = textual.substring(static_cast<int>(pos) + 1).toValue<float>();
					if (ratio2 <= 0.0f) {
						VERSO_ILLEGALFORMAT("verso-base", "Cannot parse textual aspect ratio, ratio2 should be larger than zero", textual.c_str());
					}
					this->value = ratio1 / ratio2;
					this->textual = textual;
					return;
				}
			}
		}

		VERSO_ILLEGALFORMAT("verso-base", "Cannot parse textual aspect ratio, should be \"ratio1:ratio2\"", textual.c_str());
	}


	AspectRatio(const AspectRatio& original) :
		value(original.value),
		textual(original.textual)
	{
	}


	AspectRatio(AspectRatio&& original) noexcept :
		value(std::move(original.value)),
		textual(std::move(original.textual))
	{
	}


	AspectRatio& operator =(const AspectRatio& original)
	{
		if (this != &original) {
			value = original.value;
			textual = original.textual;
		}
		return *this;
	}


	AspectRatio& operator =(AspectRatio&& original) noexcept
	{
		if (this != &original) {
			value = std::move(original.value);
			textual = std::move(original.textual);
		}
		return *this;
	}


	~AspectRatio()
	{
	}


	void setByResolution(const Vector2i& resolution)
	{
		if (resolution.y != 0) {
			value = static_cast<float>(resolution.x) / static_cast<float>(resolution.y);

			bool found = false;
			std::vector<AspectRatio> aspectRatios = getAspectRatios();
			for (size_t i=0; i<aspectRatios.size(); ++i) {
				if (value == aspectRatios[i].value) {
					textual = aspectRatios[i].textual;
					found = true;
					break;
				}
			}
			if (found == false) {
				textual = "";
				textual.append2(resolution.x);
				textual += ":";
				textual.append2(resolution.y);
			}
		}
		else {
			value = 0;
			textual = "invalid";
		}
	}


public: // static
	static const std::vector<AspectRatio>& getAspectRatios()
	{
		static std::vector<AspectRatio> knownAspectRatios;
		if (knownAspectRatios.size() == 0) {
			knownAspectRatios.push_back(AspectRatio(4.0f / 3.0f, "4:3"));
			knownAspectRatios.push_back(AspectRatio(5.0f / 4.0f, "5:4"));
			knownAspectRatios.push_back(AspectRatio(16.0f / 9.0f, "16:9"));
			knownAspectRatios.push_back(AspectRatio(16.0f / 10.0f, "16:10"));
			//knownAspectRatios.push_back(AspectRatio(1.0f, "1:1"));
			//knownAspectRatios.push_back(AspectRatio(1.85f / 1.0f, "1.85:1"));
			//knownAspectRatios.push_back(AspectRatio(2.0f / 1.0f, "2:1"));
			//knownAspectRatios.push_back(AspectRatio(2.35f / 1.0f, "2.35:1"));
			//knownAspectRatios.push_back(AspectRatio(2.39f / 1.0f, "2.39:1"));
			//knownAspectRatios.push_back(AspectRatio(2.4f / 1.0f, "12:5"));
		}
		return knownAspectRatios;
	}


	static AspectRatio getAspectRatio(const UString& name)
	{
		const std::vector<AspectRatio>& aspectRatios = getAspectRatios();
		for (auto& aspectRatio : aspectRatios) {
			if (aspectRatio.textual.equals(name)) {
				return aspectRatio;
			}
		}

		UString error("Cannot find AspectRatio by name: ");
		error.append2(name);
		VERSO_OBJECTNOTFOUND("verso-base", error.c_str(), "");
	}


	static AspectRatio* findIdenticalFromList(const AspectRatio& selected, std::vector<AspectRatio>& aspectRatios)
	{
		for (size_t i = 0; i < aspectRatios.size(); ++i) {
			if (aspectRatios[i] == selected) {
				return &aspectRatios[i];
			}
		}
		return nullptr;
	}


public: // toString
	UString toString() const
	{
		return textual;
	}


	UString toStringDebug() const
	{
		UString str("AspectRatio(");
		str.append2(value);
		str += " (";
		str += toString();
		str += ")";
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const AspectRatio& right)
	{
		return ost << right.toString();
	}
};


inline bool operator ==(const AspectRatio& left, const AspectRatio& right)
{
	return (left.value == right.value) &&
		   (left.textual == right.textual);
}


inline bool operator !=(const AspectRatio& left, const AspectRatio& right)
{
	return !(left == right);
}


inline bool operator <(const AspectRatio& left, const AspectRatio& right)
{
	if (left.value == right.value) {
		return left.textual < right.textual;
	}
	else {
		return left.value < right.value;
	}
}


inline bool operator >(const AspectRatio& left, const AspectRatio& right)
{
	return right < left;
}


inline bool operator <=(const AspectRatio& left, const AspectRatio& right)
{
	return !(right < left);
}


inline bool operator >=(const AspectRatio& left, const AspectRatio& right)
{
	return !(left < right);
}


} // End namespace Verso
