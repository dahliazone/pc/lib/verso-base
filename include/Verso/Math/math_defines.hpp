#pragma once

#include <cmath>


// M_PI is not part of C++ standard
#ifndef M_PI
        #define M_PI 3.14159265358979323846
#endif
