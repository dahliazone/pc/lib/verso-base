#pragma once

#include <Verso/Math/Easing.hpp>
#include <Verso/Math/InterpolationType.hpp>
#include <Verso/Math/Interpolation.hpp>
#include <Verso/Math/RgbaColorfKeyframeElement.hpp>

namespace Verso {


class RgbaColorfKeyframes
{
public:
	EasingType easingType;
	InterpolationType interpolationType;
	bool looping;
	std::vector<RgbaColorfKeyframeElement> data;

public:
	RgbaColorfKeyframes() :
		easingType(EasingType::Linear),
		interpolationType(InterpolationType::Undefined),
		looping(false),
		data()
	{
	}


	explicit RgbaColorfKeyframes(
			const EasingType& easingType,
			const InterpolationType& interpolationType,
			bool looping,
			const std::vector<RgbaColorfKeyframeElement>& data) :
		easingType(easingType),
		interpolationType(interpolationType),
		looping(looping),
		data(data)
	{
	}


	explicit RgbaColorfKeyframes(const RgbaColorf& singleValue) :
		easingType(EasingType::Linear),
		interpolationType(InterpolationType::Constant),
		looping(false),
		data()
	{
		data.push_back(RgbaColorfKeyframeElement(0.0f, singleValue));
	}


	RgbaColorfKeyframes(const RgbaColorfKeyframes& original) :
		easingType(original.easingType),
		interpolationType(original.interpolationType),
		looping(original.looping),
		data(original.data)
	{
	}


	RgbaColorfKeyframes(RgbaColorfKeyframes&& original) noexcept :
		easingType(std::move(original.easingType)),
		interpolationType(std::move(original.interpolationType)),
		looping(std::move(original.looping)),
		data(std::move(original.data))
	{
	}


	RgbaColorfKeyframes& operator =(const RgbaColorfKeyframes& original)
	{
		if (this != &original) {
			easingType = original.easingType;
			interpolationType = original.interpolationType;
			looping = original.looping;
			data = original.data;
		}
		return *this;
	}


	RgbaColorfKeyframes& operator =(RgbaColorfKeyframes&& original) noexcept
	{
		if (this != &original) {
			easingType = std::move(original.easingType);
			interpolationType = std::move(original.interpolationType);
			looping = std::move(original.looping);
			data = std::move(original.data);
		}
		return *this;
	}


	~RgbaColorfKeyframes()
	{
	}


public:
	void add(const RgbaColorfKeyframeElement& element)
	{
		if (data.size() > 0 && element.seconds <= data[data.size() - 1].seconds) {
			UString error("FloatKeyframe seconds at index ");
			error.append2(data.size() - 1);
			error += " was not bigger than previous value!";
			VERSO_ILLEGALPARAMETERS("verso-base", error.c_str(), "");
		}
		data.push_back(element);
	}


	void clear()
	{
		data.clear();
	}


	RgbaColorfKeyframeElement& operator[](size_t index)
	{
		return data[index];
	}


	const RgbaColorfKeyframeElement& operator[](size_t index) const
	{
		return data[index];
	}


	RgbaColorf getValueInterpolated(double seconds) const
	{
		const float lengthSeconds = data[data.size() - 1].seconds;
		if (looping == true && seconds > lengthSeconds) {
			seconds = seconds - floor(seconds / lengthSeconds) * lengthSeconds;
		}
		float delta = static_cast<float>(seconds) / lengthSeconds;
		if (delta > 1.0f) {
			delta = 1.0f;
		}
		const float deltaEased = Easing::byType(easingType, delta);
		const float secondsEased = deltaEased * lengthSeconds;

		switch (interpolationType)
		{
		case InterpolationType::Constant:
			return getValueInterpolatedLinear(secondsEased); // handles single value case
		case InterpolationType::Linear:
			return getValueInterpolatedLinear(secondsEased);
		case InterpolationType::QuadraticBezier:
			return getValueInterpolatedQuadraticBezier(secondsEased);
		default:
			std::cout << "Unsupported InterpolationType=" << interpolationTypeToString(interpolationType) << std::endl;
			VERSO_FAIL("verso-base");
		}
	}

private:
	RgbaColorf getValueInterpolatedLinear(double seconds) const
	{
		size_t length = data.size();

		if (length == 0) {
			return RgbaColorf();
		}
		else if (length == 1) {
			return data[0].value;
		}

		for (size_t i = 0; i < length - 1; ++i) {
			if (seconds >= data[i].seconds && seconds <= data[i + 1].seconds) {
				RgbaColorf prevValue(data[i].value);
				RgbaColorf nextValue(data[i + 1].value);

				float innerElapsedSeconds = static_cast<float>(seconds) - data[i].seconds;
				float innerLengthSeconds = data[i+1].seconds - data[i].seconds;
				float innerSeconds = innerElapsedSeconds / innerLengthSeconds;

				return Interpolation::lerp<RgbaColorf>(prevValue, nextValue, innerSeconds);
			}
		}

		return data[length - 1].value;
	}


	RgbaColorf getValueInterpolatedQuadraticBezier(double seconds) const
	{
		size_t length = data.size();

		if (length == 0) {
			return RgbaColorf();
		}
		else if (length == 1) {
			return data[0].value;
		}

		for (size_t i = 0; i < length - 1; ++i) {
			if (seconds >= data[i].seconds && seconds <= data[i + 1].seconds) {
				RgbaColorf prevValue(data[i].value);
				RgbaColorf nextValue(data[i + 1].value);
				RgbaColorf control1Value(data[i].control1);

				float innerElapsedSeconds = static_cast<float>(seconds) - data[i].seconds;
				float innerLengthSeconds = data[i+1].seconds - data[i].seconds;
				float innerSeconds = innerElapsedSeconds / innerLengthSeconds;

				return Interpolation::quadraticBezier<RgbaColorf>(prevValue, control1Value, nextValue, innerSeconds);
			}
		}

		return data[length - 1].value;
	}

public:
	EasingType getEasingType() const
	{
		return easingType;
	}


	void setEasingType(const EasingType& easingType)
	{
		this->easingType = easingType;
	}


	InterpolationType getInterpolationType() const
	{
		return interpolationType;
	}


	void setInterpolationType(const InterpolationType& interpolationType)
	{
		this->interpolationType = interpolationType;
	}


	bool isLooping() const
	{
		return looping;
	}


	void setLooping(bool looping)
	{
		this->looping = looping;
	}


	size_t size() const
	{
		return data.size();
	}


public: // toString
	UString toString(const UString& newLinePadding) const
	{
		if (data.size() == 1) {
			return data[0].value.toString();
		}

		UString str;
		str += "{\n" + newLinePadding + "  easingType=\"";
		str += easingTypeToString(easingType);
		str += "\", interpolationType=";
		str += interpolationTypeToString(interpolationType);
		str += "\", looping=";
		if (looping == true) {
			str += "true";
		}
		else {
			str += "false";
		}
		str += ", data=[\n";
		for (const auto& value : data) {
			str += newLinePadding + "    ";
			str += value.toString();
			str += ",\n";
		}
		str += newLinePadding + "  ].length=";
		str.append2(data.size());
		str += "}\n" + newLinePadding;
		return str;
	}


	UString toStringDebug(const UString& newLinePadding) const
	{
		UString str("RgbaColorfKeyframes(");
		str += toString(newLinePadding);
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const RgbaColorfKeyframes& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso
