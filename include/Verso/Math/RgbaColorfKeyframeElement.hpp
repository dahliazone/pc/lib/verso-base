#pragma once

#include <Verso/Math/RgbaColorf.hpp>

namespace Verso {


class RgbaColorfKeyframeElement
{
public:
	float seconds;
	RgbaColorf value;
	RgbaColorf control1;
	RgbaColorf control2;

public:
	RgbaColorfKeyframeElement(float seconds, const RgbaColorf& value,
							  const RgbaColorf& control1 = RgbaColorf(),
							  const RgbaColorf& control2 = RgbaColorf()) :
		seconds(seconds),
		value(value),
		control1(control1),
		control2(control2)
	{
	}


public: // toString
	UString toString() const
	{
		UString str("{ seconds=");
		str.append2(seconds);
		str += ", value=";
		str += value.toString();
		str += ", control1=";
		str += control1.toString();
		str += ", control2=";
		str += control2.toString();
		str += " }";
		return str;
	}


	UString toStringDebug() const
	{
		UString str("RgbaColorfKeyframeElement(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const RgbaColorfKeyframeElement& right)
	{
		return ost << right.toString();
	}
};


inline bool operator ==(const RgbaColorfKeyframeElement& left, const RgbaColorfKeyframeElement& right)
{
	return (left.seconds == right.seconds) &&
			(left.value == right.value) &&
			(left.control1 == right.control1) &&
			(left.control2 == right.control2);
}


inline bool operator !=(const RgbaColorfKeyframeElement& left, const RgbaColorfKeyframeElement& right)
{
	return !(left == right);
}


} // End namespace Verso
