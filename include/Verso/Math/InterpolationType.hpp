#pragma once

#include <Verso/System/UString.hpp>

namespace Verso {


enum class InterpolationType
{
	Undefined,
	Constant,
	Linear,
	QuadraticBezier,
	CubicBezier,
	QuinticBezier,
	CubicHermiteSpline
};


inline UString interpolationTypeToString(const InterpolationType& type)
{
	if (type == InterpolationType::Undefined)
		return "Undefined";
	else if (type == InterpolationType::Constant)
		return "Constant";
	else if (type == InterpolationType::Linear)
		return "Linear";
	else if (type == InterpolationType::QuadraticBezier)
		return "QuadraticBezier";
	else if (type == InterpolationType::CubicBezier)
		return "CubicBezier";
	else if (type == InterpolationType::QuinticBezier)
		return "QuinticBezier";
	else if (type == InterpolationType::CubicHermiteSpline)
		return "CubicHermiteSpline";
	else
		return "Unknown value";
}


// returns InterpolationType::Undefined for any erroneous string
inline InterpolationType stringToInterpolationType(const UString& str)
{
	if (str.equals("Constant"))
		return InterpolationType::Constant;
	else if (str.equals("Linear"))
		return InterpolationType::Linear;
	else if (str.equals("QuadraticBezier"))
		return InterpolationType::QuadraticBezier;
	else if (str.equals("CubicBezier"))
		return InterpolationType::CubicBezier;
	else if (str.equals("QuinticBezier"))
		return InterpolationType::QuinticBezier;
	else if (str.equals("CubicHermiteSpline"))
		return InterpolationType::CubicHermiteSpline;
	else
		return InterpolationType::Undefined;
}


} // End namespace Verso
