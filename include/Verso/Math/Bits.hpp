#pragma once

#include <Verso/Math/Math.hpp>

namespace Verso {


	namespace Bits {

		inline bool getBit8(uint8_t value, uint8_t bitIndex)
		{
			VERSO_ASSERT_MSG("verso-base", bitIndex <= 7, "Trying to access bit that is over the limit");
			if (bitIndex > 8) {
				return false;
			}

			return (value >> bitIndex) & 1;
		}


		// Returns a sub value at bitIndex that is bitCount long
		inline uint8_t getSubValue8(uint8_t value, uint8_t bitIndex, uint8_t bitCount)
		{
			constexpr uint8_t bitMask = 0xff;

			VERSO_ASSERT_MSG("verso-base", bitIndex <= 7, "Trying to access bit that is over the limit");
			if (bitIndex > 8) {
				return 0;
			}

			bitCount = Math::minValue<uint8_t>(bitCount, 8 - bitIndex);

			return (value >> bitIndex) & (bitMask >> (8 - bitCount));
		}


		// Returns given value with a sub value inserted at bitIndex that is bitCount long
		inline uint8_t insertSubValue8(uint8_t value, uint8_t subValue, uint8_t bitIndex, uint8_t bitCount)
		{
			constexpr const uint8_t bitMask = 0xff;

			VERSO_ASSERT_MSG("verso-base", bitIndex <= 7, "Trying to access bit that is over the limit");
			if (bitIndex > 8) {
				return 0;
			}

			bitCount = Math::minValue<uint8_t>(bitCount, 8 - bitIndex);

			// Zero out the area where subValue is to be inserted into
			value &= ~((bitMask >> (8 - bitCount)) << bitIndex);

			// Insert the subValue there
			return value | ((subValue & (bitMask >> (8 - bitCount))) << bitIndex);
		}

	} // End namespace Bits


} // End namespace Verso
