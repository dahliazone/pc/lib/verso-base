#pragma once

#include <Verso/System/UString.hpp>

namespace Verso {


enum class VAlign
{
	Undefined,
	Top,
	Center,
	Bottom
};


inline UString vAlignToString(const VAlign& vAlign)
{
	if (vAlign == VAlign::Undefined)
		return "Undefined";
	else if (vAlign == VAlign::Top)
		return "Top";
	else if (vAlign == VAlign::Center)
		return "Center";
	else if (vAlign == VAlign::Bottom)
		return "Bottom";
	else
		return "Unknown value";
}


// returns VAlign::Undefined for any erroneous string
inline VAlign stringToVAlign(const UString& str)
{
	if (str.equals("Top"))
		return VAlign::Top;
	else if (str.equals("Center"))
		return VAlign::Center;
	else if (str.equals("Bottom"))
		return VAlign::Bottom;
	else
		return VAlign::Undefined;
}


} // End namespace Verso
