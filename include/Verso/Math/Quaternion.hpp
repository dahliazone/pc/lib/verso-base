#pragma once

#include <Verso/Math/Vector3.hpp>
#include <Verso/Math/Matrix4x4f.hpp>
#include <Verso/Math/Math.hpp>
#include <Verso/System/assert.hpp>
#include <ostream>
#include <cfloat>

namespace Verso {


class Quaternion
{
public:
	float data[4]; // x,y,z,w

public: // constructors
	Quaternion() :
		data{0.0f, 0.0f, 0.0f, 0.0f}
	{
	}


	Quaternion(float x, float y, float z, float w) :
		data{x, y, z, w}
	{
	}


	Quaternion(Degree yaw, Degree pitch, Degree roll)
	{
		set(yaw, pitch, roll);
	}


	Quaternion(const Vector3f& axis, float angle)
	{
		set(axis, angle);
	}


	Quaternion(const Matrix4x4f& matrix)
	{
		set(matrix);
	}


	Quaternion(const Quaternion& value)
	{
		set(value);
	}


public: // setters
	void set(float x, float y, float z, float w)
	{
		this->data[0] = x;
		this->data[1] = y;
		this->data[2] = z;
		this->data[3] = w;
	}


	// Rotation around z axis, x axis and y axis in that order TODO: is it so?
	void set(Degree pitch, Degree yaw, Degree roll)
	{
		const float sinPitch = std::sin(pitch * 0.5f);
		const float cosPitch = std::cos(pitch * 0.5f);
		const float sinYaw = std::sin(yaw * 0.5f);
		const float cosYaw = std::cos(yaw * 0.5f);
		const float sinRoll = std::sin(roll * 0.5f);
		const float cosRoll = std::cos(roll * 0.5f);
		const float cosPitchCosYaw = cosPitch * cosYaw;
		const float sinPitchSinYaw = sinPitch * sinYaw;
		data[0] = sinRoll * cosPitchCosYaw    - cosRoll * sinPitchSinYaw;
		data[1] = cosRoll * sinPitch * cosYaw + sinRoll * cosPitch * sinYaw;
		data[2] = cosRoll * cosPitch * sinYaw - sinRoll * sinPitch * cosYaw;
		data[3] = cosRoll * cosPitchCosYaw    + sinRoll * sinPitchSinYaw;
	}


	void set(const Vector3f& axis, float angle)
	{
		float sinValue = std::sin(angle / 2.0f);
		data[0] = axis.x * sinValue;
		data[1] = axis.y * sinValue;
		data[2] = axis.z * sinValue;
		data[3] = std::cos(angle / 2.0f);
	}


	void set(const Matrix4x4f& matrix)
	{
		float trace = matrix.data[0][0] + matrix.data[1][1] + matrix.data[2][2];
		float sqrtTrace;

		if (trace > 0.0f) {
		   // |w| > 1/2, may as well choose w > 1/2
		   sqrtTrace = std::sqrt(trace + 1.0f);  // 2w
		   data[3] = 0.5f * sqrtTrace;
		   sqrtTrace = 0.5f / sqrtTrace;  // 1/(4w)
		   data[0] = (matrix.data[2][1] - matrix.data[1][2]) * sqrtTrace;
		   data[1] = (matrix.data[0][2] - matrix.data[2][0]) * sqrtTrace;
		   data[2] = (matrix.data[1][0] - matrix.data[0][1]) * sqrtTrace;
		}
		else {
			// |w| <= 1/2
			static size_t next[3] = { 1, 2, 0 };
			size_t i = 0;
			if (matrix.data[1][1] > matrix.data[0][0])
			   i = 1;
			if (matrix.data[2][2] > matrix.data[i][i])
			   i = 2;
			size_t j = next[i];
			size_t k = next[j];

			sqrtTrace = std::sqrt(matrix.data[i][i] - matrix.data[j][j] - matrix.data[k][k] + 1.0f);
			data[i] = 0.5f * sqrtTrace;
			sqrtTrace = 0.5f / sqrtTrace;
			data[j] = (matrix.data[j][i] + matrix.data[i][j]) * sqrtTrace;
			data[k] = (matrix.data[k][i] + matrix.data[i][k]) * sqrtTrace;
			data[3] = (matrix.data[k][j] - matrix.data[j][k]) * sqrtTrace;
		}
	}


	void set(const Quaternion& original)
	{
		memcpy(&this->data[0], &(original.data[0]), 4 * sizeof(float));
	}


public:
	// Creates a rotation from 'fromDirection' towards 'toDirection'
	void setFromToRotation(const Vector3f& fromDirection, const Vector3f& toDirection)
	{
		*this = Quaternion::fromToRotation(fromDirection, toDirection);
	}


	// Create a look rotation from 'forward' with up pointing 'upward'.
	void setLookRotation(const Vector3f& forward, const Vector3f& upward = Vector3f::up())
	{
		*this = Quaternion::lookRotation(forward, upward);
	}


	float getLength() const
	{
		return std::sqrt(data[0] * data[0] + data[1] * data[1] + data[2] * data[2] + data[3] * data[3]);
	}


	// Note: faster for relative comparing
	float getLengthSquared() const
	{
		return (data[0] * data[0]) + (data[1] * data[1]) + (data[2] * data[2]) + (data[3] * data[3]);
	}


	void normalize()
	{
		float invLength = 1.0f / getLength();
		data[0] *= invLength;
		data[1] *= invLength;
		data[2] *= invLength;
		data[3] *= invLength;
	}


	Quaternion exp() const
	{
		Radian angle = std::sqrt(data[0] * data[0] + data[1] * data[1] + data[2] * data[2]);
		float sinA = std::sin(angle);
		float cosA = std::cos(angle);

		if (std::abs(sinA) >= std::numeric_limits<float>::epsilon()) {
			float coefficient = sinA / angle;
			return Quaternion(coefficient * data[0], coefficient * data[1], coefficient * data[2], cosA);
		}
		else {
			return Quaternion(data[0], data[1], data[2], cosA);
		}
	}


	Quaternion log() const
	{
		if (std::abs(data[3]) < 1.0f) {
			Radian angle = std::acos(data[3]);
			float sinA = std::sin(angle);
			if (std::abs(sinA) >= std::numeric_limits<float>::epsilon()) {
				float coefficient = angle / sinA;
				return Quaternion(coefficient * data[0], coefficient * data[1], coefficient * data[2], 0.0f);
			}
		}

		return Quaternion(data[0], data[1], data[2], 0.0f);
	}


	Quaternion multiplyByScalar(float right) const
	{
		return Quaternion(data[0] * right, data[1] * right, data[2] * right, data[3] * right);
	}


	Quaternion multiplyByQuaternion(const Quaternion& right) const
	{
		float tx = data[3] * right.data[0] + right.data[3] * data[0] + data[1] * right.data[2] - right.data[1] * data[2];
		float ty = data[3] * right.data[1] + right.data[3] * data[1] + data[2] * right.data[0] - right.data[2] * data[0];
		float tz = data[3] * right.data[2] + right.data[3] * data[2] + data[0] * right.data[1] - right.data[0] * data[1];
		float tw = data[3] * right.data[3] - data[0] * right.data[0] - data[1] * right.data[1] - data[2] * right.data[2];
		return Quaternion(tx, ty, tz, tw);
	}


	Quaternion divideByScalar(float right) const
	{
		return Quaternion(data[0] / right, data[1] / right, data[2] / right, data[3] / right);
	}


public: // Convert to another format
	void toEulerAngles(float& pitch, float& yaw, float& roll, bool homogenous=true) const
	{
		float sqw = data[3] * data[3];
		float sqx = data[0] * data[0];
		float sqy = data[1] * data[1];
		float sqz = data[2] * data[2];

		if (homogenous) {
			pitch = atan2f(2.0f * (data[0] * data[1] + data[2] * data[3]), sqx - sqy - sqz + sqw);
			yaw = asinf(-2.0f * (data[0] * data[2] - data[1] * data[3]));
			roll = atan2f(2.0f * (data[1] * data[2] + data[0] * data[3]), -sqx - sqy + sqz + sqw);
		} else {
			pitch = atan2f(2.0f * (data[2] * data[1] + data[0] * data[3]), 1.0f - 2.0f*(sqx + sqy));
			yaw = asinf(-2.0f * (data[0] * data[2] - data[1] * data[3]));
			roll = atan2f(2.0f * (data[0] * data[1] + data[2] * data[3]), 1.0f - 2.0f*(sqy + sqz));
		}
	}


	void toAngleAxis(Degree& outAngle, Vector3f& outAxis) const
	{
		float sqrtLength = data[0] * data[0] + data[1] * data[1] + data[2] * data[2];
		if (sqrtLength > 0.0f)
		{
			outAngle = 2.0f * std::acos(data[3]);
			float invLength = Math::invSqrt(sqrtLength);
			outAxis.x = data[0] * invLength; // \TODO: are these correct?
			outAxis.y = data[1] * invLength;
			outAxis.z = data[2] * invLength;
		}
		else {
			// angle is 0 (mod 2*pi), so any axis will do
			outAngle = Radian(0.0f);
			outAxis.x = 1.0f;
			outAxis.y = 0.0f;
			outAxis.z = 0.0f;
		}
	}


	Matrix4x4f toMatrix() const
	{
		float m[16] = {
			 data[3], -data[2],  data[1], data[0],
			 data[2],  data[3], -data[0], data[1],
			-data[1],  data[0],  data[3], data[2],
			-data[0], -data[1], -data[2], data[3]
		};
		return Matrix4x4f(m);
	}


public: // operators
	float & operator [](size_t index)
	{
		VERSO_ASSERT("verso-base", index < 4);
		return data[index];
	}


	Quaternion& operator =(const Quaternion& original)
	{
		if (this != &original) {
			memcpy(&this->data[0], &(original.data[0]), 4 * sizeof(float));
		}
		return *this;
	}


	Quaternion operator +(const Quaternion& right) const
	{
		return Quaternion(data[0] + right.data[0], data[1] + right.data[1], data[2] + right.data[2], data[3] + right.data[3]);
	}


	Quaternion operator -(const Quaternion& right) const
	{
		return Quaternion(data[0] - right.data[0], data[1] - right.data[1], data[2] - right.data[2], data[3] - right.data[3]);
	}


	Quaternion operator *(float right) const
	{
		return multiplyByScalar(right);
	}


	Quaternion operator /(float right) const
	{
		return divideByScalar(right);
	}


	Vector3f operator *(const Vector3f& right) const
	{
		Vector3f uv, uuv;
		Vector3f qvec(data[0], data[1], data[2]);
		uv = qvec.crossProduct(right);
		uuv = qvec.crossProduct(uv);
		uv *= (2.0f * data[3]);
		uuv *= 2.0f;

		return right + uv + uuv;
	}


	Quaternion operator *(const Quaternion& right) const
	{
		return multiplyByQuaternion(right);
	}


public: // static
	static const Quaternion& Identity()
	{
		static const Quaternion identity(0.0f, 0.0f, 0.0f, 1.0f);
		return identity;
	}


	static float dotProduct(const Quaternion& left, const Quaternion& right)
	{
		return (right.data[3] * left.data[3]) + (right.data[0] * left.data[0]) + (right.data[1] * left.data[1]) + (right.data[2] * left.data[2]);
	}


	// Note: Works for non-unit length Quaternions also
	static Degree angleBetween(const Quaternion& left, const Quaternion& right)
	{
		return Math::radiansToDegrees(std::acos(dotProduct(left, right) / (left.getLength() * right.getLength())));
	}


	// Note: Assumes that the given quaternion is of unit length
	static Degree angleBetweenFast(const Quaternion& left, const Quaternion& right)
	{
		return Math::radiansToDegrees(std::acos(dotProduct(left, right)));
	}


	static Quaternion angleAxis(Degree angle, const Vector3f& axis)
	{
		float radianHalfAngle = degreesToRadians(angle * 0.5f);
		Quaternion out(axis * std::sin(radianHalfAngle), std::cos(radianHalfAngle));
		return out;
	}


	// Creates a rotation from 'fromDirection' towards 'toDirection'
	static Quaternion fromToRotation(const Vector3f& fromDirection, const Vector3f& toDirection)
	{
		VERSO_FAIL("verso-base"); // \TODO: implement
		(void)fromDirection; (void)toDirection; return Quaternion();
	}


	static Quaternion conjugate(const Quaternion& original)
	{
		return Quaternion(-original.data[0], -original.data[1], -original.data[2], original.data[3]);
	}


	// Note: Works for non-unit length Quaternions also
	static Quaternion inverse(const Quaternion& original)
	{
		float norm = original.data[0] * original.data[0] +
					 original.data[1] * original.data[1] +
					 original.data[2] * original.data[2] +
					 original.data[3] * original.data[3];
		if (norm > 0.0f) {
			float invNorm = 1.0f / norm;
			return Quaternion(
					-original.data[0] * invNorm,
					-original.data[1] * invNorm,
					-original.data[2] * invNorm,
					 original.data[3] * invNorm);
		}
		else {
			// No inverse exists, return zero quaternion
			return Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
		}
	}


	// Note: Assumes that the given quaternion is of unit length
	static Quaternion inverseFast(const Quaternion& original)
	{
		return Quaternion::conjugate(original);
	}


	// Create a look rotation from 'forward' with up pointing 'upward'.
	static Quaternion lookRotation(const Vector3f& forward, const Vector3f& upward = Vector3f::up())
	{
		Quaternion out(Matrix4x4f::lookRotation(forward, upward));
		return out;
	}


	// Creates a rotation from 'from' towards 'to' with angle maxDegreesDelta
	static Quaternion rotateTowards(const Quaternion& from, const Quaternion& to, Degree maxDegreesDelta)
	{
		VERSO_FAIL("verso-base"); // \TODO: implement
		(void)from; (void)to; (void)maxDegreesDelta; return Quaternion();
	}


	// Linear interpolation between from and to by t and normalizes the result afterwards.
	static Quaternion lerp(const Quaternion& from, const Quaternion& to, float delta)
	{
		Quaternion out(from + (to - from) * delta);
		out.normalize();
		return out;
	}


	// Spherical interpolation between from and to by t.
	static Quaternion slerp(const Quaternion& from, const Quaternion& to, float delta)
	{
		float angle = std::acos(dotProduct(from, to));
		float fromRatio = std::sin((1.0f - delta) * angle) / std::sin(angle);
		float toRatio = std::sin(delta * angle) / std::sin(angle);
		return from * fromRatio + to * toRatio;


		Quaternion toFixed;
		float dot = Quaternion::dotProduct(from, to);

		// dot = std::cos(angleBetweenQuaternions)
		// if (dot < 0), 'from' and 'to' are more than 90 degrees apart,
		// so we can invert one to reduce spinning
		if (dot < 0) {
			dot = -dot;
			toFixed = to * (-1.0f);
		}
		else {
			toFixed = to;
		}

		if (dot <= 0.99f) {
			float angle = std::acos(dot);
			float sina = std::sin(angle);
			float sinat = std::sin(angle * delta);
			float sinaomt = std::sin(angle * (1.0f - delta));
			return (from * sinaomt + toFixed * sinat) / sina;
		}

		// for small angles just use linear interpolation
		return Quaternion::lerp(from, toFixed, delta);
	}


	// Faster slerp which do not check for theta > 90
	static Quaternion slerpNoInvert(const Quaternion& from, const Quaternion& to, float delta)
	{
		float dot = dotProduct(from, to);

		if (dot >= -0.99f && dot <= 0.99f) {
			float angle = std::acos(dot);
			float sinA = std::sin(angle);
			float sinADelta = std::sin(angle * delta);
			float sinInterpolatedA = std::sin(angle * (1.0f - delta));
			return (from*sinInterpolatedA + to*sinADelta) / sinA;
		}

		// if the angle is small, use linear interpolation
		return Quaternion::lerp(from, to, delta);
	}


	static Quaternion intermediate(const Quaternion& before, const Quaternion& current, const Quaternion& after)
	{
		Quaternion currentConjugate(Quaternion::conjugate(current));
		return current * (( (currentConjugate * before).log() + (currentConjugate * after).log() ) / -4.0f).exp();
	}


	// Spherical cubic interpolation between 'from' and 'to' with  by delta.
	static Quaternion squad(const Quaternion& from, const Quaternion& to, const Quaternion& a,const Quaternion& b, float delta)
	{
		Quaternion c(Quaternion::slerpNoInvert(from, to, delta));
		Quaternion d(Quaternion::slerpNoInvert(a, b, delta));
		return slerpNoInvert(c, d, 2.0f * delta * (1.0f - delta));
	}


public: // toString
	UString toString() const
	{
		UString str("[ ");
		str.append2(data[0]);
		str += " ";
		str.append2(data[1]);
		str += " ";
		str.append2(data[2]);
		str += " ";
		str.append2(data[3]);
		str += " ]";
		return str;
	}


	UString toStringDebug() const
	{
		UString str("Quaternion(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Quaternion& right)
	{
		return ost << right.toString();
	}
};


inline bool operator ==(const Quaternion& left, const Quaternion& right)
{
	for (size_t i = 0; i<4; ++i) {
		if (left.data[i] != right.data[i]) {
			return false;
		}
	}
	return true;
}


inline bool operator !=(const Quaternion& left, const Quaternion& right)
{
	return !(left == right);
}


} // End namespace Verso
