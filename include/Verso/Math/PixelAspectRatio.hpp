#pragma once

#include <Verso/Math/Vector2.hpp>
#include <Verso/System/Exception/ObjectNotFoundException.hpp>

#include <vector>

namespace Verso {


// Declare operator so it can be used in the inline code
class PixelAspectRatio;
bool operator ==(const PixelAspectRatio& left, const PixelAspectRatio& right);


class PixelAspectRatio
{
public:
	float value;
	UString textual;

public:
	PixelAspectRatio() :
		value(1.0f),
		textual("1:1")
	{
	}


	PixelAspectRatio(float value, const UString& textual) :
		value(value),
		textual(textual)
	{
	}


	explicit PixelAspectRatio(const UString& textual) :
		value(0),
		textual()
	{
		size_t pos = textual.findFirstOf(':');
		if (pos != std::string::npos) {
			if (pos > 0) {
				float ratio1 = textual.substring(0, static_cast<int>(pos)).toValue<float>();
				if (ratio1 <= 0.0f) {
					VERSO_ILLEGALFORMAT("verso-base", "Cannot parse textual pixel aspect ratio, ratio1 should be larger than zero", textual.c_str());
				}

				if (pos + 1 < textual.size()) {
					float ratio2 = textual.substring(static_cast<int>(pos) + 1).toValue<float>();
					if (ratio2 <= 0.0f) {
						VERSO_ILLEGALFORMAT("verso-base", "Cannot parse textual pixel aspect ratio, ratio2 should be larger than zero", textual.c_str());
					}
					this->value = ratio1 / ratio2;
					this->textual = textual;
					return;
				}
			}
		}

		VERSO_ILLEGALFORMAT("verso-base", "Cannot parse textual pixel aspect ratio, should be \"ratio1:ratio2\"", textual.c_str());
	}


	PixelAspectRatio(const PixelAspectRatio& original) :
		value(original.value),
		textual(original.textual)
	{
	}


	PixelAspectRatio(PixelAspectRatio&& original) noexcept :
		value(std::move(original.value)),
		textual(std::move(original.textual))
	{
	}


	PixelAspectRatio& operator =(const PixelAspectRatio& original)
	{
		if (this != &original) {
			value = original.value;
			textual = original.textual;
		}
		return *this;
	}


	PixelAspectRatio& operator =(PixelAspectRatio&& original) noexcept
	{
		if (this != &original) {
			value = std::move(original.value);
			textual = std::move(original.textual);
		}
		return *this;
	}


	~PixelAspectRatio()
	{
	}

public:
	Vector2i fitToResolution(const Vector2i& inputResolution) const
	{
		if (value == 1.0f) {
			return inputResolution;
		}

		// Reduce height
		if (value < 1.0f) {
			return Vector2i(
				inputResolution.x,
				static_cast<int>(static_cast<float>(inputResolution.y) * value));
		}

		// Reduce width
		else if (value > 1.0f) {
			return Vector2i(
				static_cast<int>(static_cast<float>(inputResolution.x) / value),
				inputResolution.y);
		}

		return inputResolution;
	}


public: // static
	static const std::vector<PixelAspectRatio>& getPixelAspectRatios()
	{
		static std::vector<PixelAspectRatio> knownPixelAspectRatios;
		if (knownPixelAspectRatios.size() == 0) {
			knownPixelAspectRatios.push_back(PixelAspectRatio(  1.0f /   1.0f,     "1:1")); // 1
			knownPixelAspectRatios.push_back(PixelAspectRatio( 59.0f /  54.0f,   "59:54")); // 1.0925925...   PAL   4:3 704x576 Rec.601
			knownPixelAspectRatios.push_back(PixelAspectRatio( 12.0f /  11.0f,   "12:11")); // 1.090909...    PAL   4:3 704x576 digital
			knownPixelAspectRatios.push_back(PixelAspectRatio( 16.0f /  15.0f,   "16:15")); // 1.066666...    PAL   4:3 720x576 digital
			knownPixelAspectRatios.push_back(PixelAspectRatio(118.0f /  81.0f,  "118:81")); // 1.456790123... PAL  16:9 704x576 Rec.601
			knownPixelAspectRatios.push_back(PixelAspectRatio( 16.0f /  11.0f,   "16:11")); // 1.454545...    PAL  16:9 704x576 digital
			knownPixelAspectRatios.push_back(PixelAspectRatio( 64.0f /  45.0f,   "64:45")); // 1.424242...    PAL  16:9 720x576 digital
			knownPixelAspectRatios.push_back(PixelAspectRatio( 10.0f /  11.0f,   "10:11")); // 0.909090...    NTSC  4:3 704x480 digital
			knownPixelAspectRatios.push_back(PixelAspectRatio( 40.0f /  33.0f,   "40:33")); // 1.212121...    NTSC 16:9 704x480 digital
			knownPixelAspectRatios.push_back(PixelAspectRatio(  4.0f /   3.0f,     "4:3")); // 1.333333...    HDV / HDCAM 16:9 1440x1080 digital
			knownPixelAspectRatios.push_back(PixelAspectRatio( 24.0f /  11.0f,   "24:11")); // 2.181818...
			knownPixelAspectRatios.push_back(PixelAspectRatio( 20.0f /  11.0f,   "20:11")); // 1.818181...
			knownPixelAspectRatios.push_back(PixelAspectRatio( 32.0f /  11.0f,   "32:11")); // 2.909090...
			knownPixelAspectRatios.push_back(PixelAspectRatio( 80.0f /  33.0f,   "80:33")); // 2.424242...
			knownPixelAspectRatios.push_back(PixelAspectRatio( 18.0f /  11.0f,   "18:11")); // 1.636363...
			knownPixelAspectRatios.push_back(PixelAspectRatio( 15.0f /  11.0f,   "15:11")); // 1.363636...
			knownPixelAspectRatios.push_back(PixelAspectRatio( 64.0f /  33.0f,   "64:33")); // 1.939393...
			knownPixelAspectRatios.push_back(PixelAspectRatio(160.0f /  99.0f,  "160:99")); // 1.616161...
			knownPixelAspectRatios.push_back(PixelAspectRatio(  3.0f /   2.0f,     "3:2")); // 1.5
			knownPixelAspectRatios.push_back(PixelAspectRatio(  2.0f /   1.0f,     "2:1")); // 2
			knownPixelAspectRatios.push_back(PixelAspectRatio(  5.0f /   6.0f,     "5:6")); // 0.833333...    CGA/EGA/VGA 320x200 (a.k.a. 1:1.2)
			knownPixelAspectRatios.push_back(PixelAspectRatio(  5.0f /  12.0f,    "5:12")); // 0.416666...    CGA/EGA/VGA 640x200 (a.k.a. 1:2.4)
			knownPixelAspectRatios.push_back(PixelAspectRatio(100.0f / 137.0f, "100:137")); // 0,729927...    EGA/VGA     640x350 (a.k.a. 1:1.37)
		}
		return knownPixelAspectRatios;
	}


	static PixelAspectRatio getPixelAspectRatio(const UString& name)
	{
		const std::vector<PixelAspectRatio>& pixelAspectRatios = getPixelAspectRatios();
		for (auto& pixelAspectRatio : pixelAspectRatios) {
			if (pixelAspectRatio.textual.equals(name)) {
				return pixelAspectRatio;
			}
		}

		UString error("Cannot find PixelAspectRatio by name: ");
		error.append2(name);
		VERSO_OBJECTNOTFOUND("verso-base", error.c_str(), "");
	}


	static PixelAspectRatio* findIdenticalFromList(const PixelAspectRatio& selected, std::vector<PixelAspectRatio>& pixelAspectRatios)
	{
		for (size_t i = 0; i < pixelAspectRatios.size(); ++i) {
			if (pixelAspectRatios[i] == selected) {
				return &pixelAspectRatios[i];
			}
		}
		return nullptr;
	}


public: // toString
	UString toString() const
	{
		return textual;
	}


	UString toStringDebug() const
	{
		UString str("PixelAspectRatio(");
		str.append2(value);
		str += " (";
		str += toString();
		str += ")";
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const PixelAspectRatio& right)
	{
		return ost << right.toString();
	}
};


inline bool operator ==(const PixelAspectRatio& left, const PixelAspectRatio& right)
{
	return (left.value == right.value) &&
		   (left.textual == right.textual);
}


inline bool operator !=(const PixelAspectRatio& left, const PixelAspectRatio& right)
{
	return !(left == right);
}


inline bool operator <(const PixelAspectRatio& left, const PixelAspectRatio& right)
{
	if (left.value == right.value) {
		return left.textual < right.textual;
	}
	else {
		return left.value < right.value;
	}
}


inline bool operator >(const PixelAspectRatio& left, const PixelAspectRatio& right)
{
	return right < left;
}


inline bool operator <=(const PixelAspectRatio& left, const PixelAspectRatio& right)
{
	return !(right < left);
}


inline bool operator >=(const PixelAspectRatio& left, const PixelAspectRatio& right)
{
	return !(left < right);
}


} // End namespace Verso
