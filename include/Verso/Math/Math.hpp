#pragma once

#include <Verso/Math/Degree.hpp>
#include <Verso/Math/Radian.hpp>
#include <cmath>

namespace Verso {


class Math
{
public: // static
	static float invSqrt(float value)
	{
		return 1.0f / std::sqrt(value);
	}


	static Radian degreesToRadians(Degree angleDegrees)
	{
		return Verso::degreesToRadians(angleDegrees);
	}


	static Degree radiansToDegrees(Radian angleRadians)
	{
		return Verso::radiansToDegrees(angleRadians);
	}


	static int toNearestBiggerPowerOfTwo(int number)
	{
		int temp = 1;
		int absNumber = std::abs(number);
		while (temp < absNumber) {
			temp <<= 1;
		}
		if (number >= 0) {
			return temp;
		}
		else {
			return -temp;
		}
	}


	static size_t toNearestBiggerPowerOfTwo(size_t number)
	{
		size_t temp = 1;
		while (temp < number) {
			temp <<= 1;
		}
		return temp;
	}


	static float cotangent(Radian angleRadians)
	{
		return 1.0f / std::tan(angleRadians);
	}


	/**
	 * @return normalized value +/-[0..1] calculated from the given parameters
	 *         using formula of newValue = value / maxValue.
	 */
	template <typename T>
	static T normalizedValue(T value, T maxValue)
	{
		return value / maxValue;
	}


	/**
	 * @return normalized value +/-[0..1] calculated from the given parameters
	 *         using formula of newValue = ((-minValue + value) / (maxValue-minValue)).
	 */
	template <typename T>
	static T normalizedValue(T value, T minValue, T maxValue)
	{
		return ((-minValue + value) / (maxValue-minValue));
	}


	/**
	 * @param a First value.
	 * @param a Second value.
	 * @return smaller one of given values.
	 */
	template <typename T>
	static const T& minValue(const T& a, const T& b)
	{
		return a < b ? a : b;
	}


	/**
	 * @param a First value.
	 * @param a Second value.
	 * @return bigger one of given values.
	 */
	template <typename T>
	static const T& maxValue(const T& a, const T& b)
	{
		return a > b ? a : b;
	}


	template <typename T>
	static const T& clamp(const T& value, const T& min, const T& max)
	{
		if (value < min) {
			return min;
		}
		else if (value > max) {
			return max;
		}
		else {
			return value;
		}
	}


	/*static glm::quat rotationBetweenVectors(glm::vec3 start, glm::vec3 dest)
	{
		start = glm::normalize(start);
		dest = glm::normalize(dest);

		float cosTheta = glm::dot(start, dest);
		glm::vec3 rotationAxis;

		if (cosTheta < -1 + 0.001f){
			// special case when vectors in opposite directions:
			// there is no "ideal" rotation axis
			// So guess one; any will do as long as it's perpendicular to start
			rotationAxis = glm::cross(glm::vec3(0.0f, 0.0f, 1.0f), start);
			if (glm::length2(rotationAxis) < 0.01 ) // bad luck, they were parallel, try again!
				rotationAxis = glm::cross(glm::vec3(1.0f, 0.0f, 0.0f), start);

			rotationAxis = glm::normalize(rotationAxis);
			return glm::angleAxis(180.0f, rotationAxis);
		}

		rotationAxis = glm::cross(start, dest);

		float s = std::sqrt((1 + cosTheta) * 2);
		float invs = 1 / s;

		return glm::quat(
			s * 0.5f,
			rotationAxis.x * invs,
			rotationAxis.y * invs,
			rotationAxis.z * invs
		);
	}*/

public: // static
	static float Pi()
	{
		const static float Pi = 3.14159265358979323846f;
		return Pi;
	}


	static float TwoPi()
	{
		const static float TwoPi = Pi() * 2.0f;
		return TwoPi;
	}


	static float HalfPi()
	{
		const static float HalfPi = Pi() * 0.5f;
		return HalfPi;
	}


	static double Pid()
	{
		const static double Pid = 3.141592653589793238462643383279502884;
		return Pid;
	}


	static double TwoPid()
	{
		const static double TwoPid = Pid() * 2.0;
		return TwoPid;
	}


	static double HalfPid()
	{
		const static double HalfPid = Pid() * 0.5;
		return HalfPid;
	}
};


} // End namespace Verso
