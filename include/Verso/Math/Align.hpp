#pragma once

#include <Verso/Math/VAlign.hpp>
#include <Verso/Math/HAlign.hpp>
#include <Verso/Math/Rect.hpp>
#include <Verso/Math/Vector2.hpp>

namespace Verso {


class Align
{
public:
	HAlign hAlign;
	VAlign vAlign;

public:
	Align(HAlign hAlign = HAlign::Center, VAlign vAlign = VAlign::Center) :
		hAlign(hAlign),
		vAlign(vAlign)
	{
	}


	void set(HAlign hAlign, VAlign vAlign)
	{
		this->hAlign = hAlign;
		this->vAlign = vAlign;
	}


	Vector2i pointOffseti(const Vector2i& objSize) const
	{
		Vector2i newPos;
		switch (hAlign)
		{
		case HAlign::Left:
			newPos.x -= objSize.x / 2;
			break;
		case HAlign::Center:
			break;
		case HAlign::Right:
			newPos.x += objSize.x / 2;
			break;
		default:
			break;
		}

		switch (vAlign)
		{
		case VAlign::Top:
			newPos.y -= objSize.y / 2;
			break;
		case VAlign::Center:
			break;
		case VAlign::Bottom:
			newPos.y += objSize.y / 2;
			break;
		default:
			break;
		}

		return newPos;
	}


	Vector2f pointOffsetf(const Vector2f& objSize) const
	{
		Vector2f newPos;
		switch (hAlign)
		{
		case HAlign::Left:
			newPos.x -= objSize.x / 2.0f;
			break;
		case HAlign::Center:
			break;
		case HAlign::Right:
			newPos.x += objSize.x / 2.0f;
			break;
		default:
			break;
		}

		switch (vAlign)
		{
		case VAlign::Top:
			newPos.y -= objSize.y / 2.0f;
			break;
		case VAlign::Center:
			break;
		case VAlign::Bottom:
			newPos.y += objSize.y / 2.0f;
			break;
		default:
			break;
		}

		return newPos;
	}


	Vector2f pointOffsetCenteredQuad() const
	{
		return pointOffsetf(Vector2f(1.0f, 1.0f));
	}


	Vector2i objectInContainerOffseti(const Vector2i& objectSize, const Vector2i& containerSize) const
	{
		Vector2i newPos;
		switch (hAlign)
		{
		case HAlign::Left:
			break;
		case HAlign::Center:
			newPos.x += containerSize.x / 2 - objectSize.x / 2;
			break;
		case HAlign::Right:
			newPos.x += containerSize.x - objectSize.x;
			break;
		default:
			break;
		}

		switch (vAlign)
		{
		case VAlign::Top:
			break;
		case VAlign::Center:
			newPos.y += containerSize.y / 2 - objectSize.y / 2;
			break;
		case VAlign::Bottom:
			newPos.y += containerSize.y - objectSize.y;
			break;
		default:
			break;
		}

		return newPos;
	}


	Vector2f objectInContainerOffsetf(const Vector2f& objectSize, const Vector2f& containerSize) const
	{
		Vector2f newPos;
		switch (hAlign)
		{
		case HAlign::Left:
			break;
		case HAlign::Center:
			newPos.x += containerSize.x / 2.0f - objectSize.x / 2.0f;
			break;
		case HAlign::Right:
			newPos.x += containerSize.x - objectSize.x;
			break;
		default:
			break;
		}

		switch (vAlign)
		{
		case VAlign::Top:
			break;
		case VAlign::Center:
			newPos.y += containerSize.y / 2.0f - objectSize.y / 2.0f;
			break;
		case VAlign::Bottom:
			newPos.y += containerSize.y - objectSize.y;
			break;
		default:
			break;
		}

		return newPos;
	}


	Vector2i objectInContainerOffseti(const Vector2i& objectSize, const Recti& container) const
	{
		return container.pos + objectInContainerOffseti(objectSize, container.size);
	}


	Vector2f objectInContainerOffsetf(const Vector2f& objectSize, const Rectf& container) const
	{
		return container.pos + objectInContainerOffsetf(objectSize, container.size);
	}


	Recti objectInContaineri(const Vector2i& objectSize, const Recti& container) const
	{
		return Recti(objectInContainerOffseti(objectSize, container), objectSize);
	}


	Rectf objectInContainerf(const Vector2f& objectSize, const Rectf& container) const
	{
		return Rectf(objectInContainerOffsetf(objectSize, container), objectSize);
	}


public: // toString
	UString toString() const
	{
		UString str;
		str += hAlignToString(hAlign);
		str += " | ";
		str += vAlignToString(vAlign);
		return str;
	}


	UString toStringDebug() const
	{
		UString str("Align(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Align& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso
