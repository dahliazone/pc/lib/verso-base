#pragma once

#include <Verso/System/Endian.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/Math/Math.hpp>

namespace Verso {


class RgbaColorf
{
public:
	float r;
	float g;
	float b;
	float a;

public:
	RgbaColorf() :
		r(0.0f),
		g(0.0f),
		b(0.0f),
		a(0.0f)
	{
	}


	RgbaColorf(float r, float g, float b, float a=1.0f) :
		r(r),
		g(g),
		b(b),
		a(a)
	{
	}


	void set(const RgbaColorf& color)
	{
		this->r = color.r;
		this->g = color.g;
		this->b = color.b;
		this->a = color.a;
	}


	void set(float r, float g, float b, float a=1.0f)
	{
		this->r = r;
		this->g = g;
		this->b = b;
		this->a = a;
	}


	std::uint32_t getRgbaValue() const
	{
#if (VERSO_LITTLE_ENDIAN)
		std::uint32_t result =
			static_cast<std::uint32_t>(
					((static_cast<std::uint8_t>(a * 255.0f) << 24) +
					(static_cast<std::uint8_t>(b * 255.0f) << 16) +
					(static_cast<std::uint8_t>(g * 255.0f) << 8) +
					static_cast<std::uint8_t>(r * 255.0f)));
#else
		std::uint32_t result =
			static_cast<std::uint32_t>(
					(static_cast<std::uint8_t>(r * 255.0f) << 24) +
					(static_cast<std::uint8_t>(g * 255.0f) << 16) +
					(static_cast<std::uint8_t>(b * 255.0f) << 8) +
					static_cast<std::uint8_t>(a * 255.0f));
#endif
		return result;
	}


	std::uint32_t getArgbValue() const
	{
#if (VERSO_LITTLE_ENDIAN)
		std::uint32_t result =
			static_cast<std::uint32_t>(
					(static_cast<std::uint8_t>(b * 255.0f) << 24) +
					(static_cast<std::uint8_t>(g * 255.0f) << 16) +
					(static_cast<std::uint8_t>(r * 255.0f) << 8)  +
					static_cast<std::uint8_t>(a * 255.0f));
#else
		std::uint32_t result =
			static_cast<std::uint8_t>(
					(static_cast<std::uint8_t>(a * 255.0f) << 24) +
					(static_cast<std::uint8_t>(r * 255.0f) << 16) +
					(static_cast<std::uint8_t>(g * 255.0f) << 8)  +
					static_cast<std::uint8_t>(b * 255.0f));
#endif
		return result;
	}


	RgbaColorf getScaledHue(float factor) const
	{
		RgbaColorf c;
		c.r = r * factor;
		c.g = g * factor;
		c.b = b * factor;
		c.a = a;
		return c;
	}


	uint8_t* getArrayRgbau8(uint8_t* arr) const
	{
		arr[0] = static_cast<std::uint8_t>(r * 255.0f);
		arr[1] = static_cast<std::uint8_t>(g * 255.0f);
		arr[2] = static_cast<std::uint8_t>(b * 255.0f);
		arr[3] = static_cast<std::uint8_t>(a * 255.0f);
		return arr;
	}


	float* getArrayRgbaf(float* arr) const
	{
		arr[0] = r;
		arr[1] = g;
		arr[2] = b;
		arr[3] = a;
		return arr;
	}


	// Clamps red, green, blue in range [min, max] and alpha in range [0, 1].
	RgbaColorf clamped(float min = 0.0f, float max = 1.0f)
	{
		RgbaColorf color;
		color.r = Math::clamp(r, min, max);
		color.g = Math::clamp(g, min, max);
		color.b = Math::clamp(b, min, max);
		color.a = Math::clamp(a, 0.0f, 1.0f);
		return color;
	}


	// Returns version of the current color normalized to range [min, max]
	// with formula:
	//   float biggestSmallestDiff = biggestComponent - smallestComponent;
	//   float minMaxDiff = max - min;
	//   outComponent = min + ((-smallestComponent + inComponent) / biggestSmallestDiff) * minMaxDiff;
	// Alpha is not normalized and only clamped to range [0, 1].
	RgbaColorf normalized(float min = 0.0f, float max = 1.0f)
	{
		float smallestComponent = r;
		if (g < smallestComponent)
			smallestComponent = g;
		if (b < smallestComponent)
			smallestComponent = b;

		float biggestComponent = r;
		if (g > biggestComponent)
			biggestComponent = g;
		if (b > biggestComponent)
			biggestComponent = b;

		float biggestSmallestDiff = biggestComponent - smallestComponent;
		float minMaxDiff = max - min;

		RgbaColorf color;
		color.r = min + ((-smallestComponent + r) / biggestSmallestDiff) * minMaxDiff;
		color.g = min + ((-smallestComponent + g) / biggestSmallestDiff) * minMaxDiff;
		color.b = min + ((-smallestComponent + b) / biggestSmallestDiff) * minMaxDiff;
		color.a = Math::clamp(a, 0.0f, 1.0f);
		return color;
	}


public: // toString
	UString toString() const
	{
		UString str;
		str.append2(r);
		str += ", ";
		str.append2(g);
		str += ", ";
		str.append2(b);
		str += ", ";
		str.append2(a);
		return str;
	}


	UString toStringDebug() const
	{
		UString str("RgbaColorf(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const RgbaColorf& right)
	{
		return ost << right.toString();
	}
};


inline bool operator ==(const RgbaColorf& left, const RgbaColorf& right)
{
	return (left.r == right.r) && (left.g == right.g) && (left.b == right.b) && (left.a == right.a);
}


inline bool operator !=(const RgbaColorf& left, const RgbaColorf& right)
{
	return !(left == right);
}


inline RgbaColorf operator +(const RgbaColorf& left, const RgbaColorf& right)
{
	return RgbaColorf(left.r+right.r, left.g+right.g, left.b+right.b, left.a+right.a);
}


inline RgbaColorf& operator +=(RgbaColorf& left, const RgbaColorf& right)
{
	left.r += right.r;
	left.g += right.g;
	left.b += right.b;
	left.a += right.a;
	return left;
}


inline RgbaColorf operator -(const RgbaColorf& left, const RgbaColorf& right)
{
	return RgbaColorf(left.r-right.r, left.g-right.g, left.b-right.b, left.a-right.a);
}


inline RgbaColorf& operator -=(RgbaColorf& left, const RgbaColorf& right)
{
	left.r -= right.r;
	left.g -= right.g;
	left.b -= right.b;
	left.a -= right.a;
	return left;
}


// Note: alpha is not affected.
inline RgbaColorf operator *(const RgbaColorf& left, float right)
{
	return RgbaColorf(left.r*right, left.g*right, left.b*right, left.a);
}


// Note: alpha is not affected.
inline RgbaColorf& operator *=(RgbaColorf& left, float right)
{
	left.r *= right;
	left.g *= right;
	left.b *= right;
	return left;
}


inline RgbaColorf operator *(const RgbaColorf& left, const RgbaColorf& right)
{
	return RgbaColorf(left.r*right.r, left.g*right.g, left.b*right.b, left.a*right.a);
}


inline RgbaColorf& operator *=(RgbaColorf& left, const RgbaColorf& right)
{
	left.r *= right.r;
	left.g *= right.g;
	left.b *= right.b;
	left.a *= right.a;
	return left;
}


// Note: alpha is not affected.
inline RgbaColorf operator /(const RgbaColorf& left, float right)
{
	return RgbaColorf(left.r/right, left.g/right, left.b/right, left.a);
}


// Note: alpha is not affected.
inline RgbaColorf& operator /=(RgbaColorf& left, float right)
{
	left.r /= right;
	left.g /= right;
	left.b /= right;
	return left;
}


inline RgbaColorf operator /(const RgbaColorf& left, const RgbaColorf& right)
{
	return RgbaColorf(left.r/right.r, left.g/right.g, left.b/right.b, left.a/right.a);
}


inline RgbaColorf& operator /=(RgbaColorf& left, const RgbaColorf& right)
{
	left.r /= right.r;
	left.g /= right.g;
	left.b /= right.b;
	left.a /= right.a;
	return left;
}


} // End namespace Verso
