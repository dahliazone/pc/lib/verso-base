#pragma once

#include <Verso/System/UString.hpp>
#include <ostream>

namespace Verso {


template <typename T>
class Range
{
public:
	T minValue;
	T maxValue;

public:
	Range() :
		minValue(),
		maxValue()
	{
	}


	Range(const T& minAndMax) :
		minValue(minAndMax),
		maxValue(minAndMax)
	{
	}


	Range(const T& minValue, const T& maxValue) :
		minValue(minValue),
		maxValue(maxValue)
	{
	}


	Range(const Range<T>& original) :
		minValue(original.minValue),
		maxValue(original.maxValue)
	{
	}


	Range(Range<T>&& original) :
		minValue(std::move(original.minValue)),
		maxValue(std::move(original.maxValue))
	{
	}


	Range<T>& operator =(const Range<T>& original)
	{
		if (this != &original) {
			minValue = original.minValue;
			maxValue = original.maxValue;
		}
		return *this;
	}


	Range<T>& operator =(Range<T>&& original) noexcept
	{
		if (this != &original) {
			minValue = std::move(original.minValue);
			maxValue = std::move(original.maxValue);
		}
		return *this;
	}

public:
	T getRangeDiff() const
	{
		return maxValue - minValue;
	}


	T scaleToRange(float relativeValue) const
	{
		return minValue + getRangeDiff() * relativeValue;
	}


public: // toString
	UString toString() const
	{
		UString str("[ ");
		str.append2(minValue);
		str += " .. ";
		str.append2(maxValue);
		str += " ]";
		return str;
	}


	UString toStringDebug() const
	{
		UString str("Range<T>(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const Range<T>& right)
	{
		return ost << right.toString();
	}
};


typedef Range<int> Rangei;
typedef Range<unsigned int> Rangeu;
typedef Range<float> Rangef;
typedef Range<double> Ranged;


template<typename T>
inline bool operator ==(const Range<T>& left, const Range<T>& right)
{
	return (left.minValue == right.minValue) &&
			(left.maxValue == right.maxValue);
}


template<typename T>
inline bool operator !=(const Range<T>& left, const Range<T>& right)
{
	return !(left == right);
}


} // End namespace Verso
