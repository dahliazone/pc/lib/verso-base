#pragma once

#include <Verso/Math/Easing.hpp>
#include <Verso/Math/InterpolationType.hpp>
#include <Verso/Math/Interpolation.hpp>
#include <Verso/Math/CameraKeyframeElement.hpp>

namespace Verso {


class CameraKeyframes
{
public:
	EasingType easingType;
	InterpolationType interpolationType;
	bool looping;
	std::vector<CameraKeyframeElement> data;

public:
	CameraKeyframes() :
		easingType(EasingType::Linear),
		interpolationType(InterpolationType::Undefined),
		looping(false),
		data()
	{
	}


	explicit CameraKeyframes(
			const EasingType& easingType,
			const InterpolationType& interpolationType,
			bool looping,
			const std::vector<CameraKeyframeElement>& data) :
		easingType(easingType),
		interpolationType(interpolationType),
		looping(looping),
		data(data)
	{
	}


	explicit CameraKeyframes(
			const Vector3f& position,
			const Vector3f& direction,
			const Vector3f& up) :
		easingType(EasingType::Linear),
		interpolationType(InterpolationType::Constant),
		looping(false),
		data()
	{
		data.push_back(CameraKeyframeElement(0.0f, position, direction, up));
	}


	CameraKeyframes(const CameraKeyframes& original) :
		easingType(original.easingType),
		interpolationType(original.interpolationType),
		looping(original.looping),
		data(original.data)
	{
	}


	CameraKeyframes(CameraKeyframes&& original) noexcept :
		easingType(std::move(original.easingType)),
		interpolationType(std::move(original.interpolationType)),
		looping(std::move(original.looping)),
		data(std::move(original.data))
	{
	}


	CameraKeyframes& operator =(const CameraKeyframes& original)
	{
		if (this != &original) {
			easingType = original.easingType;
			interpolationType = original.interpolationType;
			looping = original.looping;
			data = original.data;
		}
		return *this;
	}


	CameraKeyframes& operator =(CameraKeyframes&& original) noexcept
	{
		if (this != &original) {
			easingType = std::move(original.easingType);
			interpolationType = std::move(original.interpolationType);
			looping = std::move(original.looping);
			data = std::move(original.data);
		}
		return *this;
	}


	~CameraKeyframes()
	{
	}


public:
	void add(const CameraKeyframeElement& element)
	{
		if (data.size() > 0 && element.seconds <= data[data.size() - 1].seconds) {
			UString error("FloatKeyframe seconds at index ");
			error.append2(data.size() - 1);
			error += " was not bigger than previous value!";
			VERSO_ILLEGALPARAMETERS("verso-base", error.c_str(), "");
		}
		data.push_back(element);
	}


	void clear()
	{
		data.clear();
	}


	CameraKeyframeElement& operator[](size_t index)
	{
		return data[index];
	}


	const CameraKeyframeElement& operator[](size_t index) const
	{
		return data[index];
	}


	// \TODO: Interpolation


public:
	EasingType getEasingType() const
	{
		return easingType;
	}


	void setEasingType(const EasingType& easingType)
	{
		this->easingType = easingType;
	}


	InterpolationType getInterpolationType() const
	{
		return interpolationType;
	}


	void setInterpolationType(const InterpolationType& interpolationType)
	{
		this->interpolationType = interpolationType;
	}


	bool isLooping() const
	{
		return looping;
	}


	void setLooping(bool looping)
	{
		this->looping = looping;
	}


	size_t size() const
	{
		return data.size();
	}


public: // toString
	UString toString(const UString& newLinePadding) const
	{
		if (data.size() == 1) {
			UString str("{ position=");
			str += data[0].position.toString();
			str += ", direction=";
			str += data[0].direction.toString();
			str += ", up=";
			str += data[0].up.toString();
			str += " }";
			return str;
		}

		UString str;
		str += "{\n" + newLinePadding + "  easingType=\"";
		str += easingTypeToString(easingType);
		str += "\", interpolationType=";
		str += interpolationTypeToString(interpolationType);
		str += "\", looping=";
		if (looping == true) {
			str += "true";
		}
		else {
			str += "false";
		}
		str += ", [\n";
		for (const auto& value : data) {
			str += newLinePadding + "    ";
			str += value.toString();
			str += ",\n";
		}
		str += newLinePadding + "  ].length=";
		str.append2(data.size());
		str += "\n" + newLinePadding + "}";
		return str;
	}


	UString toStringDebug(const UString& newLinePadding) const
	{
		UString str("CameraKeyframes(\n");
		str += toString(newLinePadding);
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const CameraKeyframes& right)
	{
		return ost << right.toString("");
	}
};


} // End namespace Verso
