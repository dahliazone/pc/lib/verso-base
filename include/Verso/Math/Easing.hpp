#pragma once

#include <Verso/Math/Math.hpp>
#include <Verso/Math/EasingType.hpp>

namespace Verso {


class Easing
{
public: // static
	static float byType(const EasingType& easingType, float t)
	{
		switch (easingType)
		{
		case EasingType::Linear:
			return t;
		case EasingType::EaseIn:
		case EasingType::QuadraticIn:
			return quadraticIn(t);
		case EasingType::EaseOut:
		case EasingType::QuadraticOut:
			return quadraticOut(t);
		case EasingType::EaseInOut:
		case EasingType::QuadraticInOut:
			return quadraticInOut(t);
		default:
			return t;
		}
	}

	static float linear(float t) {
		return t;
	}


	static float easeIn(float t) {
		return quadraticIn(t);
	}


	static float easeOut(float t) {
		return quadraticOut(t);
	}


	static float easeInOut(float t) {
		return quadraticInOut(t);
	}


	static float quadraticIn(float t) {
		return t * t;
	}


	static float quadraticOut(float t) {
		return t * (2.0f - t);
	}


	static float quadraticInOut(float t) {
		return (t * t) * (2.0f - t);
	}
};


} // End namespace Verso
