#pragma once

#include <Verso/Math/math_defines.hpp>
#include <Verso/System/UString.hpp>

namespace Verso {


#ifndef VERSO_BASE_MATH_DEGREE_RADIAN
#define VERSO_BASE_MATH_DEGREE_RADIAN
typedef float Radian;
typedef float Degree;
#endif


inline Degree radiansToDegrees(Radian angleRadians)
{
	return angleRadians * static_cast<float>(180 / M_PI);
}


inline UString radiansToString(Radian angleRadians)
{
	return UString::from(angleRadians);
}


} // End namespace Verso
