#pragma once

#include <Verso/System/UString.hpp>
#include <Verso/Math/Vector2.hpp>

namespace Verso {


enum class RefScale : uint8_t
{
	Unset,
	ImageSize,
	ImageSize_KeepAspectRatio_FitRect,
	ImageSize_KeepAspectRatio_FromX,
	ImageSize_KeepAspectRatio_FromY,
	ViewportSize,
	ViewportSize_KeepAspectRatio_FitRect,
	ViewportSize_KeepAspectRatio_FromX,
	ViewportSize_KeepAspectRatio_FromY,
	NoScale
};


// returns RefScale::Unset for any erroneous string
inline RefScale stringToRefScale(const UString& str)
{
	if (str.equals("ImageSize")) {
		return RefScale::ImageSize;
	}
	else if (str.equals("ImageSize_KeepAspectRatio_FitRect")) {
		return RefScale::ImageSize_KeepAspectRatio_FitRect;
	}
	else if (str.equals("ImageSize_KeepAspectRatio_FromX")) {
		return RefScale::ImageSize_KeepAspectRatio_FromX;
	}
	else if (str.equals("ImageSize_KeepAspectRatio_FromY")) {
		return RefScale::ImageSize_KeepAspectRatio_FromY;
	}
	else if (str.equals("ViewportSize")) {
		return RefScale::ViewportSize;
	}
	else if (str.equals("ViewportSize_KeepAspectRatio_FitRect")) {
		return RefScale::ViewportSize_KeepAspectRatio_FitRect;
	}
	else if (str.equals("ViewportSize_KeepAspectRatio_FromX")) {
		return RefScale::ViewportSize_KeepAspectRatio_FromX;
	}
	else if (str.equals("ViewportSize_KeepAspectRatio_FromY")) {
		return RefScale::ViewportSize_KeepAspectRatio_FromY;
	}
	else if (str.equals("NoScale")) {
		return RefScale::NoScale;
	}
	else {
		return RefScale::Unset;
	}
}


inline UString refScaleToString(RefScale refScale)
{
	switch (refScale) {
	case RefScale::Unset:
		return "Unset";
	case RefScale::ImageSize:
		return "ImageSize";
	case RefScale::ImageSize_KeepAspectRatio_FitRect:
		return "ImageSize_KeepAspectRatio_FitRect";
	case RefScale::ImageSize_KeepAspectRatio_FromX:
		return "ImageSize_KeepAspectRatio_FromX";
	case RefScale::ImageSize_KeepAspectRatio_FromY:
		return "ImageSize_KeepAspectRatio_FromY";
	case RefScale::ViewportSize:
		return "ViewportSize";
	case RefScale::ViewportSize_KeepAspectRatio_FitRect:
		return "ViewportSize_KeepAspectRatio_FitRect";
	case RefScale::ViewportSize_KeepAspectRatio_FromX:
		return "ViewportSize_KeepAspectRatio_FromX";
	case RefScale::ViewportSize_KeepAspectRatio_FromY:
		return "ViewportSize_KeepAspectRatio_FromY";
	case RefScale::NoScale:
		return "NoScale";
	default:
		return "unset";
	}
}


inline Vector2f calculateRefScaleSize(
		RefScale refScale, const Vector2f& relativeDestSize,
		const Vector2f& imageSize, float imageTargetAspectRatio,
		const Vector2i& viewportSize)
{
	Vector2f refScaleSize;
	switch (refScale) {
	case RefScale::Unset:
	{
		refScaleSize.x = 0;
		refScaleSize.y = 0;
		break;
	}
	case RefScale::ImageSize:
	{
		refScaleSize.x = imageSize.x * relativeDestSize.x;
		refScaleSize.y = imageSize.y * relativeDestSize.y;
		break;
	}

	case RefScale::ImageSize_KeepAspectRatio_FitRect:
	{
		if (relativeDestSize.x > relativeDestSize.y) {
			refScaleSize.x = imageSize.x * relativeDestSize.y;
			refScaleSize.y = imageSize.y * relativeDestSize.y;
		}
		else {
			refScaleSize.x = imageSize.x * relativeDestSize.x;
			refScaleSize.y = imageSize.y * relativeDestSize.x;
		}
		break;
	}

	case RefScale::ImageSize_KeepAspectRatio_FromX:
	{
		refScaleSize.x = imageSize.x * relativeDestSize.x;
		refScaleSize.y = imageSize.y * relativeDestSize.x;
		break;
	}

	case RefScale::ImageSize_KeepAspectRatio_FromY:
	{
		refScaleSize.x = imageSize.x * relativeDestSize.y;
		refScaleSize.y = imageSize.y * relativeDestSize.y;
		break;
	}

	case RefScale::ViewportSize:
	{
		refScaleSize.x = viewportSize.x * relativeDestSize.x;
		refScaleSize.y = viewportSize.y * relativeDestSize.y;
		break;
	}

	case RefScale::ViewportSize_KeepAspectRatio_FitRect:
	{
		if (viewportSize.x * relativeDestSize.x > viewportSize.y * relativeDestSize.y * imageTargetAspectRatio) {
			refScaleSize.y = viewportSize.y * relativeDestSize.y;
			refScaleSize.x = refScaleSize.y * imageTargetAspectRatio;
		}
		else {
			refScaleSize.x = viewportSize.x * relativeDestSize.x;
			refScaleSize.y = refScaleSize.x * (1.0f / imageTargetAspectRatio);
		}

		break;
	}

	case RefScale::ViewportSize_KeepAspectRatio_FromX:
	{
		refScaleSize.x = viewportSize.x * relativeDestSize.x;
		refScaleSize.y = refScaleSize.x * (1.0f / imageTargetAspectRatio);
		break;
	}

	case RefScale::ViewportSize_KeepAspectRatio_FromY:
	{
		refScaleSize.y = viewportSize.y * relativeDestSize.y;
		refScaleSize.x = refScaleSize.y * imageTargetAspectRatio;
		break;
	}

	case RefScale::NoScale:
	{
		refScaleSize.x = relativeDestSize.x;
		refScaleSize.y = relativeDestSize.y;
		break;
	}
	}

	return refScaleSize;
}


} // End namespace Verso
