#pragma once

#include <Verso/verso-base-common.hpp>

namespace Verso {


struct NonCopyable
{
	NonCopyable() = default;
	NonCopyable(const NonCopyable&) = delete;
	NonCopyable& operator=(const NonCopyable&) = delete;
};


} // End namespace Verso
