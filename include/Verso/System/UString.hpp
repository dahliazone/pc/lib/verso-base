#pragma once

#include <Verso/System/assert.hpp>
#include <Verso/System/Exception/IllegalFormatException.hpp>
#include <utf8.h>
#include <sstream>
#include <vector>
#include <iostream>
#include <string.h>
#include <cstdint>
#include <cctype>
#include <algorithm>

//#ifdef _MSC_VER
//#define _CRT_SECURE_NO_WARNINGS
//#endif

namespace Verso {


class UString
{
private:
	//std::vector<unsigned int> utf32;
	mutable std::string utf8Copy;
	//mutable bool changed;

public:
	UString();
	UString(const char* utf8String);
	UString(const std::string& utf8String);
	UString(const std::wstring& utf8Wstring);
	//explicit UString(const std::vector<unsigned int>& utf32Data);
	UString(const UString& original);
	UString(UString&& original) noexcept;
	~UString();

	// string like interface
	size_t size() const;
	const char* c_str() const;

	// Non-changing operations
	const char* toUtf8() const;
	void toUtf8Copy(char** charBuffer) const;

	std::string toUtf8String() const
	{
		/*updateUtf8Copy();
		if (changed == true) {
			utf8::utf32to8(this->utf32.begin(), this->utf32.end(), back_inserter(this->utf8Copy));
			changed = false;
		}*/
		return this->utf8Copy;
	}

	std::wstring toUtf8Wstring() const
	{
		/*updateUtf8Copy();
		if (changed == true) {
			utf8::utf32to8(this->utf32.begin(), this->utf32.end(), back_inserter(this->utf8Copy));
			changed = false;
		}
		return std::wstring(this->utf8Copy.begin(), this->utf8Copy.end());*/
		std::wstring result;
		utf8::utf8to16(utf8Copy.begin(), utf8Copy.end(), back_inserter(result));
		return result;
	}

	UString* clone() const;
	size_t getLength() const;
	unsigned int charAt(int index) const;
	unsigned int operator [](int index) const;

	bool isEmpty() const;

	template <typename T>
	T toValue() const
	{
		//updateUtf8Copy();
		std::stringstream ss;
		ss << this->utf8Copy;
		T result;
		ss >> result;
		if (ss.fail()) {
			VERSO_ILLEGALFORMAT("verso-base", "Cannot convert string to requested type T", this->utf8Copy.c_str());
		}
		return result;
	}

	bool beginsWith(const UString& str) const;
	bool endsWith(const UString& str) const;

	/*int compare(const UString& str) const
	{
		return this->utf32 < str.utf32;
	}*/

	//int compareIgnoreCase(const UString& str) const

	//operator ==
	bool equals(const UString& str) const
	{
		//return this->utf32 == str.utf32;
		return this->utf8Copy.compare(str.utf8Copy) == 0;
	}


	bool equalsIgnoreCase(const UString& str) const
	{
		return std::equal(utf8Copy.begin(), utf8Copy.end(), str.utf8Copy.begin(), str.utf8Copy.end(), [](char a, char b) {
			return std::tolower(static_cast<unsigned char>(a)) == std::tolower(static_cast<unsigned char>(b));
		});
	}


	//size_t indexOf(const UString& str) const


	// see: http://en.cppreference.com/w/cpp/string/basic_string/find_first_of
	size_t findFirstOf(std::uint8_t character, size_t pos = 0) const
	{
		return utf8Copy.find_first_of(character, pos);
	}


	// see: http://en.cppreference.com/w/cpp/string/basic_string/find_first_of
	size_t findFirstOf(const UString& str, size_t pos = 0) const
	{
		VERSO_ASSERT("verso-base", str.utf8Copy.length() > 0);
		return utf8Copy.find_first_of(str.utf8Copy.c_str(), pos);
	}


	// see: http://en.cppreference.com/w/cpp/string/basic_string/find_first_of
	size_t findFirstOf(const UString& str, size_t pos, size_t count) const
	{
		VERSO_ASSERT("verso-base", str.utf8Copy.length() > 0);
		return utf8Copy.find_first_of(str.utf8Copy.c_str(), pos, count);
	}


	// see: http://en.cppreference.com/w/cpp/string/basic_string/find_last_of
	size_t findLastOf(std::uint8_t character, size_t pos = std::string::npos) const
	{
		return utf8Copy.find_last_of(character, pos);
	}


	// see: http://en.cppreference.com/w/cpp/string/basic_string/find_last_of
	size_t findLastOf(const UString& str, size_t pos = std::string::npos) const
	{
		VERSO_ASSERT("verso-base", str.utf8Copy.length() > 0);
		return utf8Copy.find_last_of(str.utf8Copy.c_str(), pos);
	}


	// see: http://en.cppreference.com/w/cpp/string/basic_string/find_last_of
	size_t findLastOf(const UString& str, size_t pos, size_t count) const
	{
		VERSO_ASSERT("verso-base", str.utf8Copy.length() > 0);
		return utf8Copy.find_last_of(str.utf8Copy.c_str(), pos, count);
	}


	//bool contains(const UString& str) const
	//bool match(const UString& str) const
	//vector<UString> match(RegExp re) const ??
	//size_t matchAll(const UString&) const

	// Non-changing operations returning a new string
	std::vector<UString> split(const UString& str) const;
	//std::vector<UString> split(RegExp re) const ??
	//std::vector<UString> splitAll(const UString&) const
	//UString replace(const UString& str) const
	//UString replaceAll(RegExp re) const ???

	UString replaceAll(char findCharacter, char replaceCharacter)
	{
		UString copy(*this);
		size_t length = copy.utf8Copy.size();
		for (size_t i = 0; i < length; ++i) {
			if (copy.utf8Copy[i] == findCharacter) {
				copy.utf8Copy[i] = replaceCharacter;
			}
		}
		return copy;
	}

	//UString replaceAll() const
	UString substring(int startIndex) const;
	UString substring(int startIndex, int numChars) const;

	// Operators
	bool operator ==(const UString& rhs) const
	{
		return equals(rhs);
	}

	bool operator !=(const UString& rhs) const
	{
		//return this->utf32 != rhs.utf32;
		return this->utf8Copy != rhs.utf8Copy;
	}

	bool operator <(const UString& rhs) const
	{
		//return this->utf32 < rhs.utf32;
		return this->utf8Copy < rhs.utf8Copy;
	}

	bool operator <=(const UString& rhs) const
	{
		//return this->utf32 <= rhs.utf32;
		return this->utf8Copy <= rhs.utf8Copy;
	}

	bool operator >(const UString& rhs) const
	{
		//return this->utf32 > rhs.utf32;
		return this->utf8Copy > rhs.utf8Copy;
	}

	bool operator >=(const UString& rhs) const
	{
		//return this->utf32 >= rhs.utf32;
		return this->utf8Copy >= rhs.utf8Copy;
	}

	UString& operator =(const char* str);
	UString& operator =(const std::string& str);
	UString& operator =(const UString& original);
	UString& operator =(UString&& original) noexcept;
	//template<typename T> UString& operator =(const T& value);
	UString& operator +=(const char* str);
	UString& operator +=(const UString& str);
	//template<typename T> UString& operator +=(const T& value);
	//template<typename T> UString operator +(const T& value);

	// Changing methods
	void set(const char* utf8String);
	void set(const char* utf8String, size_t size);
	void set(const std::string& utf8String);
	void set(const std::wstring& utf8Wstring);
	void set(const UString& str);
	template<typename T> void set2(const T& value);

	void resize(size_t newSize, char character = ' ');

	void clear();
	//bool loadFromFile(const UString& fileName)
	//bool saveToFile(const UString& fileName)
	void append(const char* str);
	void append(const std::string& str);
	void append(const std::wstring& str);
	void append(const UString& utf8String);
	template<typename T> void append2(const T& value);

	//void prepend(const UString& utf8String)
	void toUpperCase();
	void toLowerCase();
	//void trim()
	//void trimLeft()
	//void trimRight()

public: // static
	template<typename T> static UString from(const T& value);
	static std::uint32_t utf8CharacterToUtf32Decimal(const char* str);

private:
	//void updateUtf8Copy() const;

public: // toString
	friend std::ostream& operator <<(std::ostream& ost, const UString& uStr)
	{
		return ost << uStr.toUtf8();
	}
};


///////////////////////////////////////////////////////////////////////////////////////////
// Non-member overloaded operators
///////////////////////////////////////////////////////////////////////////////////////////
inline UString operator +(const UString& left, const UString& right)
{
	UString str(left);
	str.append(right);
	return str;
}


inline UString operator +(const UString& left, const char* right)
{
	UString str(left);
	str.append(right);
	return str;
}


inline UString operator +(const char* left, const UString& right)
{
	UString str(left);
	str.append(right);
	return str;
}


inline UString operator +(const std::string& left, const UString& right)
{
	UString str(left);
	str.append(right);
	return str;
}


/*template<typename T>
inline UString operator +(const T& left, const UString& right)
{
	UString str(left);
	str.append2(right);
	return str;
}*/


///////////////////////////////////////////////////////////////////////////////////////////
// Constuctor / destructor
///////////////////////////////////////////////////////////////////////////////////////////
inline UString::UString() :
	//utf32(),
	utf8Copy()
	//changed(true)
{
	//std::cout << "UString(): " << c_str() << std::endl;
}


inline UString::UString(const char* utf8String) :
	//utf32(),
	utf8Copy()
	//changed(true)
{
	set(utf8String);
	//std::cout << "UString(const char*="<<utf8String<<"): " << c_str() << std::endl;
}


inline UString::UString(const std::string& utf8String) :
	//utf32(),
	utf8Copy()
	//changed(true)
{
	set(utf8String);
	//std::cout << "UString(const std::string&="<<utf8String<<"): " << c_str() << std::endl;
}

inline UString::UString(const std::wstring& utf8Wstring) :
	//utf32(),
	utf8Copy()
	//changed(true)
{
	set(utf8Wstring);
	//std::cout << "UString(const std::wstring&="<<utf8Wstring<<"): " << c_str() << std::endl;
}


/*inline UString::UString(const std::vector<unsigned int>& utf32Data) :
	//utf32(utf32Data),
	utf8Copy()
	//changed(true)
{
}*/


inline UString::UString(const UString& original) :
	//utf32(original.utf32),
	utf8Copy(original.utf8Copy)
	//changed(true)
{
	//std::cout << "UString(const UString&="<<original.c_str()<<"):"<< c_str() << std::endl;
}


inline UString::UString(UString&& original) noexcept :
	//utf32(std::move(original.utf32)),
	utf8Copy(std::move(original.utf8Copy))
	//changed(std::move(original.changed))
{
	//std::cout << "UString(UString&&="<< original.c_str()<<"): "<<c_str() << std::endl;
}


inline UString::~UString()
{
	//std::cout << "~UString(): " << c_str() << std::endl;
}


///////////////////////////////////////////////////////////////////////////////////////////
// string like interface
///////////////////////////////////////////////////////////////////////////////////////////
inline size_t UString::size() const
{
	return getLength();
}


inline const char* UString::c_str() const
{
	return toUtf8();
}


///////////////////////////////////////////////////////////////////////////////////////////
// Non-changing operations
///////////////////////////////////////////////////////////////////////////////////////////
inline const char* UString::toUtf8() const
{
	//updateUtf8Copy();
	return this->utf8Copy.c_str();
}

inline void UString::toUtf8Copy(char** charBuffer) const
{
	//updateUtf8Copy();
	*charBuffer = new char[this->utf8Copy.size()+1];
	strcpy(*charBuffer, this->utf8Copy.c_str());
	((*charBuffer)[this->utf8Copy.size()]) = '\0';
}


inline UString* UString::clone() const
{
	UString* s = new UString(*this);
	return s;
}


inline size_t UString::getLength() const
{
	//return this->utf32.size();
	// \TODO: cache size maybe?
	return static_cast<size_t>(utf8::distance(utf8Copy.c_str(), utf8Copy.c_str() + strlen(utf8Copy.c_str())));
}


inline unsigned int UString::charAt(int index) const
{
	VERSO_ASSERT_MSG("verso-base", index >= 0, "index must be zero or more."); // \TODO: this could rotate!?
	//VERSO_ASSERT_MSG("verso-base", index < static_cast<int>(this->utf32.size()), "index must not be larged than UString size.");

	//return this->utf32[static_cast<size_t>(index)];
	//octet_iterator& it, octet_iterator end

	//size_t first = 0;
	//size_t last = utf8Copy.size();
	/*typename std::iterator_traits<octet_iterator>::difference_type it;
	for (it = utf8Copy.begin(); first < last; ++it)
		utf8::next(utf8Copy.begin(), utf8Copy.end());
	return dist;*/

	try {
		/*uint32_t value = 0;
		auto it = utf8Copy.begin();
		for (int i = 0; i <= index && it < utf8Copy.end(); ++i) {
			value = utf8::next(it, utf8Copy.end());
		}
		return value;*/
		auto it = utf8Copy.begin();
		if (index > 0) {
			utf8::advance(it, index, utf8Copy.end());
		}
		return utf8::next(it, utf8Copy.end());
	}
	catch (utf8::not_enough_room& e) {
		(void)e;
		VERSO_ASSERT_MSG("verso-base", false, "index must not be larged than UString size.");
		return 0;
	}
}


inline unsigned int UString::operator [](int index) const
{
	/*VERSO_ASSERT_MSG("verso-base", index >= 0, "index must be zero or more."); // \TODO: this could rotate!?
	VERSO_ASSERT_MSG("verso-base", index < static_cast<int>(this->utf32.size()), "index must not be larged than UString size.");

	return this->utf32[static_cast<size_t>(index)];*/
	return charAt(index);
}


inline bool UString::isEmpty() const
{
	//return this->utf32.empty();
	return this->utf8Copy.empty();
}


inline bool UString::beginsWith(const UString& str) const
{
	if (substring(0, static_cast<int>(str.size())).equals(str)) {
		return true;
	}
	else {
		return false;
	}
}


inline bool UString::endsWith(const UString& str) const
{
	if (substring(static_cast<int>(size() - str.size()), static_cast<int>(str.size())).equals(str)) {
		return true;
	}
	else {
		return false;
	}
}


// Non-changing operations returning a new string

inline std::vector<UString> UString::split(const UString& str) const
{
	(void)str;
	VERSO_FAIL("verso-base"); // \TODO: implement UString::split(UString)
	return std::vector<UString>();
}


inline UString UString::substring(int startIndex) const
{
	VERSO_ASSERT("verso-base", startIndex >= 0 && "startIndex must be zero or more."); // \TODO: this could rotate!?
	/*if (startIndex >= static_cast<int>(this->utf32.size())) {
		return "";
	}

	UString s;
	for (size_t i=static_cast<size_t>(startIndex); i<this->utf32.size(); ++i) {
		s.utf32.push_back(this->utf32[i]);
	}
	s.changed = true;
	return s;*/
	UString result;
	auto it = utf8Copy.begin();

	try {
		// Skip until startIndex
		utf8::advance(it, startIndex, utf8Copy.end());

		while (it != utf8Copy.end()) {
			uint32_t character = utf8::next(it, utf8Copy.end());
			utf8::append(character, std::back_inserter(result.utf8Copy));
		}
	}
	catch (utf8::not_enough_room& e) {
		(void)e;
		return result;
	}

	return result;
}


inline UString UString::substring(int startIndex, int numChars) const
{
	VERSO_ASSERT("verso-base", startIndex >= 0 && "startIndex must be zero or more."); // \TODO: this could rotate!?
	/*VERSO_ASSERT("verso-base", numChars > 0 && "numChars must be more than zero.");

	if (startIndex >= static_cast<int>(this->utf32.size())) {
		return "";
	}

	UString s;
	int charCount = 0;
	for (size_t i=static_cast<size_t>(startIndex); i<this->utf32.size(); ++i) {
		s.utf32.push_back(this->utf32[i]);
		charCount++;
		if (charCount >= numChars)
			break;
	}
	s.changed = true;
	return s;*/

	UString result;
	auto it = utf8Copy.begin();

	try {
		// Skip until startIndex
		utf8::advance(it, startIndex, utf8Copy.end());

		for (int i = 0; it != utf8Copy.end() && i < numChars; ++i) {
			uint32_t character = utf8::next(it, utf8Copy.end());
			utf8::append(character, std::back_inserter(result.utf8Copy));
		}
		// \TODO: error handling if numChars is not reached
	}
	catch (utf8::not_enough_room& e) {
		(void)e;
		return result;
	}

	return result;
}


///////////////////////////////////////////////////////////////////////////////////////////
// Member overloaded operators
///////////////////////////////////////////////////////////////////////////////////////////
inline UString& UString::operator =(const char* str)
{
	//std::cout << "==operator = (const char*=\""<<str<<"\")==" << std::endl << std::flush;
	set(str);
	return *this;
}


inline UString& UString::operator =(const std::string& str)
{
	//std::cout << "==operator = (const std::string& str)==" << std::endl;
	set(str);
	return *this;
}


inline UString& UString::operator =(const UString& str)
{
	//std::cout << "==operator = (const UString&)==" << std::endl;
	set(str);
	return *this;
}


inline UString& UString::operator =(UString&& original) noexcept
{
	//std::cout << "==operator = (const UString&&)==" << std::endl;

	//utf32 = std::move(original.utf32);
	//changed = std::move(original.changed);
	utf8Copy = std::move(original.utf8Copy);

	//std::cout << "UString& UString::operator =(UString&&="<<original.c_str()<<"): "<<c_str() << std::endl;

	return *this;
}


/*template<typename T>
inline UString& UString::operator =(const T& value)
{
	//std::cout << "==operator = (const T&)==" << std::endl;
	set2(value);
	return *this;
}*/


inline UString& UString::operator +=(const char* str)
{
	//std::cout << "==operator += (const char*)==" << std::endl;
	append(str);
	return *this;
}


inline UString& UString::operator +=(const UString& str)
{
	//std::cout << "==operator += (const UString&)==" << std::endl;
	append(str);
	return *this;
}


/*template<typename T>
inline UString& UString::operator +=(const T& value)
{
	//std::cout << "==operator += (const T&)==" << std::endl;
	append2(value);
	return *this;
}*/


/*template<typename T>
inline UString UString::operator +(const T& value)
{
	//std::cout << "==operator + (const T&)==" << std::endl;
	UString t(this->utf32);
	t.append2(value);
	return t;
}*/


///////////////////////////////////////////////////////////////////////////////////////////
// Non-changing operations returning a new string
///////////////////////////////////////////////////////////////////////////////////////////

template<typename T>
inline void UString::set2(const T& value)
{
	std::ostringstream oss;
	oss << value;
	set(oss.str());
}


inline void UString::clear()
{
	//this->utf32.clear();
	this->utf8Copy.clear();
	//changed = false;
}


inline void UString::append(const char* str)
{
	append(UString(str));
}


inline void UString::append(const std::string& str)
{
	append(UString(str));
}


inline void UString::append(const std::wstring& str)
{
	append(UString(str));
}


inline void UString::append(const UString& utf8String)
{
	/*utf8String.updateUtf8Copy();
	this->utf32.reserve(this->utf32.size() + utf8String.utf32.size());
	this->utf32.insert(this->utf32.end(), utf8String.utf32.begin(), utf8String.utf32.end());
	changed = true;*/
	this->utf8Copy.reserve(this->utf8Copy.size() + utf8String.utf8Copy.size());
	this->utf8Copy.insert(this->utf8Copy.end(), utf8String.utf8Copy.begin(), utf8String.utf8Copy.end());
}


template<typename T>
inline void UString::append2(const T& value)
{
	std::ostringstream oss;
	oss << value;
	append(oss.str());
}


///////////////////////////////////////////////////////////////////////////////////////////
// Static methods
///////////////////////////////////////////////////////////////////////////////////////////
template<typename T>
inline UString UString::from(const T& value)
{
	std::ostringstream oss;
	oss << value;
	return UString(oss.str());
}


///////////////////////////////////////////////////////////////////////////////////////////
// Changing operations
///////////////////////////////////////////////////////////////////////////////////////////
inline void UString::set(const char* utf8String)
{
	/*this->utf32.clear();
	size_t size = strlen(utf8String);
	utf8::utf8to32(utf8String, utf8String + size, back_inserter(this->utf32));
	// Update utf8Copy
	this->changed = true;
	updateUtf8Copy();*/
	size_t size = strlen(utf8String);
	this->utf8Copy = std::string(utf8String, size);
}


inline void UString::set(const char* utf8String, size_t size)
{
	/*this->utf32.clear();
	utf8::utf8to32(utf8String, utf8String + size, back_inserter(this->utf32));
	this->utf8Copy = std::string(utf8String, size);
	this->changed = false;*/
	this->utf8Copy = std::string(utf8String, size);
}


inline void UString::set(const std::string& utf8String)
{
	/*this->utf32.clear();
	utf8::utf8to32(utf8String.begin(), utf8String.end(), back_inserter(this->utf32));
	this->utf8Copy = utf8String;
	this->changed = false;*/
	this->utf8Copy = utf8String;
}


inline void UString::set(const std::wstring& utf8Wstring)
{
	/*this->utf32.clear();
	utf8::utf8to32(utf8Wstring.begin(), utf8Wstring.end(), back_inserter(this->utf32));
	this->utf8Copy = std::string(utf8Wstring.begin(), utf8Wstring.end());
	this->changed = false;*/

	utf8::utf16to8(utf8Wstring.begin(), utf8Wstring.end(), back_inserter(utf8Copy));
}


inline void UString::set(const UString& str)
{
	/*this->utf32 = str.utf32;
	if (str.changed == false) {
		this->utf8Copy = str.utf8Copy;
		this->changed = false;
	}
	else
		this->changed = true;*/
	this->utf8Copy = str.utf8Copy;
}


inline void UString::resize(size_t newSize, char character)
{
	this->utf8Copy.resize(newSize, character);
}


inline void UString::toUpperCase()
{
	const char* utf8CString = c_str();
	std::ptrdiff_t length = utf8::distance(utf8CString, utf8CString + strlen(utf8CString));

	std::string result;
	for (int i = 0; i < length; ++i) {
		uint32_t character = utf8::next(utf8CString, utf8CString + length);
		if (character >= 97 && character <= 122) {
			character -= 32; // ASCII to upper case conversion
		}
		else if (character == 228) { // ä
			character = 196; // Ä
		}
		else if (character == 246) { // ö
			character = 214; // Ö
		}
		else if (character == 229) { // å
			character = 197; // Å
		}
		utf8::append(character, std::back_inserter(result));
	}

	set(result);
}


inline void UString::toLowerCase()
{
	const char* utf8CString = c_str();
	std::ptrdiff_t length = utf8::distance(utf8CString, utf8CString + strlen(utf8CString));

	std::string result;
	for (int i = 0; i < length; ++i) {
		uint32_t character = utf8::next(utf8CString, utf8CString + length);
		if (character >= 65 && character <= 90) {
			character += 32; // ASCII to lower case conversion
		}
		else if (character == 196) { // Ä
			character = 228;
		}
		else if (character == 214) { // Ö
			character = 246;
		}
		else if (character == 197) { // Å
			character = 229;
		}
		utf8::append(character, std::back_inserter(result));
	}

	set(result);
}


///////////////////////////////////////////////////////////////////////////////////////////
// Static methods
///////////////////////////////////////////////////////////////////////////////////////////
inline std::uint32_t UString::utf8CharacterToUtf32Decimal(const char* str)
{
	return utf8::next(str, str + strlen(str));
}


///////////////////////////////////////////////////////////////////////////////////////////
// Private methods
///////////////////////////////////////////////////////////////////////////////////////////
/*inline void UString::updateUtf8Copy() const
{
	if (changed == true) {
		this->utf8Copy.clear();
		utf8::utf32to8(this->utf32.begin(), this->utf32.end(), back_inserter(this->utf8Copy));
		changed = false;
	}
}*/


} // End namespace Verso
