#pragma once

#include <Verso/System/Exception/AssertException.hpp>
#include <Verso/System/Exception/DeviceNotFoundException.hpp>
#include <Verso/System/Exception/ErrorException.hpp>
#include <Verso/System/Exception/ExceptionInterface.hpp>
#include <Verso/System/Exception/FileNotFoundException.hpp>
#include <Verso/System/Exception/IllegalFormatException.hpp>
#include <Verso/System/Exception/IllegalParametersException.hpp>
#include <Verso/System/Exception/IllegalStateException.hpp>
#include <Verso/System/Exception/IoErrorException.hpp>
#include <Verso/System/Exception/MemoryAllocationFailedException.hpp>
#include <Verso/System/Exception/ObjectNotFoundException.hpp>
