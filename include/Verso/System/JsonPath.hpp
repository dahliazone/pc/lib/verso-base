#pragma once

#include <Verso/System/UString.hpp>
#include <Verso/Math/Align.hpp>
#include <Verso/Math/AspectRatio.hpp>
#include <Verso/Math/CameraKeyframes.hpp>
#include <Verso/Math/EasingType.hpp>
#include <Verso/Math/FloatKeyframes.hpp>
#include <Verso/Math/InterpolationType.hpp>
#include <Verso/Math/IntKeyframes.hpp>
#include <Verso/Math/PixelAspectRatio.hpp>
#include <Verso/Math/Range.hpp>
#include <Verso/Math/RefScale.hpp>
#include <Verso/Math/RgbaColorf.hpp>
#include <Verso/Math/RgbaColorfKeyframes.hpp>
#include <Verso/Math/Vector2Keyframes.hpp>
#include <Verso/Math/Vector3.hpp>
#include <Verso/Math/Vector3Keyframes.hpp>

#include <JSON.h>

namespace Verso {


VERSO_BASE_API extern JSONObject emptyJsonObject;
VERSO_BASE_API extern JSONArray emptyJsonArray;


struct JsonPathArray;


struct JsonPath
{
public:
	JSONValue* value;
	UString currentPath;
	UString fileName;

public:
	VERSO_BASE_API JsonPath();
	VERSO_BASE_API JsonPath(JSONValue* value, const UString& currentPath, const UString& fileName);
	VERSO_BASE_API JsonPath(const JsonPath& original);
	VERSO_BASE_API JsonPath(const JsonPath&& original) noexcept;
	VERSO_BASE_API JsonPath& operator =(const JsonPath& original);
	VERSO_BASE_API JsonPath& operator =(JsonPath&& original) noexcept;
	VERSO_BASE_API ~JsonPath();

public:
	VERSO_BASE_API bool isNullOrEmpty() const;
	VERSO_BASE_API JsonPath getAttribute(const UString& name, bool required) const;

	VERSO_BASE_API const JSONObject& asObject(const UString& name = "") const;
	VERSO_BASE_API JsonPathArray asArray(const UString& name = "", int minItemCount = -1, int maxItemCount = -1) const;

	VERSO_BASE_API JSONValue* readValue(const UString& name) const;

	VERSO_BASE_API bool isDefined(const UString& name = "") const;

	VERSO_BASE_API bool isNull(const UString& name = "") const;

	VERSO_BASE_API bool isObject(const UString& name = "") const;
	VERSO_BASE_API const JSONObject& readObject(const UString& name, bool required = false) const;
	VERSO_BASE_API static JSONValue* writeObject(JSONObject& jsonObject);

	VERSO_BASE_API bool isArray(const UString& name = "") const;
	VERSO_BASE_API const JSONArray& readArray(const UString& name, bool required = false) const;
	VERSO_BASE_API static JSONValue* writeArray(JSONArray& jsonArray);

	VERSO_BASE_API bool isBool(const UString& name = "") const;
	VERSO_BASE_API bool readBool(const UString& name, bool required = false, bool defaultValue = false) const;
	VERSO_BASE_API static JSONValue* writeBool(bool value);

	VERSO_BASE_API bool isInt(const UString& name = "", bool checkForDecimalZero = true) const;
	VERSO_BASE_API int readInt(const UString& name, bool required = false, int defaultValue = 0) const;
	VERSO_BASE_API static JSONValue* writeInt(int value);

	VERSO_BASE_API bool isNumber(const UString& name = "") const;

	VERSO_BASE_API double readNumberd(const UString& name, bool required = false, double defaultValue = 0.0) const;
	VERSO_BASE_API static JSONValue* writeNumberd(double value);

	VERSO_BASE_API float readNumberf(const UString& name, bool required = false, float defaultValue = 0.0f) const;
	VERSO_BASE_API static JSONValue* writeNumberf(float value);

	VERSO_BASE_API std::vector<float> readNumberfArray(const UString& name, bool required = false, const std::vector<float>& defaultValue = std::vector<float>()) const;

	VERSO_BASE_API int readNumberi(const UString& name, bool required = false, int defaultValue = 0) const;
	VERSO_BASE_API static JSONValue* writeNumberi(int value);

	VERSO_BASE_API unsigned int readNumberu(const UString& name, bool required = false, unsigned int defaultValue = 0) const;
	VERSO_BASE_API static JSONValue* writeNumberu(unsigned int value);

	VERSO_BASE_API size_t readNumbersz(const UString& name, bool required = false, size_t defaultValue = 0) const;
	VERSO_BASE_API static JSONValue* writeNumbersz(size_t value);

	VERSO_BASE_API bool isString(const UString& name = "") const;
	VERSO_BASE_API UString readString(const UString& name, bool required = false, const UString& defaultValue = "") const;
	VERSO_BASE_API static JSONValue* writeString(const UString& str);
	VERSO_BASE_API UString readStringOrArrayOfStrings(const UString& name, bool required = false, const UString& defaultValue = "") const;

	VERSO_BASE_API Align readAlign(const UString& name, bool required = false, const Align& defaultValue = Align()) const;

	VERSO_BASE_API RefScale readRefScale(const UString& name, bool required = false, const RefScale& defaultValue = RefScale::ViewportSize_KeepAspectRatio_FitRect) const;

	VERSO_BASE_API bool isColor(const UString& name = "") const;
	VERSO_BASE_API RgbaColorf readColor(const UString& name, bool required = false, const RgbaColorf& defaultValue = RgbaColorf(0.0f, 0.0f, 0.0f, 1.0f)) const;
	VERSO_BASE_API static JSONValue* writeColor(const RgbaColorf& color, bool useAlpha);

	VERSO_BASE_API Rangei readRangei(const UString& name, bool required = false, const Rangei& defaultValue = Rangei()) const;
	VERSO_BASE_API static JSONValue* writeRangei(const Rangei& range);

	VERSO_BASE_API Rangeu readRangeu(const UString& name, bool required = false, const Rangeu& defaultValue = Rangeu()) const;
	VERSO_BASE_API static JSONValue* writeRangeu(const Rangeu& range);

	VERSO_BASE_API Rangef readRangef(const UString& name, bool required = false, const Rangef& defaultValue = Rangef()) const;
	VERSO_BASE_API static JSONValue* writeRangef(const Rangef& range);

	VERSO_BASE_API Ranged readRanged(const UString& name, bool required = false, const Ranged& defaultValue = Ranged()) const;
	VERSO_BASE_API static JSONValue* writeRanged(const Ranged& range);

	VERSO_BASE_API Rectf readRectf(const UString& name, bool required = false, const Rectf& defaultValue = Rectf()) const;

	VERSO_BASE_API Vector2f readVector2f(const UString& name, bool required = false, const Vector2f& defaultValue = Vector2f()) const;

	VERSO_BASE_API bool isVector2fArrayFormat(const UString& name) const;
	VERSO_BASE_API Vector2f readVector2fArrayFormat(const UString& name, bool required = false, const Vector2f& defaultValue = Vector2f()) const;

	VERSO_BASE_API Vector2i readVector2iArrayFormat(const UString& name, bool required = false, const Vector2i& defaultValue = Vector2i()) const;

	VERSO_BASE_API Vector2i readVector2i(const UString& name, bool required = false, const Vector2i& defaultValue = Vector2i()) const;

	VERSO_BASE_API Vector3f readVector3f(const UString& name, bool required = false, const Vector3f& defaultValue = Vector3f()) const;

	VERSO_BASE_API bool isVector3fArrayFormat(const UString& name) const;
	VERSO_BASE_API Vector3f readVector3fArrayFormat(const UString& name, bool required = false, const Vector3f& defaultValue = Vector3f()) const;

	VERSO_BASE_API AspectRatio readAspectRatio(const UString& name, bool required = false, const AspectRatio& defaultValue = AspectRatio()) const;

	VERSO_BASE_API PixelAspectRatio readPixelAspectRatio(const UString& name, bool required = false, const PixelAspectRatio& = PixelAspectRatio()) const;

	VERSO_BASE_API EasingType readEasingType(const UString& name, bool required = false, const EasingType& defaultValue = EasingType::Linear) const;

	VERSO_BASE_API InterpolationType readInterpolationType(const UString& name, bool required = false, const InterpolationType& defaultValue = InterpolationType::Linear) const;

	VERSO_BASE_API IntKeyframes readIntKeyframes(const UString& name, bool required = false, const IntKeyframes& defaultValue = IntKeyframes()) const;

	VERSO_BASE_API FloatKeyframes readFloatKeyframes(const UString& name, bool required = false, const FloatKeyframes& defaultValue = FloatKeyframes()) const;

	VERSO_BASE_API Vector2fKeyframes readVector2fKeyframes(const UString& name, bool required = false, const Vector2fKeyframes& defaultValue = Vector2fKeyframes()) const;

	VERSO_BASE_API Vector3fKeyframes readVector3fKeyframes(const UString& name, bool required = false, const Vector3fKeyframes& defaultValue = Vector3fKeyframes()) const;

	VERSO_BASE_API RgbaColorfKeyframes readRgbaColorfKeyframes(const UString& name, bool required = false, const RgbaColorfKeyframes& defaultValue = RgbaColorfKeyframes()) const;

	VERSO_BASE_API CameraKeyframes readCameraKeyframes(const UString& name, bool required = false, const CameraKeyframes& defaultValue = CameraKeyframes()) const;

	VERSO_BASE_API static UString jsonValueToString(JSONValue* value, const UString& newLinePadding);

	// toString
	VERSO_BASE_API UString toString(const UString& newLinePadding) const;

	VERSO_BASE_API UString toStringDebug(const UString& newLinePadding) const;

	friend std::ostream& operator <<(std::ostream& ost, const JsonPath& right)
	{
		return ost << right.toString("");
	}
};


struct JsonPathArray
{
public:
	JsonPath path;
	std::vector<JsonPath> array;

public:
	VERSO_BASE_API JsonPathArray();
	VERSO_BASE_API JsonPathArray(const JsonPath& path, const std::vector<JsonPath>& array);
	VERSO_BASE_API JsonPathArray(const JsonPathArray& original);
	VERSO_BASE_API JsonPathArray(const JsonPathArray&& original) noexcept;
	VERSO_BASE_API JsonPathArray& operator =(const JsonPathArray& original);
	VERSO_BASE_API JsonPathArray& operator =(JsonPathArray&& original) noexcept;
	VERSO_BASE_API ~JsonPathArray();

public:
	// toString
	VERSO_BASE_API UString toString(const UString& newLinePadding) const;

	VERSO_BASE_API UString toStringDebug(const UString& newLinePadding) const;

	friend std::ostream& operator <<(std::ostream& ost, const JsonPathArray& right)
	{
		return ost << right.toString("");
	}

};


} // End namespace Verso
