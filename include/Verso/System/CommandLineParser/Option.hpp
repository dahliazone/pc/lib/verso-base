#pragma once

#include <Verso/System/UString.hpp>
#include <Verso/System/Exception/IllegalParametersException.hpp>

namespace Verso {


class Option
{
public:
	UString opt;
	UString longOpt;
	UString description;
	bool required;
	bool hasArgs;
	std::vector<UString> argNames;
	std::vector<UString> argDefaultValues;
	std::vector<UString> argValues;

public:
	VERSO_BASE_API Option();
	VERSO_BASE_API Option(const UString& opt, const UString& longOpt, const UString& description);
	VERSO_BASE_API void setRequired(bool required);
	VERSO_BASE_API void setArgNames(const UString& argName1);
	VERSO_BASE_API void setArgNames(const UString& argName1, const UString& argName2);
	VERSO_BASE_API void setArgNames(const UString& argName1, const UString& argName2, const UString& argName3);
	VERSO_BASE_API void setArgNames(const std::vector<UString>& argumentNames);
	VERSO_BASE_API void setArgDefaultValues(const UString& defaultValue1);
	VERSO_BASE_API void setArgDefaultValues(const UString& defaultValue1, const UString& defaultValue2);
	VERSO_BASE_API void setArgDefaultValues(const UString& defaultValue1, const UString& defaultValue2, const UString& defaultValue3);
	VERSO_BASE_API void setArgDefaultValues(const std::vector<UString>& defaultValues);
	VERSO_BASE_API void setArgValues(const UString& argValue1);
	VERSO_BASE_API void setArgValues(const UString& argValue1, const UString& argValue2);
	VERSO_BASE_API void setArgValues(const UString& argValue1, const UString& argValue2, const UString& argValue3);
	VERSO_BASE_API void setArgValues(const std::vector<UString>& argValues);
	VERSO_BASE_API bool match(const UString& opt) const;

private:
	VERSO_BASE_API void setArgAmount(int amount);
};


} // End namespace Verso
