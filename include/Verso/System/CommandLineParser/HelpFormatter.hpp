#pragma once

#include <Verso/System/CommandLineParser/Options.hpp>

namespace Verso {


class HelpFormatter
{
public:
	VERSO_BASE_API void printHelp(const UString& binaryName, const Options& options) const;
	VERSO_BASE_API UString generateString(const UString& binaryName, const Options& options) const;

private:
	UString generateOptionString(const Option& option) const;
};


} // End namespace Verso
