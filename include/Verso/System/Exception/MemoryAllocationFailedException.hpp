#pragma once

#include <Verso/System/Exception/ExceptionInterface.hpp>
#include <Verso/System/current_function.hpp>
#include <sstream>

namespace Verso {


class MemoryAllocationFailedException : public ExceptionInterface
{
private:
	std::string component;
	std::string message;
	std::string function;
	std::string fileName;
	long line;
	std::string resources;
	std::string output;

public:
	MemoryAllocationFailedException(
			const std::string& component,
			const std::string& message,
			const std::string& function, const std::string& fileName, long line,
			const std::string& resources = "") VERSO_NOEXCEPT;
	virtual ~MemoryAllocationFailedException() VERSO_NOEXCEPT override = default;

public: // Interface Verso::ExceptionInterface
	virtual const std::string& getType() const override;
	virtual const std::string& getComponent() const override;
	virtual const std::string& getMessage() const override;
	virtual const std::string& getFunction() const override;
	virtual const std::string& getFileName() const override;
	virtual long getLine() const override;
	virtual const std::string& getResources() const override;
	virtual const std::string& toString() const override;

public: // Interface std::runtime_error
	virtual const char* what() const VERSO_NOEXCEPT override;
};


#define VERSO_MEMORYALLOCATIONFAILED(component, message, resources) (throw MemoryAllocationFailedException(component, message, VERSO_FUNCTION, VERSO_FILE, VERSO_LINE, resources))


inline MemoryAllocationFailedException::MemoryAllocationFailedException(
		const std::string& component,
		const std::string& message,
		const std::string& function, const std::string& fileName, long line,
		const std::string& resources) VERSO_NOEXCEPT :
	ExceptionInterface(),
	component(component),
	message(message),
	function(function),
	fileName(fileName),
	line(line),
	resources(resources),
	output()
{
	std::ostringstream oss;
	oss << "  Type: " << getType() << std::endl
		<< "  Component: " << component << std::endl
		<< "  Origin: " << function << std::endl
		<< "          at \"" << fileName << "\":" << line;

	if (message.size() != 0) {
		oss << std::endl << "  Message: " << message;
	}

	if (resources.size() != 0) {
		oss << std::endl << "  Resources: " << resources;
	}

	output = oss.str();
}


///////////////////////////////////////////////////////////////////////////////////////////
// Interface Verso::ExceptionInterface
///////////////////////////////////////////////////////////////////////////////////////////

inline const std::string& MemoryAllocationFailedException::getType() const
{
	static std::string type("MemoryAllocationFailedException");
	return type;
}


inline const std::string& MemoryAllocationFailedException::getComponent() const
{
	return component;
}


inline const std::string& MemoryAllocationFailedException::getMessage() const
{
	return message;
}


inline const std::string& MemoryAllocationFailedException::getFunction() const
{
	return function;
}


inline const std::string& MemoryAllocationFailedException::getFileName() const
{
	return fileName;
}


inline long MemoryAllocationFailedException::getLine() const
{
	return line;
}


inline const std::string& MemoryAllocationFailedException::getResources() const
{
	return resources;
}


inline const std::string& MemoryAllocationFailedException::toString() const
{
	return output;
}


///////////////////////////////////////////////////////////////////////////////////////////
// Interface std::runtime_error
///////////////////////////////////////////////////////////////////////////////////////////

inline const char* MemoryAllocationFailedException::what() const VERSO_NOEXCEPT
{
	return toString().c_str();
}


} // End namespace Verso
