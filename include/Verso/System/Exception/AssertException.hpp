#pragma once

#include <Verso/System/Exception/ExceptionInterface.hpp>
#include <sstream>

namespace Verso {


class AssertException : public ExceptionInterface
{
private:
	std::string component;
	std::string message;
	std::string function;
	std::string fileName;
	long line;
	std::string resources;
	std::string output;
	std::string expression;

public:
	AssertException(
			const std::string& component,
			const std::string& message,
			const std::string& function, const std::string& fileName, long line,
			const std::string& resources,
			const std::string& expression) VERSO_NOEXCEPT;
	virtual ~AssertException() VERSO_NOEXCEPT override = default;

public: // Interface AssertException
	virtual const std::string& getExpression() const;

public: // Interface Verso::ExceptionInterface
	virtual const std::string& getType() const override;
	virtual const std::string& getComponent() const override;
	virtual const std::string& getMessage() const override;
	virtual const std::string& getFunction() const override;
	virtual const std::string& getFileName() const override;
	virtual long getLine() const override;
	virtual const std::string& getResources() const override;
	virtual const std::string& toString() const override;

public: // Interface std::runtime_error
	virtual const char* what() const VERSO_NOEXCEPT override;
};


inline AssertException::AssertException(
		const std::string& component,
		const std::string& message,
		const std::string& function, const std::string& fileName, long line,
		const std::string& resources,
		const std::string& expression) VERSO_NOEXCEPT :
	ExceptionInterface(),
	component(component),
	message(message),
	function(function),
	fileName(fileName),
	line(line),
	resources(resources),
	output(),
	expression(expression)
{
	std::ostringstream oss;
	oss << "  Type: " << getType() << std::endl
		<< "  Component: " << component << std::endl
		<< "  Origin: " << function << std::endl
		<< "          at \"" << fileName << "\":" << line;

	if (expression.size() != 0) {
		oss << std::endl << "  Expression: " << expression;
	}

	if (message.size() != 0) {
		oss << std::endl << "  Message: " << message;
	}

	if (resources.size() != 0) {
		oss << std::endl << "  Resources: " << resources;
	}

	output = oss.str();
}


///////////////////////////////////////////////////////////////////////////////////////////
// Interface AssertException
///////////////////////////////////////////////////////////////////////////////////////////

inline const std::string& AssertException::getExpression() const
{
	return expression;
}


///////////////////////////////////////////////////////////////////////////////////////////
// Interface Verso::ExceptionInterface
///////////////////////////////////////////////////////////////////////////////////////////

inline const std::string& AssertException::getType() const
{
	static std::string type("AssertException");
	return type;
}


inline const std::string& AssertException::getComponent() const
{
	return component;
}


inline const std::string& AssertException::getMessage() const
{
	return message;
}


inline const std::string& AssertException::getFunction() const
{
	return function;
}


inline const std::string& AssertException::getFileName() const
{
	return fileName;
}


inline long AssertException::getLine() const
{
	return line;
}


inline const std::string& AssertException::getResources() const
{
	return resources;
}


inline const std::string& AssertException::toString() const
{
	return output;
}


///////////////////////////////////////////////////////////////////////////////////////////
// Interface std::runtime_error
///////////////////////////////////////////////////////////////////////////////////////////

inline const char* AssertException::what() const VERSO_NOEXCEPT
{
	return toString().c_str();
}


} // End namespace Verso
