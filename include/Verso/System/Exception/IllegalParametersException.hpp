#pragma once

#include <Verso/System/Exception/ExceptionInterface.hpp>
#include <Verso/System/current_function.hpp>
#include <sstream>

namespace Verso {


class IllegalParametersException : public ExceptionInterface
{
private:
	std::string component;
	std::string message;
	std::string function;
	std::string fileName;
	long line;
	std::string resources;
	std::string output;

public:
	IllegalParametersException(
			const std::string& component,
			const std::string& message,
			const std::string& function, const std::string& fileName, long line,
			const std::string& resources = "") VERSO_NOEXCEPT;
	virtual ~IllegalParametersException() VERSO_NOEXCEPT override = default;

public: // Interface Verso::ExceptionInterface
	virtual const std::string& getType() const override;
	virtual const std::string& getComponent() const override;
	virtual const std::string& getMessage() const override;
	virtual const std::string& getFunction() const override;
	virtual const std::string& getFileName() const override;
	virtual long getLine() const override;
	virtual const std::string& getResources() const override;
	virtual const std::string& toString() const override;

public: // Interface std::runtime_error
	virtual const char* what() const VERSO_NOEXCEPT override;
};


#define VERSO_ILLEGALPARAMETERS(component, message, resources) (throw IllegalParametersException(component, message, VERSO_FUNCTION, VERSO_FILE, VERSO_LINE, resources))


inline IllegalParametersException::IllegalParametersException(
		const std::string& component,
		const std::string& message,
		const std::string& function, const std::string& fileName, long line,
		const std::string& resources) VERSO_NOEXCEPT :
	ExceptionInterface(),
	component(component),
	message(message),
	function(function),
	fileName(fileName),
	line(line),
	resources(resources),
	output()
{
	std::ostringstream oss;
	oss << "  Type: " << getType() << std::endl
		<< "  Component: " << component << std::endl
		<< "  Origin: " << function << std::endl
		<< "          at \"" << fileName << "\":" << line;

	if (message.size() != 0) {
		oss << std::endl << "  Message: " << message;
	}

	if (resources.size() != 0) {
		oss << std::endl << "  Resources: " << resources;
	}

	output = oss.str();
}


///////////////////////////////////////////////////////////////////////////////////////////
// Interface Verso::ExceptionInterface
///////////////////////////////////////////////////////////////////////////////////////////

inline const std::string& IllegalParametersException::getType() const
{
	static std::string type("IllegalParametersException");
	return type;
}


inline const std::string& IllegalParametersException::getComponent() const
{
	return component;
}


inline const std::string& IllegalParametersException::getMessage() const
{
	return message;
}


inline const std::string& IllegalParametersException::getFunction() const
{
	return function;
}


inline const std::string& IllegalParametersException::getFileName() const
{
	return fileName;
}


inline long IllegalParametersException::getLine() const
{
	return line;
}


inline const std::string& IllegalParametersException::getResources() const
{
	return resources;
}


inline const std::string& IllegalParametersException::toString() const
{
	return output;
}


///////////////////////////////////////////////////////////////////////////////////////////
// Interface std::runtime_error
///////////////////////////////////////////////////////////////////////////////////////////

inline const char* IllegalParametersException::what() const VERSO_NOEXCEPT
{
	return toString().c_str();
}


} // End namespace Verso
