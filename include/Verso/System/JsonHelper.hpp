#pragma once

#include <Verso/System/File.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>
#include <Verso/System/JsonPath.hpp>

namespace Verso {


namespace JsonHelper {




VERSO_BASE_API JsonPath loadAndParse(const UString& fileName);
VERSO_BASE_API void saveToFile(const UString& fileName, JSONValue* root, bool prettyPrint = true);
VERSO_BASE_API void free(const JsonPath& rootPath);



} // End namespace JsonHelper


} // End namespace Verso
