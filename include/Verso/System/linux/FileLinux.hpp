#pragma once

#if __linux__

#include <Verso/System/UString.hpp>
#include <vector>

namespace Verso {
namespace priv {


class FileLinux
{
public:
	static UString readSymLink(const UString& symLink);
	static UString getBasePath();
	static UString getPreferencesPath();
	static UString getFileNamePath(const UString& fileName);
	static bool mkdir(const UString& fileName);
	static void ls(const UString& directoryName, bool showFiles, bool showDirectories, bool showDotFiles, std::vector<UString>& out);
	static int64_t getSize(const char* fileName);

	inline static UString getDirectorySeparator() {
		return "/";
	}

	inline static char getDirectorySeparatorChar() {
		return '/';
	}
};


} // End namespace priv
} // End namespace Verso

#endif // End #ifdef __linux__
