#pragma once

namespace Verso {


inline void helperCurrentFunction()
{
#if defined(__GNUC__)
	#define VERSO_FUNCTION __PRETTY_FUNCTION__

#elif (defined(__INTEL_COMPILER) && (__INTEL_COMPILER >= 600)) || (defined(__IBMCPP__) && (__IBMCPP__ >= 500))
	#define VERSO_FUNCTION __FUNCTION__

#elif defined(__FUNCSIG__)
	#define VERSO_FUNCTION __FUNCSIG__

#elif defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901)
	#define VERSO_FUNCTION __func__

#else
	#define VERSO_FUNCTION "(unknown)"
#endif
}


#define VERSO_FILE (__FILE__)

#define VERSO_LINE (__LINE__)


} // End namespace Verso
