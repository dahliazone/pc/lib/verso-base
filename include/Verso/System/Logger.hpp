#pragma once

#include <Verso/System/current_function.hpp>
#include <Verso/Time/Datetime.hpp>
#include <chrono>
#include <iostream>
#include <fstream>
#ifdef _WIN32
#include <sstream>
#include <Windows.h>
#endif


namespace Verso {


class Logger
{
private:
	std::chrono::high_resolution_clock::time_point start;
	std::chrono::high_resolution_clock::time_point last;
	std::ostream* out;
	std::fstream fsOut;

private:
	Logger() :
		start(std::chrono::high_resolution_clock::now()),
		last(start),
		out(&std::cout)
	{
		std::cout << Datetime::getNow()<<" Logging started" << std::endl;
	}

public:
	static Logger& instance()
	{
		static Logger logger;
		return logger;
	}


	~Logger()
	{
		out->flush();
		if (fsOut.is_open() == true) {
			fsOut.close();
		}
		out = nullptr;
		std::cout << Datetime::getNow()<<" Logging ended" << std::endl;
	}


	void useCout()
	{
		out->flush();
		if (fsOut.is_open() == true) {
			fsOut.close();
		}

		out = &std::cout;
	}


	void useFile(const char* fileName)
	{
		out->flush();
		if (fsOut.is_open() == true) {
			fsOut.close();
		}

		fsOut.open(fileName, std::fstream::out | std::fstream::app);
		out = &fsOut;
	}


	void flush()
	{
		out->flush();
	}


	std::ostream& getOut() const
	{
		VERSO_ASSERT_MSG("verso-base", out != nullptr, "Internal variable 'out' was null.");
		return *out;
	}


	int getMsSinceLastCall()
	{
		auto now = std::chrono::high_resolution_clock::now();
		auto deltaMs = std::chrono::duration_cast<std::chrono::milliseconds>(now - last);
		last = now;
		return static_cast<int>(deltaMs.count());
	}
};


// Helpers
#if !defined _WIN32
	#define VERSO_HELPER_LOG_GENERIC(level, component, msg) \
		(::Verso::Logger::instance().getOut() << level << " [" << component << "] " << msg << \
			" [" << ::Verso::Logger::instance().getMsSinceLastCall() << " ms]" << std::endl)

	#define VERSO_HELPER_LOG_GENERIC_WITH_DEBUGINFO(level, component, msg, function, fileName, line) \
		(::Verso::Logger::instance().getOut() << level << " [" << component << "] " << msg << \
			" [" << ::Verso::Logger::instance().getMsSinceLastCall() << " ms]" << std::endl << \
			"  in " << function << " at \"" << fileName << "\":" << line << std::endl)

	#define VERSO_HELPER_LOG_EXCEPTION(component, exceptionType, exceptionToString) \
		(::Verso::Logger::instance().getOut() << "ERROR" << " [" << component << "] " << \
			"Catched exception \"" << exceptionType << "\"" << \
			" [" << ::Verso::Logger::instance().getMsSinceLastCall() << " ms]" << std::endl << \
			"  in " << VERSO_FUNCTION << " at \"" << VERSO_FILE << "\":" << VERSO_LINE << std::endl << \
			exceptionToString << std::endl)

	#define VERSO_HELPER_LOG_DIRECT(msg) \
		(::Verso::Logger::instance().getOut() << msg << std::endl)

	#define VERSO_HELPER_LOG_NEWLINE() \
		(::Verso::Logger::instance().getOut() << std::endl)

	#define VERSO_HELPER_LOG_VARIABLE(level, component, name, value) \
		(::Verso::Logger::instance().getOut() << "  " << name << ": " << value << std::endl)
#else
	#define VERSO_HELPER_LOG_GENERIC(level, component, msg) \
		if (true) { \
			std::stringstream ss; \
			ss << level << " [" << component << "] " << msg << \
				" [" << ::Verso::Logger::instance().getMsSinceLastCall() << " ms]" << std::endl; \
			std::string str = ss.str(); \
			OutputDebugStringW(std::wstring(str.begin(), str.end()).c_str()); \
			::Verso::Logger::instance().getOut() << ss.str(); \
		} else \
			(void)0

	#define VERSO_HELPER_LOG_GENERIC_WITH_DEBUGINFO(level, component, msg, function, fileName, line) \
		if (true) { \
			std::stringstream ss; \
			ss << level << " [" << component << "] " << msg << \
				" [" << ::Verso::Logger::instance().getMsSinceLastCall() << " ms]" << std::endl << \
				"  in " << function << " at \"" << fileName << "\":" << line << std::endl; \
			std::string str = ss.str(); \
			OutputDebugStringW(std::wstring(str.begin(), str.end()).c_str()); \
			::Verso::Logger::instance().getOut() << ss.str(); \
		} else \
			(void)0

	#define VERSO_HELPER_LOG_EXCEPTION(component, exceptionType, exceptionToString) \
		if (true) { \
			std::stringstream ss; \
			ss << "ERROR" << " [" << component << "] " << \
				"Catched exception \"" << exceptionType << "\"" << \
				" [" << ::Verso::Logger::instance().getMsSinceLastCall() << " ms]" << std::endl << \
				"  in " << VERSO_FUNCTION << " at \"" << VERSO_FILE << "\":" << VERSO_LINE << std::endl << \
				exceptionToString << std::endl; \
			std::string str = ss.str(); \
			OutputDebugStringW(std::wstring(str.begin(), str.end()).c_str()); \
			::Verso::Logger::instance().getOut() << ss.str(); \
		} else \
			(void)0

	#define VERSO_HELPER_LOG_DIRECT(msg) \
		if (true) { \
			std::stringstream ss; \
			ss << msg << std::endl; \
			std::string str = ss.str(); \
			OutputDebugStringW(std::wstring(str.begin(), str.end()).c_str()); \
			::Verso::Logger::instance().getOut() << ss.str(); \
		} else \
			(void)0

	#define VERSO_HELPER_LOG_NEWLINE() \
		if (true) { \
			OutputDebugStringW(L"\n"); \
			::Verso::Logger::instance().getOut() << std::endl; \
		} else \
			(void)0

	#define VERSO_HELPER_LOG_VARIABLE(level, component, name, value) \
		if (true) { \
			std::stringstream ss; \
			ss << "  " << name << ": " << value << std::endl; \
			std::string str = ss.str(); \
			OutputDebugStringW(std::wstring(str.begin(), str.end()).c_str()); \
			::Verso::Logger::instance().getOut() << ss.str(); \
		} else \
			(void)0
#endif

#define VERSO_HELPER_LOG_OMIT() ((void)0)

// Logging macros
#define VERSO_LOG_FLUSH() \
	(::Verso::Logger::instance().flush())

#define VERSO_LOG_ERR(component, msg) \
	VERSO_HELPER_LOG_GENERIC_WITH_DEBUGINFO("ERROR", component, msg, VERSO_FUNCTION, VERSO_FILE, VERSO_LINE)
#define VERSO_LOG_INFO(component, msg) \
	VERSO_HELPER_LOG_GENERIC("INFO", component, msg)

#define VERSO_LOG_ERR_VARIABLE(component, name, value) \
	VERSO_HELPER_LOG_VARIABLE("ERROR", component, name, value)
#define VERSO_LOG_INFO_VARIABLE(component, name, value) \
	VERSO_HELPER_LOG_VARIABLE("INFO", component, name, value)

#define VERSO_LOG_ERR_INDIRECT(component, msg, function, fileName, line) \
	VERSO_HELPER_LOG_GENERIC_WITH_DEBUGINFO("ERROR", component, msg, VERSO_FUNCTION, VERSO_FILE, VERSO_LINE)

#define VERSO_LOG_ERR_VERSOEXCEPTION(component, versoException) \
	VERSO_HELPER_LOG_EXCEPTION(component, versoException.getType(), versoException.toString())
#define VERSO_LOG_ERR_EXCEPTION(component, exceptionType, exceptionWhat) \
	VERSO_HELPER_LOG_EXCEPTION(component, exceptionType, "  Message: "<<exceptionWhat)
#define VERSO_LOG_ERR_UNKNOWNEXCEPTION(component) \
	VERSO_HELPER_LOG_EXCEPTION(component, "unknown exception", "  Message: check debugger for more information")

#define VERSO_LOG_ERR_DIRECT(component, msg) \
	VERSO_HELPER_LOG_DIRECT(msg)
#define VERSO_LOG_INFO_DIRECT(component, msg) \
	VERSO_HELPER_LOG_DIRECT(msg)

#define VERSO_LOG_ERR_EMPTY_ROW() \
	VERSO_HELPER_LOG_NEWLINE()
#define VERSO_LOG_INFO_EMPTY_ROW() \
	VERSO_HELPER_LOG_NEWLINE()

#ifdef NDEBUG
	#define VERSO_LOG_WARN(component, msg) VERSO_HELPER_LOG_OMIT()
	#define VERSO_LOG_DEBUG(component, msg) VERSO_HELPER_LOG_OMIT()
	#define VERSO_LOG_TRACE(component, msg) VERSO_HELPER_LOG_OMIT()

	#define VERSO_LOG_WARN_VARIABLE(component, name, value) VERSO_HELPER_LOG_OMIT()
	#define VERSO_LOG_DEBUG_VARIABLE(component, name, value) VERSO_HELPER_LOG_OMIT()
	#define VERSO_LOG_TRACE_VARIABLE(component, name, value) VERSO_HELPER_LOG_OMIT()

	#define VERSO_LOG_WARN_INDIRECT(component, msg, function, fileName, line) VERSO_HELPER_LOG_OMIT()
	#define VERSO_LOG_DEBUG_INDIRECT(component, msg, function, fileName, line) VERSO_HELPER_LOG_OMIT()
	#define VERSO_LOG_TRACE_INDIRECT(component, msg, function, fileName, line) VERSO_HELPER_LOG_OMIT()

	#define VERSO_LOG_WARN_DIRECT(component, msg) VERSO_HELPER_LOG_OMIT()
	#define VERSO_LOG_DEBUG_DIRECT(component, msg) VERSO_HELPER_LOG_OMIT()
	#define VERSO_LOG_TRACE_DIRECT(component, msg) VERSO_HELPER_LOG_OMIT()

	#define VERSO_LOG_WARN_EMPTY_ROW() VERSO_HELPER_LOG_OMIT()
	#define VERSO_LOG_DEBUG_EMPTY_ROW() VERSO_HELPER_LOG_OMIT()
	#define VERSO_LOG_TRACE_EMPTY_ROW() VERSO_HELPER_LOG_OMIT()
#else
	#define VERSO_LOG_WARN(component, msg) \
		VERSO_HELPER_LOG_GENERIC_WITH_DEBUGINFO("WARN", component, msg, VERSO_FUNCTION, VERSO_FILE, VERSO_LINE)
	#define VERSO_LOG_DEBUG(component, msg) \
		VERSO_HELPER_LOG_GENERIC_WITH_DEBUGINFO("DEBUG", component, msg, VERSO_FUNCTION, VERSO_FILE, VERSO_LINE)
	#define VERSO_LOG_TRACE(component, msg) \
		VERSO_HELPER_LOG_GENERIC_WITH_DEBUGINFO("TRACE", component, msg, VERSO_FUNCTION, VERSO_FILE, VERSO_LINE)

	#define VERSO_LOG_WARN_VARIABLE(component, name, value) \
		VERSO_HELPER_LOG_VARIABLE("WARN", component, name, value)
	#define VERSO_LOG_DEBUG_VARIABLE(component, name, value) \
		VERSO_HELPER_LOG_VARIABLE("DEBUG", component, name, value)
	#define VERSO_LOG_TRACE_VARIABLE(component, name, value) \
		VERSO_HELPER_LOG_VARIABLE("TRACE", component, name, value)

	#define VERSO_LOG_WARN_INDIRECT(component, msg, function, fileName, line) \
		VERSO_HELPER_LOG_GENERIC_WITH_DEBUGINFO("ERROR", component, msg, function, fileName, line)
	#define VERSO_LOG_DEBUG_INDIRECT(component, msg, function, fileName, line) \
		VERSO_HELPER_LOG_GENERIC_WITH_DEBUGINFO("ERROR", component, msg, function, fileName, line)
	#define VERSO_LOG_TRACE_INDIRECT(component, msg, function, fileName, line) \
		VERSO_HELPER_LOG_GENERIC_WITH_DEBUGINFO("ERROR", component, msg, function, fileName, line)

	#define VERSO_LOG_WARN_DIRECT(component, msg) \
		VERSO_HELPER_LOG_DIRECT(msg)
	#define VERSO_LOG_DEBUG_DIRECT(component, msg) \
		VERSO_HELPER_LOG_DIRECT(msg)
	#define VERSO_LOG_TRACE_DIRECT(component, msg) \
		VERSO_HELPER_LOG_DIRECT(msg)

	#define VERSO_LOG_WARN_EMPTY_ROW() \
		VERSO_HELPER_LOG_NEWLINE()
	#define VERSO_LOG_DEBUG_EMPTY_ROW() \
		VERSO_HELPER_LOG_NEWLINE()
	#define VERSO_LOG_TRACE_EMPTY_ROW() \
		VERSO_HELPER_LOG_NEWLINE()
#endif


} // End namespace Verso
