#pragma once

#include <Verso/System/UString.hpp>

namespace Verso {


VERSO_BASE_API void logErrStacktrace(bool printTitle = false);
VERSO_BASE_API UString getStacktrace(bool printTitle = false);


} // End namespace Verso
