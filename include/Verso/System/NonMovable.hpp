#pragma once

#include <Verso/verso-base-common.hpp>

namespace Verso {


class NonMovable
{
public:
	NonMovable(NonMovable&&) = delete;

	NonMovable& operator=(NonMovable&&) = delete;

protected:
	NonMovable() = default;
};


} // End namespace Verso
