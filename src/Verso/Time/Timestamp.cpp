#include <Verso/Time/Timestamp.hpp>
#include <chrono>

namespace Verso {


Timestamp Timestamp::seconds(double amount)
{
	Timestamp time;
	time.timeMicroseconds = static_cast<int64_t>(amount * 1000000.0);
	return time;
}


Timestamp Timestamp::milliseconds(int32_t amount)
{
	Timestamp time;
	time.timeMicroseconds = static_cast<int64_t>(amount * 1000);
	return time;
}


Timestamp Timestamp::microseconds(int64_t amount)
{
	Timestamp time;
	time.timeMicroseconds = amount;
	return time;
}



UString Timestamp::asDigitalClockString(bool fractions, bool forceHours) const
{
	if (forceHours == false && asHours() >= 1.0) {
		forceHours = true;
	}

	double secondsTotalDouble = static_cast<double>(timeMicroseconds) / 1000000.0;
	int secondsTotal = static_cast<int>(floor(secondsTotalDouble));
	int milliseconds = static_cast<int>(floor((secondsTotalDouble - static_cast<double>(secondsTotal)) * 1000.0));

	int hours = secondsTotal / (60 * 60);
	secondsTotal %= 60 * 60;

	int minutes = secondsTotal / 60;
	secondsTotal %= 60;

	int seconds = secondsTotal;

	char clockCStr[32];
	if (!forceHours && !fractions) {
		snprintf(clockCStr, 32, "%02d:%02d", minutes, seconds);
	}
	else if (forceHours && !fractions) {
		snprintf(clockCStr, 32, "%02d:%02d:%02d", hours, minutes, seconds);
	}
	else if (!forceHours && fractions) {
		snprintf(clockCStr, 32, "%02d:%02d.%03d", minutes, seconds, milliseconds);
	}
	else {
		snprintf(clockCStr, 32, "%02d:%02d:%02d.%03d", hours, minutes, seconds, milliseconds);
	}

	return UString(clockCStr);
}


// static

Timestamp Timestamp::Zero()
{
	const static Timestamp zero;
	return zero;
}


} // End namespace Verso

