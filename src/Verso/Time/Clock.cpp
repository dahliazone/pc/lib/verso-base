#include <Verso/Time/Clock.hpp>
#include <Verso/System/Exception.hpp>

namespace Verso {


Clock::Clock() :
    defaultSource(),
    source(&defaultSource),
    started(Timestamp::microseconds(source->getCurrentMicrosecs()))
{
}


ITimeSource* Clock::getSource()
{
	return source;
}


void Clock::changeSource(ITimeSource* source)
{
	if (source == nullptr) {
		VERSO_ILLEGALPARAMETERS("verso-base", "source was nullptr.", "");
	}
	this->source = source;
	this->started = Timestamp::microseconds(source->getCurrentMicrosecs());
}


Timestamp Clock::getElapsed() const
{
	return Timestamp::microseconds(source->getCurrentMicrosecs()) - started;
}


UString Clock::toString() const
{
	UString str("started=");
	str += started.toString();
	str += ", source=";
	str += source->getName();
	return str;
}


UString Clock::toStringDebug() const
{
	UString str("Clock(");
	str += toString();
	str += ")";
	return str;
}


} // End namespace Verso

