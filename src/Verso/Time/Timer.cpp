#include <Verso/Time/Timer.hpp>
#include <Verso/Time/Clock.hpp>

namespace Verso {


Timer::Timer() :
	started(Clock::instance().getElapsed())
{
}


///////////////////////////////////////////////////////////////////////////////////////////
// interface ITimer
///////////////////////////////////////////////////////////////////////////////////////////

Timestamp Timer::getElapsed() const
{
	return Clock::instance().getElapsed() - started;
}


Timestamp Timer::restart()
{
	Timestamp now = Clock::instance().getElapsed();
	Timestamp elapsed = now - started;
	started = now;

	return elapsed;
}


UString Timer::toString() const
{
	UString str("elapsed=");
	str.append2(getElapsed().asSeconds());
	return str;
}


UString Timer::toStringDebug() const
{
	UString str("Timer(");
	str += toString();
	str += "started=";
	str.append2(started.asSeconds());
	str += ")";
	return str;
}


} // End namespace Verso

