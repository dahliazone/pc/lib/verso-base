#include <Verso/Time/ManualTimer.hpp>

namespace Verso {


///////////////////////////////////////////////////////////////////////////////////////////
// ManualTimer specific
///////////////////////////////////////////////////////////////////////////////////////////

ManualTimer::ManualTimer() :
	elapsed()
{
}


ManualTimer::~ManualTimer()
{
}


void ManualTimer::advance(const Timestamp& amount)
{
	elapsed += amount;
}


///////////////////////////////////////////////////////////////////////////////////////////
// interface ITimer
///////////////////////////////////////////////////////////////////////////////////////////

Timestamp ManualTimer::getElapsed() const
{
	return elapsed;
}


Timestamp ManualTimer::restart()
{
	Timestamp returnValue = elapsed;
	elapsed = Timestamp::microseconds(0);
	return returnValue;
}


UString ManualTimer::toString() const
{
	UString str("elapsed=");
	str.append2(getElapsed().asSeconds());
	return str;
}


UString ManualTimer::toStringDebug() const
{
	UString str("ManualTimer(");
	str += toString();
	str += ")";
	return str;
}


} // End namespace Verso

