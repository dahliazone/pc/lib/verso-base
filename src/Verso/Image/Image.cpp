#include <Verso/Image/Image.hpp>
#include <Verso/System/Exception.hpp>
#include <Verso/System/File.hpp>
#include <Verso/System/Logger.hpp>
#include <stb/stb_image.h>
#include <stb/stb_image_write.h>

namespace Verso {


Image::Image() :
	created(false),
	resolution(0, 0),
	pixelData(nullptr),
	components(0),
	sourceFileName(""),
	targetAspectRatio()
{
}


Image::Image(const Image& original) :
	created(false),
	resolution(original.resolution),
	pixelData(nullptr),
	components(0),
	sourceFileName(original.sourceFileName),
	targetAspectRatio(original.targetAspectRatio)
{
	createFromMemory(original.resolution, original.components, original.pixelData, targetAspectRatio);
}


Image::Image(Image&& original) noexcept :
	created(std::move(original.created)),
	resolution(std::move(original.resolution)),
	pixelData(std::move(original.pixelData)),
	components(std::move(original.components)),
	sourceFileName(std::move(original.sourceFileName)),
	targetAspectRatio(std::move(original.targetAspectRatio))
{
	original.pixelData = nullptr;
	original.sourceFileName.clear();
}


Image& Image::operator =(const Image& original)
{
	if (this != &original) {
		createFromMemory(original.resolution, original.components, original.pixelData, original.targetAspectRatio);
		sourceFileName = original.sourceFileName;
	}
	return *this;
}


Image& Image::operator =(Image&& original) noexcept
{
	if (this != &original) {
		created = std::move(original.created);
		resolution = std::move(original.resolution);
		pixelData = std::move(original.pixelData);
		components = std::move(original.components);
		sourceFileName = std::move(original.sourceFileName);
		targetAspectRatio = std::move(original.targetAspectRatio);

		original.resolution = Vector2i();
		original.pixelData = nullptr;
		original.components = 0;
		original.sourceFileName.clear();
		original.targetAspectRatio = AspectRatio();
	}
	return *this;
}


Image::~Image()
{
	destroy();
}


// public

void Image::create(int width, int height, bool alpha, const AspectRatio& targetAspectRatio, const RgbaColorf& color)
{
	create(Vector2i(width, height), alpha, targetAspectRatio, color);
}

void Image::create(const Vector2i& resolution, bool alpha, const AspectRatio& targetAspectRatio, const RgbaColorf& color)
{
	VERSO_ASSERT_MSG("verso-base", isCreated() == false, "Already created!");

	allocateBuffer(resolution, alpha, targetAspectRatio);
	clear(color);
	created = true;
}


void Image::createFromMemory(int width, int height, bool alpha, const std::uint8_t* pixelData, const AspectRatio& targetAspectRatio)
{
	createFromMemory(Vector2i(width, height), alpha, pixelData, targetAspectRatio);
}

void Image::createFromMemory(const Vector2i& resolution, bool alpha, const std::uint8_t* pixelData, const AspectRatio& targetAspectRatio)
{
	VERSO_ASSERT_MSG("verso-base", isCreated() == false, "Already created!");

	allocateBuffer(resolution, alpha, targetAspectRatio);
	copyFromBuffer(resolution, pixelData);
	created = true;
}


bool Image::createFromFile(const UString& fileName, bool alpha, const AspectRatio& targetAspectRatio)
{
	VERSO_ASSERT_MSG("verso-base", isCreated() == false, "Already created!");

	if (!File::exists(fileName)) {
		VERSO_FILENOTFOUND("verso-base", "Cannot load image", fileName.c_str());
	}

	int requiredComponentsPerPixel = 3;
	if (alpha == true) {
		requiredComponentsPerPixel = 4;
	}
	int componentsPerPixel;
	unsigned char* tempData = stbi_load(fileName.c_str(), &resolution.x, &resolution.y, &componentsPerPixel, requiredComponentsPerPixel);
	if (tempData == nullptr) {
		UString error("Failed to load file. Reason: ");
		error += stbi_failure_reason();
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	create(this->resolution, alpha, targetAspectRatio, ColorGenerator::getColor(ColorRgb::Black));
	memcpy(pixelData, tempData, static_cast<size_t>(resolution.x * resolution.y * components));
	stbi_image_free(tempData);

	sourceFileName = fileName;
	created = true;

	return true;
}


void Image::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	freeBuffer();
	created = false;
}


bool Image::isCreated() const
{
	return created;
}


void Image::applyAlphaMaskFromColor(const RgbaColorf& color, std::uint8_t alpha)
{
	(void)color; (void)alpha;
	VERSO_FAIL("verso-base");
}


void Image::clear(const RgbaColorf& color)
{
	VERSO_ASSERT("verso-base", pixelData != nullptr);

	if (components == 4) {
		std::uint32_t colorValue = color.getRgbaValue();
		std::uint32_t* pixels = reinterpret_cast<std::uint32_t*>(pixelData);
		for (int i = 0; i < resolution.calculateArea(); ++i) {
			pixels[i] = colorValue;
		}
	}
	else if (components == 3) {
		uint8_t colorArr[4];
		color.getArrayRgbau8(colorArr);
		for (int i = 0; i < resolution.calculateArea() * components; i += components) {
			pixelData[i] = colorArr[0];
			pixelData[i + 1] = colorArr[1];
			pixelData[i + 2] = colorArr[2];
		}
	}
	else {
		VERSO_FAIL("verso-base"); // unknown components
	}
}


void Image::clear(const RgbaColorf& color, const Recti& area)
{
	(void)color; (void)area;
	VERSO_FAIL("verso-base");
}


void Image::copy(const Image& image, int destX, int destY, const Recti& sourceRect, bool applyAlpha)
{
	copy(image, Vector2i(destX, destY), sourceRect, applyAlpha);
}


void Image::copy(const Image& image, const Vector2i& destPosition, const Recti& sourceRect, bool applyAlpha)
{
	VERSO_ASSERT("verso-base", pixelData != nullptr);
	VERSO_ASSERT("verso-base", resolution.x > 0);
	VERSO_ASSERT("verso-base", resolution.y > 0);

	VERSO_ASSERT("verso-base", image.pixelData != nullptr);
	VERSO_ASSERT("verso-base", image.resolution.x > 0);
	VERSO_ASSERT("verso-base", image.resolution.y > 0);

	Recti fixedSourceRect(sourceRect);
	if (fixedSourceRect == Recti(0, 0, 0, 0)) {
		fixedSourceRect.size = resolution - Vector2i(destPosition.x, destPosition.y);

		if (fixedSourceRect.size.x > image.resolution.x) {
			fixedSourceRect.size.x = image.resolution.x;
		}

		if (fixedSourceRect.size.y > image.resolution.y) {
			fixedSourceRect.size.y = image.resolution.y;
		}
	}

	if (applyAlpha == false) {
		if (components == 4) {
			std::uint32_t* target = reinterpret_cast<std::uint32_t*>(pixelData);
			std::uint32_t* source = reinterpret_cast<std::uint32_t*>(image.pixelData);

			for (int y = 0; y < fixedSourceRect.size.y; ++y) {
				for (int x = 0; x < fixedSourceRect.size.x; ++x) {
					target[(destPosition.y + y) * resolution.x + destPosition.x + x] = source[(fixedSourceRect.pos.y + y) * image.resolution.x + fixedSourceRect.pos.x + x];
				}
			}
		}
		else if (components == 3) {
			std::uint8_t* target = pixelData;
			std::uint8_t* source = image.pixelData;

			for (int y = 0; y < fixedSourceRect.size.y; ++y) {
				for (int x = 0; x < fixedSourceRect.size.x; ++x) {
					size_t targetIndex = components * ((destPosition.y + y) * resolution.x + destPosition.x + x);
					size_t sourceIndex = components * ((fixedSourceRect.pos.y + y) * image.resolution.x + fixedSourceRect.pos.x + x);
					target[targetIndex] = source[sourceIndex];
					target[targetIndex + 1] = source[sourceIndex + 1];
					target[targetIndex + 2] = source[sourceIndex + 2];
				}
			}
		}
		else {
			VERSO_FAIL("verso-base"); // unknown components
		}
	}
	else {
		VERSO_FAIL("verso-base"); // alpha blitting not implemented yet
	}
}


void Image::flipHorizontally()
{
	VERSO_ASSERT("verso-base", pixelData != nullptr);
	VERSO_ASSERT("verso-base", resolution.x > 0);
	VERSO_ASSERT("verso-base", resolution.y > 0);

	std::uint8_t* targetPixelData = new std::uint8_t[resolution.x * resolution.y * components]; // Buffer is always RGB or RGBA

	if (components == 4) {
		std::uint32_t* target = reinterpret_cast<std::uint32_t*>(targetPixelData);
		std::uint32_t* source = reinterpret_cast<std::uint32_t*>(pixelData);

		for (int y = 0; y < resolution.y; ++y) {
			for (int x = 0; x < resolution.x; ++x) {
				target[y * resolution.x + (resolution.x - x - 1)] = source[y * resolution.x + x];
			}
		}
	}
	else if (components == 3) {
		std::uint8_t* target = targetPixelData;
		std::uint8_t* source = pixelData;

		for (int y = 0; y < resolution.y; ++y) {
			for (int x = 0; x < resolution.x; ++x) {
				size_t targetIndex = components * (y * resolution.x + (resolution.x - x - 1));
				size_t sourceIndex = components * (y * resolution.x + x);
				target[targetIndex] = source[sourceIndex];
				target[targetIndex + 1] = source[sourceIndex + 1];
				target[targetIndex + 2] = source[sourceIndex + 2];
			}
		}
	}
	else {
		VERSO_FAIL("verso-base"); // unknown components
	}

	delete pixelData;
	pixelData = targetPixelData;
}


void Image::flipVertically()
{
	VERSO_ASSERT("verso-base", pixelData != nullptr);
	VERSO_ASSERT("verso-base", resolution.x > 0);
	VERSO_ASSERT("verso-base", resolution.y > 0);

	std::uint8_t* target = new std::uint8_t[resolution.x * resolution.y * components]; // Buffer is always RGB or RGBA
	std::uint8_t* source = pixelData;

	for (int y = 0; y < resolution.y; ++y) {
		memcpy(&target[(resolution.y - y - 1) * resolution.x * components], &source[y * resolution.x * components], static_cast<size_t>(resolution.x * components));
	}

	freeBuffer();
	pixelData = target;
}


std::uint8_t* Image::getPixelData()
{
	return pixelData;
}


RgbaColorf Image::getPixel(int x, int y) const
{
	return getPixel(Vector2i(x, y));
}


RgbaColorf Image::getPixel(const Vector2i& position) const
{
	if (Recti(0, 0, resolution).isInside(position)) {
		size_t offset = static_cast<size_t>(position.y * resolution.x + position.x) * components;
		if (components == 4) {
			return RgbaColorf(pixelData[offset] / 255.0f, pixelData[offset + 1] / 255.0f, pixelData[offset + 2] / 255.0f, pixelData[offset + 3] / 255.0f);
		}
		else if (components == 3) {
			return RgbaColorf(pixelData[offset] / 255.0f, pixelData[offset + 1] / 255.0f, pixelData[offset + 2] / 255.0f, 1.0f);
		}
		else {
			VERSO_FAIL("verso-base"); // unknown components
		}
	}
	else {
		UString error("Given position out of bounds. position="+position.toString()+", bounds="+Recti(0, 0, resolution).toString());
		VERSO_ILLEGALPARAMETERS("verso-base", error.c_str(), "");
	}
}


void Image::setPixel(int x, int y, const RgbaColorf& color)
{
	setPixel(Vector2i(x, y), color);
}


void Image::setPixel(const Vector2i& position, const RgbaColorf& color)
{
	if (Recti(0, 0, resolution).isInside(position)) {
		if (components == 4) {
			std::uint32_t* pixels = reinterpret_cast<std::uint32_t*>(pixelData);
			pixels[position.y * resolution.x + position.x] = color.getRgbaValue();
		}
		else if (components == 3) {
			uint8_t colorArr[4];
			color.getArrayRgbau8(colorArr);
			size_t targetIndex = components * ((position.y) * resolution.x + position.x);
			pixelData[targetIndex] = colorArr[0];
			pixelData[targetIndex + 1] = colorArr[1];
			pixelData[targetIndex + 2] = colorArr[2];
		}
		else {
			VERSO_FAIL("verso-base"); // unknown components
		}
	}
}


bool Image::saveToFile(const UString& fileName, const ImageSaveFormat& imageSaveFormat, bool discardAlpha) const
{
	std::uint8_t* saveData = pixelData;
	int saveComponents = components;

	if (discardAlpha == true && components == 4) {
		saveData = new std::uint8_t[resolution.x * resolution.y * 3];
		saveComponents = 3;
		for (int i = 0; i < resolution.x * resolution.y; ++i) {
			saveData[i * 3] = pixelData[i * 4];
			saveData[i * 3 + 1] = pixelData[i * 4 + 1];
			saveData[i * 3 + 2] = pixelData[i * 4 + 2];
		}
	}

	int result = 0;
	if (imageSaveFormat == ImageSaveFormat::Png) {
		result = stbi_write_png(fileName.c_str(), resolution.x, resolution.y, saveComponents, saveData, resolution.x * saveComponents);
	}
	else if (imageSaveFormat == ImageSaveFormat::Bmp) {
		result = stbi_write_bmp(fileName.c_str(), resolution.x, resolution.y, saveComponents, saveData);
	}
	else if (imageSaveFormat == ImageSaveFormat::Tga) {
		result = stbi_write_tga(fileName.c_str(), resolution.x, resolution.y, saveComponents, saveData);
	}
	else {
		VERSO_ILLEGALPARAMETERS("verso-base", "Unknown ImageSaveFormat", "");
	}

	if (!result) {
		UString error("Failed to save file. Reason: ");
		error += stbi_failure_reason();
		VERSO_IOERROR("verso-base", error.c_str(), fileName.c_str());
	}

	if (discardAlpha == true && components == 4) {
		delete[] saveData;
		saveData = nullptr;
	}

	return true;
}


Vector2i Image::getResolutioni() const
{
	return resolution;
}


Vector2f Image::getResolutionf() const
{
	return Vector2f(static_cast<float>(resolution.x), static_cast<float>(resolution.y));
}


UString Image::getSourceFileName() const
{
	return sourceFileName;
}


size_t Image::getBytesPerPixel() const
{
	return components;
}


size_t Image::getBitsPerPixel() const
{
	return components * 8;
}


AspectRatio Image::getAspectRatioTarget() const
{
	return targetAspectRatio;
}


AspectRatio Image::getAspectRatioActual() const
{
	if (resolution.y != 0) {
		 return AspectRatio(resolution, "");  // \TODO: fix aspect ratio if known
	}
	else {
		return AspectRatio();
	}
}

// private

void Image::allocateBuffer(const Vector2i& resolution, bool alpha, const AspectRatio& targetAspectRatio)
{
	freeBuffer();

	if (alpha == false) {
		this->components = 3;
	}
	else {
		this->components = 4;
	}

	pixelData = new std::uint8_t[resolution.x * resolution.y * components]; // Buffer is always RGB or RGBA
	this->resolution = resolution;
	if (targetAspectRatio.value == 0.0f) {
		this->targetAspectRatio = AspectRatio(resolution, ""); // \TODO: fix aspect ratio if known
	}
	else {
		this->targetAspectRatio = targetAspectRatio;
	}
}


void Image::freeBuffer()
{
	if (pixelData != nullptr) {
		delete[] pixelData;
		pixelData = nullptr;
	}
}


void Image::copyFromBuffer(const Vector2i& resolution, const std::uint8_t* pixelData)
{
	memcpy(this->pixelData, pixelData, static_cast<size_t>(resolution.x * resolution.y * components));
	this->resolution = resolution;
}


// toString

UString Image::toString() const
{
	if (pixelData == nullptr) {
		return "<uninitialized>";
	}
	UString str("[");
	str += resolution.toString();
	str += "] ";
	str.append2(components);
	str += " bpp, sourceFileName=";
	if (!sourceFileName.isEmpty()) {
		str += sourceFileName;
	}
	else {
		str += "<generated>";
	}
	return str;
}


UString Image::toStringDebug() const
{
	UString str("Image(");
	str += toString();
	str += ")";
	return str;
}


} // End namespace Verso

