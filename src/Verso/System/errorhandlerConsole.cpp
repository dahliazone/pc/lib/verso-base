#include <Verso/System/errorhandlerConsole.hpp>
#include <Verso/System/Logger.hpp>
#include <Verso/System/stacktrace.hpp>

namespace Verso {


void errorHandlerConsoleEnable()
{
	std::set_terminate([]() {
		std::exception_ptr current = std::current_exception();
		try {
			std::rethrow_exception(current);
		}
		catch (ExceptionInterface& e) {
			VERSO_LOG_ERR_VERSOEXCEPTION("verso-base", e);
#ifndef NDEBUG
			logErrStacktrace();
#endif
		}
		catch (std::runtime_error& e) {
			VERSO_LOG_ERR_EXCEPTION("verso-base", "std::runtime_error", e.what());
#ifndef NDEBUG
			logErrStacktrace();
#endif
		}
		catch (std::exception& e) {
			VERSO_LOG_ERR_EXCEPTION("verso-base", "std::exception", e.what());
#ifndef NDEBUG
			logErrStacktrace();
#endif
		}
		catch (...) {
			VERSO_LOG_ERR_UNKNOWNEXCEPTION("verso-base");
#ifndef NDEBUG
			logErrStacktrace();
#endif
		}

		std::abort();
	});
}


} // End namespace Verso

