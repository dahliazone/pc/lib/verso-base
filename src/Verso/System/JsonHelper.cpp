#include <Verso/System/JsonHelper.hpp>
#include <locale>
#include <codecvt>

namespace Verso {


namespace JsonHelper {


JsonPath loadAndParse(const UString& fileName)
{
	if (File::exists(fileName) == false) {
		UString error("Cannot find JSON file!");
		VERSO_FILENOTFOUND("verso-base", error.c_str(), fileName.c_str());
	}

	// Load file
	std::ifstream fileStream(fileName.c_str(), std::ifstream::in);
	std::stringstream ss;
	ss << fileStream.rdbuf();
	UString buffer(ss.str());

	// \TODO: Strip comments
	/*std::vector<UString> lines = buffer.split("\n");
	for (auto& line : lines) {
		for (int i=0; i<line.size(); ++i) {
			if (line[i] == "\"") {
				break;
			}
			else if (line[i] == "/") {
				if (i+1 < line.size() && line[i+1] == "/") {
					// remove line
				}
			}
		}
	}*/


	// Parse JSON
	JSONValue* rootValue = JSON::Parse(buffer.c_str());
	if (rootValue == nullptr) {
		UString error("Failed to parse JSON structure! Check against JSON lint.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	return { rootValue, "root", fileName };
}


void saveToFile(const UString& fileName, JSONValue* root, bool prettyPrint)
{
	std::ofstream ofs(fileName.c_str(), std::ofstream::out);
	std::wbuffer_convert<std::codecvt_utf8<wchar_t>> converter(ofs.rdbuf());
	std::wostream out(&converter);

	out << root->Stringify(prettyPrint);

	ofs.close();
}


void free(const JsonPath& rootPath)
{
	if (rootPath.value != nullptr) {
		delete rootPath.value;
	}
}


} // End namespace JsonHelper


} // End namespace Verso

