char FileWin32_symbol = 0;

#ifdef _WIN32

#include <Verso/System/File.hpp>
#include <Verso/System/win32/FileWin32.hpp>
#include <Verso/System/Exception.hpp>
#include <sys/types.h>
#include <sys/stat.h>
#include <windows.h>
#include <direct.h>

namespace Verso {
namespace priv {


UString FileWin32::readSymLink(const UString& symLink)
{
	return symLink;
}


UString FileWin32::getBasePath()
{
	return "";
}


UString FileWin32::getPreferencesPath()
{
	return getBasePath();
}


UString FileWin32::getFileNamePath(const UString& fileName)
{
	char dirName[FILENAME_MAX];
	if (_splitpath_s(fileName.c_str(), NULL, 0, dirName, sizeof(dirName), NULL, 0, NULL, 0) != 0) {
		VERSO_ILLEGALFORMAT("verso-base", "Cannot process path from file name", fileName.c_str());
	}
	return UString(dirName);
}


bool FileWin32::mkdir(const UString& fileName)
{
	if (_mkdir(fileName.c_str()) == 0)
		return true;
	else
		return false;
}


void FileWin32::ls(const UString& directoryName, bool showFiles, bool showDirectories, bool showDotFiles, std::vector<UString>& out)
{
	HANDLE dir;
	WIN32_FIND_DATAA fileData;
	UString directoryAll(directoryName);
	directoryAll += "/*";
	if ((dir = FindFirstFileA(directoryAll.c_str(), &fileData)) == INVALID_HANDLE_VALUE) {
		// No files found
		return;
	}

	do {
		UString fileName = fileData.cFileName;

		// Hide files beginning with '.'
		if (showDotFiles == false && fileName[0] == '.') {
			continue;
		}

		bool isDirectory = (fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;

		// Hide files if requested not to be shown
		if (showFiles == false && isDirectory == false) {
			continue;
		}

		// Hide directories if requested not to be shown
		if (showDirectories == false && isDirectory == true) {
			continue;
		}

		UString fullFileName(directoryName);
		fullFileName += "/";
		fullFileName += fileName;
		out.push_back(fileName);
	}
	while (FindNextFileA(dir, &fileData));

	FindClose(dir);
}


int64_t FileWin32::getSize(const char* fileName)
{
	bool TODO_stat_based_64_bit_file_size;
	//struct __stat64 stat_buf;
	//int rc = _stat64(fileName, &stat_buf);
	//return rc == 0 ? stat_buf.st_size : -1;

	std::ifstream in(fileName, std::ifstream::ate | std::ifstream::binary);
	std::ifstream::pos_type streamPos = in.tellg();
	if (streamPos == std::ifstream::pos_type(-1)) {
		UString message("Cannot get size of non-existing file: \"");
		message.append(fileName);
		message += "\"";
		VERSO_FILENOTFOUND("verso-base", message.c_str(), fileName);
	}
	in.close();
	return static_cast<int64_t>(streamPos);
}


} // End namespace priv
} // End namespace Verso


#endif // End #ifdef _WIN32


