#include <Verso/System/CommandLineParser.hpp>

namespace Verso {


void CommandLineParser::parse(const Options& options, int argc, char* argv[])
{
	parse(options, Args::convert(argc, argv));
}


void CommandLineParser::parse(const Options& searchOptions, const std::vector<UString>& args)
{
	if (args.size() < 1) {
		VERSO_ILLEGALPARAMETERS("verso-base", "Application arguments must have at least one entry", "");
	}

	// Parse through arguments
	for (size_t i = 1; i < args.size(); ++i) {
		if (args[i].beginsWith("-")) {
			Option option;

			if (!args[i].beginsWith("--")) {
				UString optionName(args[i].substring(1));
				if (!optionName.beginsWith("psn_")) {
					if (optionName.size() != 1) {
						UString error("Single dash option is too long: ");
						error.append2(optionName);
						error += "\nFull argument list: ";
						for (size_t j = 0; j < args.size(); ++j) {
							error.append(args[j]);
							error += " ";
						}
						VERSO_ILLEGALFORMAT("verso-base", error.c_str(), optionName.c_str());
					}
					option = searchOptions.getOption(optionName);
				}
			}

			else {
				UString optionName(args[i].substring(2));
				option = searchOptions.getOption(optionName);
			}

			if (option.opt.size() != 0 || option.longOpt.size() != 0) {
				if (option.hasArgs == false) {
					this->resultOptions.addOption(option);
				}
				else if (option.hasArgs == true) {
					std::vector<UString> tmpArgs;
					for (size_t j = 1; j <= option.argNames.size(); ++j) {
						if (i + j < args.size() && !args[i + j].beginsWith("-")) {
							tmpArgs.push_back(args[i + j]);
						}
						else {
							UString error;
							error.set("Missing required argument(s) for option: '-");
							error.append2(option.opt);
							error.append2("'");
							VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
						}
					}

					option.setArgValues(tmpArgs);
					i += tmpArgs.size();
					this->resultOptions.addOption(option);
				}
			}
		}

		else {
			// Other argument?
		}
	}

	// Check that all required options were entered
	for (const auto& option : searchOptions.getOptions()) {
		if (option.required == true && !this->resultOptions.hasOption(option)) {
			static UString error; // \TODO: this is probably really bad way to throw exceptions
			error.set("Missing required option: '-");
			error.append2(option.opt);
			error.append2("'");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
	}

	// Add options with default values
	for (const auto& option : searchOptions.getOptions()) {
		if (option.argDefaultValues.size() > 0 && option.argDefaultValues[0].size() != 0 && this->resultOptions.hasOption(option) == false) {
			this->resultOptions.addOption(option);
		}
	}
}


const std::vector<Option>& CommandLineParser::getOptions() const
{
	return resultOptions.getOptions();
}


bool CommandLineParser::hasOption(const Option& option) const
{
	return resultOptions.hasOption(option.longOpt);
}


bool CommandLineParser::hasOption(const UString& optionName) const
{
	return resultOptions.hasOption(optionName);
}


std::vector<UString> CommandLineParser::getOptionValues(const Option& option) const
{
	return resultOptions.getOptionValues(option.longOpt);
}


std::vector<UString> CommandLineParser::getOptionValues(const UString& optionName) const
{
	return resultOptions.getOptionValues(optionName);
}


} // End namespace Verso

