#include <Verso/System/File.hpp>
#include <Verso/System/Exception.hpp>
#ifdef _WIN32
	#include <Verso/System/win32/FileWin32.hpp>

#elif __linux__
	#include <Verso/System/linux/FileLinux.hpp>

#elif __APPLE__ && __MACH__
	#include <Verso/System/osx/FileOsx.hpp>
#endif

namespace Verso {


///////////////////////////////////////////////////////////////////////////////////////////
// Static
///////////////////////////////////////////////////////////////////////////////////////////

UString File::getDirectorySeparator()
{
#ifdef _WIN32
	return priv::FileWin32::getDirectorySeparator();
#elif __linux__
	return priv::FileLinux::getDirectorySeparator();
#elif __APPLE__ && __MACH__
	return priv::FileOsx::getDirectorySeparator();
#else
	VERSO_ERROR("verso-base", "Unknown platform! (win32, linux, OS X only supported)", "");
#endif
}


char File::getDirectorySeparatorChar()
{
#ifdef _WIN32
	return priv::FileWin32::getDirectorySeparatorChar();
#elif __linux__
	return priv::FileLinux::getDirectorySeparatorChar();
#elif __APPLE__ && __MACH__
	return priv::FileOsx::getDirectorySeparatorChar();
#else
	VERSO_ERROR("verso-base", "Unknown platform! (win32, linux, OS X only supported)", "");
#endif
}


UString File::readSymLink(const UString& symLink)
{
#ifdef _WIN32
	return priv::FileWin32::readSymLink(symLink);
#elif __linux__
	return priv::FileLinux::readSymLink(symLink);
#elif __APPLE__ && __MACH__
	return priv::FileOsx::readSymLink(symLink);
#else
	VERSO_ERROR("verso-base", "Unknown platform! (win32, linux, OS X only supported)", "");
#endif
}


UString File::getBasePath()
{
	static bool basePathCreated = false;;
	static UString basePath = "";

	// Fetch only once
	if (basePathCreated == false) {
#ifdef _WIN32
		basePath = priv::FileWin32::getBasePath();
#elif __linux__
		basePath = priv::FileLinux::getBasePath();
#elif __APPLE__ && __MACH__
		basePath = priv::FileOsx::getBasePath();
#else
		VERSO_ERROR("verso-base", "Unknown platform! (win32, linux, OS X only supported)", "");
#endif

		basePathCreated = true;
	}
	return basePath;
}


UString File::getPreferencesPath()
{
	static bool preferencesPathCreated = false;
	static UString preferencesPath = "";

	// Fetch only once
	if (preferencesPathCreated == false) {
#ifdef _WIN32
		preferencesPath = priv::FileWin32::getPreferencesPath();
#elif __linux__
		preferencesPath = priv::FileLinux::getPreferencesPath();
#elif __APPLE__ && __MACH__
		preferencesPath = priv::FileOsx::getPreferencesPath();
#else
		VERSO_ERROR("verso-base", "Unknown platform! (win32, linux, OS X only supported)", "");
#endif

		preferencesPathCreated = true;
	}
	return preferencesPath;
}


UString File::getFileNamePath(const UString& fileName)
{
#ifdef _WIN32
	return priv::FileWin32::getFileNamePath(fileName);
#elif __linux__
	return priv::FileLinux::getFileNamePath(fileName);
#elif __APPLE__ && __MACH__
	return priv::FileOsx::getFileNamePath(fileName);
#else
	VERSO_ERROR("verso-base", "Unknown platform! (win32, linux, OS X only supported)", "");
#endif
}


UString File::addSlashToPathIfNeeded(const UString& path)
{
	size_t pathSize = path.size();
	if (pathSize > 0) {
		char c = path.charAt(static_cast<int>(pathSize) - 1);
		if (c != '/' && c != '\\') {
			UString newPath(path);
			newPath.append(getDirectorySeparator());
			return newPath;
		}
	}
	return path;
}


UString File::removeSlashFromPathIfNeeded(const UString& path)
{
	size_t pathSize = path.size();
	if (pathSize > 0) {
		char c = path.charAt(static_cast<int>(pathSize) - 1);
		if ((c == '/' || c == '\\') && pathSize > 1) {
			return path.substring(0, pathSize - 1);
		}
	}
	return path;
}


bool File::mkdir(const UString& fileName)
{
#ifdef _WIN32
	return priv::FileWin32::mkdir(fileName);
#elif __linux__
	return priv::FileLinux::mkdir(fileName);
#elif __APPLE__ && __MACH__
	return priv::FileOsx::mkdir(fileName);
#else
	VERSO_ERROR("verso-base", "Unknown platform! (win32, linux, OS X only supported)", "");
#endif
}


std::vector<UString> File::ls(const UString& directoryName, bool showFiles, bool showDirectories, bool showDotFiles)
{
	std::vector<UString> out;

#ifdef _WIN32
	priv::FileWin32::ls(directoryName, showFiles, showDirectories, showDotFiles, out);
#elif __linux__
	priv::FileLinux::ls(directoryName, showFiles, showDirectories, showDotFiles, out);
#elif __APPLE__ && __MACH__
	priv::FileOsx::ls(directoryName, showFiles, showDirectories, showDotFiles, out);
#else
	VERSO_ERROR("verso-base", "Unknown platform! (win32, linux, OS X only supported)", "");
#endif

	return out;
}


bool File::exists(const UString& fileName)
{
	return File::exists(fileName.c_str());
}


bool File::exists(const std::string& fileName)
{
	return File::exists(fileName.c_str());
}


bool File::exists(const char* fileName)
{
	FILE* fp = fopen(fileName, "r");
	if (fp == nullptr)
		return false;
	else {
		fclose(fp);
		return true;
	}
}


int64_t File::getSize(const UString& fileName)
{
	return File::getSize(fileName.c_str());
}


int64_t File::getSize(const std::string& fileName)
{
	return File::getSize(fileName.c_str());
}


int64_t File::getSize(const char* fileName)
{
#ifdef _WIN32
	return priv::FileWin32::getSize(fileName);
#elif __linux__
	return priv::FileLinux::getSize(fileName);
#elif __APPLE__ && __MACH__
	return priv::FileOsx::getSize(fileName);
#else
	VERSO_ERROR("verso-base", "Unknown platform! (win32, linux, OS X only supported)", "");
#endif
}


bool File::remove(const UString& fileName)
{
	return File::remove(fileName.c_str());
}


bool File::remove(const std::string& fileName)
{
	return File::remove(fileName.c_str());
}


bool File::remove(const char* fileName)
{
	if (std::remove(fileName) == 0) {
		return true;
	}
	else {
		return false;
	}
}


uint8_t File::readUint8(const char* headerBytes, size_t index)
{
	uint8_t result=0;
	for (int i = 0; i < 8; ++i) {
		result = (result << 8) | headerBytes[index];
	}
	return result;
}


uint16_t File::readUint16(const char* headerBytes, size_t index)
{
	uint16_t result=0;
	for (int i = 0; i < 8; ++i) {
		result = (result << 8) | headerBytes[index];
	}
	return result;
}


uint32_t File::readUint32(const char* headerBytes, size_t index)
{
	uint32_t result=0;
	for (int i = 0; i < 8; ++i) {
		result = (result << 8) | headerBytes[index];
	}
	return result;
}


uint64_t File::readUint64(const char* headerBytes, size_t index)
{
	uint64_t result=0;
	for(int i=0; i<8; ++i) {
		result = (result << 8) | headerBytes[index];
	}
	return result;
}


char* File::loadBytes(const UString& fileName, size_t& filesize)
{
	return loadBytes(fileName.c_str(), filesize);
}


char* File::loadBytes(const std::string& fileName, size_t& filesize)
{
	return loadBytes(fileName.c_str(), filesize);
}


char* File::loadBytes(const char* fileName, size_t& filesize)
{
	filesize = File::getSize(fileName);

	char* buffer = new char[filesize];

	std::ifstream file(fileName, std::ios::in | std::ios::binary);
	if (!file.read(buffer, filesize)) {
		// An error occurred!
		// myFile.gcount() returns the number of bytes read.
		// calling myFile.clear() will reset the stream state
		// so it is usable again.
		file.close();
		delete[] buffer;
		return nullptr;
	}

	file.close();
	return buffer;
}


bool File::equals(const UString& fileName1, const UString& fileName2)
{
	return equals(fileName1.c_str(), fileName2.c_str());
}


bool File::equals(const std::string& fileName1, const std::string& fileName2)
{
	return equals(fileName1.c_str(), fileName2.c_str());
}


bool File::equals(const char* fileName1, const char* fileName2)
{
	size_t fileSize1 = File::getSize(fileName1);
	size_t fileSize2 = File::getSize(fileName2);
	if (fileSize1 != fileSize2) {
		return false;
	}

	char* fileContents1 = File::loadBytes(fileName1, fileSize1);
	if (fileContents1 == nullptr) {
		return false;
	}

	char* fileContents2 = File::loadBytes(fileName1, fileSize2);
	if (fileContents2 == nullptr) {
		delete[] fileContents1;
		return false;
	}

	for (size_t i=0; i<fileSize1; ++i) {
		if (fileContents1[i] != fileContents2[i]) {
			delete[] fileContents1;
			delete[] fileContents2;
			return false;
		}
	}

	delete[] fileContents1;
	delete[] fileContents2;

	return true;
}


///////////////////////////////////////////////////////////////////////////////////////////
// Non-static
///////////////////////////////////////////////////////////////////////////////////////////

File::File() :
	fileName(),
	filePointer(nullptr)
{
}


File::File(const UString& fileName) :
	fileName(fileName),
	filePointer(nullptr)
{
}


File::~File()
{
	close();
}


bool File::isOpen() const
{
	if (filePointer == nullptr) {
		return false;
	}
	else {
		return true;
	}
}


UString File::getFileName() const
{
	return fileName;
}


void File::changeFileName(const UString& fileName)
{
	close();
	this->fileName = fileName;
}


bool File::exists()
{
	if (filePointer != nullptr) {
		return true;
	}
	return exists(fileName);
}



bool File::open(const UString& mode, const UString& fileName)
{
	UString& fileNameToOpen = this->fileName;
	if (fileName.isEmpty() == false) {
		fileNameToOpen = fileName;
	}
	if (fileNameToOpen.isEmpty() == true) {
		return false;
	}
	FILE* fp = fopen(fileNameToOpen.c_str(), mode.c_str());
	if (fp == nullptr) {
		return false;
	}
	else {
		changeFileName(fileNameToOpen); // closes the current mFp and changes the fileName
		filePointer = fp;
		return true;
	}
}


void File::close()
{
	if (isOpen() == true) {
		fclose(filePointer);
		filePointer = nullptr;
		fileName = "";
	}
}


bool File::loadToString(UString& target)
{
//	bool nullTerminated = false; // \TODO: null-termination shoud not be needed for inside File::loadToString()?
	bool closeAfter = false;
	if (isOpen() == false) {
		bool opened = open("rb"); // open as read-only binary (so Window doesn't mess up the contents)
		if (opened == false) {
			return false;
		}
		closeAfter = true;
	}

	int64_t result = File::getSize(fileName);
	if (result == -1) {
		VERSO_LOG_WARN("verso-base", "Could not get file size from \""<<fileName<<"\"");
		return false;
	}
	size_t size = static_cast<size_t>(result);

	size_t allocSize;
//	if (nullTerminated == false) {
		allocSize = size;
//	}
//	else {
//		allocSize = size + 1;
//	}

	rewind(filePointer);
	char* buffer = new char[allocSize];
	size_t bytesReaded = fread(buffer, 1, size, filePointer);

	bool returnValue = true;

	if (size != bytesReaded) {
		if (ferror(filePointer) != 0) {
			VERSO_LOG_WARN("verso-base", "Error reading file, read only " << bytesReaded << " / " << size << " from \"" << fileName << "\"");
			returnValue = false;
		}
		else if (feof(filePointer) != 0) {
			VERSO_LOG_WARN("verso-base", "At end-of-file but read only " << bytesReaded << " / " << size << " from \"" << fileName << "\"");
			returnValue = false;
		}
	}

//	if (nullTerminated == true) {
//		buffer[allocSize - 1] = '\n';
//	}

	target.set(buffer, allocSize);

	delete[] buffer;
	if (closeAfter == true) {
		close();
	}

	return returnValue;
}


} // End namespace Verso

