#include <Verso/System/CommandLineParser/Options.hpp>

namespace Verso {


Options::Options() :
	options()
{
}


Options::~Options()
{
}


void Options::addOption(const Option& option)
{
	for (auto& o: options) {
		if (option.opt.equals(o.opt)) {
			UString str("registered: \"-");
			str += o.opt;
			str += " --";
			str += o.longOpt;
			str += "  '";
			str += o.description;
			str += "'\", conflicting: \"-";
			str += option.opt;
			str += " --";
			str += option.longOpt;
			str += "  '";
			str += option.description;
			str += "'\"";
			VERSO_ILLEGALPARAMETERS("verso-base", "Option.opt matches already registed option", str.c_str());
		}

		if (option.longOpt.size() > 0 && option.longOpt.equals(o.longOpt)) {
			UString str("registered: \"-");
			str += o.opt;
			str += " --";
			str += o.longOpt;
			str += "  '";
			str += o.description;
			str += "'\", conflicting: \"-";
			str += option.opt;
			str += " --";
			str += option.longOpt;
			str += "  '";
			str += option.description;
			str += "'\"";
			VERSO_ILLEGALPARAMETERS("verso-base", "Option.longOpt matches already registed option", str.c_str());
		}
	}
	options.push_back(option);
}


Option Options::getOption(const UString& optionName) const
{
	if (optionName.size() < 1) {
		VERSO_ILLEGALPARAMETERS("verso-base", "Parameter 'optionName' is less than one character long", optionName.c_str());
	}

	for (const auto& option : options) {
		if (option.match(optionName) == true) {
			return option;
		}
	}

	VERSO_ILLEGALFORMAT("verso-base", "Cannot find option", optionName.c_str());
}


bool Options::hasOption(const UString& optionName) const
{
	for (const auto& option : options) {
		if (option.match(optionName)) {
			return true;
		}
	}

	return false;
}


bool Options::hasOption(const Option& option) const
{
	for (const auto& it : options) {
		if (it.opt.equals(option.opt)) {
			return true;
		}
	}

	return false;
}


std::vector<UString> Options::getOptionValues(const UString& optionName) const
{
	if (optionName.size() < 1) {
		VERSO_ILLEGALPARAMETERS("verso-base", "Parameter 'optionName' is less than one character long", optionName.c_str());
	}

	for (auto& option : options) {
		if (option.match(optionName) == true) {
			if (option.argValues.size() > 0 && option.argValues[0].size() != 0) {  // \TODO: is this valid way to check is only first value is checked
				return option.argValues;
			} else {
				return option.argDefaultValues;
			}
		}
	}

	VERSO_OBJECTNOTFOUND("verso-base", "Cannot find given option", optionName.c_str());
}


//	std::vector<UString> Options::getOptionValues(const Option& option) const
//	{
//	}


const std::vector<Option>& Options::getOptions() const
{
	return options;
}


std::vector<Option> Options::getRequiredOptions() const
{
	std::vector<Option> required;
	for (auto& option : options) {
		if (option.required == true) {
			required.push_back(option);
		}
	}
	return required;
}


} // End namespace Verso

