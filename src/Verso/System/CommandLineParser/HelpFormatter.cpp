#include <Verso/System/CommandLineParser/HelpFormatter.hpp>

namespace Verso {


void HelpFormatter::printHelp(const UString& binaryName, const Options& options) const
{
	std::cout << generateString(binaryName, options) << std::endl;
}


UString HelpFormatter::generateString(const UString& binaryName, const Options& options) const
{
	UString output("usage: ");
	output.append2(binaryName);
	auto requiredOptions = options.getRequiredOptions();
	for (auto& option : requiredOptions) {
		output.append2(" -");
		output.append2(option.opt);
		if (option.hasArgs == true) {
			for (auto& argName : option.argNames) {
				output.append2(" <");
				output.append2(argName);
				output.append2(">");
			}
		}
	}
	output.append2("\n");

	size_t longestOption = 0;
	for (auto& option : options.getOptions()) {
		size_t result = generateOptionString(option).size();
		if (result > longestOption) {
			longestOption = result;
		}
	}

	for (auto& option : options.getOptions()) {
		UString optionStr = generateOptionString(option);
		output.append2(optionStr);

		size_t spacing = 3 + longestOption - optionStr.size();
		for (size_t i=0; i<spacing; ++i) {
			output.append2(" ");
		}

		output.append2(option.description);

		if (option.argDefaultValues.size() > 0 && option.argDefaultValues[0].size() != 0) {
			output.append2(" (default:");
			for (auto& defaultValue : option.argDefaultValues) {
				output.append(" ");
				output.append2(defaultValue);
			}
			output.append2(")");
		}

		output.append2("\n");
	}
	output.append2("\n");

	return output;
}


///////////////////////////////////////////////////////////////////////////////////////////
// Private
///////////////////////////////////////////////////////////////////////////////////////////

UString HelpFormatter::generateOptionString(const Option& option) const
{
	UString output(" -");
	output.append2(option.opt);

	if (option.longOpt.size() != 0) {
		output.append2(",--");
		output.append2(option.longOpt);
	}

	if (option.hasArgs == true) {
		for (auto& argName : option.argNames) {
			output.append2(" <");
			output.append2(argName);
			output.append2(">");
		}
	}

	return output;
}


} // End namespace Verso

