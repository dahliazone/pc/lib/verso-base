char FileOsx_symbol = 0;

#if __APPLE__ && __MACH__

#include <Verso/System/File.hpp>
#include <Verso/System/osx/FileOsx.hpp>
#include <Verso/System/Exception.hpp>

#include <CoreFoundation/CoreFoundation.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <libgen.h>

namespace Verso {
namespace priv {


UString FileOsx::readSymLink(const UString& symLink)
{
	return symLink;
}


UString FileOsx::getBasePath()
{
	// Get the main app bundle
	CFBundleRef mainBundle = CFBundleGetMainBundle();

	// Absolute URL for the bundle directory
	CFURLRef bundleAbsUrl = CFBundleCopyBundleURL(mainBundle);

	// Relative URL for resources directory
	CFURLRef resourcesRelUrl = CFBundleCopyResourcesDirectoryURL(mainBundle);

	CFStringEncoding encoding = kCFStringEncodingUTF8;

	// Convert bundleAbsUrl to C string
	CFStringRef bundleAbsPath = CFURLCopyFileSystemPath(bundleAbsUrl, kCFURLPOSIXPathStyle);
	CFIndex length = CFStringGetLength(bundleAbsPath);
	CFIndex maxSize = CFStringGetMaximumSizeForEncoding(length, encoding);
	char* bundleAbsPathC = new char[maxSize + 1]; // extra char \0
	CFStringGetCString(bundleAbsPath, bundleAbsPathC, maxSize, encoding);
	CFRelease(bundleAbsUrl); bundleAbsUrl = nullptr;
	CFRelease(bundleAbsPath); bundleAbsPath = nullptr;

	// Convert resourcesRelUrl to C string
	CFStringRef resourcesRelPath = CFURLCopyFileSystemPath(resourcesRelUrl, kCFURLPOSIXPathStyle);
	length = CFStringGetLength(resourcesRelPath);
	maxSize = CFStringGetMaximumSizeForEncoding(length, encoding);
	char* resourcesRelPathC = new char[maxSize + 1]; // extra char \0
	CFStringGetCString(resourcesRelPath, resourcesRelPathC, maxSize, encoding);
	CFRelease(resourcesRelUrl); resourcesRelUrl = nullptr;
	CFRelease(resourcesRelPath); resourcesRelPath = nullptr;

	// Combine result
	UString result(bundleAbsPathC);
	result.append2('/');
	result.append2(resourcesRelPathC);
	result.append2('/');

	// Free temporary C strings
	free(resourcesRelPathC); resourcesRelPathC = nullptr;
	free(bundleAbsPathC); bundleAbsPathC = nullptr;

	return result;
}


UString FileOsx::getPreferencesPath()
{
	return getBasePath();
}


UString FileOsx::getFileNamePath(const UString& fileName)
{
	// Create temporary string because some implementations of dirname will write to given input string
	VERSO_ASSERT("verso-base", FILENAME_MAX >= fileName.size());
	char tmp[FILENAME_MAX];
	snprintf(tmp, sizeof(tmp), "%s", fileName.c_str());
	UString tmp2(dirname(tmp));
	tmp2.append("/");
	return tmp2;
}


bool FileOsx::mkdir(const UString& fileName)
{
	if (::mkdir(fileName.c_str(), ACCESSPERMS) == 0)
		return true;
	else
		return false;
}


void FileOsx::ls(const UString& directoryName, bool showFiles, bool showDirectories, bool showDotFiles, std::vector<UString>& out)
{
	DIR* dir = opendir(directoryName.c_str());
	if (dir == nullptr) {
		VERSO_FILENOTFOUND("verso-base", "Cannot open directory", directoryName.c_str());
	}

	struct dirent* ent;
	struct stat st;
	while ((ent = readdir(dir)) != nullptr) {
		UString fileName = ent->d_name;

		// Hide files beginning with '.'
		if (showDotFiles == false && fileName[0] == '.') {
			continue;
		}

		UString fullFileName = directoryName;
		fullFileName += "/";
		fullFileName += fileName;
		if (stat(fullFileName.c_str(), &st) == -1) {
			continue;
		}

		bool isDirectory = (st.st_mode & S_IFDIR) != 0;

		// Hide files if requested not to be shown
		if (showFiles == false && isDirectory == false) {
			continue;
		}

		// Hide directories if requested not to be shown
		if (showDirectories == false && isDirectory == true) {
			continue;
		}

		out.push_back(fileName);
	}

	closedir(dir);
}


int64_t FileOsx::getSize(const char* fileName)
{
	bool TODO_stat_based_64_bit_file_size;
	//struct __stat64 stat_buf;
	//int rc = _stat64(fileName, &stat_buf);
	//return rc == 0 ? stat_buf.st_size : -1;

	std::ifstream in(fileName, std::ifstream::ate | std::ifstream::binary);
	std::ifstream::pos_type streamPos = in.tellg();
	if (streamPos == std::ifstream::pos_type(-1)) {
		UString message("Cannot get size of non-existing file: \"");
		message.append(fileName);
		message += "\"";
		VERSO_FILENOTFOUND("verso-base", message.c_str(), fileName);
	}
	in.close();
	return static_cast<int64_t>(streamPos);
}


} // End namespace priv
} // End namespace Verso


#endif // End #ifdef __APPLE__ && __MACH__


