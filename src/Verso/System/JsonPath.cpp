#include <Verso/System/Exception.hpp>
#include <Verso/System/JsonHelper.hpp>
#include <Verso/System/JsonPath.hpp>

namespace Verso {


JSONObject emptyJsonObject;
JSONArray emptyJsonArray;


JsonPath::JsonPath() :
	value(),
	currentPath(),
	fileName()
{
}


JsonPath::JsonPath(JSONValue* value, const UString& currentPath, const UString& fileName) :
	value(value),
	currentPath(currentPath),
	fileName(fileName)
{
}


JsonPath::JsonPath(const JsonPath& original) :
	value(original.value),
	currentPath(original.currentPath),
	fileName(original.fileName)
{
}


JsonPath::JsonPath(const JsonPath&& original) noexcept :
	value(std::move(original.value)),
	currentPath(std::move(original.currentPath)),
	fileName(std::move(original.fileName))
{
}


JsonPath& JsonPath::operator =(const JsonPath& original)
{
	if (this != &original) {
		value = original.value;
		currentPath = original.currentPath;
		fileName = original.fileName;
	}
	return *this;
}


JsonPath& JsonPath::operator =(JsonPath&& original) noexcept
{
	if (this != &original) {
		value = std::move(original.value);
		currentPath = std::move(original.currentPath);
		fileName = std::move(original.fileName);
	}
	return *this;
}


JsonPath::~JsonPath()
= default;


bool JsonPath::isNullOrEmpty() const
{
	if (value == nullptr
			|| value->IsNull()
			|| (value->IsObject() == true && value->AsObject().empty())
			|| (value->IsArray() == true && value->AsArray().empty())) {
		return true;
	}
	else {
		return false;
	}
}


JsonPath JsonPath::getAttribute(const UString& name, bool required) const
{
	if (name.isEmpty()) {
		return *this;
	}

	UString pathRead(currentPath + "." + name);

	if (value == nullptr) {
		if (required == true) {
			UString error("Cannot get required attribute \"" + currentPath + "." + name + "\"!");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
		}
		else {
			return { nullptr, pathRead, fileName };
		}
	}

	JSONValue* valueRead = value->Child(name.toUtf8Wstring().c_str());
	if (required && valueRead == nullptr) {
		UString error("Cannot read required field \"" +pathRead + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	return { valueRead, pathRead, fileName };
}


const JSONObject& JsonPath::asObject(const UString& name) const
{
	JSONValue* newValue = readValue(name);
	if (newValue == nullptr) {
		UString error("Expected object but current JsonPath is null at \"" + currentPath + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	if (!newValue->IsObject()) {
		UString error("Expected object but current JsonPath is not of type object!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	return newValue->AsObject();
}


JsonPathArray JsonPath::asArray(const UString& name, int minItemCount, int maxItemCount) const
{
	UString lengthError("Expected array ");
	if (minItemCount > -1 && maxItemCount > -1) {
		lengthError += "of length ";
		lengthError.append2(minItemCount);
		lengthError += " to ";
		lengthError.append2(maxItemCount);
		lengthError += " items at \"" + currentPath + "\" ";
	}
	else if (maxItemCount > -1) {
		lengthError += "of minimum length ";
		lengthError.append2(minItemCount);
		lengthError += " items at \"" + currentPath + "\" ";
	}
	else if (minItemCount > -1) {
		lengthError += "of maximum length ";
		lengthError.append2(maxItemCount);
		lengthError += " items at \"" + currentPath + "\" ";
	}

	if (!isArray(name)) {
		UString error(lengthError + "but it's not an array!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	JsonPathArray pathArray;
	pathArray.path = getAttribute(name, true);
	int arraySize = static_cast<int>(pathArray.path.value->AsArray().size());
	if (minItemCount > -1 && arraySize < minItemCount) {
		UString error(lengthError + "but has ");
		error.append2(arraySize);
		error += " items!";
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}
	if (maxItemCount > -1 && arraySize > maxItemCount) {
		UString error(lengthError + "but has ");
		error.append2(arraySize);
		error += " items!";
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	size_t i = 0;
	const JSONArray& arr = pathArray.path.value->AsArray();
	for (auto& item : arr) {
		UString itemPath(pathArray.path.currentPath + "[");
		itemPath.append2(i);
		itemPath += "]";
		pathArray.array.push_back({ item, itemPath, fileName });
		i++;
	}
	return pathArray;
}


JSONValue* JsonPath::readValue(const UString& name) const
{
	if (name.isEmpty()) {
		return value;
	}

	if (value == nullptr) {
		UString error("Cannot check attribute \"" + name + "\" from NULL JsonPath at \"" + currentPath + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	return value->Child(name.toUtf8Wstring().c_str());
}


bool JsonPath::isDefined(const UString& name) const
{
	if (readValue(name)) {
		return true;
	}
	else {
		return false;
	}
}


bool JsonPath::isNull(const UString& name) const
{
	JSONValue* newValue = readValue(name);
	if (newValue && newValue->IsNull()) {
		return true;
	}
	else {
		return false;
	}
}


// object

bool JsonPath::isObject(const UString& name) const
{
	JSONValue* newValue = readValue(name);
	if (newValue && newValue->IsObject()) {
		return true;
	}
	else {
		return false;
	}
}


const JSONObject& JsonPath::readObject(const UString& name, bool required) const
{
	JSONValue* newValue = readValue(name);
	if (newValue && newValue->IsObject()) {
		return newValue->AsObject();
	}
	if (required == true) {
		UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}
	else {
		return emptyJsonObject;
	}
}



JSONValue* JsonPath::writeObject(JSONObject& jsonObject)
{
	return new JSONValue(jsonObject);
}


// array

bool JsonPath::isArray(const UString& name) const
{
	JSONValue* newValue = readValue(name);
	if (newValue && newValue->IsArray()) {
		return true;
	}
	else {
		return false;
	}
}


const JSONArray& JsonPath::readArray(const UString& name, bool required) const
{
	JSONValue* newValue = readValue(name);
	if (newValue && newValue->IsArray()) {
		return newValue->AsArray();
	}
	if (required == true) {
		UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}
	else {
		return emptyJsonArray;
	}
}


JSONValue* JsonPath::writeArray(JSONArray& jsonArray)
{
	return new JSONValue(jsonArray);
}


// bool

bool JsonPath::isBool(const UString& name) const
{
	JSONValue* newValue = readValue(name);
	if (newValue && newValue->IsBool()) {
		return true;
	}
	else {
		return false;
	}
}


bool JsonPath::readBool(const UString& name, bool required, bool defaultValue) const
{
	JSONValue* newValue = readValue(name);
	if (newValue && newValue->IsBool()) {
		return newValue->AsBool();
	}
	if (required == true) {
		UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}
	else {
		return defaultValue;
	}
}


JSONValue* JsonPath::writeBool(bool value)
{
	return new JSONValue(value);
}


// int

bool JsonPath::isInt(const UString& name, bool checkForDecimalZero) const
{
	JSONValue* newValue = readValue(name);
	if (newValue && newValue->IsNumber()) {
		double valueNumber = newValue->AsNumber();
		if (checkForDecimalZero == false) {
			return true;
		}
		else if (valueNumber - floor(valueNumber) == 0.0) {
			return true;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}


int JsonPath::readInt(const UString& name, bool required, int defaultValue) const
{
	JSONValue* newValue = readValue(name);
	if (newValue && newValue->IsNumber()) {
		double valueNumber = newValue->AsNumber();
		if (valueNumber - floor(valueNumber) != 0.0) {
			UString warn("Field \"" + currentPath + "." + name + "\" should be integer but instead is ");
			warn.append2(valueNumber);
			warn += " floored to ";
			warn.append2(floor(valueNumber));
			warn += ".";
			VERSO_LOG_WARN("verso-base", warn.c_str());
			valueNumber = floor(valueNumber);
		}

		return static_cast<int>(valueNumber);
	}
	if (required == true) {
		UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}
	else {
		return defaultValue;
	}
}


JSONValue* JsonPath::writeInt(int value)
{
	return new JSONValue(value);
}


// number

bool JsonPath::isNumber(const UString& name) const
{
	JSONValue* newValue = readValue(name);
	if (newValue && newValue->IsNumber()) {
		return true;
	}
	else {
		return false;
	}
}


double JsonPath::readNumberd(const UString& name, bool required, double defaultValue) const
{
	JSONValue* newValue = readValue(name);
	if (newValue && newValue->IsNumber()) {
		return newValue->AsNumber();
	}
	if (required == true) {
		UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}
	else {
		return defaultValue;
	}
}


JSONValue* JsonPath::writeNumberd(double value)
{
	return new JSONValue(value);
}


float JsonPath::readNumberf(const UString& name, bool required, float defaultValue) const
{
	JSONValue* newValue = readValue(name);
	if (newValue && newValue->IsNumber()) {
		return static_cast<float>(newValue->AsNumber());
	}
	if (required == true) {
		UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}
	else {
		return defaultValue;
	}
}


JSONValue* JsonPath::writeNumberf(float value)
{
	return new JSONValue(static_cast<double>(value));
}


std::vector<float> JsonPath::readNumberfArray(const UString& name, bool required, const std::vector<float>& defaultValue) const
{
	const JSONArray& numberArr = readArray(name, required);
	if (numberArr.size() <= 0) {
		if (required == false) {
			return defaultValue;
		}
		else {
			UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
		}
	}

	std::vector<float> numbers;
	for (auto& n : numberArr) {
		if (n->IsNumber()) {
			numbers.push_back(static_cast<float>(n->AsNumber()));
		}
		else {
			UString error("Invalid type of values in field \"" + currentPath + "." + name + "\"! Array must contain only number values.");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
		}
	}

	return numbers;
}


int JsonPath::readNumberi(const UString& name, bool required, int defaultValue) const
{
	JSONValue* newValue = readValue(name);
	if (newValue && newValue->IsNumber()) {
		return static_cast<int>(newValue->AsNumber());
	}
	if (required == true) {
		UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}
	else {
		return defaultValue;
	}
}


JSONValue* JsonPath::writeNumberi(int value)
{
	return new JSONValue(value);
}


unsigned int JsonPath::readNumberu(const UString& name, bool required, unsigned int defaultValue) const
{
	JSONValue* newValue = readValue(name);
	if (newValue && newValue->IsNumber()) {
		return static_cast<unsigned int>(newValue->AsNumber());
	}
	if (required == true) {
		UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}
	else {
		return defaultValue;
	}
}


JSONValue* JsonPath::writeNumberu(unsigned int value)
{
	return new JSONValue(static_cast<int>(value));
}


size_t JsonPath::readNumbersz(const UString& name, bool required, size_t defaultValue) const
{
	JSONValue* newValue = readValue(name);
	if (newValue && newValue->IsNumber()) {
		return static_cast<size_t>(newValue->AsNumber());
	}
	if (required == true) {
		UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}
	else {
		return defaultValue;
	}
}


JSONValue* JsonPath::writeNumbersz(size_t value)
{
	return new JSONValue(static_cast<int>(value));
}


// string

bool JsonPath::isString(const UString& name) const
{
	JSONValue* newValue = readValue(name);
	if (newValue && newValue->IsString()) {
		return true;
	}
	else {
		return false;
	}
}


UString JsonPath::readString(const UString& name, bool required, const UString& defaultValue) const
{
	JSONValue* newValue = readValue(name);
	if (newValue && newValue->IsString()) {
		return newValue->AsString();
	}
	if (required == true) {
		UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}
	else {
		return defaultValue;
	}
}


JSONValue* JsonPath::writeString(const UString& str)
{
	return new JSONValue(str.toUtf8Wstring());
}


UString JsonPath::readStringOrArrayOfStrings(const UString& name, bool required, const UString& defaultValue) const
{
	if (isArray(name)) {
		UString text;
		JSONArray textParts = readArray(name, required);
		if (!textParts.empty()) {
			for (auto& line : textParts) {
				if (line && line->IsString()) {
					text.append(line->AsString());
				}
			}
		}
		return text;
	}
	else {
		return readString(name, required, defaultValue);
	}
}


// Align

Align JsonPath::readAlign(const UString& name, bool required, const Align& defaultValue) const
{
	if (isObject(name)) {
		JsonPath alignPath = getAttribute(name, required);
		HAlign hAlign = defaultValue.hAlign;
		UString horizontal = alignPath.readString("horizontal");
		if (!horizontal.isEmpty()) {
			hAlign = stringToHAlign(horizontal);
			if (hAlign == HAlign::Undefined) {
				UString error("Unknown value (" + horizontal + ") in field \"" + alignPath.currentPath + R"(.horizontal"! Must be one of: "Left", "Center", "Right".)");
				VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
			}
		}

		VAlign vAlign = defaultValue.vAlign;
		UString vertical = alignPath.readString("vertical");
		if (!vertical.isEmpty()) {
			vAlign = stringToVAlign(vertical);
			if (vAlign == VAlign::Undefined) {
				UString error("Unknown value (" + vertical + ") in field \"" + alignPath.currentPath + R"(.vertical"! Must be one of: "Top", "Center", "Bottom".)");
				VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
			}
		}

		return { hAlign, vAlign };
	}
	else if (required == true) {
		UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}
	else {
		return defaultValue;
	}
}


// RefScale

RefScale JsonPath::readRefScale(const UString& name, bool required,  const RefScale& defaultValue) const
{
	UString refScaleStr = readString(name, required, "");
	if (!refScaleStr.isEmpty()) {
		RefScale refScale = stringToRefScale(refScaleStr);
		if (refScale == RefScale::Unset) {
			UString error("Unknown value (" + refScaleStr + ") in field \"" + currentPath + "." + name);
			error.append(R"("! Must be one of: "ImageSize", "ImageSize_KeepAspectRatio_FitRect", "ImageSize_KeepAspectRatio_FromX", "ImageSize_KeepAspectRatio_FromY", )");
			error.append(R"("ViewportSize", "ViewportSize_KeepAspectRatio_FitRect", "ViewportSize_KeepAspectRatio_FromX", "ViewportSize_KeepAspectRatio_FromY".)");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
		}
		return refScale;
	}
	else if (required == true) {
		UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	return defaultValue;
}


// color

bool JsonPath::isColor(const UString& name) const
{
	if (isArray(name)) {
		const JSONArray& vectorArr = readArray(name, false);
		if (vectorArr.size() == 3 &&
			vectorArr[0]->IsNumber() &&
			vectorArr[1]->IsNumber() &&
			vectorArr[2]->IsNumber())
		{
			return true;
		}
		else if (vectorArr.size() == 4 &&
			vectorArr[0]->IsNumber() &&
			vectorArr[1]->IsNumber() &&
			vectorArr[2]->IsNumber() &&
			vectorArr[3]->IsNumber())
		{
			return true;
		}
	}

	return false;
}


RgbaColorf JsonPath::readColor(const UString& name, bool required, const RgbaColorf& defaultValue) const
{
	const JSONArray& colorArr = readArray(name, required);
	if (colorArr.empty()) {
		if (required == false) {
			return defaultValue;
		}
		else {
			UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
		}
	}
	if (colorArr.size() != 3 && colorArr.size() != 4) {
		UString error("Invalid type of values in field \"" + currentPath + "." + name + "\"! RgbaColorf array must contain 3-4 float values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	bool typeError = false;
	float r = 0.0f, g = 0.0f, b = 0.0f, a = 1.0f;
	if (colorArr[0]->IsNumber()) {
		r = static_cast<float>(colorArr[0]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (colorArr[1]->IsNumber()) {
		g = static_cast<float>(colorArr[1]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (colorArr[2]->IsNumber()) {
		b = static_cast<float>(colorArr[2]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (colorArr.size() == 4){
		if (colorArr[3]->IsNumber()) {
			a = static_cast<float>(colorArr[3]->AsNumber());
		}
		else {
			typeError = true;
		}
	}

	if (typeError == true) {
		UString error("Invalid type of values in field \"" + currentPath + "." + name + "\"! RgbaColorf array can only contain number values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	return { r, g, b, a };
}


JSONValue* JsonPath::writeColor(const RgbaColorf& color, bool useAlpha)
{
	JSONArray array;
	array.push_back(writeNumberf(color.r));
	array.push_back(writeNumberf(color.g));
	array.push_back(writeNumberf(color.b));
	if (useAlpha) {
		array.push_back(writeNumberf(color.a));
	}
	return new JSONValue(array);
}


// Range

Rangei JsonPath::readRangei(const UString& name, bool required, const Rangei& defaultValue) const
{
	const JSONArray& rangeArr = readArray(name, required);
	if (rangeArr.empty()) {
		if (required == false) {
			return defaultValue;
		}
		else {
			UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
		}
	}
	if (rangeArr.size() != 2) {
		UString error("Invalid type of values in field \"" + currentPath + "." + name + "\"! Rangei array must contain 2 int values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	bool typeError = false;
	int a = 0, b = 0;
	if (rangeArr[0]->IsNumber()) {
		a = static_cast<int>(rangeArr[0]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (rangeArr[1]->IsNumber()) {
		b = static_cast<int>(rangeArr[1]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (typeError == true) {
		UString error("Invalid type of values in field \"" + currentPath + "." + name + "\"! Rangei array can only contain number values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	return{ a, b };
}


JSONValue* JsonPath::writeRangei(const Rangei& range)
{
	JSONArray array;
	array.push_back(writeNumberi(range.minValue));
	array.push_back(writeNumberi(range.maxValue));
	return new JSONValue(array);
}


Rangeu JsonPath::readRangeu(const UString& name, bool required, const Rangeu& defaultValue) const
{
	const JSONArray& rangeArr = readArray(name, required);
	if (rangeArr.empty()) {
		if (required == false) {
			return defaultValue;
		}
		else {
			UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
		}
	}
	if (rangeArr.size() != 2) {
		UString error("Invalid type of values in field \"" + currentPath + "." + name + "\"! Rangeu array must contain 2 unsigned int values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	bool typeError = false;
	bool signedError = false;
	unsigned int a = 0, b = 0;
	if (rangeArr[0]->IsNumber()) {
		if (rangeArr[0]->AsNumber() < 0.0) {
			signedError = true;
		}
		else {
			a = static_cast<unsigned int>(rangeArr[0]->AsNumber());
		}
	}
	else {
		typeError = true;
	}

	if (rangeArr[1]->IsNumber()) {
		if (rangeArr[1]->AsNumber() < 0.0) {
			signedError = true;
		}
		else {
			b = static_cast<unsigned int>(rangeArr[1]->AsNumber());
		}

	}
	else {
		typeError = true;
	}

	if (typeError == true) {
		UString error("Invalid type of values in field \"" + currentPath + "." + name + "\"! Rangeu array can only contain number values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	if (signedError == true) {
		UString error("Invalid type of values in field \"" + currentPath + "." + name + "\"! Rangeu array can only contain unsigned (>=0) values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	return Rangeu(a, b);
}


JSONValue* JsonPath::writeRangeu(const Rangeu& range)
{
	JSONArray array;
	array.push_back(writeNumberu(range.minValue));
	array.push_back(writeNumberu(range.maxValue));
	return new JSONValue(array);
}


Rangef JsonPath::readRangef(const UString& name, bool required, const Rangef& defaultValue) const {
	const JSONArray& rangeArr = readArray(name, required);
	if (rangeArr.empty()) {
		if (required == false) {
			return defaultValue;
		}
		else {
			UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
		}
	}
	if (rangeArr.size() != 2) {
		UString error("Invalid type of values in field \"" + currentPath + "." + name + "\"! Rangef array must contain 2 float values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	bool typeError = false;
	float a = 0.0f, b = 0.0f;
	if (rangeArr[0]->IsNumber()) {
		a = static_cast<float>(rangeArr[0]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (rangeArr[1]->IsNumber()) {
		b = static_cast<float>(rangeArr[1]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (typeError == true) {
		UString error("Invalid type of values in field \"" + currentPath + "." + name + "\"! Rangef array can only contain number values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	return{ a, b };
}


JSONValue* JsonPath::writeRangef(const Rangef& range)
{
	JSONArray array;
	array.push_back(writeNumberf(range.minValue));
	array.push_back(writeNumberf(range.maxValue));
	return new JSONValue(array);
}


Ranged JsonPath::readRanged(const UString& name, bool required, const Ranged& defaultValue) const
{
	const JSONArray& rangeArr = readArray(name, required);
	if (rangeArr.empty()) {
		if (required == false) {
			return defaultValue;
		}
		else {
			UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
	}
	if (rangeArr.size() != 2) {
		UString error("Invalid type of values in field \"" + currentPath + "." + name + "\"! Ranged array must contain 2 double values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	bool typeError = false;
	double a = 0.0, b = 0.0;
	if (rangeArr[0]->IsNumber()) {
		a = rangeArr[0]->AsNumber();
	}
	else {
		typeError = true;
	}

	if (rangeArr[1]->IsNumber()) {
		b = rangeArr[1]->AsNumber();
	}
	else {
		typeError = true;
	}

	if (typeError == true) {
		UString error("Invalid type of values in field \"" + currentPath + "." + name + "\"! Ranged array can only contain number values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	return Ranged(a, b);
}


JSONValue* JsonPath::writeRanged(const Ranged& range)
{
	JSONArray array;
	array.push_back(writeNumberd(range.minValue));
	array.push_back(writeNumberd(range.maxValue));
	return new JSONValue(array);
}


// Rectf

Rectf JsonPath::readRectf(const UString& name, bool required, const Rectf& defaultValue) const
{
	JsonPath rectPath = getAttribute(name, required);
	if (!rectPath.isNullOrEmpty()) {
		float x = rectPath.readNumberf("x", false);
		float y = rectPath.readNumberf("y", false);
		float width = rectPath.readNumberf("width", false);
		float height = rectPath.readNumberf("height", false);

		return { x, y, width, height };
	}
	else {
		return defaultValue;
	}
}


// Vector2

Vector2f JsonPath::readVector2f(const UString& name, bool required, const Vector2f& defaultValue) const
{
	JsonPath vecPath = getAttribute(name, required);
	if (!vecPath.isNullOrEmpty()) {
		float x = vecPath.readNumberf("x", false);
		float y = vecPath.readNumberf("y", false);

		return Vector2f(x, y);
	}
	else {
		return defaultValue;
	}
}


bool JsonPath::isVector2fArrayFormat(const UString& name) const
{
	if (isArray(name)) {
		const JSONArray& vectorArr = readArray(name, false);
		if (vectorArr.size() == 2 &&
			vectorArr[0]->IsNumber() &&
			vectorArr[1]->IsNumber())
		{
			return true;
		}
	}

	return false;
}


Vector2f JsonPath::readVector2fArrayFormat(const UString& name, bool required, const Vector2f& defaultValue) const
{
	if (readValue(name) == nullptr) {
		return defaultValue;
	}

	const JSONArray& vectorArr = readArray(name, required);
	if (vectorArr.empty()) {
		if (required == false) {
			UString error("Cannot read optional field \"" + currentPath + "." + name + "\": Must be of type [x, y]");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
		}
		else {
			UString error("Cannot read required field \"" + currentPath + "." + name + "\": Must be of type [x, y]");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
		}
	}
	if (vectorArr.size() != 2) {
		UString error("Invalid type of values in field \"" + currentPath + "." + name + "\": Must be of type [x, y]");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	bool typeError = false;
	float x = 0.0f, y = 0.0f;
	if (vectorArr[0]->IsNumber()) {
		x = static_cast<float>(vectorArr[0]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (vectorArr[1]->IsNumber()) {
		y = static_cast<float>(vectorArr[1]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (typeError == true) {
		UString error("Invalid type of values in field \"" + currentPath + "." + name + "\"! Vector2f array must contain only number values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	return Vector2f(x, y);
}


Vector2i JsonPath::readVector2iArrayFormat(const UString& name, bool required, const Vector2i& defaultValue) const
{
	if (readValue(name) == nullptr) {
		return defaultValue;
	}

	const JSONArray& vectorArr = readArray(name, required);
	if (vectorArr.size() <= 0) {
		if (required == false) {
			UString error("Cannot read optional field \"" + currentPath + "." + name + "\": Must be of type [x, y]");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
		}
		else {
			UString error("Cannot read required field \"" + currentPath + "." + name + "\": Must be of type [x, y]");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
		}
	}
	if (vectorArr.size() != 2) {
		UString error("Invalid type of values in field \"" + currentPath + "." + name + "\": Must be of type [x, y]");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	bool typeError = false;
	float x = 0, y = 0;
	if (vectorArr[0]->IsNumber()) {
		x = static_cast<float>(vectorArr[0]->AsNumber());
		if (x - floorf(x) != 0.0f) {
			UString warn("Field \"" + currentPath + "." + name + "[0]\" should be integer but instead is ");
			warn.append2(x);
			warn += " floored to ";
			warn.append2(floorf(x));
			warn += ".";
			VERSO_LOG_WARN("verso-base", warn.c_str());
			x = floorf(x);
		}
	}
	else {
		typeError = true;
	}

	if (vectorArr[1]->IsNumber()) {
		y = static_cast<float>(vectorArr[1]->AsNumber());
		if (y - floorf(y) != 0.0f) {
			UString warn("Field \"" + currentPath + "." + name + "[1]\" should be integer but instead is ");
			warn.append2(y);
			warn += " floored to ";
			warn.append2(floorf(y));
			warn += ".";
			VERSO_LOG_WARN("verso-base", warn.c_str());
			y = floorf(y);
		}
	}
	else {
		typeError = true;
	}

	if (typeError == true) {
		UString error("Invalid type of values in field \"" + currentPath + "." + name + "\"! Vector2f array must contain only number values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	return Vector2i(static_cast<int>(x), static_cast<int>(y));
}


Vector2i JsonPath::readVector2i(const UString& name, bool required, const Vector2i& defaultValue) const
{
	JsonPath vecPath = getAttribute(name, required);
	if (!vecPath.isNullOrEmpty()) {
		float x = vecPath.readNumberf("x", false);
		float y = vecPath.readNumberf("y", false);

		if (x - floorf(x) != 0.0f) {
			UString warn("Field \"" + currentPath + "." + name + ".x\" should be integer but instead is ");
			warn.append2(x);
			warn += " floored to ";
			warn.append2(floorf(x));
			warn += ".";
			VERSO_LOG_WARN("verso-base", warn.c_str());
			x = floorf(x);
		}

		if (y - floorf(y) != 0.0f) {
			UString warn("Field \"" + currentPath + "." + name + ".y\" should be integer but instead is ");
			warn.append2(y);
			warn += " floored to ";
			warn.append2(floorf(y));
			warn += ".";
			VERSO_LOG_WARN("verso-base", warn.c_str());
			y = floorf(y);
		}

		return Vector2i(static_cast<int>(x), static_cast<int>(y));
	}
	else {
		return defaultValue;
	}
}


// Vector3

Vector3f JsonPath::readVector3f(const UString& name, bool required, const Vector3f& defaultValue) const
{
	JsonPath vecPath = getAttribute(name, required);
	if (!vecPath.isNullOrEmpty()) {
		float x = vecPath.readNumberf("x", false);
		float y = vecPath.readNumberf("y", false);
		float z = vecPath.readNumberf("z", false);

		return Vector3f(x, y, z);
	}
	else {
		return defaultValue;
	}
}


bool JsonPath::isVector3fArrayFormat(const UString& name) const
{
	if (isArray(name)) {
		const JSONArray& vectorArr = readArray(name, false);
		if (vectorArr.size() == 3 &&
			vectorArr[0]->IsNumber() &&
			vectorArr[1]->IsNumber() &&
			vectorArr[2]->IsNumber())
		{
			return true;
		}
	}

	return false;
}


Vector3f JsonPath::readVector3fArrayFormat(const UString& name, bool required, const Vector3f& defaultValue) const
{
	if (readValue(name) == nullptr) {
		return defaultValue;
	}

	const JSONArray& vectorArr = readArray(name, required);
	if (vectorArr.size() <= 0) {
		if (required == false) {
			UString error("Cannot read value for optional field \"" + currentPath + "." + name + "\": must be of type [x, y, z]");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
		else {
			UString error("Cannot read required field \"" + currentPath + "." + name + "\":  must be of type [x, y, z]");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
	}
	if (vectorArr.size() != 3) {
		UString error("Invalid type of values in field \"" + currentPath + "." + name + "\": must be of type [x, y, z]");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	bool typeError = false;
	float x = 0.0f, y = 0.0f, z = 0.0f;
	if (vectorArr[0]->IsNumber()) {
		x = static_cast<float>(vectorArr[0]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (vectorArr[1]->IsNumber()) {
		y = static_cast<float>(vectorArr[1]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (vectorArr[2]->IsNumber()) {
		z = static_cast<float>(vectorArr[2]->AsNumber());
	}
	else {
		typeError = true;
	}

	if (typeError == true) {
		UString error("Invalid type of values in field \"" + currentPath + "." + name + "\"! Vector3f array must contain only number values.");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	return Vector3f(x, y, z);
}


// AspectRatio

AspectRatio JsonPath::readAspectRatio(const UString& name, bool required, const AspectRatio& defaultValue) const
{
	UString aspectRatio = readString(name, required, "");
	if (aspectRatio.isEmpty()) {
		return defaultValue;
	}
	else {
		return AspectRatio(aspectRatio);
	}
}


// PixelAspectRatio

PixelAspectRatio JsonPath::readPixelAspectRatio(const UString& name, bool required, const PixelAspectRatio& defaultValue) const
{
	UString pixelAspectRatio = readString(name, required, "");
	if (pixelAspectRatio.isEmpty()) {
		return defaultValue;
	}
	else {
		return PixelAspectRatio(pixelAspectRatio);
	}
}


// EasingType

EasingType JsonPath::readEasingType(const UString& name, bool required, const EasingType& defaultValue) const
{
	UString easingTypeStr = readString(name, required, "");
	if (!easingTypeStr.isEmpty()) {
		EasingType easingType = stringToEasingType(easingTypeStr);
		if (easingType == EasingType::Undefined) {
			UString error("Unknown value (" + easingTypeStr + ") in field \"" + currentPath + "." + name + "\"! Must be one of: \"Linear\", \"EaseIn\", \"EaseOut\", \"EaseInOut\", \"QuadraticIn\", \"QuadraticOut\", \"QuadraticInOut\".");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
		return easingType;
	}
	else if (required == true) {
		UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	return defaultValue;
}


// InterpolationType

InterpolationType JsonPath::readInterpolationType(const UString& name, bool required, const InterpolationType& defaultValue) const
{
	UString interpolationTypeStr = readString(name, required, "");
	if (!interpolationTypeStr.isEmpty()) {
		InterpolationType interpolationType = stringToInterpolationType(interpolationTypeStr);
		if (interpolationType == InterpolationType::Undefined) {
			UString error("Unknown value (" + interpolationTypeStr + ") in field \"" + currentPath + "." + name + "\"! Must be one of: \"Constant\", \"Linear\", \"QuadraticBezier\", \"CubicBezier\", \"QuinticBezier\", \"CubicHermiteSpline\".");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
		}
		return interpolationType;
	}
	else if (required == true) {
		UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	return defaultValue;
}


// Keyframes

IntKeyframes JsonPath::readIntKeyframes(const UString& name, bool required, const IntKeyframes& defaultValue) const
{
	if (isNumber(name)) {
		return IntKeyframes(readNumberi(name, required));
	}

	else if (isObject(name)) {
		JsonPath keyframesPath = getAttribute(name, required);

		IntKeyframes keyframes;
		keyframes.easingType = keyframesPath.readEasingType("easingType");
		keyframes.interpolationType = keyframesPath.readInterpolationType("interpolationType");
		keyframes.looping = keyframesPath.readBool("looping", false, false);

		if (keyframesPath.isArray("keyframes")) {
			JsonPathArray keyframesPathArray = keyframesPath.asArray("keyframes");

			if (!keyframesPathArray.array.empty()) {
				// Check that there's at least 2 values
				if (keyframesPathArray.array.size() < 2) {
					UString error("Invalid data in field \"" + keyframesPathArray.path.currentPath + "[]\"! IntKeyframes array must contain at least two entries.");
					VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
				}

				for (auto& keyframePath : keyframesPathArray.array) {
					if (keyframePath.isObject()) {
						float time = keyframePath.readNumberf("time", true);
						int value = keyframePath.readNumberi("value", true);
						int control1 = keyframePath.readNumberi("control1", false);
						int control2 = keyframePath.readNumberi("control2", false);
						keyframes.add(IntKeyframeElement(time, value, control1, control2));
					}
					else {
						UString error("Invalid data in field \"" + keyframePath.currentPath + R"("! IntKeyframes array must contain objects like { "delta": <number>, "value": [<number>, <number>] }!)");
						VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
					}
				}
				return keyframes;
			}
			else if (required == true) {
				UString error("Invalid data in field \"" + keyframesPathArray.path.currentPath + "[]\". Cannot be empty!");
				VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
			}
		}
		else if (required == true) {
			UString error("Cannot read required field \"" + keyframesPath.currentPath + "." + "keysframes" + "\"!");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
		}
	}

	else if (required == true) {
		UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	return defaultValue;
}


FloatKeyframes JsonPath::readFloatKeyframes(const UString& name, bool required, const FloatKeyframes& defaultValue) const
{
	if (isNumber(name)) {
		return FloatKeyframes(readNumberf(name, required));
	}

	else if (isObject(name)) {
		JsonPath keyframesPath = getAttribute(name, required);

		FloatKeyframes keyframes;
		keyframes.easingType = keyframesPath.readEasingType("easingType");
		keyframes.interpolationType = keyframesPath.readInterpolationType("interpolationType");
		keyframes.looping = keyframesPath.readBool("looping", false, false);

		if (keyframesPath.isArray("keyframes")) {
			JsonPathArray keyframesPathArray = keyframesPath.asArray("keyframes");

			if (!keyframesPathArray.array.empty()) {
				// Check that there's at least 2 values
				if (keyframesPathArray.array.size() < 2) {
					UString error("Invalid data in field \"" + keyframesPathArray.path.currentPath + "[]\"! FloatKeyframes array must contain at least two entries.");
					VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
				}

				for (auto& keyframePath : keyframesPathArray.array) {
					if (keyframePath.isObject()) {
						float time = keyframePath.readNumberf("time", true);
						float value = keyframePath.readNumberf("value", true);
						float control1 = keyframePath.readNumberf("control1", false);
						float control2 = keyframePath.readNumberf("control2", false);
						keyframes.add(FloatKeyframeElement(time, value, control1, control2));
					}
					else {
						UString error("Invalid data in field \"" + keyframePath.currentPath + R"("! FloatKeyframes array must contain objects like { "delta": <number>, "value": [<number>, <number>] }!)");
						VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
					}
				}
				return keyframes;
			}
			else if (required == true) {
				UString error("Invalid data in field \"" + keyframesPathArray.path.currentPath + "[]\". Cannot be empty!");
				VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
			}
		}
	}

	else if (required == true) {
		UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), "");
	}

	return defaultValue;
}


Vector2fKeyframes JsonPath::readVector2fKeyframes(const UString& name, bool required, const Vector2fKeyframes& defaultValue) const
{
	if (isVector2fArrayFormat(name)) {
		return Vector2fKeyframes(readVector2fArrayFormat(name, required));
	}

	else if (isObject(name)) {
		JsonPath keyframesPath = getAttribute(name, required);

		Vector2fKeyframes keyframes;
		keyframes.easingType = keyframesPath.readEasingType("easingType");
		keyframes.interpolationType = keyframesPath.readInterpolationType("interpolationType");
		keyframes.looping = keyframesPath.readBool("looping", false, false);

		if (keyframesPath.isArray("keyframes")) {
			JsonPathArray keyframesPathArray = keyframesPath.asArray("keyframes");

			if (!keyframesPathArray.array.empty()) {
				// Check that there's at least 2 values
				if (keyframesPathArray.array.size() < 2) {
					UString error("Invalid data in field \"" + keyframesPathArray.path.currentPath + "[]\"! Vector2fKeyframes array must contain at least two entries.");
					VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
				}

				for (auto& keyframePath : keyframesPathArray.array) {
					if (keyframePath.isObject()) {
						float time = keyframePath.readNumberf("time", true);
						Vector2f value = keyframePath.readVector2fArrayFormat("value", true);
						Vector2f control1 = keyframePath.readVector2fArrayFormat("control1", false);
						Vector2f control2 = keyframePath.readVector2fArrayFormat("control2", false);
						keyframes.add(Vector2fKeyframeElement(time, value, control1, control2));
					}
					else {
						UString error("Invalid data in field \"" + keyframePath.currentPath + R"("! Vector2fKeyframes array must contain objects like { "delta": <number>, "value": [<number>, <number>] }!)");
						VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
					}
				}
				return keyframes;
			}
			else if (required == true) {
				UString error("Invalid data in field \"" + keyframesPathArray.path.currentPath + "[]\". Cannot be empty!");
				VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
			}
		}
	}

	else if (required == true) {
		UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	return defaultValue;
}


Vector3fKeyframes JsonPath::readVector3fKeyframes(const UString& name, bool required, const Vector3fKeyframes& defaultValue) const
{
	if (isVector3fArrayFormat(name)) {
		return Vector3fKeyframes(readVector3fArrayFormat(name, required));
	}

	else if (isObject(name)) {
		JsonPath keyframesPath = getAttribute(name, required);

		Vector3fKeyframes keyframes;
		keyframes.easingType = keyframesPath.readEasingType("easingType");
		keyframes.interpolationType = keyframesPath.readInterpolationType("interpolationType");
		keyframes.looping = keyframesPath.readBool("looping", false, false);

		if (keyframesPath.isArray("keyframes")) {
			JsonPathArray keyframesPathArray = keyframesPath.asArray("keyframes");

			if (!keyframesPathArray.array.empty()) {
				// Check that there's at least 2 values
				if (keyframesPathArray.array.size() < 2) {
					UString error("Invalid data in field \"" + keyframesPathArray.path.currentPath + "[]\"! Vector3fKeyframes array must contain at least two entries.");
					VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
				}

				for (auto& keyframePath : keyframesPathArray.array) {
					if (keyframePath.isObject()) {
						float time = keyframePath.readNumberf("time", true);
						Vector3f value = keyframePath.readVector3fArrayFormat("value", true);
						Vector3f control1 = keyframePath.readVector3fArrayFormat("control1", false);
						Vector3f control2 = keyframePath.readVector3fArrayFormat("control2", false);
						keyframes.add(Vector3fKeyframeElement(time, value, control1, control2));
					}
					else {
						UString error("Invalid data in field \"" + keyframePath.currentPath + R"("! Vector3fKeyframes array must contain objects like { "delta": <number>, "value": [<number>, <number>, <number>] }!)");
						VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
					}
				}
				return keyframes;
			}
			else if (required == true) {
				UString error("Invalid data in field \"" + keyframesPathArray.path.currentPath + "[]\". Cannot be empty!");
				VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
			}
		}
	}

	else if (required == true) {
		UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	return defaultValue;
}


RgbaColorfKeyframes JsonPath::readRgbaColorfKeyframes(const UString& name, bool required, const RgbaColorfKeyframes& defaultValue) const
{
	if (isColor(name)) {
		return RgbaColorfKeyframes(readColor(name, required));
	}

	else if (isObject(name)) {
		JsonPath keyframesPath = getAttribute(name, required);

		RgbaColorfKeyframes keyframes;
		keyframes.easingType = keyframesPath.readEasingType("easingType");
		keyframes.interpolationType = keyframesPath.readInterpolationType("interpolationType");
		keyframes.looping = keyframesPath.readBool("looping", false, false);

		if (keyframesPath.isArray("keyframes")) {
			JsonPathArray keyframesPathArray = keyframesPath.asArray("keyframes");

			if (!keyframesPathArray.array.empty()) {
				// Check that there's at least 2 values
				if (keyframesPathArray.array.size() < 2) {
					UString error("Invalid data in field \"" + keyframesPathArray.path.currentPath + "[]\"! RgbaColorfKeyframes array must contain at least two entries.");
					VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
				}

				for (auto& keyframePath : keyframesPathArray.array) {
					if (keyframePath.isObject()) {
						float time = keyframePath.readNumberf("time", true);
						RgbaColorf value = keyframePath.readColor("value", true);
						RgbaColorf control1 = keyframePath.readColor("control1", false);
						RgbaColorf control2 = keyframePath.readColor("control2", false);
						keyframes.add(RgbaColorfKeyframeElement(time, value, control1, control2));
					}
					else {
						UString error("Invalid data in field \"" + keyframePath.currentPath + R"("! RgbaColorfKeyframes array must contain objects like { "delta": <number>, "value": [<number>, <number>, <number>] }!)");
						VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
					}
				}
				return keyframes;
			}
			else if (required == true) {
				UString error("Invalid data in field \"" + keyframesPath.currentPath + ".keyframes\". Cannot be empty!");
				VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
			}
		}
	}

	else if (required == true) {
		UString error("Cannot read required field \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	return defaultValue;
}


CameraKeyframes JsonPath::readCameraKeyframes(const UString& name, bool required, const CameraKeyframes& defaultValue) const
{
	if (isArray(name)) {
		JsonPathArray keyframesPath = asArray(name);
		if (!keyframesPath.array.empty()) {
			CameraKeyframes cameraKeyframes;

			// Check that there's at least 2 values
			if (keyframesPath.array.size() < 2) {
				UString error("Invalid data in field \"" + keyframesPath.path.currentPath + "[]\"! CameraKeyframes array must contain at least two entries.");
				VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
			}

			for (auto& keyframePath : keyframesPath.array) {
				if (keyframePath.isObject()) {
					Vector3f position = keyframePath.readVector3fArrayFormat("position", true);
					Vector3f direction = keyframePath.readVector3fArrayFormat("direction", true);
					Vector3f up = keyframePath.readVector3fArrayFormat("up", true);
					float time = keyframePath.readNumberf("time", true);
					cameraKeyframes.add(CameraKeyframeElement(time, position, direction, up));
				}
				else {
					UString error("Invalid data in field \"" + keyframePath.currentPath + R"("! CameraKeyframes array must contain objects like { "time": <number>, "position": [x,y,z], "direction": [x,y,z], "up": [x,y,z] }!)");
					VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
				}
			}
			return cameraKeyframes;
		}
		else if (required == true) {
			UString error("Invalid data in field \"" + keyframesPath.path.currentPath + ".keyframes\". Cannot be empty!");
			VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
		}
	}
	else if (required == true) {
		UString error("Expected array at \"" + currentPath + "." + name + "\"!");
		VERSO_ILLEGALFORMAT("verso-base", error.c_str(), fileName.c_str());
	}

	return defaultValue;
}


UString JsonPath::jsonValueToString(JSONValue* value, const UString& newLinePadding)
{
	UString str;
	if (value == nullptr) {
		return "undefined";
	}

	else if (value->IsNull()) {
		return "null";
	}

	else if (value->IsBool()) {
		if (value->AsBool()) {
			return "true";
		}
		else {
			return "false";
		}
	}

	else if (value->IsNumber()) {
		str.append2(value->AsNumber());
	}

	else if (value->IsString()) {
		str += "\"" + value->AsString() + "\"";
	}

	else if (value->IsArray()) {
		str += "[";
		JSONArray array = value->AsArray();
		bool first = true;
		for (auto& item : array) {
			if (!first) {
				str += ",\n" + newLinePadding + "  " + jsonValueToString(item, newLinePadding + "  ");
			}
			else {
				str += "\n" + newLinePadding + "  " + jsonValueToString(item, newLinePadding + "  ");
				first = false;
			}
		}
		str += "\n" + newLinePadding + "]";
	}

	else if (value->IsObject()) {
		bool first = true;
		str += "{ ";
		for (auto& item : value->AsObject()) {
			if (!first) {
				str += ",\n" + newLinePadding + "  \"" + item.first + "\": " + jsonValueToString(item.second, newLinePadding + "  ");
			}
			else {
				str += "\n" + newLinePadding + "  \"" + item.first + "\": " + jsonValueToString(item.second, newLinePadding + "  ");
				first = false;
			}
		}
		str += "\n" + newLinePadding + "}";
	}

	return str;
}

// toString

UString JsonPath::toString(const UString& newLinePadding) const
{
	UString newLinePadding2(newLinePadding + "  ");
	UString str("{\n" + newLinePadding2 + R"("currentPath": ")" + currentPath + "\",\n");
	str += newLinePadding2 + R"("value": )";
	str += jsonValueToString(value, "  ");
	str += R"(, "fileName": ")" + fileName + "\"\n";
	str += newLinePadding + "}";
	return str;
}


UString JsonPath::toStringDebug(const UString& newLinePadding) const
{
	UString str("Verso::JsonPath(\n");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


// JsonPathArray

JsonPathArray::JsonPathArray() :
	path(),
	array()
{
}


JsonPathArray::JsonPathArray(const JsonPath& path, const std::vector<JsonPath>& array) :
	path(path),
	array(array)
{
}


JsonPathArray::JsonPathArray(const JsonPathArray& original) :
	path(original.path),
	array(original.array)
{
}


JsonPathArray::JsonPathArray(const JsonPathArray&& original) noexcept :
	path(std::move(original.path)),
	array(std::move(original.array))
{
}


JsonPathArray& JsonPathArray::operator =(const JsonPathArray& original)
{
	if (this != &original) {
		path = original.path;
		array = original.array;
	}
	return *this;
}


JsonPathArray& JsonPathArray::operator =(JsonPathArray&& original) noexcept
{
	if (this != &original) {
		path = std::move(original.path);
		array = std::move(original.array);
	}
	return *this;
}


JsonPathArray::~JsonPathArray()
= default;


UString JsonPathArray::toString(const UString& newLinePadding) const
{
	UString str("path="+path.toString(newLinePadding)+", array=[\n");
	for (auto& path : array) {
		str += path.toString(newLinePadding + "    ");
		str += ",\n";
	}
	str += "]";
	return str;
}


UString JsonPathArray::toStringDebug(const UString& newLinePadding) const
{
	UString str("Verso::JsonPathArray(\n");
	str += toString(newLinePadding);
	str += ")";
	return str;
}


} // End namespace Verso
