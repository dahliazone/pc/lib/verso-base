# == Shared ===============================================================
! include( ../verso-base-common.pri ) {
    error("Couldn't find the verso-base-common.pri file!")
}

TEMPLATE = lib
win* {
    TEMPLATE = vclib
}

DESTDIR = "$${BUILD_DIR}"
TARGET = verso-base

DEPENDPATH += \
    $${HEADERPATH}/Verso/Image \
    $${HEADERPATH}/Verso/Math \
    $${HEADERPATH}/Verso/System/Exception \
    $${HEADERPATH}/Verso/System \
    $${HEADERPATH}/Verso/Time/TimeSource \
    $${HEADERPATH}/Verso/Time \
    $${HEADERPATH}/Verso \
    Verso/Math \
    Verso/System \
    Verso/Time/TimeSource \
    Verso/Time \
    Verso/

HEADERS += \
    $${LIBPATH}/SimpleJSON/src/JSON.h \
    $${LIBPATH}/SimpleJSON/src/JSONValue.h \
    $${HEADERPATH}/Verso/Image/Image.hpp \
    $${HEADERPATH}/Verso/Math/Align.hpp \
    $${HEADERPATH}/Verso/Math/Array2.hpp \
    $${HEADERPATH}/Verso/Math/AspectRatio.hpp \
    $${HEADERPATH}/Verso/Math/Degree.hpp \
    $${HEADERPATH}/Verso/Math/HAlign.hpp \
    $${HEADERPATH}/Verso/Math/Hash32.hpp \
    $${HEADERPATH}/Verso/Math/Interpolation.hpp \
    $${HEADERPATH}/Verso/Math/Math.hpp \
    $${HEADERPATH}/Verso/Math/Matrix4x4f.hpp \
    $${HEADERPATH}/Verso/Math/Quaternion.hpp \
    $${HEADERPATH}/Verso/Math/Radian.hpp \
    $${HEADERPATH}/Verso/Math/Random.hpp \
    $${HEADERPATH}/Verso/Math/Range.hpp \
    $${HEADERPATH}/Verso/Math/Rect.hpp \
    $${HEADERPATH}/Verso/Math/RgbaColorf.hpp \
    $${HEADERPATH}/Verso/Math/Size.hpp \
    $${HEADERPATH}/Verso/Math/Size3.hpp \
    $${HEADERPATH}/Verso/Math/Transform.hpp \
    $${HEADERPATH}/Verso/Math/VAlign.hpp \
    $${HEADERPATH}/Verso/Math/Vector2.hpp \
    $${HEADERPATH}/Verso/Math/Vector3.hpp \
    $${HEADERPATH}/Verso/Math/Vector4.hpp \
    $${HEADERPATH}/Verso/Math/Vertex.hpp \
    $${HEADERPATH}/Verso/System/Exception/AssertException.hpp \
    $${HEADERPATH}/Verso/System/Exception/DeviceNotFoundException.hpp \
    $${HEADERPATH}/Verso/System/Exception/ErrorException.hpp \
    $${HEADERPATH}/Verso/System/Exception/ExceptionInterface.hpp \
    $${HEADERPATH}/Verso/System/Exception/FileNotFoundException.hpp \
    $${HEADERPATH}/Verso/System/Exception/IllegalFormatException.hpp \
    $${HEADERPATH}/Verso/System/Exception/IllegalParametersException.hpp \
    $${HEADERPATH}/Verso/System/Exception/IoErrorException.hpp \
    $${HEADERPATH}/Verso/System/Exception/ObjectNotFoundException.hpp \
    $${HEADERPATH}/Verso/System/assert.hpp \
    $${HEADERPATH}/Verso/System/current_function.hpp \
    $${HEADERPATH}/Verso/System/Endian.hpp \
    $${HEADERPATH}/Verso/System/Exception.hpp \
    $${HEADERPATH}/Verso/System/File.hpp \
    $${HEADERPATH}/Verso/System/ForceOneInstance.hpp \
    $${HEADERPATH}/Verso/System/JsonHelper.hpp \
    $${HEADERPATH}/Verso/System/Logger.hpp \
    $${HEADERPATH}/Verso/System/NonCopyable.hpp \
    $${HEADERPATH}/Verso/System/NonMovable.hpp \
    $${HEADERPATH}/Verso/System/Logger.hpp \
    $${HEADERPATH}/Verso/System/SingletonManager.hpp \
    $${HEADERPATH}/Verso/System/string_convert.hpp \
    $${HEADERPATH}/Verso/System/UString.hpp \
    $${HEADERPATH}/Verso/Time/TimeSource/ITimeSource.hpp \
    $${HEADERPATH}/Verso/Time/TimeSource/StdChrono.hpp \
    $${HEADERPATH}/Verso/Time/Clock.hpp \
    $${HEADERPATH}/Verso/Time/Datetime.hpp \
    $${HEADERPATH}/Verso/Time/FrameTimer.hpp \
    $${HEADERPATH}/Verso/Time/FrameTimestamp.hpp \
    $${HEADERPATH}/Verso/Time/ITimer.hpp \
    $${HEADERPATH}/Verso/Time/ManualTimer.hpp \
    $${HEADERPATH}/Verso/Time/Sleep.hpp \
    $${HEADERPATH}/Verso/Time/SteppedTimer.hpp \
    $${HEADERPATH}/Verso/Time/Timer.hpp \
    $${HEADERPATH}/Verso/Time/Timestamp.hpp \
    $${HEADERPATH}/Verso/Image.hpp \
    $${HEADERPATH}/Verso/Math.hpp \
    $${HEADERPATH}/Verso/System.hpp \
    $${HEADERPATH}/Verso/Time.hpp \
    $${HEADERPATH}/Verso/Base.hpp

SOURCES += \
    $${LIBPATH}/SimpleJSON/src/JSON.cpp \
    $${LIBPATH}/SimpleJSON/src/JSONValue.cpp \
    Verso/Math/AspectRatio.cpp \
    Verso/Math/Math.cpp \
    Verso/Math/Matrix4x4f.cpp \
    Verso/Math/Quaternion.cpp \
    Verso/System/File.cpp \
    Verso/System/JsonHelper.cpp \
    Verso/System/SingletonManager.cpp \
    Verso/System/UString.Cpp \
    Verso/Time/TimeSource/StdChrono.cpp \
    Verso/Time/Clock.cpp \
    Verso/Time/Datetime.cpp \
    Verso/Time/FrameTimer.cpp \
    Verso/Time/ManualTimer.cpp \
    Verso/Time/Sleep.cpp \
    Verso/Time/SteppedTimer.cpp \
    Verso/Time/Timer.cpp \
    Verso/Time/Timestamp.cpp


# == Linux ================================================================
linux-g++-32 {
}

linux-g++ {
}

linux-* {
}


# == OS X =================================================================
macx {
}


# == Windows ==============================================================
win* {
    QMAKE_POST_LINK += @ECHO "Post-link process..."
    CONFIG += staticlib

    # Copy DLLs
    QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\libfreetype-6.dll\" \"$${DESTDIR}\"
    QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\libjpeg-9.dll\" \"$${DESTDIR}\"
    QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\libpng16-16.dll\" \"$${DESTDIR}\"
    QMAKE_POST_LINK += && COPY /y \"$${DLL_DIR}\zlib1.dll\" \"$${DESTDIR}\"

    # Copy data directory
    QMAKE_POST_LINK += && XCOPY /s /q /y /i \"$${PWD}\..\data\" \"$${DESTDIR}\data\"

    # Remove temporaries
    QMAKE_POST_LINK += && RMDIR /S /Q \"{$${PWD}}\..\debug\" \"$${PWD}\..\release\"
}
