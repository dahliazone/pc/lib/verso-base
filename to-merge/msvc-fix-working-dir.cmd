@echo off

echo Copying user properties for fixing the default working directory

copy msvc\examples.vcxproj.user examples
copy msvc\libverso-base_tests.vcxproj.user test

echo Done!
