# == Shared ===============================================================
! include( ../verso-base-common.pri ) {
    error("Couldn't find the verso-base-common.pri file!")
}

TEMPLATE = app
CONFIG += console

DESTDIR = "$${BUILD_DIR}"
TARGET = libverso-base-tests


INCLUDEPATH += "$${PWD}"
INCLUDEPATH += "$${LIBPATH}/googletest/googletest/include/"


DEPENDPATH += \
    $${PWD}


SOURCES += \
    test.cpp \
    Image__Image_test.cpp \
    Math__Array2_test.cpp \
    Math__Hash32_test.cpp \
    Math__Quaternion_test.cpp \
    Math__Matrix4x4f_test.cpp \
    System__Endian_test.cpp \
    System__UString_test.cpp \
    to_integrate__System__Logger.cpp


# == Linux ================================================================
linux-g++ {
}

linux-* {
    QMAKE_POST_LINK += echo "Post-link process..."

    # Verso-base
    LIBS += -L"$${BUILD_DIR}" -lverso-base

    # googletest
    LIBS += -L"$${LIBPATH}/googletest/build/googlemock/gtest" -lgtest -lpthread
    QMAKE_CXXFLAGS += "-isystem $${LIBPATH}/googletest/googletest/include/"

    # cmath
    LIBS += -lm

    # Startup scripts
    QMAKE_POST_LINK += && cp -rf "$${PWD}/libverso-base-test-x64.sh" "$${DESTDIR}/"

    # Data
    QMAKE_POST_LINK += && cp -rf "$${PWD}/../data" "$${DESTDIR}/"
}


# == OS X =================================================================
macx {
    CONFIG -= app_bundle
    QMAKE_POST_LINK += echo "Post link-process..."

    # Verso-base
    LIBS += -L"$${BUILD_DIR}" -lverso-base

    # googletest
    LIBS += -L"$${LIBPATH}/googletest/build/googlemock/gtest" -lgtest -lpthread
    QMAKE_CXXFLAGS += "-isystem $${LIBPATH}/googletest/googletest/include/"

    # Startup scripts
    QMAKE_POST_LINK += && cp -rf "$${PWD}/libverso-base-test-osx.sh" "$${DESTDIR}/"

    # Data
    QMAKE_POST_LINK += && cp -rf "$${PWD}/../data" "$${DESTDIR}/"
}


# == Windows ==============================================================
win* {
    CONFIG += console
    QMAKE_POST_LINK += @ECHO "Post-link process..."

    # Verso-base
    LIBS += -L"$${BUILD_DIR}" -lverso-base

    # Copy data directory
    QMAKE_POST_LINK += && XCOPY /s /q /y /i \"$${PWD}\..\data\" \"$${DESTDIR}\data\"

    # googletest & main program
    LIBS += -lgtest -lgtest_main

    # Remove temporaries
    #QMAKE_POST_LINK += && RMDIR /S /Q \"$${PWD}\..\debug\" \"$${PWD}\..\release\"
}
