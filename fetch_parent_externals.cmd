PATH=%PATH%;"C:\Program Files\SlikSvn\bin";"C:\Program Files (x86)\SlikSvn\bin";

echo "verso-base: Fetching externals from repositories..."
IF NOT EXIST ..\utf8-cpp (
	git clone https://gitlab.com/dahliazone/pc/extlib/utf8-cpp.git ..\utf8-cpp
) else (
	pushd ..\utf8-cpp
	git pull --rebase
	popd
)

IF NOT EXIST ..\SimpleJSON (
	git clone https://gitlab.com/dahliazone/pc/extlib/SimpleJSON.git ..\SimpleJSON
) else (
	pushd ..\SimpleJSON
	git pull --rebase
	popd
)

IF NOT EXIST ..\nlohmann-json (
	git clone https://gitlab.com/dahliazone/pc/extlib/nlohmann-json.git ..\nlohmann-json
) else (
	pushd ..\nlohmann-json
	git pull --rebase
	popd
)

IF NOT EXIST ..\stb (
	git clone https://gitlab.com/dahliazone/pc/extlib/stb.git ..\stb
) else (
	pushd ..\stb
	git pull --rebase
	popd
)

IF NOT EXIST ..\googletest (
	git clone https://gitlab.com/dahliazone/pc/extlib/googletest.git ..\googletest
) else (
	pushd ..\googletest
	git pull --rebase
	popd
)

echo "verso-base: All done"

