#!/bin/bash

wc -l ../include/Verso/*.hpp \
../include/Verso/Image/*.hpp \
../include/Verso/Math/*.hpp \
../include/Verso/System/*.hpp \
../include/Verso/System/Exception/*.hpp \
../include/Verso/Time/*.hpp \
../include/Verso/Time/TimeSource/*.hpp \
../src/Verso/Math/*.cpp \
../src/Verso/System/*.cpp \
../src/Verso/Time/*.cpp \
../src/Verso/Time/TimeSource/*.cpp


