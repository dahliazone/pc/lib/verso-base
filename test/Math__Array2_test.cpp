
/**
 * @file Array2Test.cpp
 * @brief Test for the class Array2.
 * @author Max Vilkki <codise@codise.org>
 *
 * TODO:
 * - \todo Test methods with T=class.
 *
 * What is not tested(!):
 * - What: BlitMasked() on out-of-bounds situations.
 *   Why: Too much work. It uses the same function for calculating valid region
 *        so if implementation does not change then it's enough that out-of-bounds
 *        situations are tested inside Blit().
 *
 * - What: BlitSourceMasked() BlitTargetMasked() BlitSourceTargetMasked()
 *   Why: Too much work. They're tested through BlitMasked().
 *
 * - What: toString*() is not tested at all.
 *   Why: Not really that important method, also hard to test outputted text.
 */

#include <Verso/Math/Array2.hpp>

#include <gtest/gtest.h>

namespace {

using Verso::Array2;


/**
 * Fixture for testing class Array2.
 */
class Array2Test : public testing::Test
{
	protected:
		Array2Test() :
			array2d3x3(3, 3),
			array2d1x2(1, 2),
			array2d4x1(4, 1),
			maze(7,7),
			maze8DirectionFilled(7, 7),
			maze4DirectionFilled(7, 7)
		{
			// Executed for each test!

			// Set elements (maze)
			maze.setElement(0, 0, 0);
			maze.setElement(1, 0, 2);
			maze.setElement(2, 0, 0);
			maze.setElement(3, 0, 1);
			maze.setElement(4, 0, 0);
			maze.setElement(5, 0, 0);
			maze.setElement(6, 0, 0);

			maze.setElement(0, 1, 2);
			maze.setElement(1, 1, 0);
			maze.setElement(2, 1, 2);
			maze.setElement(3, 1, 1);
			maze.setElement(4, 1, 0);
			maze.setElement(5, 1, 0);
			maze.setElement(6, 1, 0);

			maze.setElement(0, 2, 0);
			maze.setElement(1, 2, 2);
			maze.setElement(2, 2, 1);
			maze.setElement(3, 2, 0);
			maze.setElement(4, 2, 0);
			maze.setElement(5, 2, 0);
			maze.setElement(6, 2, 0);

			maze.setElement(0, 3, 1);
			maze.setElement(1, 3, 1);
			maze.setElement(2, 3, 0);
			maze.setElement(3, 3, 0);
			maze.setElement(4, 3, 0);
			maze.setElement(5, 3, 1);
			maze.setElement(6, 3, 1);

			maze.setElement(0, 4, 0);
			maze.setElement(1, 4, 0);
			maze.setElement(2, 4, 0);
			maze.setElement(3, 4, 0);
			maze.setElement(4, 4, 1);
			maze.setElement(5, 4, 2);
			maze.setElement(6, 4, 2);

			maze.setElement(0, 5, 0);
			maze.setElement(1, 5, 0);
			maze.setElement(2, 5, 0);
			maze.setElement(3, 5, 1);
			maze.setElement(4, 5, 2);
			maze.setElement(5, 5, 0);
			maze.setElement(6, 5, 2);

			maze.setElement(0, 6, 0);
			maze.setElement(1, 6, 0);
			maze.setElement(2, 6, 0);
			maze.setElement(3, 6, 1);
			maze.setElement(4, 6, 2);
			maze.setElement(5, 6, 2);
			maze.setElement(6, 6, 2);

			// maze now
			//	0 2 0 1 0 0 0
			//	2 0 2 1 0 0 0
			//	0 2 1 0 0 0 0
			//	1 1 0 0 0 1 1
			//	0 0 0 0 1 2 2
			//	0 0 0 1 2 0 2
			//	0 0 0 1 2 2 2

			// Set elements (maze8DirectionFilled)
			maze8DirectionFilled.setElement(0, 0, 0);
			maze8DirectionFilled.setElement(1, 0, 2);
			maze8DirectionFilled.setElement(2, 0, 0);
			maze8DirectionFilled.setElement(3, 0, 1);
			maze8DirectionFilled.setElement(4, 0, 5);
			maze8DirectionFilled.setElement(5, 0, 5);
			maze8DirectionFilled.setElement(6, 0, 5);

			maze8DirectionFilled.setElement(0, 1, 2);
			maze8DirectionFilled.setElement(1, 1, 0);
			maze8DirectionFilled.setElement(2, 1, 2);
			maze8DirectionFilled.setElement(3, 1, 1);
			maze8DirectionFilled.setElement(4, 1, 5);
			maze8DirectionFilled.setElement(5, 1, 5);
			maze8DirectionFilled.setElement(6, 1, 5);

			maze8DirectionFilled.setElement(0, 2, 0);
			maze8DirectionFilled.setElement(1, 2, 2);
			maze8DirectionFilled.setElement(2, 2, 1);
			maze8DirectionFilled.setElement(3, 2, 5);
			maze8DirectionFilled.setElement(4, 2, 5);
			maze8DirectionFilled.setElement(5, 2, 5);
			maze8DirectionFilled.setElement(6, 2, 5);

			maze8DirectionFilled.setElement(0, 3, 1);
			maze8DirectionFilled.setElement(1, 3, 1);
			maze8DirectionFilled.setElement(2, 3, 5);
			maze8DirectionFilled.setElement(3, 3, 5);
			maze8DirectionFilled.setElement(4, 3, 5);
			maze8DirectionFilled.setElement(5, 3, 1);
			maze8DirectionFilled.setElement(6, 3, 1);

			maze8DirectionFilled.setElement(0, 4, 5);
			maze8DirectionFilled.setElement(1, 4, 5);
			maze8DirectionFilled.setElement(2, 4, 5);
			maze8DirectionFilled.setElement(3, 4, 5);
			maze8DirectionFilled.setElement(4, 4, 1);
			maze8DirectionFilled.setElement(5, 4, 2);
			maze8DirectionFilled.setElement(6, 4, 2);

			maze8DirectionFilled.setElement(0, 5, 5);
			maze8DirectionFilled.setElement(1, 5, 5);
			maze8DirectionFilled.setElement(2, 5, 5);
			maze8DirectionFilled.setElement(3, 5, 1);
			maze8DirectionFilled.setElement(4, 5, 2);
			maze8DirectionFilled.setElement(5, 5, 0);
			maze8DirectionFilled.setElement(6, 5, 2);

			maze8DirectionFilled.setElement(0, 6, 5);
			maze8DirectionFilled.setElement(1, 6, 5);
			maze8DirectionFilled.setElement(2, 6, 5);
			maze8DirectionFilled.setElement(3, 6, 1);
			maze8DirectionFilled.setElement(4, 6, 2);
			maze8DirectionFilled.setElement(5, 6, 2);
			maze8DirectionFilled.setElement(6, 6, 2);

			// maze8DirectionFilled now
			//	0 2 0 1 5 5 5
			//	2 0 2 1 5 5 5
			//	0 2 1 5 5 5 5
			//	1 1 5 5 5 1 1
			//	5 5 5 5 1 2 2
			//	5 5 5 1 2 0 2
			//	5 5 5 1 2 2 2

			// Set elements (maze4DirectionFilled)
			maze4DirectionFilled.setElement(0, 0, 0);
			maze4DirectionFilled.setElement(1, 0, 2);
			maze4DirectionFilled.setElement(2, 0, 0);
			maze4DirectionFilled.setElement(3, 0, 1);
			maze4DirectionFilled.setElement(4, 0, 5);
			maze4DirectionFilled.setElement(5, 0, 5);
			maze4DirectionFilled.setElement(6, 0, 5);

			maze4DirectionFilled.setElement(0, 1, 2);
			maze4DirectionFilled.setElement(1, 1, 0);
			maze4DirectionFilled.setElement(2, 1, 2);
			maze4DirectionFilled.setElement(3, 1, 1);
			maze4DirectionFilled.setElement(4, 1, 5);
			maze4DirectionFilled.setElement(5, 1, 5);
			maze4DirectionFilled.setElement(6, 1, 5);

			maze4DirectionFilled.setElement(0, 2, 0);
			maze4DirectionFilled.setElement(1, 2, 2);
			maze4DirectionFilled.setElement(2, 2, 1);
			maze4DirectionFilled.setElement(3, 2, 5);
			maze4DirectionFilled.setElement(4, 2, 5);
			maze4DirectionFilled.setElement(5, 2, 5);
			maze4DirectionFilled.setElement(6, 2, 5);

			maze4DirectionFilled.setElement(0, 3, 1);
			maze4DirectionFilled.setElement(1, 3, 1);
			maze4DirectionFilled.setElement(2, 3, 5);
			maze4DirectionFilled.setElement(3, 3, 5);
			maze4DirectionFilled.setElement(4, 3, 5);
			maze4DirectionFilled.setElement(5, 3, 1);
			maze4DirectionFilled.setElement(6, 3, 1);

			maze4DirectionFilled.setElement(0, 4, 5);
			maze4DirectionFilled.setElement(1, 4, 5);
			maze4DirectionFilled.setElement(2, 4, 5);
			maze4DirectionFilled.setElement(3, 4, 5);
			maze4DirectionFilled.setElement(4, 4, 1);
			maze4DirectionFilled.setElement(5, 4, 2);
			maze4DirectionFilled.setElement(6, 4, 2);

			maze4DirectionFilled.setElement(0, 5, 5);
			maze4DirectionFilled.setElement(1, 5, 5);
			maze4DirectionFilled.setElement(2, 5, 5);
			maze4DirectionFilled.setElement(3, 5, 1);
			maze4DirectionFilled.setElement(4, 5, 2);
			maze4DirectionFilled.setElement(5, 5, 0);
			maze4DirectionFilled.setElement(6, 5, 2);

			maze4DirectionFilled.setElement(0, 6, 5);
			maze4DirectionFilled.setElement(1, 6, 5);
			maze4DirectionFilled.setElement(2, 6, 5);
			maze4DirectionFilled.setElement(3, 6, 1);
			maze4DirectionFilled.setElement(4, 6, 2);
			maze4DirectionFilled.setElement(5, 6, 2);
			maze4DirectionFilled.setElement(6, 6, 2);

			// maze4DirectionFilled now
			//	0 2 0 1 5 5 5
			//	2 0 2 1 5 5 5
			//	0 2 1 5 5 5 5
			//	1 1 5 5 5 1 1
			//	5 5 5 5 1 2 2
			//	5 5 5 1 2 0 2
			//	5 5 5 1 2 2 2
		}


		virtual void SetUp()
		{
			// Executed for each test!
		}


		virtual void TearDown()
		{
			// Executed for each test!
		}


		Array2<bool> array2d3x3;
		Array2<bool> array2d1x2;
		Array2<bool> array2d4x1;
		Array2<unsigned int> maze;
		Array2<unsigned int> maze8DirectionFilled;
		Array2<unsigned int> maze4DirectionFilled;
};


/**
 * Simple class for testing Array2 with T=object.
 *
 * Foobar support the required operations for Array2:
 * - constructor (with no parameters)
 * - copy constructor (automatic)
 * - destructor (automatic)
 * - operator= (automatic)
 * - operator==
 * - operator!
 * - operator<< (for Printout() and googletest
 */
class Foobar {
	public:
		Foobar() :
			foo(0),
			bar()
		{}

		Foobar(int foo, const std::string& bar) :
			foo(foo),
			bar(bar)
		{}

		int getFoo() const
		{
			return foo;
		}

		void setFoo(int foo)
		{
			this->foo = foo;
		}

		std::string getBar() const
		{
			return bar;
		}

		void setBar(const std::string& bar)
		{
			this->bar = bar;
		}

		bool operator== (const Foobar &rhs) const
		{
			if (foo != rhs.foo)
				return false;

			if (bar.compare(rhs.bar) == 0)
				return true;
			else
				return false;
		}

		Foobar operator! ()
		{
			return Foobar(!foo, bar);
		}

		friend std::ostream& operator<<(std::ostream& os, const Foobar& rhs)
		{
			os << "[" << std::showbase << std::setw(2) << rhs.foo <<
				"-\"" << rhs.bar << "\"]";
			return os;
		}

	private:
		int foo;
		std::string bar;
};


/**
 * Test that initial state if false for 3x3 array.
 */
TEST_F(Array2Test, InitialStateIsFalse_size3x3)
{
	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 0));
	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 2));
}


/**
 * Test that initial state if false for 1x2 array.
 */
TEST_F(Array2Test, InitialStateIsFalse_size1x2)
{
	EXPECT_EQ(false, array2d1x2.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d1x2.getElementCopy(0, 1));
}


/**
 * Test that initial state if false for 4x1 array.
 */
TEST_F(Array2Test, InitialStateIsFalse_size4x1)
{
	EXPECT_EQ(false, array2d4x1.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d4x1.getElementCopy(1, 0));
	EXPECT_EQ(false, array2d4x1.getElementCopy(2, 0));
	EXPECT_EQ(false, array2d4x1.getElementCopy(3, 0));
}


/**
 * Test that initial state with class Foobar for 2x2 array.
 */
TEST_F(Array2Test, InitialStateIsFalse_class_Foobar_size2x2)
{
	Array2<Foobar> array2x2(2, 2);

	Foobar foobar;

	EXPECT_EQ(foobar, array2x2.getElementCopy(0, 0));
	EXPECT_EQ(foobar, array2x2.getElementCopy(1, 0));
	EXPECT_EQ(foobar, array2x2.getElementCopy(0, 1));
	EXPECT_EQ(foobar, array2x2.getElementCopy(1, 1));
}


/**
 * Test that 3x3, 1x2 and 4x1 arrays return correct width with GetWidth().
 */
TEST_F(Array2Test, GetWidth)
{
	EXPECT_EQ(3, array2d3x3.getWidth());
	EXPECT_EQ(1, array2d1x2.getWidth());
	EXPECT_EQ(4, array2d4x1.getWidth());
}


/**
 * Test that 3x3, 1x2 and 4x1 arrays return correct height with getHeight().
 */
TEST_F(Array2Test, getHeight)
{
	EXPECT_EQ(3, array2d3x3.getHeight());
	EXPECT_EQ(2, array2d1x2.getHeight());
	EXPECT_EQ(1, array2d4x1.getHeight());
}


/**
 * Test that (x,y)-coordinate based IsInside() works.
 */
TEST_F(Array2Test, IsInside_coordinate_based)
{
	EXPECT_FALSE(array2d3x3.isInside(-1, -1));
	EXPECT_FALSE(array2d3x3.isInside(0, -1));
	EXPECT_FALSE(array2d3x3.isInside(1, -1));
	EXPECT_FALSE(array2d3x3.isInside(2, -1));
	EXPECT_FALSE(array2d3x3.isInside(3, -1));

	EXPECT_FALSE(array2d3x3.isInside(-1, 0));
	EXPECT_TRUE(array2d3x3.isInside(0, 0));
	EXPECT_TRUE(array2d3x3.isInside(1, 0));
	EXPECT_TRUE(array2d3x3.isInside(2, 0));
	EXPECT_FALSE(array2d3x3.isInside(3, 0));

	EXPECT_FALSE(array2d3x3.isInside(-1, 1));
	EXPECT_TRUE(array2d3x3.isInside(0, 1));
	EXPECT_TRUE(array2d3x3.isInside(1, 1));
	EXPECT_TRUE(array2d3x3.isInside(2, 1));
	EXPECT_FALSE(array2d3x3.isInside(3, 1));

	EXPECT_FALSE(array2d3x3.isInside(-1, 2));
	EXPECT_TRUE(array2d3x3.isInside(0, 2));
	EXPECT_TRUE(array2d3x3.isInside(1, 2));
	EXPECT_TRUE(array2d3x3.isInside(2, 2));
	EXPECT_FALSE(array2d3x3.isInside(3, 2));

	EXPECT_FALSE(array2d3x3.isInside(-1, 3));
	EXPECT_FALSE(array2d3x3.isInside(0, 3));
	EXPECT_FALSE(array2d3x3.isInside(1, 3));
	EXPECT_FALSE(array2d3x3.isInside(2, 3));
	EXPECT_FALSE(array2d3x3.isInside(3, 3));

	Array2<int> array2d1x1(1, 1);
	EXPECT_FALSE(array2d1x1.isInside(-1, -1));
	EXPECT_FALSE(array2d1x1.isInside(-1, 0));
	EXPECT_FALSE(array2d1x1.isInside(0, -1));
	EXPECT_TRUE(array2d1x1.isInside(0, 0));
	EXPECT_FALSE(array2d1x1.isInside(1, 0));
	EXPECT_FALSE(array2d1x1.isInside(0, 1));
	EXPECT_FALSE(array2d1x1.isInside(1, 1));
}


/**
 * Test that index based IsInside() works.
 */
TEST_F(Array2Test, IsInside_index_based)
{
	EXPECT_TRUE(array2d3x3.isInside(0));
	EXPECT_TRUE(array2d3x3.isInside(1));
	EXPECT_TRUE(array2d3x3.isInside(2));
	EXPECT_TRUE(array2d3x3.isInside(3));
	EXPECT_TRUE(array2d3x3.isInside(4));
	EXPECT_TRUE(array2d3x3.isInside(5));
	EXPECT_TRUE(array2d3x3.isInside(6));
	EXPECT_TRUE(array2d3x3.isInside(7));
	EXPECT_TRUE(array2d3x3.isInside(8));

	EXPECT_FALSE(array2d3x3.isInside(9));
	EXPECT_FALSE(array2d3x3.isInside(10));
	EXPECT_FALSE(array2d3x3.isInside(100));
}


/**
 * Test setting and verifying a 3x3 array using (x,y)-coordinates.
 *
 * [0 1 0]
 * [1 0 0]
 * [1 0 1]
 */
TEST_F(Array2Test, SetFormAndVerify_coordinate_based_size3x3)
{
	// Set elements
	array2d3x3.setElement(0, 0, false);
	array2d3x3.setElement(1, 0, true);
	array2d3x3.setElement(2, 0, false);

	array2d3x3.setElement(0, 1, true);
	array2d3x3.setElement(1, 1, false);
	array2d3x3.setElement(2, 1, false);

	array2d3x3.setElement(0, 2, true);
	array2d3x3.setElement(1, 2, false);
	array2d3x3.setElement(2, 2, true);

	// Verify elements (copy)
	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 2));

	// Verify elements (reference)
	EXPECT_EQ(false, array2d3x3.getElementRef(0, 0));
	EXPECT_EQ(true, array2d3x3.getElementRef(1, 0));
	EXPECT_EQ(false, array2d3x3.getElementRef(2, 0));

	EXPECT_EQ(true, array2d3x3.getElementRef(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementRef(1, 1));
	EXPECT_EQ(false, array2d3x3.getElementRef(2, 1));

	EXPECT_EQ(true, array2d3x3.getElementRef(0, 2));
	EXPECT_EQ(false, array2d3x3.getElementRef(1, 2));
	EXPECT_EQ(true, array2d3x3.getElementRef(2, 2));

}


/**
 * Test setting and verifying a 3x3 array using indices.
 *
 * [0 1 0]
 * [1 0 0]
 * [1 0 1]
 */
TEST_F(Array2Test, SetFormAndVerify_index_based_size3x3)
{
	// Set elements
	array2d3x3.setElement(0, false);
	array2d3x3.setElement(1, true);
	array2d3x3.setElement(2, false);

	array2d3x3.setElement(3, true);
	array2d3x3.setElement(4, false);
	array2d3x3.setElement(5, false);

	array2d3x3.setElement(6, true);
	array2d3x3.setElement(7, false);
	array2d3x3.setElement(8, true);

	// Verify elements (copy)
	EXPECT_EQ(false, array2d3x3.getElementCopy(0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2));

	EXPECT_EQ(true, array2d3x3.getElementCopy(3));
	EXPECT_EQ(false, array2d3x3.getElementCopy(4));
	EXPECT_EQ(false, array2d3x3.getElementCopy(5));

	EXPECT_EQ(true, array2d3x3.getElementCopy(6));
	EXPECT_EQ(false, array2d3x3.getElementCopy(7));
	EXPECT_EQ(true, array2d3x3.getElementCopy(8));

	// Verify elements (reference)
	EXPECT_EQ(false, array2d3x3.getElementRef(0));
	EXPECT_EQ(true, array2d3x3.getElementRef(1));
	EXPECT_EQ(false, array2d3x3.getElementRef(2));

	EXPECT_EQ(true, array2d3x3.getElementRef(3));
	EXPECT_EQ(false, array2d3x3.getElementRef(4));
	EXPECT_EQ(false, array2d3x3.getElementRef(5));

	EXPECT_EQ(true, array2d3x3.getElementRef(6));
	EXPECT_EQ(false, array2d3x3.getElementRef(7));
	EXPECT_EQ(true, array2d3x3.getElementRef(8));
}


/**
 * Test setting and verifying a 1x2 array using (x,y)-coordinates.
 *
 * [1]
 * [0]
 */
TEST_F(Array2Test, SetFormAndVerify_coordinate_based_size1x2)
{
	// Set elements
	array2d1x2.setElement(0, 0, true);

	array2d1x2.setElement(0, 1, false);

	// Verify elements (copy)
	EXPECT_EQ(true, array2d1x2.getElementCopy(0, 0));

	EXPECT_EQ(false, array2d1x2.getElementCopy(0, 1));

	// Verify elements (reference)
	EXPECT_EQ(true, array2d1x2.getElementRef(0, 0));

	EXPECT_EQ(false, array2d1x2.getElementRef(0, 1));
}


/**
 * Test setting and verifying a 1x2 array using indices.
 *
 * [1]
 * [0]
 */
TEST_F(Array2Test, SetFormAndVerify_index_based_size1x2)
{
	// Set elements
	array2d1x2.setElement(0, true);

	array2d1x2.setElement(1, false);

	// Verify elements (copy)
	EXPECT_EQ(true, array2d1x2.getElementCopy(0));

	EXPECT_EQ(false, array2d1x2.getElementCopy(1));

	// Verify elements (reference)
	EXPECT_EQ(true, array2d1x2.getElementRef(0));

	EXPECT_EQ(false, array2d1x2.getElementRef(1));
}


/**
 * Test setting and verifying a 4x1 array using (x,y)-coordinates.
 *
 * [0 1 0 1]
 */
TEST_F(Array2Test, SetFormAndVerify_coordinate_based_size4x1)
{
	// Set elements (x,y)-based
	array2d4x1.setElement(0, 0, false);
	array2d4x1.setElement(1, 0, true);
	array2d4x1.setElement(2, 0, false);
	array2d4x1.setElement(3, 0, true);

	// Verify elements (copy)
	EXPECT_EQ(false, array2d4x1.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d4x1.getElementCopy(1, 0));
	EXPECT_EQ(false, array2d4x1.getElementCopy(2, 0));
	EXPECT_EQ(true, array2d4x1.getElementCopy(3, 0));

	// Verify elements (reference)
	EXPECT_EQ(false, array2d4x1.getElementRef(0, 0));
	EXPECT_EQ(true, array2d4x1.getElementRef(1, 0));
	EXPECT_EQ(false, array2d4x1.getElementRef(2, 0));
	EXPECT_EQ(true, array2d4x1.getElementRef(3, 0));
}


/**
 * Test setting and verifying a 4x1 array using indices.
 *
 * [0 1 0 1]
 */
TEST_F(Array2Test, SetFormAndVerify_index_based_size4x1)
{
	// Set elements index-based
	array2d4x1.setElement(0, false);
	array2d4x1.setElement(1, true);
	array2d4x1.setElement(2, false);
	array2d4x1.setElement(3, true);

	// Verify elements (copy)
	EXPECT_EQ(false, array2d4x1.getElementCopy(0));
	EXPECT_EQ(true, array2d4x1.getElementCopy(1));
	EXPECT_EQ(false, array2d4x1.getElementCopy(2));
	EXPECT_EQ(true, array2d4x1.getElementCopy(3));

	// Verify elements (reference)
	EXPECT_EQ(false, array2d4x1.getElementRef(0));
	EXPECT_EQ(true, array2d4x1.getElementRef(1));
	EXPECT_EQ(false, array2d4x1.getElementRef(2));
	EXPECT_EQ(true, array2d4x1.getElementRef(3));
}


/**
 * Test setting and verifying a 2x2 array of class Foobar using
 * (x,y)-coordinates.
 */
TEST_F(Array2Test, SetFormAndVerify_coordinate_based_class_Foobar_size2x2)
{
	Array2<Foobar> array2x2(2, 2);

	Foobar foobar0;
	Foobar foobar1(1, "B");
	Foobar foobar2(2, "C");
	Foobar foobar3(3, "DE");

	// Set elements index-based
	array2x2.setElement(0, 0, foobar0);
	array2x2.setElement(1, 0, foobar1);
	array2x2.setElement(0, 1, foobar2);
	array2x2.setElement(1, 1, foobar3);

	// Verify elements (copy)
	EXPECT_EQ(foobar0, array2x2.getElementCopy(0, 0));
	EXPECT_EQ(foobar1, array2x2.getElementCopy(1, 0));
	EXPECT_EQ(foobar2, array2x2.getElementCopy(0, 1));
	EXPECT_EQ(foobar3, array2x2.getElementCopy(1, 1));

	// Verify elements (reference)
	EXPECT_EQ(foobar0, array2x2.getElementRef(0, 0));
	EXPECT_EQ(foobar1, array2x2.getElementRef(1, 0));
	EXPECT_EQ(foobar2, array2x2.getElementRef(0, 1));
	EXPECT_EQ(foobar3, array2x2.getElementRef(1, 1));
}


/**
 * Test setting and verifying a 2x2 array of class Foobar using indices.
 */
TEST_F(Array2Test, SetFormAndVerify_index_based_class_Foobar_size2x2)
{
	Array2<Foobar> array2x2(2, 2);

	Foobar foobar0;
	Foobar foobar1(1, "B");
	Foobar foobar2(2, "C");
	Foobar foobar3(3, "DE");

	// Set elements index-based
	array2x2.setElement(0, foobar0);
	array2x2.setElement(1, foobar1);
	array2x2.setElement(2, foobar2);
	array2x2.setElement(3, foobar3);

	// Verify elements (copy)
	EXPECT_EQ(foobar0, array2x2.getElementCopy(0));
	EXPECT_EQ(foobar1, array2x2.getElementCopy(1));
	EXPECT_EQ(foobar2, array2x2.getElementCopy(2));
	EXPECT_EQ(foobar3, array2x2.getElementCopy(3));

	// Verify elements (reference)
	EXPECT_EQ(foobar0, array2x2.getElementRef(0));
	EXPECT_EQ(foobar1, array2x2.getElementRef(1));
	EXPECT_EQ(foobar2, array2x2.getElementRef(2));
	EXPECT_EQ(foobar3, array2x2.getElementRef(3));
}


/**
 * Test that IsEqualSize() works.
 */
TEST_F(Array2Test, IsEqualSize)
{
	EXPECT_TRUE(array2d3x3.isEqualSize(array2d3x3));

	EXPECT_FALSE(array2d3x3.isEqualSize(array2d1x2));
	EXPECT_FALSE(array2d1x2.isEqualSize(array2d3x3));

	EXPECT_FALSE(array2d1x2.isEqualSize(array2d4x1));
	EXPECT_FALSE(array2d4x1.isEqualSize(array2d1x2));
}


/**
 * Test that IsEqualSize<U>() works.
 */
TEST_F(Array2Test, IsEqualSize_type_U)
{
	Array2<unsigned int> array2d3x4u(3, 4);
	Array2<unsigned int> array2d3x3u(3, 3);

	EXPECT_FALSE(array2d3x4u.isEqualSize<bool>(array2d4x1));
	EXPECT_FALSE(array2d4x1.isEqualSize<unsigned int>(array2d3x4u));

	EXPECT_TRUE(array2d3x3u.isEqualSize<bool>(array2d3x3));
	EXPECT_TRUE(array2d3x3.isEqualSize<unsigned int>(array2d3x3u));
}


/**
 * Test that IsEqual() works.
 */
TEST_F(Array2Test, IsEqual)
{
	// Set shape (array2d3x3)
	array2d3x3.setElement(0, 0, false);
	array2d3x3.setElement(1, 0, true);
	array2d3x3.setElement(2, 0, false);

	array2d3x3.setElement(0, 1, false);
	array2d3x3.setElement(1, 1, true);
	array2d3x3.setElement(2, 1, false);

	array2d3x3.setElement(0, 2, false);
	array2d3x3.setElement(1, 2, true);
	array2d3x3.setElement(2, 2, false);

	EXPECT_TRUE(array2d3x3.isEqual(array2d3x3));

	Array2<unsigned int> array2d3x3(3, 3);

	// Set shape (array2d3x3)
	array2d3x3.setElement(0, 0, 1);
	array2d3x3.setElement(1, 0, 2);
	array2d3x3.setElement(2, 0, 3);

	array2d3x3.setElement(0, 1, 4);
	array2d3x3.setElement(1, 1, 5);
	array2d3x3.setElement(2, 1, 6);

	array2d3x3.setElement(0, 2, 7);
	array2d3x3.setElement(1, 2, 8);
	array2d3x3.setElement(2, 2, 9);

	EXPECT_TRUE(array2d3x3.isEqual(array2d3x3));

	Array2<unsigned int> array2d3x3second(3, 3);

	// Set shape (array2d3x3second)
	array2d3x3second.setElement(0, 0, 1);
	array2d3x3second.setElement(1, 0, 2);
	array2d3x3second.setElement(2, 0, 3);

	array2d3x3second.setElement(0, 1, 4);
	array2d3x3second.setElement(1, 1, 5);
	array2d3x3second.setElement(2, 1, 6);

	array2d3x3second.setElement(0, 2, 7);
	array2d3x3second.setElement(1, 2, 8);
	array2d3x3second.setElement(2, 2, 9);

	EXPECT_TRUE(array2d3x3second.isEqual(array2d3x3second));
	EXPECT_TRUE(array2d3x3.isEqual(array2d3x3second));
	EXPECT_TRUE(array2d3x3second.isEqual(array2d3x3));

	array2d3x3second.setElement(0, 0, 2);
	EXPECT_FALSE(array2d3x3.isEqual(array2d3x3second));
	EXPECT_FALSE(array2d3x3second.isEqual(array2d3x3));
	array2d3x3second.setElement(0, 0, 1);

	array2d3x3second.setElement(2, 1, 10);
	EXPECT_FALSE(array2d3x3.isEqual(array2d3x3second));
	EXPECT_FALSE(array2d3x3second.isEqual(array2d3x3));
	array2d3x3second.setElement(2, 1, 6);

	array2d3x3second.setElement(2, 2, 4);
	EXPECT_FALSE(array2d3x3.isEqual(array2d3x3second));
	EXPECT_FALSE(array2d3x3second.isEqual(array2d3x3));
	array2d3x3second.setElement(2, 2, 9);

	array2d3x3second.setElement(2, 2, 4);
	array2d3x3second.setElement(2, 1, 10);
	array2d3x3second.setElement(0, 0, 2);
	EXPECT_FALSE(array2d3x3.isEqual(array2d3x3second));
	EXPECT_FALSE(array2d3x3second.isEqual(array2d3x3));
	array2d3x3second.setElement(0, 0, 1);
	array2d3x3second.setElement(2, 1, 6);
	array2d3x3second.setElement(2, 2, 9);
	EXPECT_TRUE(array2d3x3.isEqual(array2d3x3second));
	EXPECT_TRUE(array2d3x3second.isEqual(array2d3x3));

	Array2<unsigned int> array2d3x4(3, 4);

	array2d3x4.setElement(0, 0, 1);
	array2d3x4.setElement(1, 0, 2);
	array2d3x4.setElement(2, 0, 3);

	array2d3x4.setElement(0, 1, 4);
	array2d3x4.setElement(1, 1, 5);
	array2d3x4.setElement(2, 1, 6);

	array2d3x4.setElement(0, 2, 7);
	array2d3x4.setElement(1, 2, 8);
	array2d3x4.setElement(2, 2, 9);

	array2d3x4.setElement(0, 3, 10);
	array2d3x4.setElement(1, 3, 11);
	array2d3x4.setElement(2, 3, 12);

	EXPECT_TRUE(array2d3x4.isEqual(array2d3x4));
	EXPECT_FALSE(array2d3x3.isEqual(array2d3x4));
	EXPECT_FALSE(array2d3x4.isEqual(array2d3x3));

	Array2<Foobar> array2x1_1(2, 1);
	Array2<Foobar> array2x1_2(2, 1);
	Array2<Foobar> array2x1_3(2, 1);
	Foobar foobar1(1, "Foo");
	Foobar foobar2(2, "Bar");
	Foobar foobar3(3, "Meh");

	array2x1_1.setElement(0, 0, foobar1);
	array2x1_1.setElement(1, 0, foobar2);

	array2x1_2.setElement(0, 0, foobar1);
	array2x1_2.setElement(1, 0, foobar2);

	array2x1_3.setElement(0, 0, foobar1);
	array2x1_3.setElement(1, 0, foobar3);

	EXPECT_TRUE(array2x1_1.isEqual(array2x1_2));
	EXPECT_TRUE(array2x1_2.isEqual(array2x1_1));
	EXPECT_FALSE(array2x1_1.isEqual(array2x1_3));
	EXPECT_FALSE(array2x1_3.isEqual(array2x1_1));
	EXPECT_FALSE(array2x1_2.isEqual(array2x1_3));
	EXPECT_FALSE(array2x1_3.isEqual(array2x1_2));
}


/**
 * Test that Clear() works for 1x2 array.
 */
TEST_F(Array2Test, Clear_size1x2)
{
	// Set elements
	array2d1x2.setElement(0, 0, false);
	array2d1x2.setElement(0, 1, true);

	// Elements now
	//   .
	//   #

	// Clear elements with true
	array2d1x2.clear(true);

	// Elements now
	//   #
	//   #

	// Verify elements
	EXPECT_EQ(true, array2d1x2.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d1x2.getElementCopy(0, 1));
}


/**
 * Test that Clear() works for 2x1 array with T=class Foobar.
 */
TEST_F(Array2Test, Clear_class_Foobar_size2x1)
{
	Array2<Foobar> array2x1(2, 1);
	Foobar foobar1(1, "Foo");
	Foobar foobar2(2, "Bar");

	// Set elements
	array2x1.setElement(0, 0, foobar1);
	array2x1.setElement(1, 0, foobar2);

	// Clear elements with foobar2
	array2x1.clear(foobar2);

	// Verify elements
	EXPECT_EQ(foobar2, array2x1.getElementCopy(0, 0));
	EXPECT_EQ(foobar2, array2x1.getElementCopy(1, 0));
}


/**
 * Test that Clear() works for 3x3 array.
 */
TEST_F(Array2Test, Clear_size3x3)
{
	// Set elements
	array2d3x3.setElement(0, 0, true);
	array2d3x3.setElement(1, 0, true);
	array2d3x3.setElement(2, 0, false);

	array2d3x3.setElement(0, 1, true);
	array2d3x3.setElement(1, 1, false);
	array2d3x3.setElement(2, 1, true);

	array2d3x3.setElement(0, 2, false);
	array2d3x3.setElement(1, 2, true);
	array2d3x3.setElement(2, 2, true);

	// Elements now
	//   ##.
	//   #.#
	//   .##

	// Clear elements with true
	array2d3x3.clear(false);

	// Elements now
	//   ...
	//   ...
	//   ...

	// Verify elements
	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 2));
}


/**
 * Test that Clear() works for 4x1 array.
 */
TEST_F(Array2Test, Clear_size4x1)
{
	// Set elements
	array2d4x1.setElement(0, 0, false);
	array2d4x1.setElement(1, 0, true);
	array2d4x1.setElement(2, 0, false);
	array2d4x1.setElement(3, 0, true);

	// Elements now
	//   .#.#

	// Clear elements with true
	array2d4x1.clear(true);

	// Elements now
	//   ####

	// Verify elements
	EXPECT_EQ(true, array2d4x1.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d4x1.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d4x1.getElementCopy(2, 0));
	EXPECT_EQ(true, array2d4x1.getElementCopy(3, 0));
}


/**
 * Test that Invert() works for 3x3 array of booleans.
 */
TEST_F(Array2Test, Invert_size3x3_bool)
{
	Array2<bool> array2d3x3(3, 3);

	// Set elements
	array2d3x3.setElement(0, 0, true);
	array2d3x3.setElement(1, 0, false);
	array2d3x3.setElement(2, 0, true);

	array2d3x3.setElement(0, 1, false);
	array2d3x3.setElement(1, 1, true);
	array2d3x3.setElement(2, 1, false);

	array2d3x3.setElement(0, 2, true);
	array2d3x3.setElement(1, 2, false);
	array2d3x3.setElement(2, 2, true);

	// Elements now
	//   #.#
	//   .#.
	//   #.#

	// Invert elements
	array2d3x3.invert();

	// Elements now
	//   .#.
	//   #.#
	//   .#.

	// Verify elements
	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 2));

	// Invert elements
	array2d3x3.invert();

	// Elements now
	//   #.#
	//   .#.
	//   #.#

	// Verify elements
	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 2));
}


/**
 * Test that Invert() works for 3x3 array of ints.
 */
TEST_F(Array2Test, Invert_size3x3_int)
{
	Array2<int> array2d3x3(3, 3);

	// Set elements
	array2d3x3.setElement(0, 0, 1);
	array2d3x3.setElement(1, 0, 0);
	array2d3x3.setElement(2, 0, 3);

	array2d3x3.setElement(0, 1, 4);
	array2d3x3.setElement(1, 1, 0);
	array2d3x3.setElement(2, 1, 6);

	array2d3x3.setElement(0, 2, 0);
	array2d3x3.setElement(1, 2, 8);
	array2d3x3.setElement(2, 2, 0);

	// Elements now
	//   1 0 3
	//   4 0 6
	//   0 8 0

	// Invert elements
	array2d3x3.invert();

	// Elements now
	//   0 1 0
	//   0 1 0
	//   1 0 1

	// Verify elements
	EXPECT_EQ(0, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(1, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(0, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(0, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(1, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(0, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(1, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(0, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(1, array2d3x3.getElementCopy(2, 2));

	// Invert elements
	array2d3x3.invert();

	// Elements now
	//   1 0 1
	//   1 0 1
	//   0 1 0

	// Verify elements
	EXPECT_EQ(1, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(0, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(1, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(1, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(0, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(1, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(0, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(1, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(0, array2d3x3.getElementCopy(2, 2));
}


/**
 * Test that Invert() works for 2x1 array of class Foobar.
 */
TEST_F(Array2Test, Invert_size2x1_class_Foobar)
{
	Array2<Foobar> array2x1(2, 1);
	Foobar foobar1(1, "Foo");
	Foobar foobar2(2, "Bar");

	Foobar foobar1_inv(0, "Foo");
	Foobar foobar2_inv(0, "Bar");

	// Set elements
	array2x1.setElement(0, 0, foobar1);
	array2x1.setElement(1, 0, foobar2);

	// Invert elements
	array2x1.invert();

	// Verify elements
	EXPECT_EQ(foobar1_inv, array2x1.getElementCopy(0, 0));
	EXPECT_EQ(foobar2_inv, array2x1.getElementCopy(1, 0));
}


/**
 * Test that TurnClockwise90() works for 1x1 array.
 */
TEST_F(Array2Test, TurnClockwise90_size1x1)
{
	Array2<bool> array2d1x1(1, 1);

	// Set elements
	array2d1x1.setElement(0, 0, true);

	// Elements now
	//   #

	// Verify elements
	EXPECT_EQ(true, array2d1x1.getElementCopy(0, 0));

	// Turn 90 degrees clockwise
	array2d1x1.turnClockwise90();

	// Elements now
	//   #

	// Verify elements
	EXPECT_EQ(true, array2d1x1.getElementCopy(0, 0));

	// Set elements
	array2d1x1.setElement(0, 0, false);

	// Elements now
	//   .

	// Verify elements
	EXPECT_EQ(false, array2d1x1.getElementCopy(0, 0));

	// Turn 90 degrees clockwise
	array2d1x1.turnClockwise90();

	// Elements now
	//   .

	// Verify elements
	EXPECT_EQ(false, array2d1x1.getElementCopy(0, 0));
}


/**
 * Test that TurnClockwise90() works for 2x2 array.
 */
TEST_F(Array2Test, TurnClockwise90_size2x2)
{
	Array2<bool> array2d2x2(2, 2);

	// Set elements
	array2d2x2.setElement(0, 0, true);
	array2d2x2.setElement(1, 0, true);

	array2d2x2.setElement(0, 1, false);
	array2d2x2.setElement(1, 1, false);

	// Elements now
	//   ##
	//   ..

	// Verify elements
	EXPECT_EQ(true, array2d2x2.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d2x2.getElementCopy(1, 0));

	EXPECT_EQ(false, array2d2x2.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d2x2.getElementCopy(1, 1));

	// Turn 90 degrees clockwise
	array2d2x2.turnClockwise90();

	// Elements now
	//   .#
	//   .#

	// Verify elements
	EXPECT_EQ(false, array2d2x2.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d2x2.getElementCopy(1, 0));

	EXPECT_EQ(false, array2d2x2.getElementCopy(0, 1));
	EXPECT_EQ(true, array2d2x2.getElementCopy(1, 1));
}


/**
 * Test that TurnClockwise90() works for 3x3 array.
 */
TEST_F(Array2Test, TurnClockwise90_size3x3)
{
	// Set elements
	array2d3x3.setElement(0, 0, true);
	array2d3x3.setElement(1, 0, true);
	array2d3x3.setElement(2, 0, true);

	array2d3x3.setElement(0, 1, true);
	array2d3x3.setElement(1, 1, false);
	array2d3x3.setElement(2, 1, false);

	array2d3x3.setElement(0, 2, false);
	array2d3x3.setElement(1, 2, false);
	array2d3x3.setElement(2, 2, false);

	// Elements now
	//   ###
	//   #..
	//   ...

	// Verify elements
	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 2));

	// Turn 90 degrees clockwise
	array2d3x3.turnClockwise90();

	// Elements now
	//   .##
	//   ..#
	//   ..#

	// Verify elements
	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 2));

	// Turn 90 degrees clockwise
	array2d3x3.turnClockwise90();

	// Elements now
	//   ...
	//   ..#
	//   ###

	// Verify elements
	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 2));

	// Turn 90 degrees clockwise
	array2d3x3.turnClockwise90();

	// Elements now
	//   #..
	//   #..
	//   ##.

	// Verify elements
	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 2));

	// Turn 90 degrees clockwise
	array2d3x3.turnClockwise90();

	// Elements now
	//   ###
	//   #..
	//   ...

	// Verify elements
	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 2));
}


/**
 * Test that TurnClockwise90() works for 4x4 array.
 */
TEST_F(Array2Test, TurnClockwise90_size4x4)
{
	Array2<bool> array2d4x4(4, 4);

	// Set elements
	array2d4x4.setElement(0, 0, true);
	array2d4x4.setElement(1, 0, true);
	array2d4x4.setElement(2, 0, true);
	array2d4x4.setElement(3, 0, false);

	array2d4x4.setElement(0, 1, true);
	array2d4x4.setElement(1, 1, false);
	array2d4x4.setElement(2, 1, true);
	array2d4x4.setElement(3, 1, false);

	array2d4x4.setElement(0, 2, true);
	array2d4x4.setElement(1, 2, false);
	array2d4x4.setElement(2, 2, true);
	array2d4x4.setElement(3, 2, true);

	array2d4x4.setElement(0, 3, false);
	array2d4x4.setElement(1, 3, false);
	array2d4x4.setElement(2, 3, false);
	array2d4x4.setElement(3, 3, false);

	// Elements now
	//   ###.
	//   #.#.
	//   #.##
	//   ....

	// Verify elements
	EXPECT_EQ(true, array2d4x4.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d4x4.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d4x4.getElementCopy(2, 0));
	EXPECT_EQ(false, array2d4x4.getElementCopy(3, 0));

	EXPECT_EQ(true, array2d4x4.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d4x4.getElementCopy(1, 1));
	EXPECT_EQ(true, array2d4x4.getElementCopy(2, 1));
	EXPECT_EQ(false, array2d4x4.getElementCopy(3, 1));

	EXPECT_EQ(true, array2d4x4.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d4x4.getElementCopy(1, 2));
	EXPECT_EQ(true, array2d4x4.getElementCopy(2, 2));
	EXPECT_EQ(true, array2d4x4.getElementCopy(3, 2));

	EXPECT_EQ(false, array2d4x4.getElementCopy(0, 3));
	EXPECT_EQ(false, array2d4x4.getElementCopy(1, 3));
	EXPECT_EQ(false, array2d4x4.getElementCopy(2, 3));
	EXPECT_EQ(false, array2d4x4.getElementCopy(3, 3));

	// Turn 90 degrees clockwise
	array2d4x4.turnClockwise90();

	// Elements now
	//   .###
	//   ...#
	//   .###
	//   .#..

	// Verify elements
	EXPECT_EQ(false, array2d4x4.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d4x4.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d4x4.getElementCopy(2, 0));
	EXPECT_EQ(true, array2d4x4.getElementCopy(3, 0));

	EXPECT_EQ(false, array2d4x4.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d4x4.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d4x4.getElementCopy(2, 1));
	EXPECT_EQ(true, array2d4x4.getElementCopy(3, 1));

	EXPECT_EQ(false, array2d4x4.getElementCopy(0, 2));
	EXPECT_EQ(true, array2d4x4.getElementCopy(1, 2));
	EXPECT_EQ(true, array2d4x4.getElementCopy(2, 2));
	EXPECT_EQ(true, array2d4x4.getElementCopy(3, 2));

	EXPECT_EQ(false, array2d4x4.getElementCopy(0, 3));
	EXPECT_EQ(true, array2d4x4.getElementCopy(1, 3));
	EXPECT_EQ(false, array2d4x4.getElementCopy(2, 3));
	EXPECT_EQ(false, array2d4x4.getElementCopy(3, 3));
}


/**
 * Test that TurnClockwise90() works for 5x5 array.
 */
TEST_F(Array2Test, TurnClockwise90_size5x5)
{
	Array2<bool> array2d5x5(5, 5);

	// Set elements
	array2d5x5.setElement(0, 0, false);
	array2d5x5.setElement(1, 0, true);
	array2d5x5.setElement(2, 0, false);
	array2d5x5.setElement(3, 0, false);
	array2d5x5.setElement(4, 0, false);

	array2d5x5.setElement(0, 1, false);
	array2d5x5.setElement(1, 1, true);
	array2d5x5.setElement(2, 1, false);
	array2d5x5.setElement(3, 1, true);
	array2d5x5.setElement(4, 1, true);

	array2d5x5.setElement(0, 2, false);
	array2d5x5.setElement(1, 2, false);
	array2d5x5.setElement(2, 2, true);
	array2d5x5.setElement(3, 2, false);
	array2d5x5.setElement(4, 2, false);

	array2d5x5.setElement(0, 3, true);
	array2d5x5.setElement(1, 3, false);
	array2d5x5.setElement(2, 3, true);
	array2d5x5.setElement(3, 3, false);
	array2d5x5.setElement(4, 3, false);

	array2d5x5.setElement(0, 4, false);
	array2d5x5.setElement(1, 4, false);
	array2d5x5.setElement(2, 4, false);
	array2d5x5.setElement(3, 4, true);
	array2d5x5.setElement(4, 4, true);

	// Elements now
	//   .#...
	//   .#.##
	//   ..#..
	//   #.#..
	//   ...##

	// Verify elements
	EXPECT_EQ(false, array2d5x5.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d5x5.getElementCopy(1, 0));
	EXPECT_EQ(false, array2d5x5.getElementCopy(2, 0));
	EXPECT_EQ(false, array2d5x5.getElementCopy(3, 0));
	EXPECT_EQ(false, array2d5x5.getElementCopy(4, 0));

	EXPECT_EQ(false, array2d5x5.getElementCopy(0, 1));
	EXPECT_EQ(true, array2d5x5.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d5x5.getElementCopy(2, 1));
	EXPECT_EQ(true, array2d5x5.getElementCopy(3, 1));
	EXPECT_EQ(true, array2d5x5.getElementCopy(4, 1));

	EXPECT_EQ(false, array2d5x5.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d5x5.getElementCopy(1, 2));
	EXPECT_EQ(true, array2d5x5.getElementCopy(2, 2));
	EXPECT_EQ(false, array2d5x5.getElementCopy(3, 2));
	EXPECT_EQ(false, array2d5x5.getElementCopy(4, 2));

	EXPECT_EQ(true, array2d5x5.getElementCopy(0, 3));
	EXPECT_EQ(false, array2d5x5.getElementCopy(1, 3));
	EXPECT_EQ(true, array2d5x5.getElementCopy(2, 3));
	EXPECT_EQ(false, array2d5x5.getElementCopy(3, 3));
	EXPECT_EQ(false, array2d5x5.getElementCopy(4, 3));

	EXPECT_EQ(false, array2d5x5.getElementCopy(0, 4));
	EXPECT_EQ(false, array2d5x5.getElementCopy(1, 4));
	EXPECT_EQ(false, array2d5x5.getElementCopy(2, 4));
	EXPECT_EQ(true, array2d5x5.getElementCopy(3, 4));
	EXPECT_EQ(true, array2d5x5.getElementCopy(4, 4));

	// Turn 90 degrees clockwise
	array2d5x5.turnClockwise90();

	// Elements now
	//   .#...
	//   ...##
	//   .##..
	//   #..#.
	//   #..#.

	// Verify elements
	EXPECT_EQ(false, array2d5x5.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d5x5.getElementCopy(1, 0));
	EXPECT_EQ(false, array2d5x5.getElementCopy(2, 0));
	EXPECT_EQ(false, array2d5x5.getElementCopy(3, 0));
	EXPECT_EQ(false, array2d5x5.getElementCopy(4, 0));

	EXPECT_EQ(false, array2d5x5.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d5x5.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d5x5.getElementCopy(2, 1));
	EXPECT_EQ(true, array2d5x5.getElementCopy(3, 1));
	EXPECT_EQ(true, array2d5x5.getElementCopy(4, 1));

	EXPECT_EQ(false, array2d5x5.getElementCopy(0, 2));
	EXPECT_EQ(true, array2d5x5.getElementCopy(1, 2));
	EXPECT_EQ(true, array2d5x5.getElementCopy(2, 2));
	EXPECT_EQ(false, array2d5x5.getElementCopy(3, 2));
	EXPECT_EQ(false, array2d5x5.getElementCopy(4, 2));

	EXPECT_EQ(true, array2d5x5.getElementCopy(0, 3));
	EXPECT_EQ(false, array2d5x5.getElementCopy(1, 3));
	EXPECT_EQ(false, array2d5x5.getElementCopy(2, 3));
	EXPECT_EQ(true, array2d5x5.getElementCopy(3, 3));
	EXPECT_EQ(false, array2d5x5.getElementCopy(4, 3));

	EXPECT_EQ(true, array2d5x5.getElementCopy(0, 4));
	EXPECT_EQ(false, array2d5x5.getElementCopy(1, 4));
	EXPECT_EQ(false, array2d5x5.getElementCopy(2, 4));
	EXPECT_EQ(true, array2d5x5.getElementCopy(3, 4));
	EXPECT_EQ(false, array2d5x5.getElementCopy(4, 4));
}


/**
 * Test that TurnClockwise90() works for 2x1 array.
 */
TEST_F(Array2Test, TurnClockwise90_size2x1)
{
	Array2<bool> array2d2x1(2, 1);

	// Set elements
	array2d2x1.setElement(0, 0, true);
	array2d2x1.setElement(1, 0, false);

	// Elements now
	//   #.

	// Verify elements
	EXPECT_EQ(true, array2d2x1.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d2x1.getElementCopy(1, 0));

	// Turn 90 degrees clockwise
	array2d2x1.turnClockwise90();

	// Elements now
	//   #
	//   .

	// Verify changed size
	EXPECT_EQ(1, array2d2x1.getWidth());
	EXPECT_EQ(2, array2d2x1.getHeight());

	// Verify elements
	EXPECT_EQ(true, array2d2x1.getElementCopy(0, 0));

	EXPECT_EQ(false, array2d2x1.getElementCopy(0, 1));

	// Turn 90 degrees clockwise
	array2d2x1.turnClockwise90();

	// Elements now
	//   .#

	// Verify changed size
	EXPECT_EQ(2, array2d2x1.getWidth());
	EXPECT_EQ(1, array2d2x1.getHeight());

	// Verify elements
	EXPECT_EQ(false, array2d2x1.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d2x1.getElementCopy(1, 0));

	// Turn 90 degrees clockwise
	array2d2x1.turnClockwise90();

	// Elements now
	//   .
	//   #

	// Verify changed size
	EXPECT_EQ(1, array2d2x1.getWidth());
	EXPECT_EQ(2, array2d2x1.getHeight());

	// Verify elements
	EXPECT_EQ(false, array2d2x1.getElementCopy(0, 0));

	EXPECT_EQ(true, array2d2x1.getElementCopy(0, 1));

	// Turn 90 degrees clockwise
	array2d2x1.turnClockwise90();

	// Elements now
	//   #.

	// Verify changed size
	EXPECT_EQ(2, array2d2x1.getWidth());
	EXPECT_EQ(1, array2d2x1.getHeight());

	// Verify elements
	EXPECT_EQ(true, array2d2x1.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d2x1.getElementCopy(1, 0));
}


/**
 * Test that TurnClockwise90() works for 3x2 array.
 */
TEST_F(Array2Test, TurnClockwise90_size3x2)
{
	Array2<bool> array2d3x2(3, 2);

	// Set elements
	array2d3x2.setElement(0, 0, true);
	array2d3x2.setElement(1, 0, true);
	array2d3x2.setElement(2, 0, true);

	array2d3x2.setElement(0, 1, true);
	array2d3x2.setElement(1, 1, false);
	array2d3x2.setElement(2, 1, false);

	// Elements now
	//   ###
	//   #..

	// Verify elements
	EXPECT_EQ(true, array2d3x2.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d3x2.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d3x2.getElementCopy(2, 0));

	EXPECT_EQ(true, array2d3x2.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x2.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d3x2.getElementCopy(2, 1));

	// Turn 90 degrees clockwise
	array2d3x2.turnClockwise90();

	// Elements now
	//   ##
	//   .#
	//   .#

	// Verify changed size
	EXPECT_EQ(2, array2d3x2.getWidth());
	EXPECT_EQ(3, array2d3x2.getHeight());

	// Verify elements
	EXPECT_EQ(true, array2d3x2.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d3x2.getElementCopy(1, 0));

	EXPECT_EQ(false, array2d3x2.getElementCopy(0, 1));
	EXPECT_EQ(true, array2d3x2.getElementCopy(1, 1));

	EXPECT_EQ(false, array2d3x2.getElementCopy(0, 2));
	EXPECT_EQ(true, array2d3x2.getElementCopy(1, 2));


	// Turn 90 degrees clockwise
	array2d3x2.turnClockwise90();

	// Elements now
	//   ..#
	//   ###

	// Verify changed size
	EXPECT_EQ(3, array2d3x2.getWidth());
	EXPECT_EQ(2, array2d3x2.getHeight());

	// Verify elements
	EXPECT_EQ(false, array2d3x2.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d3x2.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d3x2.getElementCopy(2, 0));

	EXPECT_EQ(true, array2d3x2.getElementCopy(0, 1));
	EXPECT_EQ(true, array2d3x2.getElementCopy(1, 1));
	EXPECT_EQ(true, array2d3x2.getElementCopy(2, 1));

	// Turn 90 degrees clockwise
	array2d3x2.turnClockwise90();

	// Elements now
	//   #.
	//   #.
	//   ##

	// Verify changed size
	EXPECT_EQ(2, array2d3x2.getWidth());
	EXPECT_EQ(3, array2d3x2.getHeight());

	// Verify elements
	EXPECT_EQ(true, array2d3x2.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d3x2.getElementCopy(1, 0));

	EXPECT_EQ(true, array2d3x2.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x2.getElementCopy(1, 1));

	EXPECT_EQ(true, array2d3x2.getElementCopy(0, 2));
	EXPECT_EQ(true, array2d3x2.getElementCopy(1, 2));

	// Turn 90 degrees clockwise
	array2d3x2.turnClockwise90();

	// Elements now
	//   ###
	//   #..

	// Verify changed size
	EXPECT_EQ(3, array2d3x2.getWidth());
	EXPECT_EQ(2, array2d3x2.getHeight());

	// Verify elements
	EXPECT_EQ(true, array2d3x2.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d3x2.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d3x2.getElementCopy(2, 0));

	EXPECT_EQ(true, array2d3x2.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x2.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d3x2.getElementCopy(2, 1));
}


/**
 * Test that TurnCounterclockwise90() works for 1x1 array.
 */
TEST_F(Array2Test, TurnCounterclockwise90_size1x1)
{
	Array2<bool> array2d1x1(1, 1);

	// Set elements
	array2d1x1.setElement(0, 0, true);

	// Elements now
	//   #

	// Verify elements
	EXPECT_EQ(true, array2d1x1.getElementCopy(0, 0));

	// Turn 90 degrees counterclockwise
	array2d1x1.turnCounterclockwise90();

	// Elements now
	//   #

	// Verify elements
	EXPECT_EQ(true, array2d1x1.getElementCopy(0, 0));

	// Set elements
	array2d1x1.setElement(0, 0, false);

	// Elements now
	//   .

	// Verify elements
	EXPECT_EQ(false, array2d1x1.getElementCopy(0, 0));

	// Turn 90 degrees clockwise
	array2d1x1.turnCounterclockwise90();

	// Elements now
	//   .

	// Verify elements
	EXPECT_EQ(false, array2d1x1.getElementCopy(0, 0));
}


/**
 * Test that TurnCounterclockwise90() works for 2x2 array.
 */
TEST_F(Array2Test, TurnCounterclockwise90_size2x2)
{
	Array2<bool> array2d2x2(2, 2);

	// Set elements
	array2d2x2.setElement(0, 0, true);
	array2d2x2.setElement(1, 0, true);

	array2d2x2.setElement(0, 1, false);
	array2d2x2.setElement(1, 1, false);

	// Elements now
	//   ##
	//   ..

	// Verify elements
	EXPECT_EQ(true, array2d2x2.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d2x2.getElementCopy(1, 0));

	EXPECT_EQ(false, array2d2x2.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d2x2.getElementCopy(1, 1));

	// Turn 90 degrees counterclockwise
	array2d2x2.turnCounterclockwise90();

	// Elements now
	//   #.
	//   #.

	// Verify elements
	EXPECT_EQ(true, array2d2x2.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d2x2.getElementCopy(1, 0));

	EXPECT_EQ(true, array2d2x2.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d2x2.getElementCopy(1, 1));
}


/**
 * Test that TurnCounterclockwise90() works for 3x3 array.
 */
TEST_F(Array2Test, TurnCounterclockwise90_size3x3)
{
	// Set elements
	array2d3x3.setElement(0, 0, true);
	array2d3x3.setElement(1, 0, true);
	array2d3x3.setElement(2, 0, true);

	array2d3x3.setElement(0, 1, true);
	array2d3x3.setElement(1, 1, false);
	array2d3x3.setElement(2, 1, false);

	array2d3x3.setElement(0, 2, false);
	array2d3x3.setElement(1, 2, false);
	array2d3x3.setElement(2, 2, false);

	// Elements now
	//   ###
	//   #..
	//   ...

	// Verify elements
	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 2));

	// Turn 90 degrees counterclockwise
	array2d3x3.turnCounterclockwise90();

	// Elements now
	//   #..
	//   #..
	//   ##.

	// Verify elements
	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 2));

	// Turn 90 degrees counterclockwise
	array2d3x3.turnCounterclockwise90();

	// Elements now
	//   ...
	//   ..#
	//   ###

	// Verify elements
	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 2));

	// Turn 90 degrees counterclockwise
	array2d3x3.turnCounterclockwise90();

	// Elements now
	//   .##
	//   ..#
	//   ..#

	// Verify elements
	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 2));

	// Turn 90 degrees counterclockwise
	array2d3x3.turnCounterclockwise90();

	// Elements now
	//   ###
	//   #..
	//   ...

	// Verify elements
	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 2));
}


/**
 * Test that TurnCounterclockwise90() works for 4x4 array.
 */
TEST_F(Array2Test, TurnCounterclockwise90_size4x4)
{
	Array2<bool> array2d4x4(4, 4);

	// Set elements
	array2d4x4.setElement(0, 0, true);
	array2d4x4.setElement(1, 0, true);
	array2d4x4.setElement(2, 0, true);
	array2d4x4.setElement(3, 0, false);

	array2d4x4.setElement(0, 1, true);
	array2d4x4.setElement(1, 1, false);
	array2d4x4.setElement(2, 1, true);
	array2d4x4.setElement(3, 1, false);

	array2d4x4.setElement(0, 2, true);
	array2d4x4.setElement(1, 2, false);
	array2d4x4.setElement(2, 2, true);
	array2d4x4.setElement(3, 2, true);

	array2d4x4.setElement(0, 3, false);
	array2d4x4.setElement(1, 3, false);
	array2d4x4.setElement(2, 3, false);
	array2d4x4.setElement(3, 3, false);

	// Elements now
	//   ###.
	//   #.#.
	//   #.##
	//   ....

	// Verify elements
	EXPECT_EQ(true, array2d4x4.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d4x4.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d4x4.getElementCopy(2, 0));
	EXPECT_EQ(false, array2d4x4.getElementCopy(3, 0));

	EXPECT_EQ(true, array2d4x4.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d4x4.getElementCopy(1, 1));
	EXPECT_EQ(true, array2d4x4.getElementCopy(2, 1));
	EXPECT_EQ(false, array2d4x4.getElementCopy(3, 1));

	EXPECT_EQ(true, array2d4x4.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d4x4.getElementCopy(1, 2));
	EXPECT_EQ(true, array2d4x4.getElementCopy(2, 2));
	EXPECT_EQ(true, array2d4x4.getElementCopy(3, 2));

	EXPECT_EQ(false, array2d4x4.getElementCopy(0, 3));
	EXPECT_EQ(false, array2d4x4.getElementCopy(1, 3));
	EXPECT_EQ(false, array2d4x4.getElementCopy(2, 3));
	EXPECT_EQ(false, array2d4x4.getElementCopy(3, 3));

	// Turn 90 degrees counterclockwise
	array2d4x4.turnCounterclockwise90();

	// Elements now
	//   ..#.
	//   ###.
	//   #...
	//   ###.

	// Verify elements
	EXPECT_EQ(false, array2d4x4.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d4x4.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d4x4.getElementCopy(2, 0));
	EXPECT_EQ(false, array2d4x4.getElementCopy(3, 0));

	EXPECT_EQ(true, array2d4x4.getElementCopy(0, 1));
	EXPECT_EQ(true, array2d4x4.getElementCopy(1, 1));
	EXPECT_EQ(true, array2d4x4.getElementCopy(2, 1));
	EXPECT_EQ(false, array2d4x4.getElementCopy(3, 1));

	EXPECT_EQ(true, array2d4x4.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d4x4.getElementCopy(1, 2));
	EXPECT_EQ(false, array2d4x4.getElementCopy(2, 2));
	EXPECT_EQ(false, array2d4x4.getElementCopy(3, 2));

	EXPECT_EQ(true, array2d4x4.getElementCopy(0, 3));
	EXPECT_EQ(true, array2d4x4.getElementCopy(1, 3));
	EXPECT_EQ(true, array2d4x4.getElementCopy(2, 3));
	EXPECT_EQ(false, array2d4x4.getElementCopy(3, 3));
}


/**
 * Test that TurnCounterclockwise90() works for 5x5 array.
 */
TEST_F(Array2Test, TurnCounterclockwise90_size5x5)
{
	Array2<bool> array2d5x5(5, 5);

	// Set elements
	array2d5x5.setElement(0, 0, false);
	array2d5x5.setElement(1, 0, true);
	array2d5x5.setElement(2, 0, false);
	array2d5x5.setElement(3, 0, false);
	array2d5x5.setElement(4, 0, false);

	array2d5x5.setElement(0, 1, false);
	array2d5x5.setElement(1, 1, true);
	array2d5x5.setElement(2, 1, false);
	array2d5x5.setElement(3, 1, true);
	array2d5x5.setElement(4, 1, true);

	array2d5x5.setElement(0, 2, false);
	array2d5x5.setElement(1, 2, false);
	array2d5x5.setElement(2, 2, true);
	array2d5x5.setElement(3, 2, false);
	array2d5x5.setElement(4, 2, false);

	array2d5x5.setElement(0, 3, true);
	array2d5x5.setElement(1, 3, false);
	array2d5x5.setElement(2, 3, true);
	array2d5x5.setElement(3, 3, false);
	array2d5x5.setElement(4, 3, false);

	array2d5x5.setElement(0, 4, false);
	array2d5x5.setElement(1, 4, false);
	array2d5x5.setElement(2, 4, false);
	array2d5x5.setElement(3, 4, true);
	array2d5x5.setElement(4, 4, true);

	// Elements now
	//   .#...
	//   .#.##
	//   ..#..
	//   #.#..
	//   ...##

	// Verify elements
	EXPECT_EQ(false, array2d5x5.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d5x5.getElementCopy(1, 0));
	EXPECT_EQ(false, array2d5x5.getElementCopy(2, 0));
	EXPECT_EQ(false, array2d5x5.getElementCopy(3, 0));
	EXPECT_EQ(false, array2d5x5.getElementCopy(4, 0));

	EXPECT_EQ(false, array2d5x5.getElementCopy(0, 1));
	EXPECT_EQ(true, array2d5x5.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d5x5.getElementCopy(2, 1));
	EXPECT_EQ(true, array2d5x5.getElementCopy(3, 1));
	EXPECT_EQ(true, array2d5x5.getElementCopy(4, 1));

	EXPECT_EQ(false, array2d5x5.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d5x5.getElementCopy(1, 2));
	EXPECT_EQ(true, array2d5x5.getElementCopy(2, 2));
	EXPECT_EQ(false, array2d5x5.getElementCopy(3, 2));
	EXPECT_EQ(false, array2d5x5.getElementCopy(4, 2));

	EXPECT_EQ(true, array2d5x5.getElementCopy(0, 3));
	EXPECT_EQ(false, array2d5x5.getElementCopy(1, 3));
	EXPECT_EQ(true, array2d5x5.getElementCopy(2, 3));
	EXPECT_EQ(false, array2d5x5.getElementCopy(3, 3));
	EXPECT_EQ(false, array2d5x5.getElementCopy(4, 3));

	EXPECT_EQ(false, array2d5x5.getElementCopy(0, 4));
	EXPECT_EQ(false, array2d5x5.getElementCopy(1, 4));
	EXPECT_EQ(false, array2d5x5.getElementCopy(2, 4));
	EXPECT_EQ(true, array2d5x5.getElementCopy(3, 4));
	EXPECT_EQ(true, array2d5x5.getElementCopy(4, 4));

	// Turn 90 degrees counterclockwise
	array2d5x5.turnCounterclockwise90();

	// Elements now
	//   .#..#
	//   .#..#
	//   ..##.
	//   ##...
	//   ...#.

	// Verify elements
	EXPECT_EQ(false, array2d5x5.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d5x5.getElementCopy(1, 0));
	EXPECT_EQ(false, array2d5x5.getElementCopy(2, 0));
	EXPECT_EQ(false, array2d5x5.getElementCopy(3, 0));
	EXPECT_EQ(true, array2d5x5.getElementCopy(4, 0));

	EXPECT_EQ(false, array2d5x5.getElementCopy(0, 1));
	EXPECT_EQ(true, array2d5x5.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d5x5.getElementCopy(2, 1));
	EXPECT_EQ(false, array2d5x5.getElementCopy(3, 1));
	EXPECT_EQ(true, array2d5x5.getElementCopy(4, 1));

	EXPECT_EQ(false, array2d5x5.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d5x5.getElementCopy(1, 2));
	EXPECT_EQ(true, array2d5x5.getElementCopy(2, 2));
	EXPECT_EQ(true, array2d5x5.getElementCopy(3, 2));
	EXPECT_EQ(false, array2d5x5.getElementCopy(4, 2));

	EXPECT_EQ(true, array2d5x5.getElementCopy(0, 3));
	EXPECT_EQ(true, array2d5x5.getElementCopy(1, 3));
	EXPECT_EQ(false, array2d5x5.getElementCopy(2, 3));
	EXPECT_EQ(false, array2d5x5.getElementCopy(3, 3));
	EXPECT_EQ(false, array2d5x5.getElementCopy(4, 3));

	EXPECT_EQ(false, array2d5x5.getElementCopy(0, 4));
	EXPECT_EQ(false, array2d5x5.getElementCopy(1, 4));
	EXPECT_EQ(false, array2d5x5.getElementCopy(2, 4));
	EXPECT_EQ(true, array2d5x5.getElementCopy(3, 4));
	EXPECT_EQ(false, array2d5x5.getElementCopy(4, 4));
}


/**
 * Test that TurnCounterclockwise90() works for 2x1 array.
 */
TEST_F(Array2Test, TurnCounterclockwise90_size2x1)
{
	Array2<bool> array2d2x1(2, 1);

	// Set elements
	array2d2x1.setElement(0, 0, true);
	array2d2x1.setElement(1, 0, false);

	// Elements now
	//   #.

	// Verify elements
	EXPECT_EQ(true, array2d2x1.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d2x1.getElementCopy(1, 0));

	// Turn 90 degrees counterclockwise
	array2d2x1.turnCounterclockwise90();

	// Elements now
	//   .
	//   #

	// Verify changed size
	EXPECT_EQ(1, array2d2x1.getWidth());

	EXPECT_EQ(2, array2d2x1.getHeight());

	// Verify elements
	EXPECT_EQ(false, array2d2x1.getElementCopy(0, 0));

	EXPECT_EQ(true, array2d2x1.getElementCopy(0, 1));

	// Turn 90 degrees counterclockwise
	array2d2x1.turnCounterclockwise90();

	// Elements now
	//   .#

	// Verify changed size
	EXPECT_EQ(2, array2d2x1.getWidth());
	EXPECT_EQ(1, array2d2x1.getHeight());

	// Verify elements
	EXPECT_EQ(false, array2d2x1.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d2x1.getElementCopy(1, 0));

	// Turn 90 degrees counterclockwise
	array2d2x1.turnCounterclockwise90();

	// Elements now
	//   #
	//   .

	// Verify changed size
	EXPECT_EQ(1, array2d2x1.getWidth());
	EXPECT_EQ(2, array2d2x1.getHeight());

	// Verify elements
	EXPECT_EQ(true, array2d2x1.getElementCopy(0, 0));

	EXPECT_EQ(false, array2d2x1.getElementCopy(0, 1));

	// Turn 90 degrees counterclockwise
	array2d2x1.turnCounterclockwise90();

	// Elements now
	//   #.

	// Verify changed size
	EXPECT_EQ(2, array2d2x1.getWidth());
	EXPECT_EQ(1, array2d2x1.getHeight());

	// Verify elements
	EXPECT_EQ(true, array2d2x1.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d2x1.getElementCopy(1, 0));
}


/**
 * Test that TurnCounterclockwise90() works for 3x2 array.
 */
TEST_F(Array2Test, TurnCounterclockwise90_size3x2)
{
	Array2<bool> array2d3x2(3, 2);

	// Set elements
	array2d3x2.setElement(0, 0, true);
	array2d3x2.setElement(1, 0, true);
	array2d3x2.setElement(2, 0, true);

	array2d3x2.setElement(0, 1, true);
	array2d3x2.setElement(1, 1, false);
	array2d3x2.setElement(2, 1, false);

	// Elements now
	//   ###
	//   #..

	// Verify elements
	EXPECT_EQ(true, array2d3x2.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d3x2.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d3x2.getElementCopy(2, 0));

	EXPECT_EQ(true, array2d3x2.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x2.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d3x2.getElementCopy(2, 1));

	// Turn 90 degrees counterclockwise
	array2d3x2.turnCounterclockwise90();

	// Elements now
	//   #.
	//   #.
	//   ##

	// Verify changed size
	EXPECT_EQ(2, array2d3x2.getWidth());
	EXPECT_EQ(3, array2d3x2.getHeight());

	// Verify elements
	EXPECT_EQ(true, array2d3x2.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d3x2.getElementCopy(1, 0));

	EXPECT_EQ(true, array2d3x2.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x2.getElementCopy(1, 1));

	EXPECT_EQ(true, array2d3x2.getElementCopy(0, 2));
	EXPECT_EQ(true, array2d3x2.getElementCopy(1, 2));


	// Turn 90 degrees counterclockwise
	array2d3x2.turnCounterclockwise90();

	// Elements now
	//   ..#
	//   ###

	// Verify changed size
	EXPECT_EQ(3, array2d3x2.getWidth());
	EXPECT_EQ(2, array2d3x2.getHeight());

	// Verify elements
	EXPECT_EQ(false, array2d3x2.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d3x2.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d3x2.getElementCopy(2, 0));

	EXPECT_EQ(true, array2d3x2.getElementCopy(0, 1));
	EXPECT_EQ(true, array2d3x2.getElementCopy(1, 1));
	EXPECT_EQ(true, array2d3x2.getElementCopy(2, 1));

	// Turn 90 degrees counterclockwise
	array2d3x2.turnCounterclockwise90();

	// Elements now
	//   ##
	//   .#
	//   .#

	// Verify changed size
	EXPECT_EQ(2, array2d3x2.getWidth());
	EXPECT_EQ(3, array2d3x2.getHeight());

	// Verify elements
	EXPECT_EQ(true, array2d3x2.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d3x2.getElementCopy(1, 0));

	EXPECT_EQ(false, array2d3x2.getElementCopy(0, 1));
	EXPECT_EQ(true, array2d3x2.getElementCopy(1, 1));

	EXPECT_EQ(false, array2d3x2.getElementCopy(0, 2));
	EXPECT_EQ(true, array2d3x2.getElementCopy(1, 2));

	// Turn 90 degrees counterclockwise
	array2d3x2.turnCounterclockwise90();

	// Elements now
	//   ###
	//   #..

	// Verify changed size
	EXPECT_EQ(3, array2d3x2.getWidth());
	EXPECT_EQ(2, array2d3x2.getHeight());

	// Verify elements
	EXPECT_EQ(true, array2d3x2.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d3x2.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d3x2.getElementCopy(2, 0));

	EXPECT_EQ(true, array2d3x2.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x2.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d3x2.getElementCopy(2, 1));
}


/**
 * Test that Array2<bool> cannot be constructed from invalid size.
 */
TEST_F(Array2Test, ConstructedWithInvalidSize)
{
	EXPECT_THROW(Array2<bool> array1(0, 0), Verso::AssertException);
	EXPECT_THROW(Array2<bool> array2(1, 0), Verso::AssertException);
	EXPECT_THROW(Array2<bool> array3(0, 1), Verso::AssertException);
}


/**
 * Test that GetElementCopy() and setElement() cannot use out of bounds coordinates.
 */
TEST_F(Array2Test, CannotUseOutOfBoundsCoordinates)
{
	// Minus values will throw the unsigned int around to a very larger number
	// which is invalid for nearly ~all arrays.
	EXPECT_THROW(array2d3x3.getElementCopy(-1, 2), Verso::AssertException);
	EXPECT_THROW(array2d3x3.getElementCopy(3, 1), Verso::AssertException);
	EXPECT_THROW(array2d3x3.getElementCopy(0, -1), Verso::AssertException);
	EXPECT_THROW(array2d3x3.getElementCopy(2, 3), Verso::AssertException);
	EXPECT_THROW(array2d3x3.getElementCopy(3, -1), Verso::AssertException);
	EXPECT_THROW(array2d3x3.getElementCopy(-1, 3), Verso::AssertException);

	EXPECT_THROW(array2d3x3.setElement(-1, 2, false), Verso::AssertException);
	EXPECT_THROW(array2d3x3.setElement(3, 1, false), Verso::AssertException);
	EXPECT_THROW(array2d3x3.setElement(0, -1, false), Verso::AssertException);
	EXPECT_THROW(array2d3x3.setElement(2, 3, false), Verso::AssertException);
	EXPECT_THROW(array2d3x3.setElement(3, -1, false), Verso::AssertException);
	EXPECT_THROW(array2d3x3.setElement(-1, 3, false), Verso::AssertException);

	EXPECT_THROW(array2d3x3.setElement(-1, 2, true), Verso::AssertException);
	EXPECT_THROW(array2d3x3.setElement(3, 1, true), Verso::AssertException);
	EXPECT_THROW(array2d3x3.setElement(0, -1, true), Verso::AssertException);
	EXPECT_THROW(array2d3x3.setElement(2, 3, true), Verso::AssertException);
	EXPECT_THROW(array2d3x3.setElement(3, -1, true), Verso::AssertException);
	EXPECT_THROW(array2d3x3.setElement(-1, 3, true), Verso::AssertException);
}


/**
 * Test that GetElementCopy() and setElement() cannot use out of bounds indices.
 */
TEST_F(Array2Test, CannotUseOutOfBoundsIndices)
{
	// Minus values will throw the unsigned int around to a very larger number
	// which is invalid for nearly ~all arrays.
	EXPECT_THROW(array2d3x3.getElementCopy(9), Verso::AssertException);
	EXPECT_THROW(array2d3x3.getElementCopy(10), Verso::AssertException);
	EXPECT_THROW(array2d3x3.getElementCopy(1230), Verso::AssertException);

	EXPECT_THROW(array2d3x3.setElement(9, false), Verso::AssertException);
	EXPECT_THROW(array2d3x3.setElement(10, false), Verso::AssertException);
	EXPECT_THROW(array2d3x3.setElement(1230, false), Verso::AssertException);

	EXPECT_THROW(array2d3x3.setElement(9, true), Verso::AssertException);
	EXPECT_THROW(array2d3x3.setElement(10, true), Verso::AssertException);
	EXPECT_THROW(array2d3x3.setElement(1230, true), Verso::AssertException);
}


/**
 * Test that FlipHorizontally() works for 1x1 array.
 */
TEST_F(Array2Test, FlipHorizontally_size1x1)
{
	Array2<bool> array2d1x1(1, 1);

	// Set elements
	array2d1x1.setElement(0, 0, true);

	// Elements now
	//   #

	// Verify elements
	EXPECT_EQ(true, array2d1x1.getElementCopy(0, 0));

	// Flip horizontally
	array2d1x1.flipHorizontally();

	// Elements now
	//   #

	// Verify elements
	EXPECT_EQ(true, array2d1x1.getElementCopy(0, 0));

	// Flip horizontally
	array2d1x1.flipHorizontally();

	// Elements now
	//   #

	// Verify elements
	EXPECT_EQ(true, array2d1x1.getElementCopy(0, 0));
}


/**
 * Test that FlipHorizontally() works for 2x2 array.
 */
TEST_F(Array2Test, FlipHorizontally_size2x2)
{
	Array2<bool> array2d2x2(2, 2);

	// Set elements
	array2d2x2.setElement(0, 0, true);
	array2d2x2.setElement(1, 0, false);

	array2d2x2.setElement(0, 1, true);
	array2d2x2.setElement(1, 1, false);

	// Elements now
	//   #.
	//   #.

	// Verify elements
	EXPECT_EQ(true, array2d2x2.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d2x2.getElementCopy(1, 0));

	EXPECT_EQ(true, array2d2x2.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d2x2.getElementCopy(1, 1));

	// Flip horizontally
	array2d2x2.flipHorizontally();

	// Elements now
	//   .#
	//   .#

	// Verify elements
	EXPECT_EQ(false, array2d2x2.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d2x2.getElementCopy(1, 0));

	EXPECT_EQ(false, array2d2x2.getElementCopy(0, 1));
	EXPECT_EQ(true, array2d2x2.getElementCopy(1, 1));

	// Flip horizontally
	array2d2x2.flipHorizontally();

	// Elements now
	//   #.
	//   #.

	// Verify elements
	EXPECT_EQ(true, array2d2x2.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d2x2.getElementCopy(1, 0));

	EXPECT_EQ(true, array2d2x2.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d2x2.getElementCopy(1, 1));
}


/**
 * Test that FlipHorizontally() works for 3x3 array.
 */
TEST_F(Array2Test, FlipHorizontally_size3x3)
{
	// Set elements
	array2d3x3.setElement(0, 0, true);
	array2d3x3.setElement(1, 0, true);
	array2d3x3.setElement(2, 0, true);

	array2d3x3.setElement(0, 1, true);
	array2d3x3.setElement(1, 1, false);
	array2d3x3.setElement(2, 1, false);

	array2d3x3.setElement(0, 2, false);
	array2d3x3.setElement(1, 2, false);
	array2d3x3.setElement(2, 2, true);

	// Elements now
	//   ###
	//   #..
	//   ..#

	// Verify elements
	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 2));

	// Flip horizontally
	array2d3x3.flipHorizontally();

	// Elements now
	//   ###
	//   ..#
	//   #..

	// Verify elements
	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 2));

	// Flip horizontally
	array2d3x3.flipHorizontally();

	// Elements now
	//   ###
	//   #..
	//   ..#

	// Verify elements
	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 2));
}


/**
 * Test that FlipHorizontally() works for 5x1 array.
 */
TEST_F(Array2Test, FlipHorizontally_size5x1)
{
	Array2<bool> array2d5x1(5, 1);

	// Set elements
	array2d5x1.setElement(0, 0, true);
	array2d5x1.setElement(1, 0, false);
	array2d5x1.setElement(2, 0, true);
	array2d5x1.setElement(3, 0, true);
	array2d5x1.setElement(4, 0, false);

	// Elements now
	//   #.##.

	// Verify elements
	EXPECT_EQ(true, array2d5x1.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d5x1.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d5x1.getElementCopy(2, 0));
	EXPECT_EQ(true, array2d5x1.getElementCopy(3, 0));
	EXPECT_EQ(false, array2d5x1.getElementCopy(4, 0));

	// Flip horizontally
	array2d5x1.flipHorizontally();

	// Elements now
	//   .##.#

	// Verify elements
	EXPECT_EQ(false, array2d5x1.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d5x1.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d5x1.getElementCopy(2, 0));
	EXPECT_EQ(false, array2d5x1.getElementCopy(3, 0));
	EXPECT_EQ(true, array2d5x1.getElementCopy(4, 0));

	// Flip horizontally
	array2d5x1.flipHorizontally();

	// Elements now
	//   #.##.

	// Verify elements
	EXPECT_EQ(true, array2d5x1.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d5x1.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d5x1.getElementCopy(2, 0));
	EXPECT_EQ(true, array2d5x1.getElementCopy(3, 0));
	EXPECT_EQ(false, array2d5x1.getElementCopy(4, 0));
}


/**
 * Test that FlipVertically() works for 1x1 array.
 */
TEST_F(Array2Test, FlipVertically_size1x1)
{
	Array2<bool> array2d1x1(1, 1);

	// Set elements
	array2d1x1.setElement(0, 0, false);

	// Elements now
	//   .

	// Verify elements
	EXPECT_EQ(false, array2d1x1.getElementCopy(0, 0));

	// Flip vertically
	array2d1x1.flipVertically();

	// Elements now
	//   .

	// Verify elements
	EXPECT_EQ(false, array2d1x1.getElementCopy(0, 0));

	// Flip vertically
	array2d1x1.flipVertically();

	// Elements now
	//   .

	// Verify elements
	EXPECT_EQ(false, array2d1x1.getElementCopy(0, 0));
}


/**
 * Test that FlipVertically() works for 2x2 array.
 */
TEST_F(Array2Test, FlipVertically_size2x2)
{
	Array2<bool> array2d2x2(2, 2);

	// Set elements
	array2d2x2.setElement(0, 0, false);
	array2d2x2.setElement(1, 0, false);

	array2d2x2.setElement(0, 1, true);
	array2d2x2.setElement(1, 1, true);

	// Elements now
	//   ..
	//   ##

	// Verify elements
	EXPECT_EQ(false, array2d2x2.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d2x2.getElementCopy(1, 0));

	EXPECT_EQ(true, array2d2x2.getElementCopy(0, 1));
	EXPECT_EQ(true, array2d2x2.getElementCopy(1, 1));

	// Flip vertically
	array2d2x2.flipVertically();

	// Elements now
	//   ##
	//   ..

	// Verify elements
	EXPECT_EQ(true, array2d2x2.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d2x2.getElementCopy(1, 0));

	EXPECT_EQ(false, array2d2x2.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d2x2.getElementCopy(1, 1));

	// Flip vertically
	array2d2x2.flipVertically();

	// Elements now
	//   ..
	//   ##

	// Verify elements
	EXPECT_EQ(false, array2d2x2.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d2x2.getElementCopy(1, 0));

	EXPECT_EQ(true, array2d2x2.getElementCopy(0, 1));
	EXPECT_EQ(true, array2d2x2.getElementCopy(1, 1));
}


/**
 * Test that FlipVertically() works for 3x3 array.
 */
TEST_F(Array2Test, FlipVertically_size3x3)
{
	// Set elements
	array2d3x3.setElement(0, 0, true);
	array2d3x3.setElement(1, 0, true);
	array2d3x3.setElement(2, 0, true);

	array2d3x3.setElement(0, 1, true);
	array2d3x3.setElement(1, 1, false);
	array2d3x3.setElement(2, 1, false);

	array2d3x3.setElement(0, 2, false);
	array2d3x3.setElement(1, 2, false);
	array2d3x3.setElement(2, 2, true);

	// Elements now
	//   ###
	//   #..
	//   ..#

	// Verify elements
	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 2));

	// Flip vertically
	array2d3x3.flipVertically();

	// Elements now
	//   ..#
	//   #..
	//   ###

	// Verify elements
	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 2));

	// Flip vertically
	array2d3x3.flipVertically();

	// Elements now
	//   ###
	//   #..
	//   ..#

	// Verify elements
	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(1, 0));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 0));

	EXPECT_EQ(true, array2d3x3.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 1));
	EXPECT_EQ(false, array2d3x3.getElementCopy(2, 1));

	EXPECT_EQ(false, array2d3x3.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d3x3.getElementCopy(1, 2));
	EXPECT_EQ(true, array2d3x3.getElementCopy(2, 2));
}


/**
 * Test that FlipVertically() works for 1x5 array.
 */
TEST_F(Array2Test, FlipVertically_size1x5)
{
	Array2<bool> array2d1x5(1, 5);

	// Set elements
	array2d1x5.setElement(0, 0, true);
	array2d1x5.setElement(0, 1, false);
	array2d1x5.setElement(0, 2, false);
	array2d1x5.setElement(0, 3, true);
	array2d1x5.setElement(0, 4, false);

	// Elements now
	//   #
	//   .
	//   .
	//   #
	//   .

	// Verify elements
	EXPECT_EQ(true, array2d1x5.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d1x5.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d1x5.getElementCopy(0, 2));
	EXPECT_EQ(true, array2d1x5.getElementCopy(0, 3));
	EXPECT_EQ(false, array2d1x5.getElementCopy(0, 4));

	// Flip vertically
	array2d1x5.flipVertically();

	// Elements now
	//   .
	//   #
	//   .
	//   .
	//   #

	// Verify elements
	EXPECT_EQ(false, array2d1x5.getElementCopy(0, 0));
	EXPECT_EQ(true, array2d1x5.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d1x5.getElementCopy(0, 2));
	EXPECT_EQ(false, array2d1x5.getElementCopy(0, 3));
	EXPECT_EQ(true, array2d1x5.getElementCopy(0, 4));

	// Flip vertically
	array2d1x5.flipVertically();

	// Elements now
	//   #
	//   .
	//   .
	//   #
	//   .

	// Verify elements
	EXPECT_EQ(true, array2d1x5.getElementCopy(0, 0));
	EXPECT_EQ(false, array2d1x5.getElementCopy(0, 1));
	EXPECT_EQ(false, array2d1x5.getElementCopy(0, 2));
	EXPECT_EQ(true, array2d1x5.getElementCopy(0, 3));
	EXPECT_EQ(false, array2d1x5.getElementCopy(0, 4));
}

/**
 * Test that Blit() works when blitting 5x6 array to 3x4 array at coordinates (-1,-1).
 */
TEST_F(Array2Test, Blit_size5x6_to_3x4)
{
	Array2<unsigned int> array2d3x4int(3, 4);

	// Set elements
	array2d3x4int.setElement(0, 0, 1);
	array2d3x4int.setElement(1, 0, 2);
	array2d3x4int.setElement(2, 0, 3);

	array2d3x4int.setElement(0, 1, 4);
	array2d3x4int.setElement(1, 1, 5);
	array2d3x4int.setElement(2, 1, 6);

	array2d3x4int.setElement(0, 2, 7);
	array2d3x4int.setElement(1, 2, 8);
	array2d3x4int.setElement(2, 2, 9);

	array2d3x4int.setElement(0, 3, 10);
	array2d3x4int.setElement(1, 3, 11);
	array2d3x4int.setElement(2, 3, 12);

	// 4x3 elements now
	//    1  2  3
	//    4  5  6
	//    7  8  9
	//   10 11 12

	Array2<unsigned int> array2d5x6int(5, 6);

	// Set elements
	array2d5x6int.setElement(0, 0, 1);
	array2d5x6int.setElement(1, 0, 2);
	array2d5x6int.setElement(2, 0, 3);
	array2d5x6int.setElement(3, 0, 4);
	array2d5x6int.setElement(4, 0, 5);

	array2d5x6int.setElement(0, 1, 6);
	array2d5x6int.setElement(1, 1, 7);
	array2d5x6int.setElement(2, 1, 8);
	array2d5x6int.setElement(3, 1, 9);
	array2d5x6int.setElement(4, 1, 10);

	array2d5x6int.setElement(0, 2, 11);
	array2d5x6int.setElement(1, 2, 12);
	array2d5x6int.setElement(2, 2, 13);
	array2d5x6int.setElement(3, 2, 14);
	array2d5x6int.setElement(4, 2, 15);

	array2d5x6int.setElement(0, 3, 16);
	array2d5x6int.setElement(1, 3, 17);
	array2d5x6int.setElement(2, 3, 18);
	array2d5x6int.setElement(3, 3, 19);
	array2d5x6int.setElement(4, 3, 20);

	array2d5x6int.setElement(0, 4, 21);
	array2d5x6int.setElement(1, 4, 22);
	array2d5x6int.setElement(2, 4, 23);
	array2d5x6int.setElement(3, 4, 24);
	array2d5x6int.setElement(4, 4, 25);

	array2d5x6int.setElement(0, 5, 26);
	array2d5x6int.setElement(1, 5, 27);
	array2d5x6int.setElement(2, 5, 28);
	array2d5x6int.setElement(3, 5, 29);
	array2d5x6int.setElement(4, 5, 30);

	// 5x6 elements now
	//    1  2  3  4  5
	//    6  7  8  9 10
	//   11 12 13 14 15
	//   16 17 18 19 20
	//   21 22 23 24 25
	//   26 27 28 29 30

	// Blit 5x6 to 3x4
	array2d3x4int.blit(-1, -1, array2d5x6int);

	// 3x4 elements now
	//     7  8  9
	//    12 13 14
	//    17 18 19
	//    22 23 24

	// Verify elements
	EXPECT_EQ(7, array2d3x4int.getElementCopy(0, 0));
	EXPECT_EQ(8, array2d3x4int.getElementCopy(1, 0));
	EXPECT_EQ(9, array2d3x4int.getElementCopy(2, 0));

	EXPECT_EQ(12, array2d3x4int.getElementCopy(0, 1));
	EXPECT_EQ(13, array2d3x4int.getElementCopy(1, 1));
	EXPECT_EQ(14, array2d3x4int.getElementCopy(2, 1));

	EXPECT_EQ(17, array2d3x4int.getElementCopy(0, 2));
	EXPECT_EQ(18, array2d3x4int.getElementCopy(1, 2));
	EXPECT_EQ(19, array2d3x4int.getElementCopy(2, 2));

	EXPECT_EQ(22, array2d3x4int.getElementCopy(0, 3));
	EXPECT_EQ(23, array2d3x4int.getElementCopy(1, 3));
	EXPECT_EQ(24, array2d3x4int.getElementCopy(2, 3));
}


/**
 * Test that Blit() works when blitting 5x5 array to 3x3 array at coordinates (1,2).
 */
TEST_F(Array2Test, Blit_size5x5_to_3x3_test1)
{
	Array2<unsigned int> array2d3x3int(3, 3);

	// Set elements
	array2d3x3int.setElement(0, 0, 1);
	array2d3x3int.setElement(1, 0, 2);
	array2d3x3int.setElement(2, 0, 3);

	array2d3x3int.setElement(0, 1, 4);
	array2d3x3int.setElement(1, 1, 5);
	array2d3x3int.setElement(2, 1, 6);

	array2d3x3int.setElement(0, 2, 7);
	array2d3x3int.setElement(1, 2, 8);
	array2d3x3int.setElement(2, 2, 9);

	// 3x3 elements now
	//   1 2 3
	//   4 5 6
	//   7 8 9

	Array2<unsigned int> array2d5x5int(5, 5);

	// Set elements
	array2d5x5int.setElement(0, 0, 1);
	array2d5x5int.setElement(1, 0, 2);
	array2d5x5int.setElement(2, 0, 3);
	array2d5x5int.setElement(3, 0, 4);
	array2d5x5int.setElement(4, 0, 5);

	array2d5x5int.setElement(0, 1, 6);
	array2d5x5int.setElement(1, 1, 7);
	array2d5x5int.setElement(2, 1, 8);
	array2d5x5int.setElement(3, 1, 9);
	array2d5x5int.setElement(4, 1, 10);

	array2d5x5int.setElement(0, 2, 11);
	array2d5x5int.setElement(1, 2, 12);
	array2d5x5int.setElement(2, 2, 13);
	array2d5x5int.setElement(3, 2, 14);
	array2d5x5int.setElement(4, 2, 15);

	array2d5x5int.setElement(0, 3, 16);
	array2d5x5int.setElement(1, 3, 17);
	array2d5x5int.setElement(2, 3, 18);
	array2d5x5int.setElement(3, 3, 19);
	array2d5x5int.setElement(4, 3, 20);

	array2d5x5int.setElement(0, 4, 21);
	array2d5x5int.setElement(1, 4, 22);
	array2d5x5int.setElement(2, 4, 23);
	array2d5x5int.setElement(3, 4, 24);
	array2d5x5int.setElement(4, 4, 25);

	// 5x5 elements now
	//    1  2  3  4  5
	//    6  7  8  9 10
	//   11 12 13 14 15
	//   16 17 18 19 20
	//   21 22 23 24 25

	// Blit 5x5 to 3x3
	array2d3x3int.blit(1, 2, array2d5x5int);

	// 3x3 elements now
	//   1 2 3
	//   4 5 6
	//   7 1 2

	// Verify elements
	EXPECT_EQ(1, array2d3x3int.getElementCopy(0, 0));
	EXPECT_EQ(2, array2d3x3int.getElementCopy(1, 0));
	EXPECT_EQ(3, array2d3x3int.getElementCopy(2, 0));

	EXPECT_EQ(4, array2d3x3int.getElementCopy(0, 1));
	EXPECT_EQ(5, array2d3x3int.getElementCopy(1, 1));
	EXPECT_EQ(6, array2d3x3int.getElementCopy(2, 1));

	EXPECT_EQ(7, array2d3x3int.getElementCopy(0, 2));
	EXPECT_EQ(1, array2d3x3int.getElementCopy(1, 2));
	EXPECT_EQ(2, array2d3x3int.getElementCopy(2, 2));
}


/**
 * Test that Blit() works when blitting 5x5 array to 3x3 array at coordinates (-4,-3).
 */
TEST_F(Array2Test, Blit_size5x5_to_3x3_test2)
{
	Array2<unsigned int> array2d3x3int(3, 3);

	// Set elements
	array2d3x3int.setElement(0, 0, 1);
	array2d3x3int.setElement(1, 0, 2);
	array2d3x3int.setElement(2, 0, 3);

	array2d3x3int.setElement(0, 1, 4);
	array2d3x3int.setElement(1, 1, 5);
	array2d3x3int.setElement(2, 1, 6);

	array2d3x3int.setElement(0, 2, 7);
	array2d3x3int.setElement(1, 2, 8);
	array2d3x3int.setElement(2, 2, 9);

	// 3x3 elements now
	//   1 2 3
	//   4 5 6
	//   7 8 9

	Array2<unsigned int> array2d5x5int(5, 5);

	// Set elements
	array2d5x5int.setElement(0, 0, 1);
	array2d5x5int.setElement(1, 0, 2);
	array2d5x5int.setElement(2, 0, 3);
	array2d5x5int.setElement(3, 0, 4);
	array2d5x5int.setElement(4, 0, 5);

	array2d5x5int.setElement(0, 1, 6);
	array2d5x5int.setElement(1, 1, 7);
	array2d5x5int.setElement(2, 1, 8);
	array2d5x5int.setElement(3, 1, 9);
	array2d5x5int.setElement(4, 1, 10);

	array2d5x5int.setElement(0, 2, 11);
	array2d5x5int.setElement(1, 2, 12);
	array2d5x5int.setElement(2, 2, 13);
	array2d5x5int.setElement(3, 2, 14);
	array2d5x5int.setElement(4, 2, 15);

	array2d5x5int.setElement(0, 3, 16);
	array2d5x5int.setElement(1, 3, 17);
	array2d5x5int.setElement(2, 3, 18);
	array2d5x5int.setElement(3, 3, 19);
	array2d5x5int.setElement(4, 3, 20);

	array2d5x5int.setElement(0, 4, 21);
	array2d5x5int.setElement(1, 4, 22);
	array2d5x5int.setElement(2, 4, 23);
	array2d5x5int.setElement(3, 4, 24);
	array2d5x5int.setElement(4, 4, 25);

	// 5x5 elements now
	//    1  2  3  4  5
	//    6  7  8  9 10
	//   11 12 13 14 15
	//   16 17 18 19 20
	//   21 22 23 24 25

	// Blit 5x5 to 3x3
	array2d3x3int.blit(-4, -3, array2d5x5int);

	// 3x3 elements now
	//   20  2  3
	//   25  5  6
	//    7  8  9

	// Verify elements
	EXPECT_EQ(20, array2d3x3int.getElementCopy(0, 0));
	EXPECT_EQ(2, array2d3x3int.getElementCopy(1, 0));
	EXPECT_EQ(3, array2d3x3int.getElementCopy(2, 0));

	EXPECT_EQ(25, array2d3x3int.getElementCopy(0, 1));
	EXPECT_EQ(5, array2d3x3int.getElementCopy(1, 1));
	EXPECT_EQ(6, array2d3x3int.getElementCopy(2, 1));

	EXPECT_EQ(7, array2d3x3int.getElementCopy(0, 2));
	EXPECT_EQ(8, array2d3x3int.getElementCopy(1, 2));
	EXPECT_EQ(9, array2d3x3int.getElementCopy(2, 2));
}


/**
 * Test that Blit() works when blitting 3x3 array to 5x5 array at coordinates (1,1).
 */
TEST_F(Array2Test, Blit_size3x3_to_5x5)
{
	Array2<unsigned int> array2d5x5int(5, 5);

	// Set elements
	array2d5x5int.setElement(0, 0, 1);
	array2d5x5int.setElement(1, 0, 2);
	array2d5x5int.setElement(2, 0, 3);
	array2d5x5int.setElement(3, 0, 4);
	array2d5x5int.setElement(4, 0, 5);

	array2d5x5int.setElement(0, 1, 6);
	array2d5x5int.setElement(1, 1, 7);
	array2d5x5int.setElement(2, 1, 8);
	array2d5x5int.setElement(3, 1, 9);
	array2d5x5int.setElement(4, 1, 10);

	array2d5x5int.setElement(0, 2, 11);
	array2d5x5int.setElement(1, 2, 12);
	array2d5x5int.setElement(2, 2, 13);
	array2d5x5int.setElement(3, 2, 14);
	array2d5x5int.setElement(4, 2, 15);

	array2d5x5int.setElement(0, 3, 16);
	array2d5x5int.setElement(1, 3, 17);
	array2d5x5int.setElement(2, 3, 18);
	array2d5x5int.setElement(3, 3, 19);
	array2d5x5int.setElement(4, 3, 20);

	array2d5x5int.setElement(0, 4, 21);
	array2d5x5int.setElement(1, 4, 22);
	array2d5x5int.setElement(2, 4, 23);
	array2d5x5int.setElement(3, 4, 24);
	array2d5x5int.setElement(4, 4, 25);

	// 5x5 elements now
	//    1  2  3  4  5
	//    6  7  8  9 10
	//   11 12 13 14 15
	//   16 17 18 19 20
	//   21 22 23 24 25

	Array2<unsigned int> array2d3x3int(3, 3);

	// Set elements
	array2d3x3int.setElement(0, 0, 1);
	array2d3x3int.setElement(1, 0, 2);
	array2d3x3int.setElement(2, 0, 3);

	array2d3x3int.setElement(0, 1, 4);
	array2d3x3int.setElement(1, 1, 5);
	array2d3x3int.setElement(2, 1, 6);

	array2d3x3int.setElement(0, 2, 7);
	array2d3x3int.setElement(1, 2, 8);
	array2d3x3int.setElement(2, 2, 9);

	// 3x3 elements now
	//   1 2 3
	//   4 5 6
	//   7 8 9

	// Blit 3x3 to 5x5
	array2d5x5int.blit(1, 1, array2d3x3int);

	// 5x5 elements now
	//    1  2  3  4  5
	//    6  1  2  3 10
	//   11  4  5  6 15
	//   16  7  8  9 20
	//   21 22 23 24 25

	// Verify elements
	EXPECT_EQ(1, array2d5x5int.getElementCopy(0, 0));
	EXPECT_EQ(2, array2d5x5int.getElementCopy(1, 0));
	EXPECT_EQ(3, array2d5x5int.getElementCopy(2, 0));
	EXPECT_EQ(4, array2d5x5int.getElementCopy(3, 0));
	EXPECT_EQ(5, array2d5x5int.getElementCopy(4, 0));

	EXPECT_EQ(6, array2d5x5int.getElementCopy(0, 1));
	EXPECT_EQ(1, array2d5x5int.getElementCopy(1, 1));
	EXPECT_EQ(2, array2d5x5int.getElementCopy(2, 1));
	EXPECT_EQ(3, array2d5x5int.getElementCopy(3, 1));
	EXPECT_EQ(10, array2d5x5int.getElementCopy(4, 1));

	EXPECT_EQ(11, array2d5x5int.getElementCopy(0, 2));
	EXPECT_EQ(4, array2d5x5int.getElementCopy(1, 2));
	EXPECT_EQ(5, array2d5x5int.getElementCopy(2, 2));
	EXPECT_EQ(6, array2d5x5int.getElementCopy(3, 2));
	EXPECT_EQ(15, array2d5x5int.getElementCopy(4, 2));

	EXPECT_EQ(16, array2d5x5int.getElementCopy(0, 3));
	EXPECT_EQ(7, array2d5x5int.getElementCopy(1, 3));
	EXPECT_EQ(8, array2d5x5int.getElementCopy(2, 3));
	EXPECT_EQ(9, array2d5x5int.getElementCopy(3, 3));
	EXPECT_EQ(20, array2d5x5int.getElementCopy(4, 3));

	EXPECT_EQ(21, array2d5x5int.getElementCopy(0, 4));
	EXPECT_EQ(22, array2d5x5int.getElementCopy(1, 4));
	EXPECT_EQ(23, array2d5x5int.getElementCopy(2, 4));
	EXPECT_EQ(24, array2d5x5int.getElementCopy(3, 4));
	EXPECT_EQ(25, array2d5x5int.getElementCopy(4, 4));
}


/**
 * Test that Blit() works when blitting 13x19 array to 2x1 array at coordinates (-3,-4).
 */
TEST_F(Array2Test, Blit_size13x19_to_2x1)
{
	Array2<unsigned int> array2d2x1int(2, 1);
	Array2<unsigned int> array2d13x19int(13, 19);

	// Set elements
	array2d13x19int.setElement(3, 4, 321);
	array2d13x19int.setElement(4, 4, 123);

	// 13x19 elements now
	//     0   0   0   0   0 ...
	//     0   0   0   0   0 ...
	//     0   0   0   0   0 ...
	//     0   0   0   0   0 ...
	//     0   0   0 321 123 ...
	//     0   0   0   0   0 ...
	//     ...

	// Blit 13x19 to 2x1
	array2d2x1int.blit(-3, -4, 240, 129, array2d13x19int);

	// 2x1 elements now
	//    321 123

	EXPECT_EQ(321, array2d2x1int.getElementCopy(0, 0));
	EXPECT_EQ(123, array2d2x1int.getElementCopy(1, 0));
}




/**
 * Test that Blit() works when blitting 1x2 array to 3x3 array at out of bounds
 * coordinates.
 */
TEST_F(Array2Test, Blit_size1x2_3x3_out_of_bounds)
{
	Array2<unsigned int> array2d1x2int(1, 2);
	Array2<unsigned int> array2d3x3int(3, 3);

	// Set elements
	array2d1x2int.setElement(0, 0, 19);
	array2d1x2int.setElement(0, 1, 15);

	// 1x2 elements now
	//	[ 19 ]
	//	[ 15 ]

	// Set elements
	array2d3x3int.setElement(0, 0, 1);
	array2d3x3int.setElement(1, 0, 2);
	array2d3x3int.setElement(2, 0, 3);

	array2d3x3int.setElement(0, 1, 4);
	array2d3x3int.setElement(1, 1, 5);
	array2d3x3int.setElement(2, 1, 6);

	array2d3x3int.setElement(0, 2, 7);
	array2d3x3int.setElement(1, 2, 8);
	array2d3x3int.setElement(2, 2, 9);

	// 3x3 elements now
	//	[ 1 2 3 ]
	//	[ 4 5 6 ]
	//	[ 7 8 9 ]

	// Blit 1x2 to 3x3 out of bounds severel times
	array2d3x3int.blit(-100, -100, array2d1x2int);
	array2d3x3int.blit(-1, -2, array2d1x2int);
	array2d3x3int.blit(1, -2, array2d1x2int);

	array2d3x3int.blit(-1, 0, array2d1x2int);
	array2d3x3int.blit(3, 0, array2d1x2int);

	array2d3x3int.blit(-1, 1, array2d1x2int);
	array2d3x3int.blit(3, 1, array2d1x2int);

	array2d3x3int.blit(-1, 2, array2d1x2int);
	array2d3x3int.blit(3, 2, array2d1x2int);

	array2d3x3int.blit(-1, 3, array2d1x2int);
	array2d3x3int.blit(1, 3, array2d1x2int);
	array2d3x3int.blit(3, 3, array2d1x2int);

	// 3x3 elements now
	//	[ 1 2 3 ]
	//	[ 4 5 6 ]
	//	[ 7 8 9 ]

	EXPECT_EQ(1, array2d3x3int.getElementCopy(0, 0));
	EXPECT_EQ(2, array2d3x3int.getElementCopy(1, 0));
	EXPECT_EQ(3, array2d3x3int.getElementCopy(2, 0));

	EXPECT_EQ(4, array2d3x3int.getElementCopy(0, 1));
	EXPECT_EQ(5, array2d3x3int.getElementCopy(1, 1));
	EXPECT_EQ(6, array2d3x3int.getElementCopy(2, 1));

	EXPECT_EQ(7, array2d3x3int.getElementCopy(0, 2));
	EXPECT_EQ(8, array2d3x3int.getElementCopy(1, 2));
	EXPECT_EQ(9, array2d3x3int.getElementCopy(2, 2));
}


/**
 * Test that RefitInsideValidArea() works in out of bounds situations.
 */
TEST_F(Array2Test, RefitInsideValidArea_out_of_bounds)
{
	Array2<unsigned int> array2d1x2int(1, 2);
	Array2<unsigned int> array2d3x3int(3, 3);

	int x1, y1, x2, y2, sX, sY;

	x1=-100; y1=-100; x2=-100+1; y2=-100+2; sX=0; sY=0;
	EXPECT_FALSE(array2d3x3int.refitInsideValidArea(x1, y1, x2, y2, array2d1x2int, sX, sY));
	x1=-1; y1=-2; x2=-1+1; y2=-2+2; sX=0; sY=0;
	EXPECT_FALSE(array2d3x3int.refitInsideValidArea(x1, y1, x2, y2, array2d1x2int, sX, sY));
	x1=1; y1=-2; x2=1+1; y2=-2+2; sX=0; sY=0;
	EXPECT_FALSE(array2d3x3int.refitInsideValidArea(x1, y1, x2, y2, array2d1x2int, sX, sY));

	x1=-1; y1=0; x2=-1+1; y2=0+2; sX=0; sY=0;
	EXPECT_FALSE(array2d3x3int.refitInsideValidArea(x1, y1, x2, y2, array2d1x2int, sX, sY));
	x1=3; y1=0; x2=3+1; y2=0+2; sX=0; sY=0;
	EXPECT_FALSE(array2d3x3int.refitInsideValidArea(x1, y1, x2, y2, array2d1x2int, sX, sY));

	x1=-1; y1=1; x2=-1+1; y2=1+2; sX=0; sY=0;
	EXPECT_FALSE(array2d3x3int.refitInsideValidArea(x1, y1, x2, y2, array2d1x2int, sX, sY));
	x1=3; y1=1; x2=3+1; y2=1+2; sX=0; sY=0;
	EXPECT_FALSE(array2d3x3int.refitInsideValidArea(x1, y1, x2, y2, array2d1x2int, sX, sY));

	x1=-1; y1=2; x2=-1+1; y2=2+2; sX=0; sY=0;
	EXPECT_FALSE(array2d3x3int.refitInsideValidArea(x1, y1, x2, y2, array2d1x2int, sX, sY));
	x1=3; y1=2; x2=3+1; y2=2+2; sX=0; sY=0;
	EXPECT_FALSE(array2d3x3int.refitInsideValidArea(x1, y1, x2, y2, array2d1x2int, sX, sY));

	x1=-1; y1=3; x2=-1+1; y2=3+2; sX=0; sY=0;
	EXPECT_FALSE(array2d3x3int.refitInsideValidArea(x1, y1, x2, y2, array2d1x2int, sX, sY));
	x1=1; y1=3; x2=1+1; y2=3+2; sX=0; sY=0;
	EXPECT_FALSE(array2d3x3int.refitInsideValidArea(x1, y1, x2, y2, array2d1x2int, sX, sY));
	x1=3; y1=3; x2=3+1; y2=3+2; sX=0; sY=0;
	EXPECT_FALSE(array2d3x3int.refitInsideValidArea(x1, y1, x2, y2, array2d1x2int, sX, sY));
}


/**
 * Test that BlitMasked() works when blitting 3x3 array in the
 * middle of 5x5 array using source mask, target mask and
 * source & target masks.
 */
TEST_F(Array2Test, BlitMasked_size_3x3_to_5x5)
{
	Array2<unsigned int> sourceArray(3, 3);
	Array2<bool> sourceMask(3, 3);

	Array2<unsigned int> targetArray(5, 5);
	Array2<bool> targetMask(5, 5);

	// Set elements (sourceArray)
	sourceArray.setElement(0, 0, 1);
	sourceArray.setElement(1, 0, 2);
	sourceArray.setElement(2, 0, 3);

	sourceArray.setElement(0, 1, 4);
	sourceArray.setElement(1, 1, 5);
	sourceArray.setElement(2, 1, 6);

	sourceArray.setElement(0, 2, 7);
	sourceArray.setElement(1, 2, 8);
	sourceArray.setElement(2, 2, 9);

	// sourceArray now
	//   1 2 3
	//   4 5 6
	//   7 8 9

	// Set elements (sourceMask)
	sourceMask.setElement(0, 0, false);
	sourceMask.setElement(1, 0, true);
	sourceMask.setElement(2, 0, false);

	sourceMask.setElement(0, 1, true);
	sourceMask.setElement(1, 1, true);
	sourceMask.setElement(2, 1, true);

	sourceMask.setElement(0, 2, false);
	sourceMask.setElement(1, 2, true);
	sourceMask.setElement(2, 2, false);

	// sourceMask now
	//   .#.
	//   ###
	//   .#.

	// Set elements (targetArray)
	targetArray.setElement(0, 0, 10);
	targetArray.setElement(1, 0, 20);
	targetArray.setElement(2, 0, 30);
	targetArray.setElement(3, 0, 40);
	targetArray.setElement(4, 0, 50);

	targetArray.setElement(0, 1, 60);
	targetArray.setElement(1, 1, 70);
	targetArray.setElement(2, 1, 80);
	targetArray.setElement(3, 1, 90);
	targetArray.setElement(4, 1, 100);

	targetArray.setElement(0, 2, 110);
	targetArray.setElement(1, 2, 120);
	targetArray.setElement(2, 2, 130);
	targetArray.setElement(3, 2, 140);
	targetArray.setElement(4, 2, 150);

	targetArray.setElement(0, 3, 160);
	targetArray.setElement(1, 3, 170);
	targetArray.setElement(2, 3, 180);
	targetArray.setElement(3, 3, 190);
	targetArray.setElement(4, 3, 200);

	targetArray.setElement(0, 4, 210);
	targetArray.setElement(1, 4, 220);
	targetArray.setElement(2, 4, 230);
	targetArray.setElement(3, 4, 240);
	targetArray.setElement(4, 4, 250);

	// targetArray now
	//    10  20  30  40  50
	//    60  70  80  90 100
	//   110 120 130 140 150
	//   160 170 180 190 200
	//   210 220 230 240 250

	// Set elements (targetMask)
	targetMask.setElement(0, 0, true);
	targetMask.setElement(1, 0, true);
	targetMask.setElement(2, 0, true);
	targetMask.setElement(3, 0, true);
	targetMask.setElement(4, 0, true);

	targetMask.setElement(0, 1, true);
	targetMask.setElement(1, 1, true);
	targetMask.setElement(2, 1, true);
	targetMask.setElement(3, 1, true);
	targetMask.setElement(4, 1, true);

	targetMask.setElement(0, 2, true);
	targetMask.setElement(1, 2, true);
	targetMask.setElement(2, 2, false);
	targetMask.setElement(3, 2, true);
	targetMask.setElement(4, 2, true);

	targetMask.setElement(0, 3, false);
	targetMask.setElement(1, 3, false);
	targetMask.setElement(2, 3, false);
	targetMask.setElement(3, 3, false);
	targetMask.setElement(4, 3, false);

	targetMask.setElement(0, 4, false);
	targetMask.setElement(1, 4, false);
	targetMask.setElement(2, 4, false);
	targetMask.setElement(3, 4, false);
	targetMask.setElement(4, 4, false);

	// targetMask now
	//   #####
	//   #####
	//   ##.##
	//   .....
	//   .....

	//Array2<unsigned int> sourceArray(3, 3);
	//Array2<bool> sourceMask(3, 3);
	//Array2<unsigned int> targetArray(5, 5);
	//Array2<bool> targetMask(5, 5);
	//BlitMasked(int x, int y, const Array2& source,
	//	Array2<bool>* sourceMask, Array2<bool>* targetMask)

	Array2<unsigned int> tempTargetArray = targetArray;
	tempTargetArray.blitMasked(1, 1, sourceArray, &sourceMask, 0);

	// tempTargetArray now
	//    10  20  30  40  50
	//    60  70   2  90 100
	//   110   4   5   6 150
	//   160 170   8 190 200
	//   210 220 230 240 250

	// Verify elements (tempTargetArray)
	EXPECT_EQ(10, tempTargetArray.getElementCopy(0, 0));
	EXPECT_EQ(20, tempTargetArray.getElementCopy(1, 0));
	EXPECT_EQ(30, tempTargetArray.getElementCopy(2, 0));
	EXPECT_EQ(40, tempTargetArray.getElementCopy(3, 0));
	EXPECT_EQ(50, tempTargetArray.getElementCopy(4, 0));

	EXPECT_EQ(60, tempTargetArray.getElementCopy(0, 1));
	EXPECT_EQ(70, tempTargetArray.getElementCopy(1, 1));
	EXPECT_EQ(2, tempTargetArray.getElementCopy(2, 1));
	EXPECT_EQ(90, tempTargetArray.getElementCopy(3, 1));
	EXPECT_EQ(100, tempTargetArray.getElementCopy(4, 1));

	EXPECT_EQ(110, tempTargetArray.getElementCopy(0, 2));
	EXPECT_EQ(4, tempTargetArray.getElementCopy(1, 2));
	EXPECT_EQ(5, tempTargetArray.getElementCopy(2, 2));
	EXPECT_EQ(6, tempTargetArray.getElementCopy(3, 2));
	EXPECT_EQ(150, tempTargetArray.getElementCopy(4, 2));

	EXPECT_EQ(160, tempTargetArray.getElementCopy(0, 3));
	EXPECT_EQ(170, tempTargetArray.getElementCopy(1, 3));
	EXPECT_EQ(8, tempTargetArray.getElementCopy(2, 3));
	EXPECT_EQ(190, tempTargetArray.getElementCopy(3, 3));
	EXPECT_EQ(200, tempTargetArray.getElementCopy(4, 3));

	EXPECT_EQ(210, tempTargetArray.getElementCopy(0, 4));
	EXPECT_EQ(220, tempTargetArray.getElementCopy(1, 4));
	EXPECT_EQ(230, tempTargetArray.getElementCopy(2, 4));
	EXPECT_EQ(240, tempTargetArray.getElementCopy(3, 4));
	EXPECT_EQ(250, tempTargetArray.getElementCopy(4, 4));

	tempTargetArray = targetArray;
	tempTargetArray.blitMasked(1, 1, sourceArray, 0, &targetMask);

	// tempTargetArray now
	//    10  20  30  40  50
	//    60   1   2   3 100
	//   110   4 130   6 150
	//   160 170 180 190 200
	//   210 220 230 240 250

	// Verify elements (tempTargetArray)
	EXPECT_EQ(10, tempTargetArray.getElementCopy(0, 0));
	EXPECT_EQ(20, tempTargetArray.getElementCopy(1, 0));
	EXPECT_EQ(30, tempTargetArray.getElementCopy(2, 0));
	EXPECT_EQ(40, tempTargetArray.getElementCopy(3, 0));
	EXPECT_EQ(50, tempTargetArray.getElementCopy(4, 0));

	EXPECT_EQ(60, tempTargetArray.getElementCopy(0, 1));
	EXPECT_EQ(1, tempTargetArray.getElementCopy(1, 1));
	EXPECT_EQ(2, tempTargetArray.getElementCopy(2, 1));
	EXPECT_EQ(3, tempTargetArray.getElementCopy(3, 1));
	EXPECT_EQ(100, tempTargetArray.getElementCopy(4, 1));

	EXPECT_EQ(110, tempTargetArray.getElementCopy(0, 2));
	EXPECT_EQ(4, tempTargetArray.getElementCopy(1, 2));
	EXPECT_EQ(130, tempTargetArray.getElementCopy(2, 2));
	EXPECT_EQ(6, tempTargetArray.getElementCopy(3, 2));
	EXPECT_EQ(150, tempTargetArray.getElementCopy(4, 2));

	EXPECT_EQ(160, tempTargetArray.getElementCopy(0, 3));
	EXPECT_EQ(170, tempTargetArray.getElementCopy(1, 3));
	EXPECT_EQ(180, tempTargetArray.getElementCopy(2, 3));
	EXPECT_EQ(190, tempTargetArray.getElementCopy(3, 3));
	EXPECT_EQ(200, tempTargetArray.getElementCopy(4, 3));

	EXPECT_EQ(210, tempTargetArray.getElementCopy(0, 4));
	EXPECT_EQ(220, tempTargetArray.getElementCopy(1, 4));
	EXPECT_EQ(230, tempTargetArray.getElementCopy(2, 4));
	EXPECT_EQ(240, tempTargetArray.getElementCopy(3, 4));
	EXPECT_EQ(250, tempTargetArray.getElementCopy(4, 4));

	tempTargetArray = targetArray;
	tempTargetArray.blitMasked(1, 1, sourceArray, &sourceMask, &targetMask);

	// tempTargetArray now
	//    10  20  30  40  50
	//    60  70   2  90 100
	//   110   4 130   6 150
	//   160 170 180 190 200
	//   210 220 230 240 250

	// Verify elements (tempTargetArray)
	EXPECT_EQ(10, tempTargetArray.getElementCopy(0, 0));
	EXPECT_EQ(20, tempTargetArray.getElementCopy(1, 0));
	EXPECT_EQ(30, tempTargetArray.getElementCopy(2, 0));
	EXPECT_EQ(40, tempTargetArray.getElementCopy(3, 0));
	EXPECT_EQ(50, tempTargetArray.getElementCopy(4, 0));

	EXPECT_EQ(60, tempTargetArray.getElementCopy(0, 1));
	EXPECT_EQ(70, tempTargetArray.getElementCopy(1, 1));
	EXPECT_EQ(2, tempTargetArray.getElementCopy(2, 1));
	EXPECT_EQ(90, tempTargetArray.getElementCopy(3, 1));
	EXPECT_EQ(100, tempTargetArray.getElementCopy(4, 1));

	EXPECT_EQ(110, tempTargetArray.getElementCopy(0, 2));
	EXPECT_EQ(4, tempTargetArray.getElementCopy(1, 2));
	EXPECT_EQ(130, tempTargetArray.getElementCopy(2, 2));
	EXPECT_EQ(6, tempTargetArray.getElementCopy(3, 2));
	EXPECT_EQ(150, tempTargetArray.getElementCopy(4, 2));

	EXPECT_EQ(160, tempTargetArray.getElementCopy(0, 3));
	EXPECT_EQ(170, tempTargetArray.getElementCopy(1, 3));
	EXPECT_EQ(180, tempTargetArray.getElementCopy(2, 3));
	EXPECT_EQ(190, tempTargetArray.getElementCopy(3, 3));
	EXPECT_EQ(200, tempTargetArray.getElementCopy(4, 3));

	EXPECT_EQ(210, tempTargetArray.getElementCopy(0, 4));
	EXPECT_EQ(220, tempTargetArray.getElementCopy(1, 4));
	EXPECT_EQ(230, tempTargetArray.getElementCopy(2, 4));
	EXPECT_EQ(240, tempTargetArray.getElementCopy(3, 4));
	EXPECT_EQ(250, tempTargetArray.getElementCopy(4, 4));
}


/**
 * Test that BlitMasked() refuses to use invalid size source
 * and/or target masks.
 */
TEST_F(Array2Test, BlitMasked_invalid_mask_size)
{
	Array2<unsigned int> sourceArray(3, 3);
	Array2<bool> sourceMask(3, 3);
	Array2<bool> sourceMask2(3, 4);
	Array2<unsigned int> targetArray(5, 5);
	Array2<bool> targetMask(5, 5);
	Array2<bool> targetMask2(4, 5);

	EXPECT_THROW(targetArray.blitMasked(1, 1, sourceArray, &targetMask, 0), Verso::AssertException);
	EXPECT_THROW(targetArray.blitMasked(1, 1, sourceArray, 0, &sourceMask), Verso::AssertException);
	EXPECT_THROW(targetArray.blitMasked(1, 1, sourceArray, &targetMask, &sourceMask), Verso::AssertException);

	EXPECT_THROW(targetArray.blitMasked(1, 1, sourceArray, &sourceMask2, 0), Verso::AssertException);
	EXPECT_THROW(targetArray.blitMasked(1, 1, sourceArray, 0, &targetMask2), Verso::AssertException);
	EXPECT_THROW(targetArray.blitMasked(1, 1, sourceArray, &sourceMask2, &targetMask2), Verso::AssertException);
}


/**
 * Test that FloodFill4Directions() works.
 */
TEST_F(Array2Test, FloodFill4Directions)
{
	//maze.Printout(1);

	EXPECT_EQ(23, maze.floodFill4Directions(3, 3, 5));
	EXPECT_TRUE(maze.isEqual(maze4DirectionFilled));

	//maze.Printout(1);

	EXPECT_EQ(1, maze.floodFill4Directions(5, 5, 6));

	// Update the expected result
	maze4DirectionFilled.setElement(5, 5, 6);
	EXPECT_TRUE(maze.isEqual(maze4DirectionFilled));

	//maze.Printout(1);

	EXPECT_EQ(1, maze.floodFill4Directions(0, 0, 2));

	// Update the expected result
	maze4DirectionFilled.setElement(0, 0, 2);
	EXPECT_TRUE(maze.isEqual(maze4DirectionFilled));

	//maze.Printout(1);

	EXPECT_EQ(1, maze.floodFill4Directions(2, 0, 3));

	// Update the expected result
	maze4DirectionFilled.setElement(2, 0, 3);
	EXPECT_TRUE(maze.isEqual(maze4DirectionFilled));

	//maze.Printout(1);

	EXPECT_EQ(1, maze.floodFill4Directions(1, 1, 2));

	// Update the expected result
	maze4DirectionFilled.setElement(1, 1, 2);
	EXPECT_TRUE(maze.isEqual(maze4DirectionFilled));

	//maze.Printout(1);

	EXPECT_EQ(1, maze.floodFill4Directions(0, 2, 9));

	// Update the expected result
	maze4DirectionFilled.setElement(0, 2, 9);
	EXPECT_TRUE(maze.isEqual(maze4DirectionFilled));

	//maze.Printout(1);

	EXPECT_EQ(7, maze.floodFill4Directions(4, 5, 9));

	// Update the expected result
	maze4DirectionFilled.setElement(4, 5, 9);
	maze4DirectionFilled.setElement(4, 6, 9);
	maze4DirectionFilled.setElement(5, 6, 9);
	maze4DirectionFilled.setElement(6, 6, 9);
	maze4DirectionFilled.setElement(6, 5, 9);
	maze4DirectionFilled.setElement(6, 4, 9);
	maze4DirectionFilled.setElement(5, 4, 9);
	EXPECT_TRUE(maze.isEqual(maze4DirectionFilled));

	//maze.Printout(1);

	// Out-of-bounds coordinates
	EXPECT_EQ(0, maze.floodFill4Directions(-1, 1, 1));
	EXPECT_EQ(0, maze.floodFill4Directions(0, -1, 9));
	EXPECT_EQ(0, maze.floodFill4Directions(-3, -1, 2));
	EXPECT_EQ(0, maze.floodFill4Directions(3, 7, 4));
	EXPECT_EQ(0, maze.floodFill4Directions(7, 4, 9));
	EXPECT_EQ(0, maze.floodFill4Directions(7, 7, 9));
	EXPECT_EQ(0, maze.floodFill4Directions(-1, 7, 9));
	EXPECT_EQ(0, maze.floodFill4Directions(8, -1, 9));
}


/**
 * Test that FloodFill4Directions() works with T=class Foobar.
 */
TEST_F(Array2Test, FloodFill4Directions_class_Foobar)
{
	Array2<Foobar> array3x3(3, 3);
	Foobar foobar1(1, "Foo");
	Foobar foobar2(2, "Bar");
	Foobar foobar3(3, "Meh");

	// Set elements
	array3x3.setElement(0, 0, foobar1);
	array3x3.setElement(1, 0, foobar2);
	array3x3.setElement(2, 0, foobar1);

	array3x3.setElement(0, 1, foobar2);
	array3x3.setElement(1, 1, foobar1);
	array3x3.setElement(2, 1, foobar1);

	array3x3.setElement(0, 2, foobar1);
	array3x3.setElement(1, 2, foobar1);
	array3x3.setElement(2, 2, foobar2);

	// Array now 1=foobar1, 2=foobar2, 3=foobar3
	//	1 2 1
	//	2 1 1
	//	1 1 2

	// Flood fill with foobar3 starting from (2,0)
	EXPECT_EQ(5, array3x3.floodFill4Directions(2, 0, foobar3));

	// Verify elements
	EXPECT_EQ(foobar1, array3x3.getElementCopy(0, 0));
	EXPECT_EQ(foobar2, array3x3.getElementCopy(1, 0));
	EXPECT_EQ(foobar3, array3x3.getElementCopy(2, 0));

	EXPECT_EQ(foobar2, array3x3.getElementCopy(0, 1));
	EXPECT_EQ(foobar3, array3x3.getElementCopy(1, 1));
	EXPECT_EQ(foobar3, array3x3.getElementCopy(2, 1));

	EXPECT_EQ(foobar3, array3x3.getElementCopy(0, 2));
	EXPECT_EQ(foobar3, array3x3.getElementCopy(1, 2));
	EXPECT_EQ(foobar2, array3x3.getElementCopy(2, 2));
}


/**
 * Test that FloodFill8Directions() works.
 */
TEST_F(Array2Test, FloodFill8Directions)
{
	// maze starting values
	//	[ 0 2 0 1 0 0 0 ]
	//	[ 2 0 2 1 0 0 0 ]
	//	[ 0 2 1 0 0 0 0 ]
	//	[ 1 1 0 0 0 1 1 ]
	//	[ 0 0 0 0 1 2 2 ]
	//	[ 0 0 0 1 2 0 2 ]
	//	[ 0 0 0 1 2 2 2 ]

	//maze.Printout(1);

	//std::cout << "Floodfill (3,3) value 5" << std::endl;
	EXPECT_EQ(23, maze.floodFill8Directions(3, 3, 5));

	// maze after floodfill
	//	[ 0 2 0 1 5 5 5 ]
	//	[ 2 0 2 1 5 5 5 ]
	//	[ 0 2 1 5 5 5 5 ]
	//	[ 1 1 5 5 5 1 1 ]
	//	[ 5 5 5 5 1 2 2 ]
	//	[ 5 5 5 1 2 0 2 ]
	//	[ 5 5 5 1 2 2 2 ]
	//maze.Printout(1);

	EXPECT_TRUE(maze.isEqual(maze8DirectionFilled));
	//maze8DirectionFilled.Printout(1);

	//std::cout << "Floodfill (0,0) value 9" << std::endl;
	EXPECT_EQ(4, maze.floodFill8Directions(0, 0, 9));

	// maze after floodfill
	//	[ 9 2 9 1 5 5 5 ]
	//	[ 2 9 2 1 5 5 5 ]
	//	[ 9 2 1 5 5 5 5 ]
	//	[ 1 1 5 5 5 1 1 ]
	//	[ 5 5 5 5 1 2 2 ]
	//	[ 5 5 5 1 2 0 2 ]
	//	[ 5 5 5 1 2 2 2 ]
	//maze.Printout(1);

	// Update the expected result
	maze8DirectionFilled.setElement(0, 0, 9);
	maze8DirectionFilled.setElement(2, 0, 9);
	maze8DirectionFilled.setElement(1, 1, 9);
	maze8DirectionFilled.setElement(0, 2, 9);
	EXPECT_TRUE(maze.isEqual(maze8DirectionFilled));
	//maze8DirectionFilled.Printout(1);

	//std::cout << "Floodfill (2,0) value 7" << std::endl;
	EXPECT_EQ(4, maze.floodFill8Directions(2, 0, 7));

	// maze after floodfill
	//	[ 7 2 7 1 5 5 5 ]
	//	[ 2 7 2 1 5 5 5 ]
	//	[ 7 2 1 5 5 5 5 ]
	//	[ 1 1 5 5 5 1 1 ]
	//	[ 5 5 5 5 1 2 2 ]
	//	[ 5 5 5 1 2 0 2 ]
	//	[ 5 5 5 1 2 2 2 ]
	//maze.Printout(1);

	maze8DirectionFilled.setElement(0, 0, 7);
	maze8DirectionFilled.setElement(2, 0, 7);
	maze8DirectionFilled.setElement(1, 1, 7);
	maze8DirectionFilled.setElement(0, 2, 7);
	EXPECT_TRUE(maze.isEqual(maze8DirectionFilled));
	//maze8DirectionFilled.Printout(1);

	//std::cout << "Floodfill (1,1) value 3" << std::endl;
	EXPECT_EQ(4, maze.floodFill8Directions(1, 1, 3));

	// maze after floodfill
	//	[ 3 2 3 1 5 5 5 ]
	//	[ 2 3 2 1 5 5 5 ]
	//	[ 3 2 1 5 5 5 5 ]
	//	[ 1 1 5 5 5 1 1 ]
	//	[ 5 5 5 5 1 2 2 ]
	//	[ 5 5 5 1 2 0 2 ]
	//	[ 5 5 5 1 2 2 2 ]
	//maze.Printout(1);

	maze8DirectionFilled.setElement(0, 0, 3);
	maze8DirectionFilled.setElement(2, 0, 3);
	maze8DirectionFilled.setElement(1, 1, 3);
	maze8DirectionFilled.setElement(0, 2, 3);
	EXPECT_TRUE(maze.isEqual(maze8DirectionFilled));
	//maze8DirectionFilled.Printout(1);

	//std::cout << "Floodfill (0,2) value 4" << std::endl;
	EXPECT_EQ(4, maze.floodFill8Directions(0, 2, 4));

	// maze after floodfill
	//	[ 4 2 4 1 5 5 5 ]
	//	[ 2 4 2 1 5 5 5 ]
	//	[ 4 2 1 5 5 5 5 ]
	//	[ 1 1 5 5 5 1 1 ]
	//	[ 5 5 5 5 1 2 2 ]
	//	[ 5 5 5 1 2 0 2 ]
	//	[ 5 5 5 1 2 2 2 ]
	//maze.Printout(1);

	maze8DirectionFilled.setElement(0, 0, 4);
	maze8DirectionFilled.setElement(2, 0, 4);
	maze8DirectionFilled.setElement(1, 1, 4);
	maze8DirectionFilled.setElement(0, 2, 4);
	EXPECT_TRUE(maze.isEqual(maze8DirectionFilled));
	//maze8DirectionFilled.Printout(1);

	//std::cout << "Floodfill (1,2) value 5" << std::endl;
	EXPECT_EQ(4, maze.floodFill8Directions(1, 2, 5));

	// maze after floodfill
	//	[ 4 5 4 1 5 5 5 ]
	//	[ 5 4 5 1 5 5 5 ]
	//	[ 4 5 1 5 5 5 5 ]
	//	[ 1 1 5 5 5 1 1 ]
	//	[ 5 5 5 5 1 2 2 ]
	//	[ 5 5 5 1 2 0 2 ]
	//	[ 5 5 5 1 2 2 2 ]
	//maze.Printout(1);

	maze8DirectionFilled.setElement(1, 0, 5);
	maze8DirectionFilled.setElement(0, 1, 5);
	maze8DirectionFilled.setElement(2, 1, 5);
	maze8DirectionFilled.setElement(1, 2, 5);
	EXPECT_TRUE(maze.isEqual(maze8DirectionFilled));
	//maze8DirectionFilled.Printout(1);

	//std::cout << "Floodfill (5,5) value 9" << std::endl;
	EXPECT_EQ(1, maze.floodFill8Directions(5, 5, 9));

	// maze after floodfill
	//	[ 4 5 4 1 5 5 5 ]
	//	[ 5 4 5 1 5 5 5 ]
	//	[ 4 5 1 5 5 5 5 ]
	//	[ 1 1 5 5 5 1 1 ]
	//	[ 5 5 5 5 1 2 2 ]
	//	[ 5 5 5 1 2 9 2 ]
	//	[ 5 5 5 1 2 2 2 ]
	//maze.Printout(1);

	maze8DirectionFilled.setElement(5, 5, 9);
	EXPECT_TRUE(maze.isEqual(maze8DirectionFilled));
	//maze8DirectionFilled.Printout(1);

	//std::cout << "Floodfill (3,6) value 6" << std::endl;
	EXPECT_EQ(5, maze.floodFill8Directions(3, 6, 6));

	// maze after floodfill
	//	[ 4 5 4 1 5 5 5 ]
	//	[ 5 4 5 1 5 5 5 ]
	//	[ 4 5 1 5 5 5 5 ]
	//	[ 1 1 5 5 5 6 6 ]
	//	[ 5 5 5 5 6 2 2 ]
	//	[ 5 5 5 6 2 9 2 ]
	//	[ 5 5 5 6 2 2 2 ]
	//maze.Printout(1);

	maze8DirectionFilled.setElement(3, 6, 6);
	maze8DirectionFilled.setElement(3, 5, 6);
	maze8DirectionFilled.setElement(4, 4, 6);
	maze8DirectionFilled.setElement(5, 3, 6);
	maze8DirectionFilled.setElement(6, 3, 6);
	EXPECT_TRUE(maze.isEqual(maze8DirectionFilled));
	//maze8DirectionFilled.Printout(1);

	//std::cout << "Floodfill (3,1) value 6" << std::endl;
	EXPECT_EQ(5, maze.floodFill8Directions(3, 1, 6));

	// maze after floodfill
	//	[ 4 5 4 6 5 5 5 ]
	//	[ 5 4 5 6 5 5 5 ]
	//	[ 4 5 6 5 5 5 5 ]
	//	[ 6 6 5 5 5 6 6 ]
	//	[ 5 5 5 5 6 2 2 ]
	//	[ 5 5 5 6 2 9 2 ]
	//	[ 5 5 5 6 2 2 2 ]
	//maze.Printout(1);

	maze8DirectionFilled.setElement(3, 0, 6);
	maze8DirectionFilled.setElement(3, 1, 6);
	maze8DirectionFilled.setElement(2, 2, 6);
	maze8DirectionFilled.setElement(1, 3, 6);
	maze8DirectionFilled.setElement(0, 3, 6);
	EXPECT_TRUE(maze.isEqual(maze8DirectionFilled));
	//maze8DirectionFilled.Printout(1);

	// Out-of-bounds coordinates
	EXPECT_EQ(0, maze.floodFill8Directions(-1, 1, 1));
	EXPECT_EQ(0, maze.floodFill8Directions(0, -1, 9));
	EXPECT_EQ(0, maze.floodFill8Directions(-3, -1, 2));
	EXPECT_EQ(0, maze.floodFill8Directions(3, 7, 4));
	EXPECT_EQ(0, maze.floodFill8Directions(7, 4, 9));
	EXPECT_EQ(0, maze.floodFill8Directions(7, 7, 9));
	EXPECT_EQ(0, maze.floodFill8Directions(-1, 7, 9));
	EXPECT_EQ(0, maze.floodFill8Directions(8, -1, 9));
}


} // End namespace nameless namespace

