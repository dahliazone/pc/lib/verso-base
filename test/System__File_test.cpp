/**
 * @file System__File_test.cpp
 * @brief Test for the class File.
 * @author Max Vilkki <codise@codise.org>
 *
 * TODO:
 * - \todo
 *
 * What is not tested(!):
 * - What:
 *   Why:
 */

#include <Verso/System/File.hpp>
#include <gtest/gtest.h>
#include <iostream>

namespace {

using Verso::UString;
using Verso::File;

/**
 * Test File::addSlashToPathIfNeeded(const UString&).
 */
TEST(SystemFileTest, File_addSlashToPathIfNeeded)
{
	UString empty("");
	EXPECT_EQ(File::addSlashToPathIfNeeded(empty), "");

	UString root1("/");
	EXPECT_EQ(File::addSlashToPathIfNeeded(root1), "/");

	UString root2("C:\\");
	EXPECT_EQ(File::addSlashToPathIfNeeded(root2), "C:\\");

	UString root3("C:/");
	EXPECT_EQ(File::addSlashToPathIfNeeded(root3), "C:/");

	UString singleWith1("Foo/");
	EXPECT_EQ(File::addSlashToPathIfNeeded(singleWith1), "Foo/");

	UString singleWith2("Foo\\");
	EXPECT_EQ(File::addSlashToPathIfNeeded(singleWith2), "Foo\\");

	UString multiWith1("Oh/yaeh/");
	EXPECT_EQ(File::addSlashToPathIfNeeded(multiWith1), "Oh/yaeh/");

	UString multiWith2("Oh\\yeah\\");
	EXPECT_EQ(File::addSlashToPathIfNeeded(multiWith2), "Oh\\yeah\\");

#ifdef _WIN32
	UString singleWithout("Foo");
	EXPECT_EQ(File::addSlashToPathIfNeeded(singleWithout), "Foo\\");

	UString multiWithout("Oh/yaeh");
	EXPECT_EQ(File::addSlashToPathIfNeeded(multiWithout), "Oh/yaeh\\");
#else
	UString singleWithout("Foo");
	EXPECT_EQ(File::addSlashToPathIfNeeded(singleWithout), "Foo/");

	UString multiWithout("Oh/yaeh");
	EXPECT_EQ(File::addSlashToPathIfNeeded(multiWithout), "Oh/yaeh/");
#endif
}


/**
 * Test File::removeSlashFromPathIfNeeded(const UString&).
 */
TEST(SystemFileTest, File_removeSlashFromPathIfNeeded)
{
	UString empty("");
	EXPECT_EQ(File::removeSlashFromPathIfNeeded(empty), "");

	UString root1("/");
	EXPECT_EQ(File::removeSlashFromPathIfNeeded(root1), "/");

	UString root2("C:\\");
	EXPECT_EQ(File::removeSlashFromPathIfNeeded(root2), "C:");

	UString root3("C:/");
	EXPECT_EQ(File::removeSlashFromPathIfNeeded(root3), "C:");

	UString singleWith1("Foo/");
	EXPECT_EQ(File::removeSlashFromPathIfNeeded(singleWith1), "Foo");

	UString singleWith2("Foo\\");
	EXPECT_EQ(File::removeSlashFromPathIfNeeded(singleWith2), "Foo");

	UString multiWith1("Oh/yaeh/");
	EXPECT_EQ(File::removeSlashFromPathIfNeeded(multiWith1), "Oh/yaeh");

	UString multiWith2("Oh\\yeah\\");
	EXPECT_EQ(File::removeSlashFromPathIfNeeded(multiWith2), "Oh\\yeah");

	UString singleWithout("Foo");
	EXPECT_EQ(File::removeSlashFromPathIfNeeded(singleWithout), "Foo");

	UString multiWithout("Oh/yaeh");
	EXPECT_EQ(File::removeSlashFromPathIfNeeded(multiWithout), "Oh/yaeh");
}


} // End namespace nameless namespace

