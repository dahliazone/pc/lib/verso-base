/**
 * @file System__UString_test.cpp
 * @brief Test for the class UString.
 * @author Max Vilkki <codise@codise.org>
 *
 * TODO:
 * - \todo
 *
 * What is not tested(!):
 * - What:
 *   Why:
 */

#include <Verso/System/UString.hpp>
#include <gtest/gtest.h>

namespace {

using Verso::UString;
using namespace std;


/**
 * Test UString constructors.
 */
TEST(SystemUStringTest, Constructors)
{
	UString s_empty("");
	const char* s_empty_res = s_empty.c_str();
	EXPECT_STRCASEEQ("", s_empty_res);

	UString s_aBcd("aBcd");
	const char* s_aBcd_res = s_aBcd.c_str();
	EXPECT_STRCASEEQ("aBcd", s_aBcd_res);
}


/**
 * Test UString operator +=.
 */
TEST(SystemUStringTest, OperatorPlusEquals)
{
	UString s_start("start");
	UString s_aBcd("aBcd");

	s_start += s_aBcd;
	const char* s_start_res = s_start.c_str();
	EXPECT_STRCASEEQ("startaBcd", s_start_res);
}


/**
 * Test UString::findFirstOf().
 */
TEST(SystemUStringTest, FindFirstOf)
{
	UString s_abcd("abcd");
	UString s_cd("cd");
	UString s_d("d");
	UString s_q("q");

	// Find from empty string
	EXPECT_EQ(UString("").findFirstOf(s_cd), std::string::npos);

	// Find empty string
	//EXPECT_EQ(s_abcd.findFirstOf(""), string::npos);
	EXPECT_THROW(s_abcd.findFirstOf(""), Verso::AssertException);

	// Find empty string from empty string
	//EXPECT_EQ(UString("").findFirstOf(""), string::npos);
	EXPECT_THROW(UString("").findFirstOf(""), Verso::AssertException);

	// Find "cd" from "abcd"
	EXPECT_EQ(s_abcd.findFirstOf(s_cd), 2);

	// Find "d" from "abcd"
	EXPECT_EQ(s_abcd.findFirstOf(s_d), 3);

	// Don't find "cd" from "abcd" with pos = 4
	EXPECT_EQ(s_abcd.findFirstOf(s_cd, 4), string::npos);

	// Find "cd" from "abcd" with pos = 3
	EXPECT_EQ(s_abcd.findFirstOf(s_cd, 3), 3);

	// Find "cd" from "abcd" with pos = 2
	EXPECT_EQ(s_abcd.findFirstOf(s_cd, 2), 2);

	// Find "cd" from "abcd" with pos = 1
	EXPECT_EQ(s_abcd.findFirstOf(s_cd, 1), 2);

	// Find "cd" from "abcd" with pos = 0
	EXPECT_EQ(s_abcd.findFirstOf(s_cd, 0), 2);

	// Don't find "q" from "abcd"
	EXPECT_EQ(s_abcd.findFirstOf(s_q), string::npos);

	// Don't find "abcd" from "q"
	EXPECT_EQ(s_q.findFirstOf(s_abcd), string::npos);


	UString s_aAadweEfafAb("aAadweEfafAb");
	UString s_a("a");
	UString s_A("A");
	UString s_D("D");

	// Find "a" from "aAadweEfafAb"
	EXPECT_EQ(s_aAadweEfafAb.findFirstOf(s_a), 0);

	// Find "A" from "aAadweEfafAb"
	EXPECT_EQ(s_aAadweEfafAb.findFirstOf(s_A), 1);

	// Don't find "D" from "aAadweEfafAb"
	EXPECT_EQ(s_aAadweEfafAb.findFirstOf(s_D), string::npos);


	UString fileName_data_foo_bar_obj("data/foo/bar.obj");
	UString fileName_jee_png("jee.png");
	UString s_slash("/");

	// Find "/" from "data/foo/bar.obj"
	EXPECT_EQ(fileName_data_foo_bar_obj.findFirstOf(s_slash), 4);

	// Find "/" from "jee.png"
	EXPECT_EQ(fileName_jee_png.findFirstOf(s_slash), string::npos);
}


/**
 * Test UString::findLastOf().
 */
TEST(SystemUStringTest, FindLastOf)
{
	UString s_abcd("abcd");
	UString s_cd("cd");
	UString s_d("d");
	UString s_q("q");

	// Find from empty string
	EXPECT_EQ(UString("").findLastOf(s_cd), std::string::npos);

	// Find empty string
	//EXPECT_EQ(s_abcd.findLastOf(""), string::npos);
	EXPECT_THROW(s_abcd.findLastOf(""), Verso::AssertException);

	// Find empty string from empty string
	//EXPECT_EQ(UString("").findLastOf(""), string::npos);
	EXPECT_THROW(UString("").findLastOf(""), Verso::AssertException);

	// Find "cd" from "abcd"
	EXPECT_EQ(s_abcd.findLastOf(s_cd), 3);

	// Find "d" from "abcd"
	EXPECT_EQ(s_abcd.findLastOf(s_d), 3);

	// Find "cd" from "abcd" with pos = 3
	EXPECT_EQ(s_abcd.findLastOf(s_cd, 3), 3);

	// Find "cd" from "abcd" with pos = 2
	EXPECT_EQ(s_abcd.findLastOf(s_cd, 2), 2);

	// Don't find "cd" from "abcd" with pos = 1
	EXPECT_EQ(s_abcd.findLastOf(s_cd, 1), string::npos);

	// Don't find "cd" from "abcd" with pos = 0
	EXPECT_EQ(s_abcd.findLastOf(s_cd, 0), string::npos);

	// Don't find "q" from "abcd"
	EXPECT_EQ(s_abcd.findLastOf(s_q), string::npos);

	// Don't find "abcd" from "q"
	EXPECT_EQ(s_q.findLastOf(s_abcd), string::npos);


	UString s_aAadweEfafAb("aAadweEfafAb");
	UString s_a("a");
	UString s_A("A");
	UString s_D("D");

	// Find "a" from "aAadweEfafAb"
	EXPECT_EQ(s_aAadweEfafAb.findLastOf(s_a), 8);

	// Find "A" from "aAadweEfafAb"
	EXPECT_EQ(s_aAadweEfafAb.findLastOf(s_A), 10);

	// Don't find "D" from "aAadweEfafAb"
	EXPECT_EQ(s_aAadweEfafAb.findLastOf(s_D), string::npos);


	UString fileName_data_foo_bar_obj("data/foo/bar.obj");
	UString fileName_jee_png("jee.png");
	UString s_slash("/");

	// Find "/" from "data/foo/bar.obj"
	EXPECT_EQ(fileName_data_foo_bar_obj.findLastOf(s_slash), 8);

	// Find "/" from "jee.png"
	EXPECT_EQ(fileName_jee_png.findLastOf(s_slash), string::npos);
}


} // End namespace nameless namespace

