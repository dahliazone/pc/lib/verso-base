/**
 * @file Math__Matrix4x4f_test.cpp
 * @brief Test for the class Align.
 * @author Max Vilkki <codise@codise.org>
 *
 * TODO:
 * - \todo
 *
 * What is not tested(!):
 * - What:
 *   Why:
 */

#include <Verso/Math/Align.hpp>
#include <gtest/gtest.h>

namespace {


using Verso::Align;
using Verso::HAlign;
using Verso::VAlign;
using Verso::Vector2i;
using Verso::Vector2f;
using Verso::Recti;
using Verso::Rectf;


/**
 * Test constructor()
 */
TEST(MathAlignTest, Constructor)
{
	Align a;
	EXPECT_EQ(a.hAlign, HAlign::Center);
	EXPECT_EQ(a.vAlign, VAlign::Center);
}


/**
 * Test constructor(HAlign, VAlign)
 */
TEST(MathAlignTest, Constructor_HAlign_VAlign)
{
	Align a(HAlign::Right, VAlign::Bottom);
	EXPECT_EQ(a.hAlign, HAlign::Right);
	EXPECT_EQ(a.vAlign, VAlign::Bottom);
}


/**
 * Test set(HAlign, VAlign)
 */
TEST(MathAlignTest, set)
{
	Align a1;
	a1.set(HAlign::Left, VAlign::Bottom);
	EXPECT_EQ(a1.hAlign, HAlign::Left);
	EXPECT_EQ(a1.vAlign, VAlign::Bottom);
}


/**
 * Test pointOffseti(const Vector2i&) const
 */
TEST(MathAlignTest, pointOffseti)
{
	Align leftTop(HAlign::Left, VAlign::Top);
	Align centerTop(HAlign::Center, VAlign::Top);
	Align rightTop(HAlign::Right, VAlign::Top);
	Align undefinedTop(HAlign::Undefined, VAlign::Top);

	Align leftCenter(HAlign::Left, VAlign::Center);
	Align centerCenter(HAlign::Center, VAlign::Center);
	Align rightCenter(HAlign::Right, VAlign::Center);
	Align undefinedCenter(HAlign::Undefined, VAlign::Center);

	Align leftBottom(HAlign::Left, VAlign::Bottom);
	Align centerBottom(HAlign::Center, VAlign::Bottom);
	Align rightBottom(HAlign::Right, VAlign::Bottom);
	Align undefinedBottom(HAlign::Undefined, VAlign::Bottom);

	Align leftUndefined(HAlign::Left, VAlign::Undefined);
	Align centerUndefined(HAlign::Center, VAlign::Undefined);
	Align rightUndefined(HAlign::Right, VAlign::Undefined);
	Align undefinedUndefined(HAlign::Undefined, VAlign::Undefined);

	Vector2i size(10, 20);
	int halfX = size.x / 2;
	int halfY = size.y / 2;

	EXPECT_EQ(leftTop.pointOffseti(size), Vector2i(-halfX, -halfY));
	EXPECT_EQ(centerTop.pointOffseti(size), Vector2i(0, -halfY));
	EXPECT_EQ(rightTop.pointOffseti(size), Vector2i(halfX, -halfY));
	EXPECT_EQ(undefinedTop.pointOffseti(size), Vector2i(0, -halfY));

	EXPECT_EQ(leftCenter.pointOffseti(size), Vector2i(-halfX, 0));
	EXPECT_EQ(centerCenter.pointOffseti(size), Vector2i(0, 0));
	EXPECT_EQ(rightCenter.pointOffseti(size), Vector2i(halfX, 0));
	EXPECT_EQ(undefinedCenter.pointOffseti(size), Vector2i(0, 0));

	EXPECT_EQ(leftBottom.pointOffseti(size), Vector2i(-halfX, halfY));
	EXPECT_EQ(centerBottom.pointOffseti(size), Vector2i(0, halfY));
	EXPECT_EQ(rightBottom.pointOffseti(size), Vector2i(halfX, halfY));
	EXPECT_EQ(undefinedBottom.pointOffseti(size), Vector2i(0, halfY));

	EXPECT_EQ(leftUndefined.pointOffseti(size), Vector2i(-halfX, 0));
	EXPECT_EQ(centerUndefined.pointOffseti(size), Vector2i(0, 0));
	EXPECT_EQ(rightUndefined.pointOffseti(size), Vector2i(halfX, 0));
	EXPECT_EQ(undefinedUndefined.pointOffseti(size), Vector2i(0, 0));
}


/**
 * Test pointOffsetf(const Vector2f&) const
 */
TEST(MathAlignTest, pointOffsetf)
{
	Align leftTop(HAlign::Left, VAlign::Top);
	Align centerTop(HAlign::Center, VAlign::Top);
	Align rightTop(HAlign::Right, VAlign::Top);
	Align undefinedTop(HAlign::Undefined, VAlign::Top);

	Align leftCenter(HAlign::Left, VAlign::Center);
	Align centerCenter(HAlign::Center, VAlign::Center);
	Align rightCenter(HAlign::Right, VAlign::Center);
	Align undefinedCenter(HAlign::Undefined, VAlign::Center);

	Align leftBottom(HAlign::Left, VAlign::Bottom);
	Align centerBottom(HAlign::Center, VAlign::Bottom);
	Align rightBottom(HAlign::Right, VAlign::Bottom);
	Align undefinedBottom(HAlign::Undefined, VAlign::Bottom);

	Align leftUndefined(HAlign::Left, VAlign::Undefined);
	Align centerUndefined(HAlign::Center, VAlign::Undefined);
	Align rightUndefined(HAlign::Right, VAlign::Undefined);
	Align undefinedUndefined(HAlign::Undefined, VAlign::Undefined);

	Vector2f size(3.0f, 0.5f);
	float halfX = size.x / 2.0f;
	float halfY = size.y / 2.0f;

	EXPECT_EQ(leftTop.pointOffsetf(size), Vector2f(-halfX, -halfY));
	EXPECT_EQ(centerTop.pointOffsetf(size), Vector2f(0, -halfY));
	EXPECT_EQ(rightTop.pointOffsetf(size), Vector2f(halfX, -halfY));
	EXPECT_EQ(undefinedTop.pointOffsetf(size), Vector2f(0, -halfY));

	EXPECT_EQ(leftCenter.pointOffsetf(size), Vector2f(-halfX, 0));
	EXPECT_EQ(centerCenter.pointOffsetf(size), Vector2f(0, 0));
	EXPECT_EQ(rightCenter.pointOffsetf(size), Vector2f(halfX, 0));
	EXPECT_EQ(undefinedCenter.pointOffsetf(size), Vector2f(0, 0));

	EXPECT_EQ(leftBottom.pointOffsetf(size), Vector2f(-halfX, halfY));
	EXPECT_EQ(centerBottom.pointOffsetf(size), Vector2f(0, halfY));
	EXPECT_EQ(rightBottom.pointOffsetf(size), Vector2f(halfX, halfY));
	EXPECT_EQ(undefinedBottom.pointOffsetf(size), Vector2f(0, halfY));

	EXPECT_EQ(leftUndefined.pointOffsetf(size), Vector2f(-halfX, 0));
	EXPECT_EQ(centerUndefined.pointOffsetf(size), Vector2f(0, 0));
	EXPECT_EQ(rightUndefined.pointOffsetf(size), Vector2f(halfX, 0));
	EXPECT_EQ(undefinedUndefined.pointOffsetf(size), Vector2f(0, 0));
}


/**
 * Test pointOffsetCenteredQuad() const
 */
TEST(MathAlignTest, pointOffsetCenteredQuad)
{
	Align leftTop(HAlign::Left, VAlign::Top);
	Align centerTop(HAlign::Center, VAlign::Top);
	Align rightTop(HAlign::Right, VAlign::Top);
	Align undefinedTop(HAlign::Undefined, VAlign::Top);

	Align leftCenter(HAlign::Left, VAlign::Center);
	Align centerCenter(HAlign::Center, VAlign::Center);
	Align rightCenter(HAlign::Right, VAlign::Center);
	Align undefinedCenter(HAlign::Undefined, VAlign::Center);

	Align leftBottom(HAlign::Left, VAlign::Bottom);
	Align centerBottom(HAlign::Center, VAlign::Bottom);
	Align rightBottom(HAlign::Right, VAlign::Bottom);
	Align undefinedBottom(HAlign::Undefined, VAlign::Bottom);

	Align leftUndefined(HAlign::Left, VAlign::Undefined);
	Align centerUndefined(HAlign::Center, VAlign::Undefined);
	Align rightUndefined(HAlign::Right, VAlign::Undefined);
	Align undefinedUndefined(HAlign::Undefined, VAlign::Undefined);

	float halfX = 0.5f;
	float halfY = 0.5f;

	EXPECT_EQ(leftTop.pointOffsetCenteredQuad(), Vector2f(-halfX, -halfY));
	EXPECT_EQ(centerTop.pointOffsetCenteredQuad(), Vector2f(0, -halfY));
	EXPECT_EQ(rightTop.pointOffsetCenteredQuad(), Vector2f(halfX, -halfY));
	EXPECT_EQ(undefinedTop.pointOffsetCenteredQuad(), Vector2f(0, -halfY));

	EXPECT_EQ(leftCenter.pointOffsetCenteredQuad(), Vector2f(-halfX, 0));
	EXPECT_EQ(centerCenter.pointOffsetCenteredQuad(), Vector2f(0, 0));
	EXPECT_EQ(rightCenter.pointOffsetCenteredQuad(), Vector2f(halfX, 0));
	EXPECT_EQ(undefinedCenter.pointOffsetCenteredQuad(), Vector2f(0, 0));

	EXPECT_EQ(leftBottom.pointOffsetCenteredQuad(), Vector2f(-halfX, halfY));
	EXPECT_EQ(centerBottom.pointOffsetCenteredQuad(), Vector2f(0, halfY));
	EXPECT_EQ(rightBottom.pointOffsetCenteredQuad(), Vector2f(halfX, halfY));
	EXPECT_EQ(undefinedBottom.pointOffsetCenteredQuad(), Vector2f(0, halfY));

	EXPECT_EQ(leftUndefined.pointOffsetCenteredQuad(), Vector2f(-halfX, 0));
	EXPECT_EQ(centerUndefined.pointOffsetCenteredQuad(), Vector2f(0, 0));
	EXPECT_EQ(rightUndefined.pointOffsetCenteredQuad(), Vector2f(halfX, 0));
	EXPECT_EQ(undefinedUndefined.pointOffsetCenteredQuad(), Vector2f(0, 0));
}


/**
 * Test objectInContainerOffseti(const Vector2i&, const Vector2i&) const
 */
TEST(MathAlignTest, objectInContainerOffseti)
{
	Align leftTop(HAlign::Left, VAlign::Top);
	Align centerTop(HAlign::Center, VAlign::Top);
	Align rightTop(HAlign::Right, VAlign::Top);
	Align undefinedTop(HAlign::Undefined, VAlign::Top);

	Align leftCenter(HAlign::Left, VAlign::Center);
	Align centerCenter(HAlign::Center, VAlign::Center);
	Align rightCenter(HAlign::Right, VAlign::Center);
	Align undefinedCenter(HAlign::Undefined, VAlign::Center);

	Align leftBottom(HAlign::Left, VAlign::Bottom);
	Align centerBottom(HAlign::Center, VAlign::Bottom);
	Align rightBottom(HAlign::Right, VAlign::Bottom);
	Align undefinedBottom(HAlign::Undefined, VAlign::Bottom);

	Align leftUndefined(HAlign::Left, VAlign::Undefined);
	Align centerUndefined(HAlign::Center, VAlign::Undefined);
	Align rightUndefined(HAlign::Right, VAlign::Undefined);
	Align undefinedUndefined(HAlign::Undefined, VAlign::Undefined);

	Vector2i oSize(20, 10);
	Vector2i cSize(100, 200);

	int halfOX = oSize.x / 2;
	int halfOY = oSize.y / 2;
	int halfCX = cSize.x / 2;
	int halfCY = cSize.y / 2;

	EXPECT_EQ(leftTop.objectInContainerOffseti(oSize, cSize), Vector2i(0, 0));
	EXPECT_EQ(centerTop.objectInContainerOffseti(oSize, cSize), Vector2i(halfCX - halfOX, 0));
	EXPECT_EQ(rightTop.objectInContainerOffseti(oSize, cSize), Vector2i(cSize.x - oSize.x, 0));
	EXPECT_EQ(undefinedTop.objectInContainerOffseti(oSize, cSize), Vector2i(0, 0));

	EXPECT_EQ(leftCenter.objectInContainerOffseti(oSize, cSize), Vector2i(0, halfCY - halfOY));
	EXPECT_EQ(centerCenter.objectInContainerOffseti(oSize, cSize), Vector2i(halfCX - halfOX, halfCY - halfOY));
	EXPECT_EQ(rightCenter.objectInContainerOffseti(oSize, cSize), Vector2i(cSize.x - oSize.x, halfCY - halfOY));
	EXPECT_EQ(undefinedCenter.objectInContainerOffseti(oSize, cSize), Vector2i(0, halfCY - halfOY));

	EXPECT_EQ(leftBottom.objectInContainerOffseti(oSize, cSize), Vector2i(0, cSize.y - oSize.y));
	EXPECT_EQ(centerBottom.objectInContainerOffseti(oSize, cSize), Vector2i(halfCX - halfOX, cSize.y - oSize.y));
	EXPECT_EQ(rightBottom.objectInContainerOffseti(oSize, cSize), Vector2i(cSize.x - oSize.x, cSize.y - oSize.y));
	EXPECT_EQ(undefinedBottom.objectInContainerOffseti(oSize, cSize), Vector2i(0, cSize.y - oSize.y));

	EXPECT_EQ(leftUndefined.objectInContainerOffseti(oSize, cSize), Vector2i(0, 0));
	EXPECT_EQ(centerUndefined.objectInContainerOffseti(oSize, cSize), Vector2i(halfCX - halfOX, 0));
	EXPECT_EQ(rightUndefined.objectInContainerOffseti(oSize, cSize), Vector2i(cSize.x - oSize.x, 0));
	EXPECT_EQ(undefinedUndefined.objectInContainerOffseti(oSize, cSize), Vector2i(0, 0));
}


/**
 * Test objectInContainerOffsetf(const Vector2f&, const Vector2f&) const
 */
TEST(MathAlignTest, objectInContainerOffsetf)
{
	Align leftTop(HAlign::Left, VAlign::Top);
	Align centerTop(HAlign::Center, VAlign::Top);
	Align rightTop(HAlign::Right, VAlign::Top);
	Align undefinedTop(HAlign::Undefined, VAlign::Top);

	Align leftCenter(HAlign::Left, VAlign::Center);
	Align centerCenter(HAlign::Center, VAlign::Center);
	Align rightCenter(HAlign::Right, VAlign::Center);
	Align undefinedCenter(HAlign::Undefined, VAlign::Center);

	Align leftBottom(HAlign::Left, VAlign::Bottom);
	Align centerBottom(HAlign::Center, VAlign::Bottom);
	Align rightBottom(HAlign::Right, VAlign::Bottom);
	Align undefinedBottom(HAlign::Undefined, VAlign::Bottom);

	Align leftUndefined(HAlign::Left, VAlign::Undefined);
	Align centerUndefined(HAlign::Center, VAlign::Undefined);
	Align rightUndefined(HAlign::Right, VAlign::Undefined);
	Align undefinedUndefined(HAlign::Undefined, VAlign::Undefined);

	Vector2f oSize(3.7f, 2.4f);
	Vector2f cSize(1.5f, 0.75f);

	float halfOX = oSize.x / 2.0f;
	float halfOY = oSize.y / 2.0f;
	float halfCX = cSize.x / 2.0f;
	float halfCY = cSize.y / 2.0f;

	EXPECT_EQ(leftTop.objectInContainerOffsetf(oSize, cSize), Vector2f(0, 0));
	EXPECT_EQ(centerTop.objectInContainerOffsetf(oSize, cSize), Vector2f(halfCX - halfOX, 0));
	EXPECT_EQ(rightTop.objectInContainerOffsetf(oSize, cSize), Vector2f(cSize.x - oSize.x, 0));
	EXPECT_EQ(undefinedTop.objectInContainerOffsetf(oSize, cSize), Vector2f(0, 0));

	EXPECT_EQ(leftCenter.objectInContainerOffsetf(oSize, cSize), Vector2f(0, halfCY - halfOY));
	EXPECT_EQ(centerCenter.objectInContainerOffsetf(oSize, cSize), Vector2f(halfCX - halfOX, halfCY - halfOY));
	EXPECT_EQ(rightCenter.objectInContainerOffsetf(oSize, cSize), Vector2f(cSize.x - oSize.x, halfCY - halfOY));
	EXPECT_EQ(undefinedCenter.objectInContainerOffsetf(oSize, cSize), Vector2f(0, halfCY - halfOY));

	EXPECT_EQ(leftBottom.objectInContainerOffsetf(oSize, cSize), Vector2f(0, cSize.y - oSize.y));
	EXPECT_EQ(centerBottom.objectInContainerOffsetf(oSize, cSize), Vector2f(halfCX - halfOX, cSize.y - oSize.y));
	EXPECT_EQ(rightBottom.objectInContainerOffsetf(oSize, cSize), Vector2f(cSize.x - oSize.x, cSize.y - oSize.y));
	EXPECT_EQ(undefinedBottom.objectInContainerOffsetf(oSize, cSize), Vector2f(0, cSize.y - oSize.y));

	EXPECT_EQ(leftUndefined.objectInContainerOffsetf(oSize, cSize), Vector2f(0, 0));
	EXPECT_EQ(centerUndefined.objectInContainerOffsetf(oSize, cSize), Vector2f(halfCX - halfOX, 0));
	EXPECT_EQ(rightUndefined.objectInContainerOffsetf(oSize, cSize), Vector2f(cSize.x - oSize.x, 0));
	EXPECT_EQ(undefinedUndefined.objectInContainerOffsetf(oSize, cSize), Vector2f(0, 0));
}


/**
 * Test objectInContainerOffseti(const Vector2i&, const Recti&) const
 */
TEST(MathAlignTest, objectInContainerOffseti_Recti)
{
	Align leftTop(HAlign::Left, VAlign::Top);
	Align centerTop(HAlign::Center, VAlign::Top);
	Align rightTop(HAlign::Right, VAlign::Top);
	Align undefinedTop(HAlign::Undefined, VAlign::Top);

	Align leftCenter(HAlign::Left, VAlign::Center);
	Align centerCenter(HAlign::Center, VAlign::Center);
	Align rightCenter(HAlign::Right, VAlign::Center);
	Align undefinedCenter(HAlign::Undefined, VAlign::Center);

	Align leftBottom(HAlign::Left, VAlign::Bottom);
	Align centerBottom(HAlign::Center, VAlign::Bottom);
	Align rightBottom(HAlign::Right, VAlign::Bottom);
	Align undefinedBottom(HAlign::Undefined, VAlign::Bottom);

	Align leftUndefined(HAlign::Left, VAlign::Undefined);
	Align centerUndefined(HAlign::Center, VAlign::Undefined);
	Align rightUndefined(HAlign::Right, VAlign::Undefined);
	Align undefinedUndefined(HAlign::Undefined, VAlign::Undefined);

	Vector2i oSize(20, 10);
	Vector2i cPos(500, 600);
	Vector2i cSize(250, 500);
	Recti cRect(cPos, cSize);

	int halfOX = oSize.x / 2;
	int halfOY = oSize.y / 2;
	int halfCX = cRect.size.x / 2;
	int halfCY = cRect.size.y / 2;

	EXPECT_EQ(leftTop.objectInContainerOffseti(oSize, cRect), cPos + Vector2i(0, 0));
	EXPECT_EQ(centerTop.objectInContainerOffseti(oSize, cRect), cPos + Vector2i(halfCX - halfOX, 0));
	EXPECT_EQ(rightTop.objectInContainerOffseti(oSize, cRect), cPos + Vector2i(cSize.x - oSize.x, 0));
	EXPECT_EQ(undefinedTop.objectInContainerOffseti(oSize, cRect), cPos + Vector2i(0, 0));

	EXPECT_EQ(leftCenter.objectInContainerOffseti(oSize, cRect), cPos + Vector2i(0, halfCY - halfOY));
	EXPECT_EQ(centerCenter.objectInContainerOffseti(oSize, cRect), cPos + Vector2i(halfCX - halfOX, halfCY - halfOY));
	EXPECT_EQ(rightCenter.objectInContainerOffseti(oSize, cRect), cPos + Vector2i(cSize.x - oSize.x, halfCY - halfOY));
	EXPECT_EQ(undefinedCenter.objectInContainerOffseti(oSize, cRect), cPos + Vector2i(0, halfCY - halfOY));

	EXPECT_EQ(leftBottom.objectInContainerOffseti(oSize, cRect), cPos + Vector2i(0, cSize.y - oSize.y));
	EXPECT_EQ(centerBottom.objectInContainerOffseti(oSize, cRect), cPos + Vector2i(halfCX - halfOX, cSize.y - oSize.y));
	EXPECT_EQ(rightBottom.objectInContainerOffseti(oSize, cRect), cPos + Vector2i(cSize.x - oSize.x, cSize.y - oSize.y));
	EXPECT_EQ(undefinedBottom.objectInContainerOffseti(oSize, cRect), cPos + Vector2i(0, cSize.y - oSize.y));

	EXPECT_EQ(leftUndefined.objectInContainerOffseti(oSize, cRect), cPos + Vector2i(0, 0));
	EXPECT_EQ(centerUndefined.objectInContainerOffseti(oSize, cRect), cPos + Vector2i(halfCX - halfOX, 0));
	EXPECT_EQ(rightUndefined.objectInContainerOffseti(oSize, cRect), cPos + Vector2i(cSize.x - oSize.x, 0));
	EXPECT_EQ(undefinedUndefined.objectInContainerOffseti(oSize, cRect), cPos + Vector2i(0, 0));
}


/**
 * Test objectInContainerOffsetf(const Vector2f&, const Rectf&) const
 */
TEST(MathAlignTest, objectInContainerOffsetf_Rectf)
{
	Align leftTop(HAlign::Left, VAlign::Top);
	Align centerTop(HAlign::Center, VAlign::Top);
	Align rightTop(HAlign::Right, VAlign::Top);
	Align undefinedTop(HAlign::Undefined, VAlign::Top);

	Align leftCenter(HAlign::Left, VAlign::Center);
	Align centerCenter(HAlign::Center, VAlign::Center);
	Align rightCenter(HAlign::Right, VAlign::Center);
	Align undefinedCenter(HAlign::Undefined, VAlign::Center);

	Align leftBottom(HAlign::Left, VAlign::Bottom);
	Align centerBottom(HAlign::Center, VAlign::Bottom);
	Align rightBottom(HAlign::Right, VAlign::Bottom);
	Align undefinedBottom(HAlign::Undefined, VAlign::Bottom);

	Align leftUndefined(HAlign::Left, VAlign::Undefined);
	Align centerUndefined(HAlign::Center, VAlign::Undefined);
	Align rightUndefined(HAlign::Right, VAlign::Undefined);
	Align undefinedUndefined(HAlign::Undefined, VAlign::Undefined);

	Vector2f oSize(2.0f, 1.0f);
	Vector2f cPos(5.0f, 6.0f);
	Vector2f cSize(2.5f, 5.0f);
	Rectf cRect(cPos, cSize);

	float halfOX = oSize.x / 2.0f;
	float halfOY = oSize.y / 2.0f;
	float halfCX = cRect.size.x / 2.0f;
	float halfCY = cRect.size.y / 2.0f;

	EXPECT_EQ(leftTop.objectInContainerOffsetf(oSize, cRect), cPos + Vector2f(0, 0));
	EXPECT_EQ(centerTop.objectInContainerOffsetf(oSize, cRect), cPos + Vector2f(halfCX - halfOX, 0));
	EXPECT_EQ(rightTop.objectInContainerOffsetf(oSize, cRect), cPos + Vector2f(cSize.x - oSize.x, 0));
	EXPECT_EQ(undefinedTop.objectInContainerOffsetf(oSize, cRect), cPos + Vector2f(0, 0));

	EXPECT_EQ(leftCenter.objectInContainerOffsetf(oSize, cRect), cPos + Vector2f(0, halfCY - halfOY));
	EXPECT_EQ(centerCenter.objectInContainerOffsetf(oSize, cRect), cPos + Vector2f(halfCX - halfOX, halfCY - halfOY));
	EXPECT_EQ(rightCenter.objectInContainerOffsetf(oSize, cRect), cPos + Vector2f(cSize.x - oSize.x, halfCY - halfOY));
	EXPECT_EQ(undefinedCenter.objectInContainerOffsetf(oSize, cRect), cPos + Vector2f(0, halfCY - halfOY));

	EXPECT_EQ(leftBottom.objectInContainerOffsetf(oSize, cRect), cPos + Vector2f(0, cSize.y - oSize.y));
	EXPECT_EQ(centerBottom.objectInContainerOffsetf(oSize, cRect), cPos + Vector2f(halfCX - halfOX, cSize.y - oSize.y));
	EXPECT_EQ(rightBottom.objectInContainerOffsetf(oSize, cRect), cPos + Vector2f(cSize.x - oSize.x, cSize.y - oSize.y));
	EXPECT_EQ(undefinedBottom.objectInContainerOffsetf(oSize, cRect), cPos + Vector2f(0, cSize.y - oSize.y));

	EXPECT_EQ(leftUndefined.objectInContainerOffsetf(oSize, cRect), cPos + Vector2f(0, 0));
	EXPECT_EQ(centerUndefined.objectInContainerOffsetf(oSize, cRect), cPos + Vector2f(halfCX - halfOX, 0));
	EXPECT_EQ(rightUndefined.objectInContainerOffsetf(oSize, cRect), cPos + Vector2f(cSize.x - oSize.x, 0));
	EXPECT_EQ(undefinedUndefined.objectInContainerOffsetf(oSize, cRect), cPos + Vector2f(0, 0));
}


/**
 * Test objectInContaineri(const Vector2i&, const Recti&) const
 */
TEST(MathAlignTest, objectInContaineri)
{
	Align leftTop(HAlign::Left, VAlign::Top);
	Align centerTop(HAlign::Center, VAlign::Top);
	Align rightTop(HAlign::Right, VAlign::Top);
	Align undefinedTop(HAlign::Undefined, VAlign::Top);

	Align leftCenter(HAlign::Left, VAlign::Center);
	Align centerCenter(HAlign::Center, VAlign::Center);
	Align rightCenter(HAlign::Right, VAlign::Center);
	Align undefinedCenter(HAlign::Undefined, VAlign::Center);

	Align leftBottom(HAlign::Left, VAlign::Bottom);
	Align centerBottom(HAlign::Center, VAlign::Bottom);
	Align rightBottom(HAlign::Right, VAlign::Bottom);
	Align undefinedBottom(HAlign::Undefined, VAlign::Bottom);

	Align leftUndefined(HAlign::Left, VAlign::Undefined);
	Align centerUndefined(HAlign::Center, VAlign::Undefined);
	Align rightUndefined(HAlign::Right, VAlign::Undefined);
	Align undefinedUndefined(HAlign::Undefined, VAlign::Undefined);

	Vector2i oSize(20, 10);
	Vector2i cPos(500, 600);
	Vector2i cSize(250, 500);
	Recti cRect(cPos, cSize);

	int halfOX = oSize.x / 2;
	int halfOY = oSize.y / 2;
	int halfCX = cRect.size.x / 2;
	int halfCY = cRect.size.y / 2;

	EXPECT_EQ(leftTop.objectInContaineri(oSize, cRect), Recti(cPos + Vector2i(0, 0), oSize));
	EXPECT_EQ(centerTop.objectInContaineri(oSize, cRect), Recti(cPos + Vector2i(halfCX - halfOX, 0), oSize));
	EXPECT_EQ(rightTop.objectInContaineri(oSize, cRect), Recti(cPos + Vector2i(cSize.x - oSize.x, 0), oSize));
	EXPECT_EQ(undefinedTop.objectInContaineri(oSize, cRect), Recti(cPos + Vector2i(0, 0), oSize));

	EXPECT_EQ(leftCenter.objectInContaineri(oSize, cRect), Recti(cPos + Vector2i(0, halfCY - halfOY), oSize));
	EXPECT_EQ(centerCenter.objectInContaineri(oSize, cRect), Recti(cPos + Vector2i(halfCX - halfOX, halfCY - halfOY), oSize));
	EXPECT_EQ(rightCenter.objectInContaineri(oSize, cRect), Recti(cPos + Vector2i(cSize.x - oSize.x, halfCY - halfOY), oSize));
	EXPECT_EQ(undefinedCenter.objectInContaineri(oSize, cRect), Recti(cPos + Vector2i(0, halfCY - halfOY), oSize));

	EXPECT_EQ(leftBottom.objectInContaineri(oSize, cRect), Recti(cPos + Vector2i(0, cSize.y - oSize.y), oSize));
	EXPECT_EQ(centerBottom.objectInContaineri(oSize, cRect), Recti(cPos + Vector2i(halfCX - halfOX, cSize.y - oSize.y), oSize));
	EXPECT_EQ(rightBottom.objectInContaineri(oSize, cRect), Recti(cPos + Vector2i(cSize.x - oSize.x, cSize.y - oSize.y), oSize));
	EXPECT_EQ(undefinedBottom.objectInContaineri(oSize, cRect), Recti(cPos + Vector2i(0, cSize.y - oSize.y), oSize));

	EXPECT_EQ(leftUndefined.objectInContaineri(oSize, cRect), Recti(cPos + Vector2i(0, 0), oSize));
	EXPECT_EQ(centerUndefined.objectInContaineri(oSize, cRect), Recti(cPos + Vector2i(halfCX - halfOX, 0), oSize));
	EXPECT_EQ(rightUndefined.objectInContaineri(oSize, cRect), Recti(cPos + Vector2i(cSize.x - oSize.x, 0), oSize));
	EXPECT_EQ(undefinedUndefined.objectInContaineri(oSize, cRect), Recti(cPos + Vector2i(0, 0), oSize));
}


/**
 * Test objectInContainerf(const Vector2f&, const Rectf&) const
 */
TEST(MathAlignTest, objectInContainerf)
{
	Align leftTop(HAlign::Left, VAlign::Top);
	Align centerTop(HAlign::Center, VAlign::Top);
	Align rightTop(HAlign::Right, VAlign::Top);
	Align undefinedTop(HAlign::Undefined, VAlign::Top);

	Align leftCenter(HAlign::Left, VAlign::Center);
	Align centerCenter(HAlign::Center, VAlign::Center);
	Align rightCenter(HAlign::Right, VAlign::Center);
	Align undefinedCenter(HAlign::Undefined, VAlign::Center);

	Align leftBottom(HAlign::Left, VAlign::Bottom);
	Align centerBottom(HAlign::Center, VAlign::Bottom);
	Align rightBottom(HAlign::Right, VAlign::Bottom);
	Align undefinedBottom(HAlign::Undefined, VAlign::Bottom);

	Align leftUndefined(HAlign::Left, VAlign::Undefined);
	Align centerUndefined(HAlign::Center, VAlign::Undefined);
	Align rightUndefined(HAlign::Right, VAlign::Undefined);
	Align undefinedUndefined(HAlign::Undefined, VAlign::Undefined);

	Vector2f oSize(0.05f, 0.075f);
	Vector2f cPos(0.2f, 0.6f);
	Vector2f cSize(0.25f, 0.15f);
	Rectf cRect(cPos, cSize);

	float halfOX = oSize.x / 2.0f;
	float halfOY = oSize.y / 2.0f;
	float halfCX = cRect.size.x / 2.0f;
	float halfCY = cRect.size.y / 2.0f;

	EXPECT_EQ(leftTop.objectInContainerf(oSize, cRect), Rectf(cPos + Vector2f(0, 0), oSize));
	EXPECT_EQ(centerTop.objectInContainerf(oSize, cRect), Rectf(cPos + Vector2f(halfCX - halfOX, 0), oSize));
	EXPECT_EQ(rightTop.objectInContainerf(oSize, cRect), Rectf(cPos + Vector2f(cSize.x - oSize.x, 0), oSize));
	EXPECT_EQ(undefinedTop.objectInContainerf(oSize, cRect), Rectf(cPos + Vector2f(0, 0), oSize));

	EXPECT_EQ(leftCenter.objectInContainerf(oSize, cRect), Rectf(cPos + Vector2f(0, halfCY - halfOY), oSize));
	EXPECT_EQ(centerCenter.objectInContainerf(oSize, cRect), Rectf(cPos + Vector2f(halfCX - halfOX, halfCY - halfOY), oSize));
	EXPECT_EQ(rightCenter.objectInContainerf(oSize, cRect), Rectf(cPos + Vector2f(cSize.x - oSize.x, halfCY - halfOY), oSize));
	EXPECT_EQ(undefinedCenter.objectInContainerf(oSize, cRect), Rectf(cPos + Vector2f(0, halfCY - halfOY), oSize));

	EXPECT_EQ(leftBottom.objectInContainerf(oSize, cRect), Rectf(cPos + Vector2f(0, cSize.y - oSize.y), oSize));
	EXPECT_EQ(centerBottom.objectInContainerf(oSize, cRect), Rectf(cPos + Vector2f(halfCX - halfOX, cSize.y - oSize.y), oSize));
	EXPECT_EQ(rightBottom.objectInContainerf(oSize, cRect), Rectf(cPos + Vector2f(cSize.x - oSize.x, cSize.y - oSize.y), oSize));
	EXPECT_EQ(undefinedBottom.objectInContainerf(oSize, cRect), Rectf(cPos + Vector2f(0, cSize.y - oSize.y), oSize));

	EXPECT_EQ(leftUndefined.objectInContainerf(oSize, cRect), Rectf(cPos + Vector2f(0, 0), oSize));
	EXPECT_EQ(centerUndefined.objectInContainerf(oSize, cRect), Rectf(cPos + Vector2f(halfCX - halfOX, 0), oSize));
	EXPECT_EQ(rightUndefined.objectInContainerf(oSize, cRect), Rectf(cPos + Vector2f(cSize.x - oSize.x, 0), oSize));
	EXPECT_EQ(undefinedUndefined.objectInContainerf(oSize, cRect), Rectf(cPos + Vector2f(0, 0), oSize));
}


/**
 * Test toString() const
 */
TEST(MathAlignTest, toString)
{
	Align leftTop(HAlign::Left, VAlign::Top);
	Align centerTop(HAlign::Center, VAlign::Top);
	Align rightTop(HAlign::Right, VAlign::Top);
	Align undefinedTop(HAlign::Undefined, VAlign::Top);

	Align leftCenter(HAlign::Left, VAlign::Center);
	Align centerCenter(HAlign::Center, VAlign::Center);
	Align rightCenter(HAlign::Right, VAlign::Center);
	Align undefinedCenter(HAlign::Undefined, VAlign::Center);

	Align leftBottom(HAlign::Left, VAlign::Bottom);
	Align centerBottom(HAlign::Center, VAlign::Bottom);
	Align rightBottom(HAlign::Right, VAlign::Bottom);
	Align undefinedBottom(HAlign::Undefined, VAlign::Bottom);

	Align leftUndefined(HAlign::Left, VAlign::Undefined);
	Align centerUndefined(HAlign::Center, VAlign::Undefined);
	Align rightUndefined(HAlign::Right, VAlign::Undefined);
	Align undefinedUndefined(HAlign::Undefined, VAlign::Undefined);

	EXPECT_EQ(leftTop.toString(), "Left | Top");
	EXPECT_EQ(centerTop.toString(), "Center | Top");
	EXPECT_EQ(rightTop.toString(), "Right | Top");
	EXPECT_EQ(undefinedTop.toString(), "Undefined | Top");

	EXPECT_EQ(leftCenter.toString(), "Left | Center");
	EXPECT_EQ(centerCenter.toString(), "Center | Center");
	EXPECT_EQ(rightCenter.toString(), "Right | Center");
	EXPECT_EQ(undefinedCenter.toString(), "Undefined | Center");

	EXPECT_EQ(leftBottom.toString(), "Left | Bottom");
	EXPECT_EQ(centerBottom.toString(), "Center | Bottom");
	EXPECT_EQ(rightBottom.toString(), "Right | Bottom");
	EXPECT_EQ(undefinedBottom.toString(), "Undefined | Bottom");

	EXPECT_EQ(leftUndefined.toString(), "Left | Undefined");
	EXPECT_EQ(centerUndefined.toString(), "Center | Undefined");
	EXPECT_EQ(rightUndefined.toString(), "Right | Undefined");
	EXPECT_EQ(undefinedUndefined.toString(), "Undefined | Undefined");
}


/**
 * Test toStringDebug() const
 */
TEST(MathAlignTest, toStringDebug)
{
	Align leftTop(HAlign::Left, VAlign::Top);
	Align centerTop(HAlign::Center, VAlign::Top);
	Align rightTop(HAlign::Right, VAlign::Top);
	Align undefinedTop(HAlign::Undefined, VAlign::Top);

	Align leftCenter(HAlign::Left, VAlign::Center);
	Align centerCenter(HAlign::Center, VAlign::Center);
	Align rightCenter(HAlign::Right, VAlign::Center);
	Align undefinedCenter(HAlign::Undefined, VAlign::Center);

	Align leftBottom(HAlign::Left, VAlign::Bottom);
	Align centerBottom(HAlign::Center, VAlign::Bottom);
	Align rightBottom(HAlign::Right, VAlign::Bottom);
	Align undefinedBottom(HAlign::Undefined, VAlign::Bottom);

	Align leftUndefined(HAlign::Left, VAlign::Undefined);
	Align centerUndefined(HAlign::Center, VAlign::Undefined);
	Align rightUndefined(HAlign::Right, VAlign::Undefined);
	Align undefinedUndefined(HAlign::Undefined, VAlign::Undefined);

	EXPECT_EQ(leftTop.toStringDebug(), "Align(Left | Top)");
	EXPECT_EQ(centerTop.toStringDebug(), "Align(Center | Top)");
	EXPECT_EQ(rightTop.toStringDebug(), "Align(Right | Top)");
	EXPECT_EQ(undefinedTop.toStringDebug(), "Align(Undefined | Top)");

	EXPECT_EQ(leftCenter.toStringDebug(), "Align(Left | Center)");
	EXPECT_EQ(centerCenter.toStringDebug(), "Align(Center | Center)");
	EXPECT_EQ(rightCenter.toStringDebug(), "Align(Right | Center)");
	EXPECT_EQ(undefinedCenter.toStringDebug(), "Align(Undefined | Center)");

	EXPECT_EQ(leftBottom.toStringDebug(), "Align(Left | Bottom)");
	EXPECT_EQ(centerBottom.toStringDebug(), "Align(Center | Bottom)");
	EXPECT_EQ(rightBottom.toStringDebug(), "Align(Right | Bottom)");
	EXPECT_EQ(undefinedBottom.toStringDebug(), "Align(Undefined | Bottom)");

	EXPECT_EQ(leftUndefined.toStringDebug(), "Align(Left | Undefined)");
	EXPECT_EQ(centerUndefined.toStringDebug(), "Align(Center | Undefined)");
	EXPECT_EQ(rightUndefined.toStringDebug(), "Align(Right | Undefined)");
	EXPECT_EQ(undefinedUndefined.toStringDebug(), "Align(Undefined | Undefined)");
}


} // End namespace nameless namespace

