
/*
#include <thread>

class VersoLoggingTest
{
public:
	VersoLoggingTest()
	{
		VERSO_LOG_INFO("kabbala", "Hello from class constructor!");
	}


	virtual ~VersoLoggingTest()
	{
		VERSO_LOG_INFO("kabbala", "Hello from class destructor!");
	}


	UString getFoo() const
	{
		VERSO_LOG_WARN("kabbala", "Hello from member function!");
		return "foo";
	}

	static int getNumber()
	{
		VERSO_LOG_TRACE("kabbala", "Hello from static class function!");
		return 123;
	}
};


namespace VersoLoggingNamespace {
	class VersoLoggingTest2
	{
	public:
		VersoLoggingTest2()
		{
			VERSO_LOG_INFO("kabbala", "Hello from class constructor!");
		}

		virtual ~VersoLoggingTest2()
		{
			VERSO_LOG_INFO("kabbala", "Hello from class destructor!");
		}

		UString getFoo() const
		{
			VERSO_LOG_WARN("kabbala", "Hello from member function!");
			return "foo";
		}

		static int getNumber()
		{
			VERSO_LOG_TRACE("kabbala", "Hello from static class function!");
			return 123;
		}
	};
};

int test()
{
		VERSO_LOG_INFO("verso-base/test", "Verso-Base TestBench");

	Logger::singleton().useFile("myLog.txt");

	VERSO_LOG_WARN("verso-base/test", "My msg jaada jaada");
	VERSO_LOG_DEBUG("verso-base/test", "My msg jaada jaada");
	VERSO_LOG_INFO("verso-base/test", "My msg jaada jaada");
	VERSO_LOG_FLUSH();
	VERSO_LOG_TRACE("verso-base/test", "My msg jaada jaada456");
	Logger::singleton().useCout();
	VERSO_LOG_ERR("verso-base/test", "My msg jaada jaada123");

	VersoLoggingTest tester;
	tester.getFoo();
	VersoLoggingTest::getNumber();

	VersoLoggingNamespace::VersoLoggingTest2 tester2;
	tester2.getFoo();
	VersoLoggingNamespace::VersoLoggingTest2::getNumber();

	for (size_t j=0; j<10; ++j) {
		int rnd = random()%100;
		std::this_thread::sleep_for(std::chrono::milliseconds(rnd));
		VERSO_LOG_INFO("verso-base/test", "Sleep for "<<rnd<<" seconds");
		VERSO_LOG_INFO_VARIABLE("verso-base/test", "ms", rnd);
	}


	std::this_thread::sleep_for(std::chrono::milliseconds(200));
	VERSO_LOG_INFO("verso-base/test", "test sleep 200 ms");
}

*/
