# Verso-Base

Base library for other Verso libraries with useful classes related to strings, math, file I/O,
error handling and images. Can be used for console applications.

`**4.11.2016 03:16** Tested debug & release builds in Win32, Linux64 and OS X.`

